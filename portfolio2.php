<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Gp Bootstrap Template</title>

    <!-- Bootstrap -->
    <link href="gp/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="gp/css/font-awesome.min.css">
	<link href="gp/css/animate.min.css" rel="stylesheet">
    <link href="gp/css/prettyPhoto.css" rel="stylesheet">      
	<link href="gp/css/main.css" rel="stylesheet">
	 <link href="gp/css/responsive.css" rel="stylesheet">
	 <!--[if lt IE 9]>
    <script src="gp/js/html5shiv.js"></script>
    <script src="gp/js/respond.min.js"></script>
    <![endif]-->       
    
  </head>
  <body class="homepage">   
<?php include "header.php"; ?>
		
	<section id="portfolio">
        <div class="container">
            <div class="center">
               <h2>Portfolio</h2>
               <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>
            </div>
        

            <ul class="portfolio-filter text-center">
                <li><a class="btn btn-default active" href="gp/#" data-filter="*">All Works</a></li>
                <li><a class="btn btn-default" href="gp/#" data-filter=".bootstrap">Creative</a></li>
                <li><a class="btn btn-default" href="gp/#" data-filter=".html">Photography</a></li>
                <li><a class="btn btn-default" href="gp/#" data-filter=".wordpress">Web Development</a></li>
            </ul><!--/#portfolio-filter-->

            <div class="row">
                <div class="portfolio-items">
                    <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="gp/images/portfolio/recent/item1.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="gp/#">Business theme</a></h3>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>
                                    <a class="preview" href="gp/images/portfolio/full/item1.png" rel="prettyPhoto"><i class="fa fa-eye"></i> Ampliar</a>
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item joomla bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="gp/images/portfolio/recent/item2.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="gp/#">Business theme</a></h3>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>
                                    <a class="preview" href="gp/images/portfolio/full/item2.png" rel="prettyPhoto"><i class="fa fa-eye"></i> Ampliar</a>
                                </div> 
                            </div>
                        </div>          
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item bootstrap wordpress col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="gp/images/portfolio/recent/item3.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="gp/#">Business theme</a></h3>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>
                                    <a class="preview" href="gp/images/portfolio/full/item3.png" rel="prettyPhoto"><i class="fa fa-eye"></i> Ampliar</a>
                                </div> 
                            </div>
                        </div>        
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item joomla wordpress apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="gp/images/portfolio/recent/item4.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="gp/#">Business theme</a></h3>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>
                                    <a class="preview" href="gp/images/portfolio/full/item4.png" rel="prettyPhoto"><i class="fa fa-eye"></i> Ampliar</a>
                                </div> 
                            </div>
                        </div>           
                    </div><!--/.portfolio-item-->
          
                    <div class="portfolio-item joomla html bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="gp/images/portfolio/recent/item5.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="gp/#">Business theme</a></h3>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>
                                    <a class="preview" href="gp/images/portfolio/full/item5.png" rel="prettyPhoto"><i class="fa fa-eye"></i> Ampliar</a>
                                </div> 
                            </div>
                        </div>      
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item wordpress html apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="gp/images/portfolio/recent/item6.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="gp/#">Business theme</a></h3>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>
                                    <a class="preview" href="gp/images/portfolio/full/item6.png" rel="prettyPhoto"><i class="fa fa-eye"></i> Ampliar</a>
                                </div> 
                            </div>
                        </div>         
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item wordpress html col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="gp/images/portfolio/recent/item7.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="gp/#">Business theme</a></h3>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>
                                    <a class="preview" href="gp/images/portfolio/full/item7.png" rel="prettyPhoto"><i class="fa fa-eye"></i> Ampliar</a>
                                </div> 
                            </div>
                        </div>          
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="gp/images/portfolio/recent/item8.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="gp/#">Business theme</a></h3>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>
                                    <a class="preview" href="gp/images/portfolio/full/item8.png" rel="prettyPhoto"><i class="fa fa-eye"></i> Ampliar</a>
                                </div> 
                            </div>
                        </div>          
                    </div><!--/.portfolio-item-->
                </div>
            </div>
        </div>
    </section><!--/#portfolio-item-->
	
	<section id="bottom">
         <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="row">
               <div class="col-md-3 col-sm-6">
                  <div class="widget">
                     <h3>tuabogado.ec</h3>
                     <ul>
                        <li><a href="quienessomos.php">¿Quiénes somos?</a></li>
                        <li><a href="terminos.php">Términos y Condiciones</a></li>
                        <li><a href="politicasprivacidad.php">Política de Privacidad</a></li>
                     </ul>
                  </div>
               </div>
               <!--/.col-md-3-->
               <div class="col-md-3 col-sm-6">
                  <div class="widget">
                     <h3>Abogados</h3>
                     <ul>
                        <li><a href="login.php">Acceso Abogados</a></li>
                        <li><a href="register.php">Regístrate gratis</a></li>
                        <li><a href="planes.php">Planes y tarifas</a></li>
                     </ul>
                  </div>
               </div>
               <!--/.col-md-3-->
               <div class="col-md-3 col-sm-6">
                  <div class="widget">
                     <h3>Visitas</h3>
<?php include "visitas.php"; ?>
                     <h3><span class="label label-default"> <?php echo $visitas; ?></span> </h3>
                  </div>
               </div>
               <!--/.col-md-3-->
               <div class="col-md-3 col-sm-6">
                  <div class="widget">
                     <h3>Contacto</h3>
                     <ul>
                        <li><a href="contactanos.php">Contáctanos</a></li>
                        <li><a href="preguntasfrecuentes.php">Preguntas Frecuentes</a></li>
                     </ul>
                  </div>
               </div>
               <!--/.col-md-3-->
            </div>
         </div>

    </section><!--/#bottom-->
	
	<footer id="footer" class="midnight-blue">
         <div class="container">
            <div class="row">
               <div class="col-sm-6">
                  &copy; 2018 <a target="_blank" href="http://www.tuabogado.ec/" title="">tuabogado.ec</a>. All Rights Reserved.
               </div>
               <!--
                  All links in the footer should remain intact.
                  Licenseing information is available at: http://bootstraptaste.com/license/
                  You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Gp
                  -->
               <div class="col-sm-6">
                  <ul class="pull-right">
                     <li><a href="index.php">Inicio</a></li>
                     <li><a href="anunciate.php">Regístrate</a></li>
                     <li><a href="blog.php">Blog</a></li>
                     <li><a href="contactanos.php">Contáctenos</a></li>
                  </ul>
               </div>
            </div>
         </div>

    </footer><!--/#footer-->
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="gp/js/jquery.js"></script>
    <script src="gp/js/bootstrap.min.js"></script>
    <script src="gp/js/jquery.prettyPhoto.js"></script>
    <script src="gp/js/jquery.isotope.min.js"></script>   
    <script src="gp/js/wow.min.js"></script>
	<script src="gp/js/main.js"></script>
  </body>
</html>

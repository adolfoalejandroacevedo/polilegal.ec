<?php session_start(); ?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>
         Tu Abogado:
         Anuncios :: Directorio de Abogados del Ecuador
      </title>
      <link href="/favicon.png" type="image/x-icon" rel="icon"/>
      <link href="/favicon.png" type="image/x-icon" rel="shortcut icon"/>
      <link href="/css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="/css/font-awesome.min.css">
      <link rel="stylesheet" href="/font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <link href="/css/animate.min.css" rel="stylesheet">
      <link href="/css/prettyPhoto.css" rel="stylesheet">
      <link href="/css/main.css" rel="stylesheet">
      <link href="/css/responsive.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="/bower_components/datatables.net-dt/css/jquery.dataTables.css">
      <link rel="stylesheet" type="text/css" href="/css/bootstrap-markdown.min.css">
      <style>
         .pricingTable{
         text-align: center;
         transition: all 0.5s ease 0s;
         }
         .pricingTable:hover{
         box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
         }
         .pricingTable .pricingTable-header{
         color: #feffff;
         }
         .pricingTable .heading{
         display: block;
         padding-top: 25px;
         }
         .pricingTable .heading > h3{
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .textotitle{
         color: #feffff;
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .pricingTable .subtitle{
         display: block;
         font-size: 13px;
         margin-top: 5px;
         text-transform: capitalize;
         }
         .pricingTable .price-value{
         display: block;
         font-size: 60px;
         font-weight: 700;
         padding-bottom: 25px;
         }
         .pricingTable .price-value span{
         display: block;
         font-size: 14px;
         line-height: 20px;
         text-transform: uppercase;
         }
         .pricingTable .pricingContent{
         /*text-transform: capitalize;*/
         background: #fbfbfb;
         color: #fefeff;
         }
         .pricingTable .pricingContent ul{
         list-style: none;
         padding: 15px 14px 10px;
         margin: 0;
         text-align: left;
         }
         .pricingTable .pricingContent ul li{
         font-size: 14px;
         padding: 12px 0;
         border-bottom: 1px dashed #e1e1e1;
         color: #9da1ad;
         }
         .pricingTable .pricingContent ul li i{
         font-size: 14px;
         float: right;
         }
         .pricingTable .pricingTable-sign-up{
         padding: 20px 0;
         background: #fbfbfb;
         color: #fff;
         text-transform: capitalize;
         }
         .pricingTable .btn-block{
         width: 60%;
         margin: 0 auto;
         font-size: 17px;
         color: #fff;
         text-transform: capitalize;
         border: none;
         border-radius: 5px;
         padding: 10px;
         transition: all 0.5s ease 0s;
         }
         .pricingTable .btn-block:before{
         content: "\f007";
         font-family: 'FontAwesome';
         margin-right: 10px;
         }
         .pricingTable.blue .pricingTable-header,
         .pricingTable.blue .btn-block{
         background: #fce400;
         }
         .pricingTable.pink .pricingTable-header,
         .pricingTable.pink .btn-block{
         background: #c9b600;
         }
         .pricingTable.orange .pricingTable-header,
         .pricingTable.orange .btn-block{
         background: #e0d83a;
         }
         .pricingTable.green .pricingTable-header,
         .pricingTable.green .btn-block{
         background: #e0cd00;
         }
         .pricingTable.blue .btn-block:hover,
         .pricingTable.pink .btn-block:hover,
         .pricingTable.orange .btn-block:hover,
         .pricingTable.green .btn-block:hover{
         background: #e6e6e6;
         color: #939393;
         }
         @media screen and (max-width: 990px){
         .pricingTable{ margin-bottom: 20px; }
         }
      </style>
   </head>
   <body class="homepage">
<?php include "header.php"; ?>
      <!--/header-->
      <section id="blog" class="container">
         <ol class="breadcrumb">
            <li><a href="/">Inicio</a></li>
            <li><a href="/users/users/profile">Mi perfil</a></li>
            <li class="active">Mis anuncios</li>
         </ol>
         <div class="container">
            <div class="center">
               <h2> Mis anuncios </h2>
            </div>
            <div class="team">
               <div class="row clearfix">
                  <div class="col-md-3 col-sm-6">
                     <div class="single-profile-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="media">
                           <div style="height: 260px;">
                              <img src="/files/Anuncios/foto/Selección_513.png" class="thumbnail" width= "100%">
                           </div>
                           <div class="media-body">
                              <div class="center">
                                 <div style="min-height: 50px;">
                                    <h4><a href="/anuncios/view/primer-articulo">Primer articulo</a></h4>
                                 </div>
                                 <h6><a href="/areas/view/1">Penal</a> </h6>
                                 <h6>Ciudad: <a href="/ciudades/view/10">Latacunga</a></h6>
                                 <ul class="tag clearfix">
                                    <li class="btn"><a href="/tiposervicios/view/1">Abogado</a></li>
                                 </ul>
                                 <ul class="social_icons">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="+591234455"><i class="fa fa-phone"></i></a></li>
                                 </ul>
                              </div>
                           </div>
                           <p style="font-size: smaller;">
                           <p>Este es un articulo de prueba</p>
                           ...</p>
                           <div style="text-align: center;">
                              <a href="/anuncios/view/primer-articulo" role="button" class="btn btn-success ">
                              VER
                              </a>
                           </div>
                        </div>
                        <!--/.media -->
                     </div>
                  </div>
                  <!--/.col-lg-4 -->
                  <div class="col-md-3 col-sm-6">
                     <div class="single-profile-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="media">
                           <div style="height: 260px;">
                              <img src="/files/Anuncios/foto/Selección_506.png" class="thumbnail" width= "100%">
                           </div>
                           <div class="media-body">
                              <div class="center">
                                 <div style="min-height: 50px;">
                                    <h4><a href="/anuncios/view/segundo-anuncio">Segundo anuncio</a></h4>
                                 </div>
                                 <h6><a href="/areas/view/2">Civil</a> </h6>
                                 <h6>Ciudad: <a href="/ciudades/view/3">Babahoyo</a></h6>
                                 <ul class="tag clearfix">
                                    <li class="btn"><a href="/tiposervicios/view/1">Abogado</a></li>
                                 </ul>
                                 <ul class="social_icons">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="+591234455"><i class="fa fa-phone"></i></a></li>
                                 </ul>
                              </div>
                           </div>
                           <p style="font-size: smaller;">
                           <p>Este es mi sugundo articulo</p>
                           ...</p>
                           <div style="text-align: center;">
                              <a href="/anuncios/view/segundo-anuncio" role="button" class="btn btn-success ">
                              VER
                              </a>
                           </div>
                        </div>
                        <!--/.media -->
                     </div>
                  </div>
                  <!--/.col-lg-4 -->
                  <div class="col-md-3 col-sm-6">
                     <div class="single-profile-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="media">
                           <div style="height: 260px;">
                              <img src="/files/Anuncios/foto/Selección_499.png" class="thumbnail" width= "100%">
                           </div>
                           <div class="media-body">
                              <div class="center">
                                 <div style="min-height: 50px;">
                                    <h4><a href="/anuncios/view/anuncio-3">Anuncio 3</a></h4>
                                 </div>
                                 <h6><a href="/areas/view/2">Civil</a> </h6>
                                 <h6>Ciudad: <a href="/ciudades/view/9">Ibarra</a></h6>
                                 <ul class="tag clearfix">
                                    <li class="btn"><a href="/tiposervicios/view/2">Escritorio Jurídico</a></li>
                                 </ul>
                                 <ul class="social_icons">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="+591234455"><i class="fa fa-phone"></i></a></li>
                                 </ul>
                              </div>
                           </div>
                           <p style="font-size: smaller;">
                           <p>Este es un tercer anuncio</p>
                           ...</p>
                           <div style="text-align: center;">
                              <a href="/anuncios/view/anuncio-3" role="button" class="btn btn-success ">
                              VER
                              </a>
                           </div>
                        </div>
                        <!--/.media -->
                     </div>
                  </div>
                  <!--/.col-lg-4 -->
                  <div class="col-md-3 col-sm-6">
                     <div class="single-profile-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="media">
                           <div style="height: 260px;">
                              <img src="/files/Anuncios/foto/Selección_472.png" class="thumbnail" width= "100%">
                           </div>
                           <div class="media-body">
                              <div class="center">
                                 <div style="min-height: 50px;">
                                    <h4><a href="/anuncios/view/el-cuarto-anuncio">El cuarto anuncio</a></h4>
                                 </div>
                                 <h6><a href="/areas/view/3">Laboral</a> </h6>
                                 <h6>Ciudad: <a href="/ciudades/view/4">Cuenca</a></h6>
                                 <ul class="tag clearfix">
                                    <li class="btn"><a href="/tiposervicios/view/2">Escritorio Jurídico</a></li>
                                 </ul>
                                 <ul class="social_icons">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="+591234455"><i class="fa fa-phone"></i></a></li>
                                 </ul>
                              </div>
                           </div>
                           <p style="font-size: smaller;">
                           <p>Este es el cuarto anuncio</p>
                           ...</p>
                           <div style="text-align: center;">
                              <a href="/anuncios/view/el-cuarto-anuncio" role="button" class="btn btn-success ">
                              VER
                              </a>
                           </div>
                        </div>
                        <!--/.media -->
                     </div>
                  </div>
                  <!--/.col-lg-4 -->
               </div>
            </div>
            <div class="paginator">
               <ul class="pagination">
                  <li class="disabled"><a>&lt; anterior</a></li>
                  <li class="disabled"><a>siguiente &gt;</a></li>
               </ul>
               <p>Página 1 de 1, mostrando 4 registros de 4 en total</p>
            </div>
         </div>
      </section>
      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="/bower_components/jquery/dist/jquery.min.js"></script>
      <script src="/js/bootstrap.min.js"></script>
      <script src="/js/markdown.js"></script>
      <script src="/js/to-markdown.js"></script>
      <script src="/js/bootstrap-markdown.js"></script>
      <script src="/js/bootstrap-markdown.es.js"></script>
      <script type="text/javascript" src="/js/jquery.simpleslider.js"></script>
      <script type="text/javascript" charset="utf8" src="/bower_components/datatables.net/js/jquery.dataTables.min.js"> </script>
      <script src="/js/jquery.prettyPhoto.js"></script>
      <script src="/js/jquery.isotope.min.js"></script>
      <script src="/js/wow.min.js"></script>
      <script src="/js/main.js"></script>
   </body>
</html>

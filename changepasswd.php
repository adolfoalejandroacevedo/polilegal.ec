<?php

$config = require 'config.php';

//Obtencion de las variables POST
$hash = $_POST["hash"];
$passwd1 = $_POST["passwd1"];
$passwd2 = $_POST["passwd2"];

//Validmos que los passwords no sean diferentes
if($passwd1 != $passwd2){
	echo "Passwords Diferentes";
	exit;
}

//Validamos que el password cumpla con los requisitos
$error_clave = "";
if (!validar_clave($passwd1, $error_clave)) {
	echo "Error en Password";
	exit;
}

//Cambiamos el password
$conn=mysqli_connect($config['database']['server'],$config['database']['username'],$config['database']['password'],$config['database']['db']);
$sql = "UPDATE users SET password=MD5('$passwd1') WHERE hash='$hash' AND active='1'";
if (mysqli_query($conn, $sql)) echo "Password Cambiado"; else error_log("Error: " . $sql . "..." . mysqli_error($conn));

//Funcion para requisitos del password
function validar_clave($clave,&$error_clave){
   if(strlen($clave) < 6){
      $error_clave = "La clave debe tener al menos 6 caracteres";
      return false;
   }
   if(strlen($clave) > 16){
      $error_clave = "La clave no puede tener más de 16 caracteres";
      return false;
   }
   if (!preg_match('`[a-z]`',$clave)){
      $error_clave = "La clave debe tener al menos una letra minúscula";
      return false;
   }
   if (!preg_match('`[A-Z]`',$clave)){
      $error_clave = "La clave debe tener al menos una letra mayúscula";
      return false;
   }
   if (!preg_match('`[0-9]`',$clave)){
      $error_clave = "La clave debe tener al menos un caracter numérico";
      return false;
   }
   $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/';
   if (!preg_match($pattern, $clave)) {
      $error_clave = "La clave debe tener al menos un caracter especial";
      return false;
   }
   $error_clave = "";
   return true;
}

?>

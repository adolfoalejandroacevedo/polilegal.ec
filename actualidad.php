<?php 
   session_start();
   session_unset();
	$current = "actualidad";
?>
<!DOCTYPE HTML>
<html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
  <meta http-equiv="content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
  <title>Polilegal | Actualidad</title>
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
  <link id="default-css" href="style.css" rel="stylesheet" type="text/css">
  <link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" /> 
<!--[if IE 7]>
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lt IE 9]>
<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--Fonts-->
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--jquery-->
<script src="js/modernizr-2.6.2.min.js"></script>
</head>
<body>
	<!--wrapper starts-->
  <div class="wrapper">
    <!--inner-wrapper starts-->
    <div class="inner-wrapper">
      <!--header starts-->
		<?php include 'header.php'; ?>
           <!--header ends-->
            <!--main starts-->
            <div id="main">

              <div class="breadcrumb-section">
                <div class="container">
                  <h1> Actualidad </h1>
                  <div class="breadcrumb">
                    <a href="index.php"> Inicio </a>
                    <a href="tabs-toggles.php"> Actualidad </a>
                  </div>
                </div>
              </div>
              
              <!--container starts-->
              <div class="container">	

                <!--primary starts-->
                <section id="primary" class="content-full-width">
                    <div class="dt-sc-toggle-frame">
                  <h5 class="dt-sc-toggle"><a href="#">ÚLTIMAS REFORMAS A LA LEY DE COMPAÑIAS.</a></h5>
                  <div class="dt-sc-toggle-content">
                    <div class="dt-sc-block" style="text-align: justify;">
		      Respecto a las últimas reformas a la Ley de Compañías de diciembre de 2019, a continuación se mencionan las novedades más importantes. <br><br>FUSIONES.- Cuando una compañía se fusione o absorba con otra u otras, la escritura ya no deberá contener el balance final de las compañías fusionadas o absorbidas, sino únicamente las modificaciones estatutarias resultantes del aumento de capital de la compañía absorbente y el número de acciones que hayan de ser entregadas a cada uno de los nuevos accionistas. <br><br>ESCISIONES MÚLTIPLES.- De ahora en adelante, se permitirán las escisiones múltiples.  La ley las define como aquellas en las que intervienen 2 o más sociedades que se separan de ciertas unidades de negocio creando una nueva compañía, a la que las sociedades que se escinden traspasarán activos, parte de su patrimonio y pasivos (de considerarlo necesario), para permitir a la nueva compañía cumplir su objeto social.  Los socios de la nueva compañía serán: las sociedades que se escinden; o, los socios o accionistas de cada sociedad que se escinde.  En ambos casos, a prorrata del aporte y de su participación resultante en la sociedad que se crea, salvo decisión en contrario por la Junta General. <br><br>OPERACIONES COMBINADAS.- La norma permite realizar operaciones combinadas entre sociedades que involucren transformación, fusión y escisión en un mismo acto, con el fin de crear, absorber o transformar múltiples sociedades. <br><br>FUSIONES TRANSFRONTERIZAS.- Se permite la fusión entre una o más sociedades extranjeras con una o más sociedades ecuatorianas para establecerse y operar a través de la compañía ecuatoriana.  Para ello, las compañías absorbidas deberán cancelarse en su país de origen.  Pueden participar en este tipo de fusiones las compañías anónimas, en comandita por acciones y responsabilidad limitada.  En estos procesos, las compañías deberán preparar un balance cortado al día anterior al de la fecha de la escritura pública de fusión.  Este balance no requiere ser insertado en la escritura púbica; la compañía únicamente deberá ponerlo a disposición de los socios, accionistas y terceros interesados en el domicilio social. <br><br>DISOLUCIÓN DISPUESTA POR LA SUPERCIAS.- Las compañías disueltas por cualquier causal no podrán realizar nuevas operaciones relativas al objeto social.  La sociedad en liquidación deberá dar cumplimiento con las declaraciones conforme lo establecen las normas tributarias.  En el caso de que una compañía percibiere utilidad de cualquier fuente, producto de las operaciones propias de su liquidación societaria, deberá declarar y pagar los impuestos que se generen de acuerdo a la normativa tributaria. <br><br>CONTRIBUCIONES EN CASOS DE DISOLUCIÓN.- La ley señala que no se generarán contribuciones societarias a la SUPERCIAS, a las compañías que se encuentren en disolución, a partir de la fecha de la resolución de disolución o liquidación.  En este caso, la contribución se calculará proporcionalmente hasta la fecha de la resolución de disolución o de liquidación, de conformidad a los activos reales reflejados en el balance general o estado de situación del ejercicio fiscal respectivo. En el caso de que la compañía disuelta se reactive, se generará una contribución societaria durante el tiempo que permaneció en estado de disolución y liquidación.  Las compañías que superen su situación de disolución estarán obligadas a pagar las contribuciones, intereses y multas que se adeudaren, lo cual se calculará antes de la emisión de la resolución de reactivación. <br><br>POLILEGAL S.A. <br> Enero 2020
                   </div>
                 </div>
               </div>
                    <div class="dt-sc-toggle-frame">
                  <h5 class="dt-sc-toggle"><a href="#">Reglamento para el Porte y Uso de Armas y Tecnologías No Letales; y Equipos de Protección para las Entidades Complementarias de Seguridad Ciudadana y Orden Público de los Gobiernos Autónomos Descentralizados Municipales y Metropolitanos.</a></h5>
                  <div class="dt-sc-toggle-content">
                    <div class="dt-sc-block" style="text-align: justify;">
		      El día de ayer se expidió en el Registro Oficial No. 64, el Reglamento para el Porte y Uso de Armas y Tecnologías No Letales; y Equipos de Protección para las Entidades Complementarias de Seguridad Ciudadana y Orden Público de los Gobiernos Autónomos Descentralizados Municipales y Metropolitanos.  <br><br>Este cuerpo legal tiene un ámbito de aplicación obligatoria a nivel nacional.  Clasifica a las armas no letales en: a) Armas no letales antipersonales; b) Armas no letales antimateriales; y c) Armas no letales para el control animal.   <br><br>Previa a su obtención, el Reglamento dispone que se requerirá de un informe técnico-operativo que justifique la necesidad de su adquisición en coordinación con la Policía Nacional, quien también se encargará de emitir un informe de factibilidad.  <br><br>También señala, que para el uso y manipulación adecuada de estas armas y tecnologías no letales y equipos de protección, las entidades que se rigen bajo este reglamento deberán recibir la capacitación adecuada y obligatoria en base a los principios de eficacia, eficiencia, diligencia, oportunidad, coordinación y complementariedad.  Estas capacitaciones serán certificadas previo el cumplimiento de requisitos médicos, psicológicos y laborales.  <br><br>¿Quiénes podrán hacer uso de estas herramientas? El Cuerpo de Control de Agentes Municipales o Metropolitanos y el Cuerpo de Agentes Civiles de Tránsito, excluyendo a los Cuerpos de Bomberos, quienes por su naturaleza de sus funciones no utilizan herramientas de este tipo.   <br><br>Entre las armas no letales que serán permitidas se encuentran: tolete, sirenas, linternas, balizas de color naranja, esparcidores de agente químico.  En cuanto a tecnologías no letales se encuentran: vehículo personal eléctrico /electrónico, aeronaves no tripuladas para reconocimiento visual, megáfonos, cámaras fotográficas y cámaras de video.  Se regulan equipos de protección como cascos, chalecos anticorte, protectores visuales, protectores auditivos, esposas, guantes, botas de punta de acero, equipos e insumos médicos, accesorios para rescates, herramientas para montañas, vallas, conos de señalización y señaléticas.  <br><br>El Ministerio, en este caso de Gobierno, a través de la Policía Nacional será el encargado de coordinar y liderar el plan de capacitaciones en un plazo de tres meses contados desde la vigencia del presente Reglamento.  <br><br>POLILEGAL S.A. <br> Octubre 2019
                   </div>
                 </div>
               </div>
               <div class="dt-sc-toggle-frame">
                  <h5 class="dt-sc-toggle"><a href="#">NOVEDADES DE LA LEY ORGÁNICA REFORMATORIA DEL CÓDIGO ORGÁNICO GENERAL DE PROCESOS.</a></h5>
                  <div class="dt-sc-toggle-content">
                    <div class="dt-sc-block" style="text-align: justify;">
		      Desde el 26 de junio de 2019, está vigente la Ley Orgánica Reformatoria del Código Orgánico General de Procesos. A continuación expondremos los temas de mayor relevancia:<br><br>IDENTIFICACIÓN DEL DOMICILIO: Cuando en un contrato existan cláusulas de domicilio, si éste es modificado, la parte está en la obligación de notificar a la otra parte.  En el caso de que no sea notificado, la competencia radicará en el juez del domicilio fijado originalmente en el contrato.  <br><br>DEMANDAS CONTRA EL ESTADO: En estos casos, la competencia se radicará en el domicilio del actor, pero la citación a la entidad se realizará en el lugar de su sede principal.<br><br>PROCURACIÓN JUDICIAL: Se aclara el tema respecto a la constitución de la procuración judicial para los abogados de las instituciones públicas que carecen de personería jurídica, que será por delegación del Procurador General; o con oficio, en caso de tener personería jurídica.  Adicionalmente, agrega la facultad de constituir una procuración mediante escrito reconocido conforme la ley, ante el juzgador. <br><br>CITACIÓN POR BOLETAS: Se agrega que la citación al demandado podrá realizarse por 3 boletas en su domicilio, residencia, lugar de trabajo o asiento principal de sus negocios a cualquier persona de la familia o dependiente. <br><br>FALTA DE COMPARECENCIA A LAS AUDIENCIAS: Se aclara que si la parte actora comparece pero sin su defensor, el juez suspenderá la audiencia y volverá a convocar, por una sola vez, y a petición de parte. <br><br>DILIGENCIAS PREPARATORIAS: Se podrá solicitar como diligencia preparatoria la recepción de declaraciones testimoniales, en especial, las urgentes de personas de avanzada edad o grave enfermedad que se tema su fallecimiento, o de quienes estén próximos a ausentarse del país. <br><br>APREMIOS EN CASOS DE ALIMENTOS: Se establecen los casos de prohibición de salida del país, apremio parcial y apremio total por incumplimiento de pago de dos o más pensiones alimenticias, sean o no sucesivas; además del uso eventual de dispositivos de vigilancia electrónica. <br><br>CALIFICACIÓN DE LA DEMANDA: Ahora, el juez otorgará un término de 5 días para aclarar o completar la demanda.  Además, el juez, al momento de calificar la demanda no podrá pronunciarse sobre el anuncio de los medios probatorios. <br><br>EXCEPCIONES EN LA CONTESTACIÓN A LA DEMANDA: Se aclara que las excepciones podrán reformarse hasta antes de que el juez dicte la providencia convocando a la audiencia preliminar o única.  En el caso de existir reforma de excepciones, se notificará a la parte actora, concediéndole un término de 10 días (5 en materia de niñez y adolescencia) para anunciar prueba nueva. <br><br>PRUEBA DOCUMENTAL DE GRAN VOLUMEN: En estos casos, la reforma mantiene los 15 días antes de la audiencia de juicio para que estas pruebas sean examinadas por la otra parte, y dispone un término de 10 días antes de la audiencia única en los demás procedimientos (5 días término en materia de niñez y adolescencia). <br><br>RETIRO DE LA DEMANDA: En los casos en los que la parte actora retire la demanda, se agrega que la demanda tiene que ser devuelta aún sin estar calificada.  Además, dispone que la misma demanda puede ser retirada hasta un máximo de dos ocasiones. <br><br>PROCEDENCIA DEL ABANDONO: Se reforma el término del abandono, de 180 días, por el plazo de 6 meses contados desde el día siguiente de la notificación de la última providencia dictada y recaída en alguna gestión útil para dar curso progresivo a los autos o desde el día siguiente al de la actuación procesal ordenada en dicha providencia.  Adicionalmente, se agrega que el abandono no procede, cuando se encuentre pendiente el despacho de escritos por parte del juez. <br><br>IMPROCEDENCIA DEL ABANDONO: La Ley reformatoria agrega que no cabe el abandono cuando en las causas estén involucrados derechos de adultos mayores, personas con discapacidad; así como derechos laborales de los trabajadores, y en procesos de carácter voluntario. <br><br>RECURSOS DE APELACIÓN: Retira la obligatoriedad de interponer la apelación de manera oral en la respectiva audiencia, de tal manera que se la podrá hacer de esta forma o de manera escrita. <br><br>RECURSOS DE CASACIÓN: Este recurso, ahora se interpondrá en el término de 30 días posteriores a la ejecutoria del auto o sentencia o del auto que niegue o acepte su ampliación o aclaración.  <br><br>LEGITIMACIÓN PASIVA EN PROCEDIMIENTOS CONTENCIOSO ADMINISTRATIVOS Y TRIBUTARIOS: Se reforma en el sentido de que la demanda ahora se podrá proponer contra la máxima autoridad, el representante legal de la institución con personería jurídica o el servidor público de quien provenga el acto o disposición a la que se refiere la demanda. <br><br>SUSPENSIÓN DE LA EJECUCIÓN COACTIVA: Se modifica la consignación de la cantidad para que el trámite de las excepciones suspenda la ejecución coactiva, que ahora será del 10% de la cantidad a la que asciende la deuda, sus intereses y costas. <br><br>PROCEDIMIENTOS SUMARIOS: Se agrega que esta vía es ahora aplicable para el caso de controversias relativas a facturas por bienes y servicios, siempre que la pretensión no sea exigible en procedimiento monitorio o en la vía ejecutiva.  Por otro lado, se dispone que para el caso de controversias por despido intempestivo de mujeres embarazadas o en período de lactancia, así como de dirigentes sindicales, en esta vía, se aplicarán los términos reducidos como en el caso de niñez y adolescencia.  Finalmente, se podrá también tramitar por esta vía la partición no voluntaria. <br><br>PROCEDIMIENTOS VOLUNTARIOS: Se reforma el tema de divorcios, en el sentido de que por esta vía voluntaria, cabe el divorcio o terminación de unión de hecho por mutuo consentimiento cuando haya hijos dependientes y que su situación sobre tenencia, visitas y alimentos no se encuentre resuelta previamente.  <br><br>TÍTULOS DE EJECUCIÓN: Se agrega como títulos de ejecución: los autos que aprueban una conciliación parcial, en caso de incumplimiento de los acuerdos aprobados; el auto que contiene la orden de pago en el procedimiento monitorio, ante la falta de oposición del demandado; y, la hipoteca.  <br><br>POLILEGAL S.A. <br>Julio 2019
                   </div>
                 </div>
               </div>
                    <div class="dt-sc-toggle-frame">
                  <h5 class="dt-sc-toggle"><a href="#">La importancia de la Directiva Nº 001/2019/DGP/PN en el marco legal administrativo policial.</a></h5>
                  <div class="dt-sc-toggle-content">
                    <div class="dt-sc-block" style="text-align: justify;">
		      El pasado 12 de abril de 2019 fue publicada en la Orden General No. 071 de la Policía Nacional, la Directiva Nº 001/2019/DGP/PN, la cual pasa a regular los aspectos fundamentales de funcionamiento del componente de Talento Humano de la Policía Nacional del Ecuador, en base al Código Orgánico de las Entidades de Seguridad Ciudadana y Orden Público (COESCOP). <br><br>Como es de conocimiento público, el COESCOP debería ser complementado por su correspondiente normativa reglamentaria, tal y como lo dispone la Disposición Transitoria Primera del propio cuerpo normativo, normativa infra legal que hasta día de hoy no ha sido publicada, generando incertidumbre e inseguridad jurídica entre los miembros de las entidades de seguridad ciudadana que están sometidos a la regulación del COESCOP, y a su vez, entre los operadores de justicia, los cuales debemos enfrentar los numerosos vacíos legales del código orgánico. <br><br>Al respecto, la Directiva 001/2019/DGP/PN, regula, de manera provisional, y hasta que se emitan los mencionados reglamentos, la organización y funcionamiento del componente de Talento Humano de la Policía Nacional, lo cual significa que esta nueva norma regula aspectos tan relevantes como los ingresos de aspirantes y cadetes al servicio activo, ascensos, evaluaciones anuales desempeño y competencias, traslados y comisiones, cesación, reincorporaciones, etc.  Algunas de estas nuevas figuras fueron introducidas por el COESCOP, pero carecían de definición y desarrollo jurídico. <br><br>Adicionalmente, la directiva mencionada deroga a su vez la Directiva Nº 2018/001/DGP/PN, la cual regulaba los procesos de concesión de condecoraciones y felicitaciones a los servidores policiales.  A partir de ahora, la Directiva publicada el 12 de abril de 2019 regula directamente este ámbito, ello con el fin de que los servidores policiales conozcan cómo proceder ante Talento Humano con el fin de solicitar estos reconocimientos profesionales. <br><br>El ámbito de vacaciones, licencias y permisos queda regulado por las mismas disposiciones aplicables a los servidores públicos en general, es decir, bajo el régimen de la Ley Orgánica del Servicio Público y su Reglamento.  <br><br>Por lo tanto, podemos afirmar que la Directiva Nº 001/2019/DGP/PN, la cual entró en vigor el pasado 12 de abril de 2019 con su publicación, es, junto con el COESCOP, la norma de mayor relevancia en el ámbito administrativo policial, ya que no solo completa un gran número de vacíos legales que el código presentaba, sino que complementa y detalla nuevos conceptos jurídicos que hasta ahora permanecían indeterminados, generando una mayor certeza y claridad sobre este ámbito jurídico. <br><br>Autor: Víctor Fernández. <br>Junio 2019
                   </div>
                 </div>
               </div>
                 <div class="dt-sc-toggle-frame">
                  <h5 class="dt-sc-toggle"><a href="#">El nuevo Estatuto Orgánico de Gestión Organizacional por Procesos de la Policía Nacional.</a></h5>
                  <div class="dt-sc-toggle-content">
                    <div class="dt-sc-block" style="text-align: justify;">
		      El pasado 14 de mayo de 2019, a través del Acuerdo Ministerial No. 0080, el Ministerio del Interior expidió el Estatuto Orgánico de Gestión Organizacional por Procesos de la Policía Nacional cuya finalidad es normar la estructura, competencias y organización institucional de la Policía Nacional.<br><br>En lo principal, este cuerpo legal se compone de los principios y valores fundamentales que lo rigen, entre los cuales se encuentran el respeto a los derechos humanos, participación ciudadana, modernización, tolerancia, credibilidad, honor, entre otros.  Adicionalmente, como objetivos institucionales, se plantean entre los más importantes, el incrementar la efectividad operativa, incrementar la confianza ciudadana, incrementar la transparencia en la gestión institucional, e incrementar el bienestar del talento humano policial.<br><br>Por otro lado, otorga la responsabilidad al Comité de Gestión de Calidad del Servicio y el Bienestar Institucional de proponer,  monitorear y evaluar la aplicación de las políticas, normas y prioridades relativas al mejoramiento de la eficiencia institucional.  Se establece que este Comité estará integrado por: a) El Comandante General o su delegado; b) El Director Nacional de Planificación; c) Un responsable de las direcciones generales y direcciones nacionales; y, d) El responsable de la Dirección Nacional de Administración de Talento Humano.<br><br>Posteriormente, el Estatuto norma la estructura institucional policial, determinando quién es el responsable de cada área de gestión, así como cuál es la misión de cada una de éstas, y sus atribuciones y responsabilidades de acuerdo al cargo; por ejemplo, están entre ellas, la Gestión Nacional de Asuntos Internos, la Gestión de Asesoría Jurídica, la Gestión de Comunicación Interna, la Gestión Nacional de Operaciones Especiales y Servicios Especializados, etc., cada una con su misión, su responsable y las atribuciones y responsabilidades.<br><br>De otra parte, en sus disposiciones generales, realiza énfasis en cuestiones relevantes como las relacionadas con el Servicio de Cesantía, el ISSPOL, la Federación Deportiva Policial y el Instituto Nacional de Estudios Históricos Policiales, señalando que estos organismos contarán con su propia normativa en cuanto a su organización y funcionamiento, por lo que no forman parte de este Estatuto.  Así también, dispone que este nuevo cuerpo legal constituirá el instrumento base para la actualización, rectificación o incorporación de nuevos puestos en el Manual de Descripción, Valoración y Clasificación de Puestos (cargos) de la Policía Nacional.<br><br>Finalmente, en sus Disposiciones Transitorias, otorga el plazo de 180 a la Dirección Nacional de Administración de Talento Humano, Dirección Nacional de Planificación y Dirección Nacional Financiera para que, en base a este instrumento, elaboren el Manual de Descripción, Valoración y Clasificación de Puestos, el Modelo de Gestión Institucional y ejecute la reestructuración de las UDAFs y EODs, respectivamente. <br><br>POLILEGAL S.A.<br>Mayo 2019
                   </div>
                 </div>
               </div>  
                 <div class="dt-sc-toggle-frame">
                  <h5 class="dt-sc-toggle"><a href="#">Las vías de impugnación previstas en el Código Orgánico Administrativo (COA).</a></h5>
                  <div class="dt-sc-toggle-content">
                    <div class="dt-sc-block" style="text-align: justify;">
		      El pasado mes de julio de 2018 entró en vigor el Código Orgánico Administrativo (COA), un nuevo cuerpo normativo que introduce destacables novedades jurídicas en el ámbito administrativo respecto a la anterior regulación.<br><br>Una de esas novedades se encuentra en el Título IV del Libro Segundo del COA, a través del cual se regulan las impugnaciones que pueden ser planteadas en el ámbito administrativo.  Los dos recursos que prevé el COA en sede administrativa son el Recurso de Apelación y el Recurso Extraordinario de Revisión, permitiendo la posibilidad de elección entre la impugnación en vía judicial o administrativa del acto administrativo correspondiente, siempre teniendo en cuenta que si se opta por atacar el acto administrativo acudiendo a las instancias judiciales se cerrará irrevocablemente la opción de acudir a la vía de impugnación administrativa, tal y como lo señala el Art. 217 numeral 3.<br><br>Centrándonos en la impugnación en sede administrativa, el COA establece unas Reglas Generales que son aplicables tanto al Recurso de Apelación como al Recurso Extraordinario de Revisión, entre las cuales destacan, por ejemplo, los requisitos de forma y contenido mínimo que deben tener todos los recursos administrativos, la subsanación de los recursos o etapas de la tramitación.<br><br>A nivel doctrinal, podemos diferenciar tres tipos de actos administrativos en función de su impugnabilidad en sede administrativa, según el COA: 1) El acto administrativo apelable; 2) El acto administrativo que ha causado estado en vía administrativa; 3) El acto administrativo firme.<br><br>1) El primero, se caracteriza porque puede ser impugnado en sede administrativa a través del Recurso de Apelación.  Para ello, el COA dispone en su Art. 224 que dentro del término de diez días contados desde la notificación del acto administrativo este puede ser impugnado por esta vía, pudiendo aportar, la parte recurrente, nuevos hechos o documentos que permitan desvirtuar lo resuelto por el órgano administrativo que expidió el acto, o alegando la nulidad del acto administrativo objeto de la apelación.  Es importante resaltar que el COA permite solicitar la suspensión del acto administrativo cuando la ejecución del mismo pudiera causar perjuicios de imposible o difícil reparación o cuando la apelación se fundamente en alguna causa de nulidad legalmente prevista, siempre que se solicite de forma expresa en el recurso.  El órgano ante el cual se interponga el recurso dispondrá del plazo máximo de un mes para resolver la apelación y notificar la decisión, sin que se determinen en la norma las consecuencias jurídicas si se incumple ese tiempo.<br><br>2) Un acto administrativo que causa estado es, según el COA, a) las resoluciones de los Recursos de Apelación, b) aquellos actos administrativos sobre los cuales no se interpuso Recurso de Apelación y c) aquellos actos administrativos sobre los cuales solo se ha planteado acción contenciosa administrativa.  Lo más importante a tener en cuenta sobre estos actos es que solo se podrán impugnar en sede administrativa mediante Recurso Extraordinario de Revisión, siempre y cuando el acto que ha causado estado incurra en alguna de las causales de impugnación señaladas en el Art. 232 del COA, y en el tiempo señalado en el mismo precepto.  Para este recurso, el código prevé el término de veinte días para ser admitido a trámite por el órgano competente desde su presentación, así como el plazo de un mes desde que fue admitido para ser resuelto y notificada la resolución respectiva.  La diferencia respecto del Recurso de Apelación es que el COA establece que en caso de no admitirse o resolverse el recurso en ese tiempo, operará el silencio administrativo negativo, es decir, quedará desestimado el mismo.  Desde nuestro punto de vista, consideramos que esta decisión del legislador es un retroceso considerable respecto de la anterior normativa, ya que genera incertidumbre para el administrado y, en cierta medida, fomenta la inactividad de los órganos administrativos encargados de resolver este tipo de recursos.<br><br>3) Por último, el COA define los actos administrativos firmes como aquellos sobre los cuales no cabe ninguna impugnación en el ámbito administrativo, por lo que frente a los mismos solo podría operar la impugnación en vía judicial, a través de la jurisdicción contencioso administrativa.<br><br>Autor: Víctor Fernández Bracero.<br>Diciembre 2018
                   </div>
                 </div>
               </div>    
                 <div class="dt-sc-toggle-frame">
                  <h5 class="dt-sc-toggle"><a href="#">Las faltas disciplinarias leves en el Código Orgánico de Entidades de Seguridad Ciudadana y Orden Público.</a></h5>
                  <div class="dt-sc-toggle-content">
                    <div class="dt-sc-block" style="text-align: justify;">
		      El Código Orgánico de Entidades de Seguridad Ciudadana y Orden Público (COESCOP), el cual cumplió medio año de vigencia el pasado 19 de junio de 2018, regula varios aspectos fundamentales de las entidades de seguridad ecuatorianas, entre ellas, de la Policía Nacional del Ecuador.  Uno de los puntos de mayor relevancia es el de la potestad sancionadora sobre los servidores policiales, los cuales están sometidos disciplinariamente a lo dispuesto por esta nueva normativa.<br><br>Las faltas disciplinarias previstas en el COESCOP son de tres rangos: leves, graves o muy graves, regulando la misma norma cuáles son los efectos jurídicos y las sanciones que pueden ser impuestas a los infractores.<br><br>El Art. 119 de la norma señala once causales consideradas como faltas leves, es decir, acciones u omisiones consideradas como infracciones leves, que no lleguen a ser graves ni muy graves.  Según lo dispuesto en el Art. 42 y siguientes del COESCOP, las faltas leves pueden ser sancionadas con las siguientes medidas disciplinarias: amonestación verbal, amonestación escrita, sanción pecuniaria menor (4% del salario del servidor) y sanción pecuniaria mayor (8% del salario del servidor), imponiéndose una u otra sanción en función de la reincidencia en el tiempo por parte del servidor policial.<br><br>La potestad para sancionar una falta leve corresponde al superior jerárquico del servidor.  El presunto infractor, una vez le sea notificado el memorando en el cual se le atribuye la supuesta comisión de una falta de este tipo, dispone de dos días término para presentar el escrito con las pruebas de descargo a su favor ante el superior que le pretende sancionar. Si el superior considera que lo aportado por el subalterno no desvirtúa la causal señalada, resolverá motivadamente imponerle una de las sanciones señaladas, resolución ante la cual el servidor puede presentar escrito de apelación ante el superior jerárquico de quien le sancionó, en el término de tres días desde que le fue notificada la sanción.<br><br>Autor: Víctor Fernández Bracero.<br>Agosto 2018
                   </div>
                 </div>
               </div>    
                 <div class="dt-sc-toggle-frame">
                  <h5 class="dt-sc-toggle"><a href="#">Los nuevos tipos de sanciones establecidas en el Código Orgánico de las Entidades de Seguridad Ciudadana y Orden Público.</a></h5>
                  <div class="dt-sc-toggle-content">
                    <div class="dt-sc-block" style="text-align: justify;">
                     2017-11-30 <br> Como es de conocimiento general, el Código Orgánico de las Entidades de Seguridad Ciudadana y Orden Público entrará en vigor en las próximas semanas. Una de las importantes novedades que introduce el nuevo Código para la Policía Nacional es el nuevo sistema de sanciones aplicables a las faltas disciplinarias. La Sección Segunda del Capítulo IV del Título Preliminar tiene por título "Faltas y Sanciones Administrativas", y divide los tipos de faltas en leves, graves y muy graves.<br><br>Para estas faltas, se prevé la aplicación de las siguientes sanciones:<br><br>1) Amonestación verbal: La aplicación de esta sanción está prevista cuando el servidor policial cometa una única falta considerada leve, y será impuesta por su superior jerárquico inmediato.<br><br>2) Amonestación escrita: Este tipo de sanción está prevista en el supuesto de que el servidor policial cometa una segunda falta leve en el lapso de un año, contado desde que cometió la primera falta leve. Al igual que en el anterior caso, también la impondrá el superior jerárquico del infractor.<br><br>3) Sanción pecuniaria menor: En el caso de que el policía cometa una tercera falta leve en menos de un año, el superior jerárquico del infractor le impondrá una sanción correspondiente al 4% de su remuneración mensual.<br><br>4) Sanción pecuniaria mayor: Esta sanción puede ser aplicada en dos situaciones distintas: a) cuando el servidor cometa una falta grave; o b) cuando reincida en tres o más faltas leves en el lapso de un año. En este caso, si la multa se debe al cometimiento de una falta grave, la competencia para sancionar es exclusiva de la Inspectoría General de la Policía Nacional. El porcentaje de la sanción será el 8% de la remuneración mensual del sancionado.<br><br>5) Suspensión de funciones: Consiste en la separación temporal del servidor policial hasta un máximo de treinta días sin goce de la remuneración, en los casos en que reitere dos faltas graves en un plazo no superior al año. Durante la suspensión, no podrá ejercer actividades atribuibles a su cargo y su función, ni tampoco hacer uso de los bienes institucionales.<br><br>6) Destitución: Se trata de la sanción más severa prevista por el Código Orgánico, ya que supone la separación definitiva de la profesión. Esta sanción se aplicará en el caso de a) reiteración de dos o más faltas graves en el lapso de un año; b) si comete una falta administrativa catalogada como muy grave; o c) por otras causales que se impondrán en las nuevas leyes y reglamentos policiales próximos a expedirse.<br><br>Todas las faltas quedarán registradas en la Hoja de Vida profesional del sancionado. No obstante, se permite la “rehabilitación de faltas”, es decir, la posibilidad de que la falta registrada se elimine de la Hoja de Vida, mediante la correspondiente solicitud ante la máxima autoridad del Ministerio Rector de Seguridad Ciudadana.<br><br>Para PoliLegal Abogados siempre será un deber mantenerle informado de las novedades jurídicas relevantes en el ejercicio de su actividad profesional.<br><br>Para más información y consultas, no dude en visitarnos o ponerse en contacto con nosotros.
                   </div>
                 </div>
               </div>    
                 <div class="dt-sc-toggle-frame">
                  <h5 class="dt-sc-toggle"><a href="#">La Mala Conducta Profesional en la Policía Nacional del Ecuador: qué es y cómo proceder.</a></h5>
                  <div class="dt-sc-toggle-content">
                    <div class="dt-sc-block" style="text-align: justify;">
                     2017-10-30 <br> Seguramente, el tema de Mala Conducta Profesional es uno de los que mayor controversia genera en el seno de la normativa administrativa interna policial.<br><br>  
En primer lugar, nos referiremos a su definición que conlleva ciertas ambigüedades y falta de concreción. La Ley de Personal de la Policía Nacional establece que: “constituye mala conducta profesional todo acto ejecutado por un miembro de la Policía que lesione gravemente el prestigio de la institución o que atente gravemente la moral y las buenas costumbres, así como la reincidencia en el cometimiento de faltas disciplinarias por lo que hubiere sido sancionado. Repútese como reincidencia la repetición de faltas en la vida profesional atento al tiempo y a su gravedad.”<br><br> 
Como puede observarse, la definición no es la más acertada; utiliza términos jurídicos indeterminados como “el prestigio de la institución”, “atente gravemente la moral y las buenas costumbres” o “reincidencia en el cometimiento de faltas”, para cuya interpretación se requiere observar desarrollo jurisprudencial y/o doctrinal, es decir, es necesario acudir a otros pronunciamientos externos al contenido de la propia ley para poder dotar de algún sentido a esos conceptos.   Quizás, la definición vigente hubiese sido correcta como cierre a un precepto en el cual se hubiera incluido una lista de conductas concretas o, por lo menos, un listado de conductas generales, pero a través de las cuales se justificara claramente que las mismas constituyen Mala Conducta Profesional.   Esto conlleva a que los organismos policiales puedan decidir, bajo su criterio, cuándo una conducta se califica como Mala o no, lo cual lógicamente podría atentar contra la seguridad jurídica.<br><br> 
En todo caso, la Ley de Personal de la Policía Nacional señala que, cuando se presuma que un servidor policial ha llevado a cabo una Mala Conducta Profesional, se le pondrá en situación de A Disposición hasta un máximo de sesenta días, tiempo durante el cual la Inspectoría General deberá llevar a cabo las diligencias e investigaciones que correspondan, a fin de que Consejo respectivo pueda resolver si se ha incurrido en Mala Conducta Profesional o no.  Una vez que Inspectoría General emite el informe con lo investigado y las respectivas conclusiones, lo remitirá al Consejo para que, bajo su criterio, decida.<br><br>
Si el Consejo correspondiente decide que el servidor no ha incurrido en Mala Conducta, recuperará su situación de Activo y volverá a desempeñar funciones dentro de la planta orgánica de la institución.<br><br>
En el supuesto de que el Consejo resuelva que sí ha existido Mala Conducta por parte del servidor, en esa misma resolución también se solicitará al Comandante General, en el caso de Clases y Policías, o al Ministro del Interior, en el caso de Oficiales, que se proceda a dar de baja al servidor policial, esto conforme lo dispone la Ley de Personal, en las causales de baja.  En este caso, el servidor policial podría presentar un Recurso de Apelación ante el Consejo que corresponda (que para estos efectos son el Consejo de Generales, en cuanto a las Resoluciones del Consejo Superior, y el Consejo Superior en cuanto a las Resoluciones del Consejo de Clases y Policías).  Si la Mala Conducta la hubiera declarado el Consejo de Generales, el Oficial podrá presentar la Reconsideración ante ese mismo organismo.  Cualquiera de estas peticiones se deberá presentar en el término de 15 días desde el momento en el cual se notificó la Resolución declarando la Mala Conducta Profesional. 
                   </div>
                 </div>
               </div>
                 <div class="dt-sc-toggle-frame">
                  <h5 class="dt-sc-toggle"><a href="#">El nuevo Código Orgánico de las Entidades de Seguridad Ciudadana y Orden Público: algunos cambios relevantes para la Policía Nacional</a></h5>
                  <div class="dt-sc-toggle-content">
                    <div class="dt-sc-block" style="text-align: justify;">
                     2017-06-30 <br> El pasado miércoles 21 de junio de 2017 se publicó el Código Orgánico de las Entidades de Seguridad Ciudadana, a través del Registro Oficial Suplemento Nº 19. <br> Los cambios que introduce este nuevo Código en el ámbito de la seguridad y orden público, así como en las instituciones públicas que históricamente se han encargado de dichas responsabilidades, son notables.  Respecto de la Policía Nacional del Ecuador, es importante destacar las siguientes novedades:<br> <br> A partir de ahora se crea el Ministerio Rector de la Seguridad Ciudadana, Protección Interna y Orden Público, el cual se configura como máximo organismo responsable de las áreas reguladas en el Código, por lo que será dicho organismo el que se encargará de fijar las directrices que debe seguir la Policía Nacional en el desempeño de sus funciones constitucionales. <br><br> El Ministro/a titular de dicha institución conocerá en última instancia los Recursos de Apelación y Recursos Extraordinarios de Revisión de los actos administrativo relacionados con los procesos que afecten a las carreras profesionales de policía. <br><br> Se endurece el Régimen Disciplinario, las faltas pasarán a considerarse como leves, graves o muy graves.  Adicionalmente, se podrá suspender del servicio al servidor policial hasta por 30 días de empleo y remuneración por la reiteración de dos faltas graves en el lapso de un año, y en caso de una falta calificada como muy grave se aplicará la destitución.  <br> <br> Los procedimientos disciplinarios se basarán en la oralidad, agilitando los trámites y procedimientos sancionadores. <br><br> Respecto de la estructura orgánica, los servidores policiales quedan divididos entre a) servidores policiales directivos y b) servidores policiales técnico operativos, y aumentan los años de permanencia de la siguiente manera: General Superior (2), General Inspector (3), General de Distrito (5), Coronel (7), Teniente Coronel (7), Mayor (7), Capitán (7), Teniente (5), Subteniente (4).  Policía (4), Cabo Segundo (5), Cabo Primero (7), Sargento Segundo (7), Sargento Primero (7), Suboficial Segundo (4), Suboficial Primero (3), Suboficial Mayor (2).<br> <br> Sobre el procedimiento para sancionar faltas disciplinarias, se establecen cambios de competencia.  Para faltas leves, la competencia para sancionar será del superior jerárquico, mientras que para faltas graves o muy graves la competencia investigativa corresponde a Asuntos Internos de la Policía Nacional y la competencia resolutiva es de la Inspectoría General, a través de un procedimiento Sumario Administrativo. <br> <br> Se establece el plazo de 180 días desde la publicación del Código para que la Policía Nacional apruebe los nuevos estatutos orgánicos y reglamentos que regularán sus funciones. <br><br> Con este Código se derogan las siguientes normas: Ley Orgánica de la Policía Nacional, Ley de Personal de la Policía Nacional, y Reglamento Sustitutivo al Reglamento de Disciplina de la Policía Nacional, así como toda la normativa derivada de éstas. <br> <br> Para una completa asesoría, visítenos en nuestras oficinas.
                   </div>
                 </div>
               </div>
               <div class="dt-sc-toggle-frame">
                <h5 class="dt-sc-toggle"><a href="#"> Lista de Eliminación Anual de la Policía Nacional: causas y cómo proceder </a></h5>
                <div class="dt-sc-toggle-content">
                  <div class="dt-sc-block" style="text-align: justify;">
                    2017-04-30 <br> 
                    La Ley de Personal de la Policía Nacional faculta al Consejo de Clases y Policías, Consejo Superior y Consejo de Generales, a establecer cuotas de eliminación hasta el 15 de abril de cada año.  La Dirección General de Personal es quien presenta a los Consejos la nómina preliminar del personal policial que integra la lista de eliminación.  El Consejo respectivo se encarga de estudiar dicha lista y emitir la Resolución que corresponda conforme a la Ley de Personal y a su Reglamento. <br> 

                    ¿Cuáles son las causales para conformar la Lista de Eliminación Anual? <br>

                    La Ley de Personal dispone que conformarán dicha Lista, el personal policial que se encuentre configurado en una o más de las siguientes causales: <br>

                    -Haber sido reprobado en un curso policial, técnico, científico o académico en el país o en el exterior, para el cual haya sido designado por la institución<br>
                    -No presentarse al segundo llamamiento del respectivo Consejo para realizar el curso de ascenso<br>
                    -No haber sido calificado idóneo para el ascenso al inmediato grado superior<br>
                    -Constar por dos años consecutivos en listas 4 de clasificación anual<br>
                    -No haber sido calificado por segunda ocasión al curso de promoción para ascenso <br> 
                    -Quien habiendo cumplido 20 años de servicio activo y efectivo, por estar comprendido en el 5% más bajo dentro de la ubicación en su promoción, calculado en forma prevista en el Reglamento.<br><br>
                    ¿Qué puede hacer el personal policial que conste en la Lista de Eliminación Anual?<br><br>

                    En estos casos, la Ley proporciona dos vías para los integrantes de la Lista:<br>

                    1. Puede solicitar su baja voluntaria<br>

                    2. Puede apelar <br><br>

                    En el segundo caso, la Apelación deberá cumplir las siguientes condiciones:<br>

                    a) Presentarse en un plazo máximo de 15 días, contados a partir de la fecha de notificación personal.<br>

                    b) Deberá presentarse por una sola vez. <br>

                    c) De acuerdo al caso, la Apelación deberá dirigirse al Consejo Superior o Consejo de Generales. <br>

                    d) En caso de tratarse de oficiales Generales y Superiores, se deberá interponer la Reconsideración ante el mismo Consejo.  <br><br>

                    Para una asesoría completa, visítenos en nuestras oficinas. <br>
                  </div>
                </div>
              </div>
              <div class="dt-sc-toggle-frame">
                <h5 class="dt-sc-toggle"><a href="#"> Las sanciones por faltas disciplinarias en el Reglamento Sustitutivo al Reglamento de Disciplina de la Policía Nacional </a></h5>
                <div class="dt-sc-toggle-content">
                  <div class="dt-sc-block" style="text-align: justify;">
                    2017-03-28<br>
                    A través del Acuerdo Ministerial No. 8010 del Ministerio del Interior, publicado en el Registro Oficial No. 939, de 7 de febrero de 2017 se expidió el Reglamento Sustitutivo al Reglamento de Disciplina de la Policía Nacional, el cual se constituye en el nuevo instrumento jurídico para establecer un régimen disciplinario dentro de la institución.  Uno de los cambios más importantes se refiere a la regulación de las sanciones por faltas disciplinarias.<br>

                    Respecto de ello, en el anterior Reglamento se establecían como sanciones: la destitución o baja, el arresto, la reprensión, el recargo del servicio, y la fajina. En el nuevo Reglamento Sustitutivo, el Art. 31 modifica algunas de estas reprensiones; por ejemplo, el arresto, el recargo del servicio, y la fajina se eliminan completamente, y se sustituyen por la suspensión del cargo sin goce de remuneración hasta 30 días, y con sanción pecuniaria del 1 al 7% de la remuneración mensual unificada.  La destitución o baja, y las reprensiones se mantienen en el nuevo Reglamento.<br>

                    Asimismo, el Reglamento Sustitutivo, en su Art. 37 cuantifica las sanciones en sus diferentes niveles homologados a arrestos disciplinarios, las cuales tendrían incidencia en los Deméritos conforme el Reglamento de Evaluación para el Ascenso de los Oficiales de la Policía Nacional. <br>
                    
					  <img src="img blog/01.png" border='2' width='50%' height='50%'> </img>
             		   <img src="img blog/02.png" border='2' width='50%' height='50%'> </img>
             		   <img src="img blog/03.png" border='2' width='50%' height='50%'> </img>
             		   <img src="img blog/04.png" border='2' width='50%' height='50%'> </img>
             		   <img src="img blog/05.png" border='2' width='50%' height='50%'> </img>
             		   <img src="img blog/06.png" border='2' width='50%' height='50%'> </img>
             		   <img src="img blog/07.png" border='2' width='50%' height='50%'> </img>
             		   
             		<P>Para el caso de Clases y Policías, el artículo dispone que los valores de cuantificación de las sanciones disciplinarias por reprensiones tendrán un valor ponderado equivalente al 100% de la interpolación geométrica, equivalencia que se determinará en función de los grados jerárquicos de los niveles de mando o conducción, mando intermedio o supervisión operativa, y ejecución operativa, equiparando el grado de Policía con el de Subteniente de Policía y así sucesivamente en relación al resto de grados jerárquicos.  Cabe tomar en cuenta que para aplicar o modificar las sanciones no existirá equivalencia de atenuantes y agravantes.<BR>

Este Reglamento Sustitutivo al Reglamento de Disciplina propone un procedimiento disciplinario que otorgue todas las garantías constitucionales, y así mismo, propugna que se convierta en un incentivo para el servidor policial en la realización de sus tareas, constituyéndose en un elemento trascendental para una policía moderna.  De todas formas, cada caso relacionado con las sanciones por distintas faltas disciplinarias dentro de la Institución, deberá analizarse individualmente y en apego a las disposiciones constitucionales y legales vigentes en nuestro ordenamiento. <BR>

Para PoliLegal es un gusto mantenerle informado y ofrecerle los servicios jurídicos derivados de esta información. </P>   
                 
                   
                    
                  </div>
                </div>
              </div>
            </div>
            <!--dt-sc-toggle-frame-set ends-->
          </div>     
          
          <!--primary ends-->
          
        </div>
        <!--container ends-->
        
        
        
      </div>
      <!--main ends-->
      
      <!--footer starts-->
		<?php include 'footer.php'; ?>
         <!--footer ends-->
        </div>
        <!--inner-wrapper ends-->    
      </div>
      <!--wrapper ends-->
      <!--wrapper ends-->
		<?php include "asistencias.php"; ?>
      <a href="" title="Go to Top" class="back-to-top"> <span class="fa fa-angle-up"></span> </a> 
      <!--Java Scripts--> 
      <!--Java Scripts-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <script type="text/javascript" src="js/jquery-migrate.min.js"></script>
      <script type="text/javascript" src="js/jquery.validate.min.js"></script>
      <script type="text/javascript" src="js/jquery-easing-1.3.js"></script>
      <script type="text/javascript" src="js/jquery.sticky.js"></script>
      <script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
      <script type="text/javascript" src="js/jquery.smartresize.js"></script> 
      <script type="text/javascript" src="js/shortcodes.js"></script>   
      
      <script type="text/javascript" src="js/custom.js"></script>
      
      <!-- Layer Slider --> 
      <script type="text/javascript" src="js/jquery-transit-modified.js"></script> 
      <script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script> 
      <script type='text/javascript' src="js/greensock.js"></script> 
      <script type='text/javascript' src="js/layerslider.transitions.js"></script> 
      
      <script type="text/javascript">var lsjQuery = jQuery;</script><script type="text/javascript"> lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice('layerslider_2','jquery'); } else { lsjQuery("#layerslider_2").layerSlider({responsiveUnder: 1240, layersContainer: 1170, skinsPath: 'js/layerslider/skins/'}) } }); </script>

                     <!-- Cerrar Session -->
                     <script>
                        jQuery(document).ready(function(){
                           jQuery("#close_session").click(function(){
                              alert('Su sesión ha sido cerrada');
                              window.location.href='closesession.php';
                           });
                        });
                     </script>
      
    </body>
    </html>

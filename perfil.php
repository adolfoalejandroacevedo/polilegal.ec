<?php session_start(); ?>
<?php
	header('Content-Type: text/html; charset=utf-8');
	include "detect_mobile.php";
	$iduser = $_GET['iduser'];
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
	mysqli_set_charset($conn, "utf8");
   $sql = "SELECT users.id id,
						email,
						nombres,
						users.ciudad ciudadid,
						ciudades.ciudad ciudad,
						planes.plan plan,
						plandestacado,
						users.plan
						planid,
						foto 
           FROM users 
           INNER JOIN planes 
           ON users.plan=idplan 
			  INNER JOIN ciudades
			  ON users.ciudad=ciudades.id
           WHERE users.id='$iduser'";
   if ($result = mysqli_query($conn, $sql)){
      $row = mysqli_fetch_assoc($result);
		$email = $row['email'];
   }else error_log("Error: " . $sql . "..." . mysqli_error($conn));

	switch($row['planid']){
		case "1":
			$img = ($row['plandestacado'] == 0) ? "images/imgpersonal.png":"uploads/".$row['foto'];
			break;
		case "2":
			$img = "uploads/".$row['foto'];
			break;
		case "3":
			$img = "uploads/".$row['foto'];
			break;
	}

   //Contador de visitas
   $ip = $_SERVER['REMOTE_ADDR'];
   $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}"));
   $hostname = (isset($details->hostname)) ? $details->hostname:"";
   $city = (isset($details->city)) ? $details->city:"";
   $region = (isset($details->region)) ? $details->region:"";
   $country = (isset($details->country)) ? $details->country:"";
   $loc = (isset($details->loc)) ? $details->loc:"";
   $org = (isset($details->org)) ? $details->org:"";
   $sql = "INSERT INTO visitas (
               iduser,
               ip,
               hostname,
               city,
               region,
               country,
               loc,
               org) 
            VALUE (
               '$iduser',
               '$ip',
               '$hostname',
               '$city',
               '$region',
               '$country',
               '$loc',
               '$org')";
   if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
	 <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Plataforma Digital de Abogados del Ecuador | Abogado Destacado</title>
	 <meta name="keywords" content="abogado, ecuador">
	 <meta name="description" content="Abogado destacado, conoce más acerca de este abogado (áreas, contactos, teléfono, redes, descripción, calificar, dirección,
												 ocalización, galería, imágenes" />
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
   <link rel="stylesheet" href="gp/css/font-awesome.min.css">
   <link href="gp/css/animate.min.css" rel="stylesheet">
    <link href="gp/css/prettyPhoto.css" rel="stylesheet">      
   <link href="css/main.css" rel="stylesheet">
    <link href="gp/css/responsive.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <!-- JQuery Validator and form -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.js"></script>

	 <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       

      <style>
			hr {
				 display:block;
				 border:0px;
				 height:19px;
				 background-image:url('salique/images/separators.png');
				 margin-top: 0px;
			}
			#portfolio .row {
				margin-left: 0px;
				margin-right: 0px;
				margin-bottom: 10px;
			}
			.lista {
				font-size: 16px;
			}
			ul#areas li {
				display: inline;
			}
			ul#areas {
				padding-left: 0px;
			}
			.ciudad h4{
				font-family: 'Open Sans', Arial, sans-serif;
				font-size: 18px;
				font-weight: 500;
				color: #000;
			}
			#portfolio {
				padding-top: 0px;
				padding-bottom: 0px;
				margin-top: 0px;
			}
			#blog {
				padding-bottom: 20px;
				padding-top: 50px;
				padding-left: 0px;
				padding-right: 0px;
			}
			.col-md-12 {
				padding-left: 0px;
				padding-right: 0px;
			}
			.col-md-9 {
				padding-right: 0px;
				padding-left: 15px;
			}
			.col-md-6 {
				padding-right: 0px;
				padding-left: 15px;
			}
			.col-md-3 {
				padding-right: 0px;
				padding-left: 15px;
			}
			.container {
				padding-left: 15px;
				padding-right: 15px;
			}
			.team {
				background-color: #f9f9f9;
				padding-top: 0px;
				padding-bottom: 0px;
				margin-bottom: 0px;
			}
			.btn {
				padding-left: 0px;
			}
			.rateButton {
				padding-left: 10px;
			}
          /* Color rojo para el texto de error de los campos */
         .error{
            color: red;
         }
         /* Borde rojo y grosor de linea de los inputs */
         .modalform input[type=email].error,
         .modalform input[type=text].error{
            padding:15px 18px;
            border:1px solid #FF0000;
         }
         textarea.error {
            border:1px solid #FF0000;
         }


      </style>    
 
  </head>
  <body class="homepage" style="background: #f9f9f9">   
		<?php include "header.php"; ?>
      <!--/header-->

		<section id="blog" class="container">
			<div class="container wow fadeInDown">
				<ol class="breadcrumb" align="center" style="margin-bottom: 0px">
					<li class="active" id="bottom">
						<h3><?php echo strtoupper($row['nombres']); ?></h3>
					</li>
				</ol>
				<div class="separador">
					<hr>
				</div>
				<div class="row">
					<div class="col-md-3" style="height: 300px;">
						<div class="portfolio-items" id="portafolio" style="height: 300px;">
							<div class="portfolio-item apps">
								<div class="recent-work-wrap" >
								<img class="thumbnail" src="<?php echo $img; ?>" style="width: 270px;" alt="">
									<div class="ciudad" style="text-align: center;">
										<h4>Ciudad: <?php echo $row['ciudad']; ?></h4>
									</div>
									<div class="overlay" style="height: 250px;">
										<div class="recent-work-inner">
											<div class="col-md-6" align="left">
												<a class="preview" href="<?php echo $img; ?>"
													rel="prettyPhoto"><i class="fa fa-eye"></i> Ampliar</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-9">
                  <h1 class="maintitle" style="color: #000">
                     <span>Areas de Práctica </span>
                  </h1>
						<div class="team jumbotron" style="padding-left: 0px;">
							<ul class="tag clearfix">
							<?php
								switch($row['planid']){
									case 1:
										$sql = "SELECT area FROM areas INNER JOIN usersareas ON areas.id=usersareas.idarea WHERE iduser='$iduser' LIMIT 1";
										break;
									case 2:
										$sql = "SELECT area FROM areas INNER JOIN usersareas ON areas.id=usersareas.idarea WHERE iduser='$iduser' LIMIT 5";
										break;
									case 3:
										$sql = "SELECT area FROM areas INNER JOIN usersareas ON areas.id=usersareas.idarea WHERE iduser='$iduser'";
								}
								if($result = mysqli_query($conn, $sql)){
									while($row2 = mysqli_fetch_assoc($result)){
							?>
										<li class="btn"><a><?php echo (isset($row2['area'])) ? $row2['area']:''; ?></a></li>
							<?php }
								}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
							?>
							</ul>
						</div>
					</div>
					<?php if($row['planid'] == 1) $col = 9; else $col = 6; ?>
					<div class="col-md-<?php echo $col; ?>">
                  <h1 class="maintitle" style="color: #000">
                     <span>Contactos </span>
                  </h1>
                  <ul class="tag clearfix">
                  <?php
                     switch($row['planid']){
                        case 1:
                           $sql = "SELECT email FROM emails WHERE iduser='$iduser' LIMIT 1";
                           break;
                        case 2:
                           $sql = "SELECT email FROM emails WHERE iduser='$iduser' LIMIT 1";
                           break;
                        case 3:
                           $sql = "SELECT email FROM emails WHERE iduser='$iduser'";
                     }
                     if($result = mysqli_query($conn, $sql)){
                        while($row2 = mysqli_fetch_assoc($result)){
            if(isset($row2['email'])) $mail = $row2['email']; else $mail = '';
                  ?>
                           <li class="btn"><a href="mailto:<?php echo $mail; ?>">
                           <?php echo $mail; ?></a></li>
                  <?php }
                     }else error_log("Error: " . $sql . "..." . mysqli_error($conn));
                  ?>
                  </ul>

					</div>
					<?php if($row['planid'] == 2 || $row['planid'] == 3) { ?>
					<div class="col-md-3">
                  <h1 class="maintitle" style="color: #000">
                     <span>Teléfonos </span>
                  </h1>
						<?php } ?>
						<?php if($row['planid'] == 2 || $row['planid'] == 3) { ?>
						<ul class="tag clearfix">
							<?php
								if($row['planid'] == 2){
									$sql = "SELECT phone FROM phones WHERE iduser='$iduser' LIMIT 1";
								}else if($row['planid'] == 3){
									$sql = "SELECT phone FROM phones WHERE iduser='$iduser'";
								}
								if($result = mysqli_query($conn, $sql)){
									while($row2 = mysqli_fetch_assoc($result)){
							?>
										 <li class="btn"><a href="#"><?php echo $row2['phone']; ?></a></li>
						<?php }
							}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
						?>
						</ul>
					</div>
					<?php } if($row['planid'] == 3) { ?>
               <div class="col-md-6">
                  <h1 class="maintitle" style="color: #000">
                     <span>Redes Sociales </span>
                  </h1>
						<div class="team jumbotron" style="padding-left: 0px;">
							<ul class="social_icons">
								<?php
									$sql = "SELECT name, url FROM social INNER JOIN socialnetworks ON idsocial=socialnetworks.id WHERE iduser='$iduser'";
									if ($result = mysqli_query($conn, $sql)){
										while($row2 = mysqli_fetch_assoc($result)){
								?>			
											<li><a href="<?php echo $row2['url']; ?>"><i class="fab fa-<?php echo $row2['name']; ?>"></i></a></li>
								<?php } 
									}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
								?>
							</ul>
						</div>
					</div>
					<div class="col-md-3">
						<h1 class="maintitle" style="color: #000">
							<span>Calificar</span>
						<h1>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<button type="button" class="btn btn-warning btn-sm rateButton" aria-label="Left Align">
							<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
							</button>
							<button type="button" class="btn btn-warning btn-grey btn-sm rateButton" aria-label="Left Align">
							<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
							</button>
							<button type="button" class="btn btn-warning btn-grey btn-sm rateButton" aria-label="Left Align">
							<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
							</button>
							<button type="button" class="btn btn-warning btn-grey btn-sm rateButton" aria-label="Left Align">
							<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
							</button>
							<button type="button" class="btn btn-warning btn-grey btn-sm rateButton" aria-label="Left Align">
							<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
							</button>
						</div>
						<!--
						<a href="#"><img src="images/starrate.png" width="100%" height="35px"/></a>
						-->
					</div>
					<?php } ?>
				</div>
				<?php 
					$sql = "SELECT description FROM descriptions WHERE iduser='$iduser'";
					if ($result = mysqli_query($conn, $sql)){
						$row2 = mysqli_fetch_assoc($result);
					}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
				?>
				<div class="row">
					<?php if($row['planid'] > 1) { ?>
					<div class="col-md-12" align="left">
						<h1 class="maintitle" style="color: #000">
							<span>Descripción </span>
						</h1>
					</div>
					<div class="col-md-12" align="left">
						<p><?php echo nl2br($row2['description']); ?></p>
					</div>
					<div class="col-md-12" align="left">
						<h1 class="maintitle" style="color: #000">
							<span>Dirección de lugar de trabajo </span>
						</h1>
					</div>
					<div class="col-md-12" align="left">
						<?php
							$sql = "SELECT address FROM jobaddress WHERE iduser='$iduser'";
							if ($result = mysqli_query($conn, $sql)){
								$row2 = mysqli_fetch_assoc($result);
							}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
						?>
						<p><?php echo nl2br($row2['address']); ?></p>
					</div>
					<?php } if($row['planid'] == 3) { ?>
					<div class="col-md-12" align="left">
						<h1 class="maintitle" style="color: #000">
							<span>Sitio Web </span>
						</h1>
					</div>
					<div class="col-md-12" align="left">
						<?php
							$sql = "SELECT url FROM sitioweb WHERE iduser='$iduser'";
							if ($result = mysqli_query($conn, $sql)){
								$row2 = mysqli_fetch_assoc($result);
							}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
						?>
						<p><a href="<?php echo $row2['url']; ?>"><?php echo $row2['url']; ?></a></p>
					</div>
               <div class="col-md-12" align="left">
                  <h1 class="maintitle" style="color: #000">
                     <span>Geolocalización </span>
                  </h1>
               </div>
					<div class="col-md-12" align="left">
						<?php
							$sql = "SELECT geo FROM geolocation WHERE iduser='$iduser'";
							if ($result = mysqli_query($conn, $sql)){
								$row2 = mysqli_fetch_assoc($result);
							}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
						?>
						<iframe class="gmap" id="geomap" src="<?php echo $row2['geo']; ?>" style="width: 100%; height: 400px"></iframe>
					</div>
					<div class="col-md-12" align="left">
						<h1 class="maintitle" style="color: #000">
							<span>Galería de imágenes </span>
						</h1>
					</div>
					<?php } ?>
            </div>
			</div>
		</section>
		<?php if($row['planid'] == 3) { ?>
   	<section id="portfolio">
			<div class="container wow fadeInDown" style="padding-right: 0px;padding-left: 0px;">
				<div class="row">
					<div class="portfolio-items" id="portafolio">
                     <?php
                        $sql = "SELECT * FROM galeria WHERE iduser='$iduser'";
                        if ($result = mysqli_query($conn, $sql)){
                           while ($row = mysqli_fetch_assoc($result)) {
                     ?>
                    <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3" id="<?php echo "$row[id]"; ?>" >
                        <div class="recent-work-wrap" >
                            <img class="img-responsive" src="<?php echo "uploads/".$row['img']; ?>" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
												<div class="col-md-6" align="left">
                                    	<a class="preview" href="<?php echo "uploads/".$row['img']; ?>" rel="prettyPhoto"><i class="fa fa-eye"></i> Ampliar</a>
												</div>
												<div class="col-md-6" align="right">
													<a class="fotos" href="<?php echo "uploads/".$row['img']; ?>" 
													data-idfoto="<?php echo "$row[id]"; ?>" ><i class="fa fa-trash-o"></i> Eliminar</a>
												</div>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
                     <?php
                        }
                     }else{
                        error_log("Error: " . $sql . "..." . mysqli_error($conn));
                     }
                     ?>
					</div>
				</div>
			</div>
		</section>
		<?php } ?>
		<!--/#portfolio-item-->

		<!--Promociones -->		
		<section style="padding-top: 0px; padding-bottom: 0px;">
			<div class="container wow fadeInDown" style="padding-right: 0px;padding-left: 0px;">
            <div class="row" style="height: 600px; padding-right: 15px;padding-left: 15px;">

					<div class="col-md-12" align="left" >
						<h1 class="maintitle" style="color: #000">
							<span>PROMOCIONES</span>
						</h1>
					</div>

						<div class="team jumbotron" style="background-image: url('images/abogados01.jpg'); background-repeat: no-repeat; 
							background-size: auto; width: 100%; 
							margin-top: 50px; padding-top: 0px; margin-bottom: 50px; padding-bottom: 0px;">
							<div class="carousel slide" data-ride="carousel" data-interval="6000">        
						</div>
								<div class="container">
									<div class="row destacados">
										<div id="carousel-reviews" class="carousel slide" data-ride="carousel">
											<div class="carousel-inner">
											<?php
												$sql = "SELECT titulo, 
																	subtitulo, 
																	contenido, 
																	img,
																	id,
																	iduser
															FROM cupones 
															WHERE iduser='$iduser' AND active='1'";
												if ($result = mysqli_query($conn, $sql)){
													$numreg = mysqli_num_rows($result);
													$cont = 0;
													$fade = "fadeInLeft";
													$n = 4;
													$nitems = ($_SESSION['mobile']) ? 1:4;
													while($row = mysqli_fetch_assoc($result)){
														if($cont != 0){
															if ($cont%2==0){
																$fade = "fadeInLeft";
																$n=4;
															}else{
																$fade = "fadeInRight";
																$n=3;
															}
														}
														if ($cont == 0){
											?>		
															<div class="item active">
											<?php		} else if ($cont%$nitems == 0){ ?>
															<div class="item">
											<?php } ?>
											<?php if(!$_SESSION['mobile']) { ?>
													<div class="col-md-3 wow <?php echo $fade; ?>">
											<?php } ?>
														<div class="box1">
															<h<?php echo $n; ?> style="text-transform: none"><?php echo $row['titulo']; ?></h<?php echo $n; ?>>
															<div class="page-header">
																<!--
																<img src="uploads/<?php echo utf8_decode($row['img']); ?>" 
																class="img-responsive" width="100%" height="167px">
																-->
																<img class="thumbnail" width="100%" height="150px"src="uploads/<?php echo utf8_decode($row['img']); ?>" alt="">
																<p style="font-size: 14px; font-weight: 400;"><?php echo $row['subtitulo']; ?></p>
															</div>
															<em><?php echo substr($row['contenido'],0,111)." ..."; ?>
															<a href="cupon.php?idcupon=<?php echo $row['id']; ?>">Ver más</a></em>
														</div>
											<?php if(!$_SESSION['mobile']) { ?>
													</div>
											<?php } ?>
											<?php
												$cont += 1;
												if($numreg == $cont || $cont%$nitems == 0){
											?>
												</div>
											<?php } ?>
											<?php } ?>	
											<?php	}else error_log("Error: " . $sql . "..." . mysqli_error($conn)); ?>
												<!-- End item -->
												<a class="left carousel-control" href="#carousel-reviews" role="button" data-slide="prev" 
													style="background-image: linear-gradient(to right,rgba(14, 12, 1, 0.95) 0,rgba(0,0,0,.0001) 100%);width: 50px">
													<span class="glyphicon glyphicon-chevron-left"></span>
												</a>
												<a class="right carousel-control" href="#carousel-reviews" role="button" data-slide="next" 
													style="background-image: linear-gradient(to right,rgba(0,0,0,.0001) 0,rgba(14, 12, 1, 0.95) 100%);width: 50px">
													<span class="glyphicon glyphicon-chevron-right"></span>
												</a>
											</div>
										</div>
									</div>
									<!-- End row -->
								</div>
							</div>
						</div>

				</div>
			</div>	
		</section>
      <!--/#Promociones-->

		<!--Articulos -->		
		<section style="padding-top: 0px; padding-bottom: 0px;">
			<div class="container wow fadeInDown" style="padding-right: 0px;padding-left: 0px;">
            <div class="row" style="padding-right: 15px;padding-left: 15px;">

					<div class="col-md-12" align="left" >
						<h1 class="maintitle" style="color: #000">
							<span>ARTÍCULOS</span>
						</h1>
					</div>

					<div class="team jumbotron" style="background-color: white; background-repeat: no-repeat; 
						background-size: auto; width: 100%; height: 420px; margin-top: 50px; padding-top: 0px; margin-bottom: 0px;">         
						<div class="container">
							<div class="row">         
						</div>
						<div class="carousel slide" data-ride="carousel" data-interval="6000">         
							<div class="container">
								<div class="row text-center">
									<div id="carousel-reviews3" class="carousel slide" data-ride="carousel">
										<div class="carousel-inner" style="height: 400px">
										<?php
											$sql = "SELECT titulo, 
																contenido, 
																img, 
																t1.id idart,
																iduser
														FROM articulos t1 
														INNER JOIN users t2
														ON iduser=t2.id 
														WHERE iduser='$iduser'
													 ";
											if ($result = mysqli_query($conn, $sql)){
												$numreg = mysqli_num_rows($result);
												$cont = 0;
												$nitems = ($_SESSION['mobile']) ? 1:3;
												while($row = mysqli_fetch_assoc($result)){
													if ($cont == 0){
										?>
											<div class="item active">
										<?php		} else if ($cont%$nitems == 0){ ?>
											<div class="item">
										<?php 	} ?>
												<div class="col-sm-6 col-md-4 " align="center">
													<div style="margin: 5px; border: 1px; border-style: solid;">
														<div class="blog-box  wow fadeInRight" style="height: 367px">
															<div class="blog-box-image">
																<img src="uploads/<?php echo $row['img']; ?>"  style="height: 200px; width: 300px;" class="img-responsive" alt="">
															</div>
															<div class="blog-box-content">
																<div style="height: 60px;">
																	<h4 style="padding: 3px;">
																	<a href="articulo.php?idart=<?php echo $row['idart']; ?>"><?php echo substr($row['titulo'],0,78)."...."; ?></a>
																	</h4>
																</div>
																<p style="font-size: 14px; font-weight: normal"><?php echo substr($row['contenido'],0,112)." ..."; ?></p>
																<a href="articulo.php?idart=<?php echo $row['idart']; ?>" class="btn btn-default site-btn">Leer más</a>
															</div>
														</div>
													</div>
													<!-- End Col -->	
												</div>
										<?php
													$cont += 1;
													if($numreg == $cont || $cont%$nitems == 0){
										?>
											</div>
										<?php		}
												}
											}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
										?>
											<a class="left carousel-control" href="#carousel-reviews3" role="button" data-slide="prev" 
												style="background-image: linear-gradient(to right,rgba(14, 12, 1, 0.95) 0,rgba(0,0,0,.0001) 100%);width: 50px">
												<span class="glyphicon glyphicon-chevron-left"></span>
											</a>
											<a class="right carousel-control" href="#carousel-reviews3" role="button" data-slide="next" 
												style="background-image: linear-gradient(to right,rgba(0,0,0,.0001) 0,rgba(14, 12, 1, 0.95) 100%);width: 50px">
												<span class="glyphicon glyphicon-chevron-right"></span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>	
		</section>
      <!--/#Articulos-->

      <!--Recomendaciones -->
      <section style="padding-top: 0px; padding-bottom: 0px;">
         <div class="container wow fadeInDown" style="padding-right: 0px;padding-left: 0px;">
            <div class="row" style="padding-right: 15px;padding-left: 15px;">

               <div class="col-md-12" align="left" >
                  <h1 class="maintitle" style="color: #000">
                     <span>RECOMENDACIONES</span>
                  </h1>
               </div>

					<div class="team jumbotron" style="background-image: url('images/abogados00.jpg'); 
					background-repeat: no-repeat; background-size: auto; width: 100%; height: 390px; margin-top: 50px;">
						<div class="carousel slide" data-ride="carousel" data-interval="6000">         
							<div class="container">
								<div class="row">
									<div id="carousel-reviews4" class="carousel slide" data-ride="carousel">
										<div class="carousel-inner">
											<?php
												$sql = "SELECT userId, 
																	ratingNumber, 
																	title, 
																	comments,
																	nombres, 
																	t3.ciudad ciudad,
																	visible
															FROM item_rating t1
															INNER JOIN users t2
															ON t2.id=userId 
															INNER JOIN ciudades t3
															ON t1.ciudad=t3.id
															WHERE userId='$iduser' AND visible='Si'";
												if ($result = mysqli_query($conn, $sql)){
													$numreg = mysqli_num_rows($result);
													$cont = 0;
													$nitems = ($_SESSION['mobile']) ? 1:3;
													while($row = mysqli_fetch_assoc($result)){
														if ($cont == 0){
											?>
											<div class="item active">
											<?php		} else if ($cont%$nitems == 0){ ?>
											<div class="item">
											<?php 	} ?>
												<div class="col-md-4 col-sm-6">
													<div class="block-text rel zmin">
														<a title="" href="perfil.php?iduser=<?php echo $row['userId']; ?>"><?php echo $row['nombres']; ?>
														</a>
														<div class="mark">Mi calificación: 
															<span class="rating-input">
															<?php
																for($i = 0; $i < 5; $i++){
																	if($i < $row['ratingNumber']){
															?>
																<span data-value="<?php echo $i; ?>" class="glyphicon glyphicon-star"></span>
															<?php }else{ ?> 
																<span data-value="<?php echo $i; ?>" class="glyphicon glyphicon-star-empty"></span>
															<?php } 
																}
															?>
															</span>
														</div>
														<p style="font-size: 14px;"><?php echo $row['comments']; ?></p>
														<ins class="ab zmin sprite sprite-i-triangle block"></ins>
													</div>
													<div class="person-text rel">
														<img src="" height="50px" class="img-circle" />
														<a title="" href="#"><?php echo $row['title']; ?></a>
														<i><?php echo $row['ciudad']; ?></i>
													</div>
												</div>
												<?php
													$cont += 1;
													if($numreg == $cont || $cont%$nitems == 0){
												?>
											</div>
												<?php	}
													}
												}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
												?>
										</div>
										<a class="left carousel-control" href="#carousel-reviews4" role="button" data-slide="prev">
										<span class="glyphicon glyphicon-chevron-left"></span>
										</a>
										<a class="right carousel-control" href="#carousel-reviews4" role="button" data-slide="next">
										<span class="glyphicon glyphicon-chevron-right"></span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</section>
		<!--/#Recomendaciones-->

		<div style="text-align: center; margin-top: 25px; margin-bottom: 25px;">
			<button type="button" id="<?php echo $email; ?>" 
				data-iduser="<?php echo $iduser; ?>" class="btn btn-success " data-toggle="modal" data-target="#myModal">
				<h2 style="padding-left: 25px; padding-right: 13px;">COTIZAR</h2>
			</button>
		</div>

      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->

      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Solicitar cotización</h4>
               </div>
               <div class="modal-body">
                  <form name="frmcotiza" id="frmcotiza" class="jumbotron modalform" style="padding: 25px;" method="post" action="solicitudcotiza.php">
                     <input type="hidden" name='formname' value="frmcotiza">
                     <input type="hidden" name='username' id="email" value="<?php echo $email; ?>">
                     <input type="hidden" name='iduser' id="userid" value="<?php echo $iduser; ?>">
                     <div class="form-group">
                        <label for="">Nombre</label>
                        <input type="text" name="name" id="modalname" value="" class="form-control">
                     </div>
                     <div class="form-group">
                        <label for="">Correo electrónico</label>
                        <input type="text" name="email" value="" class="form-control">
                     </div>
                     <div class="form-group">
                        <label for="">Teléfono</label>
                        <input type="text" name="phone" value="" class="form-control">
                     </div>
                     <div class="form-group textarea">
                        <label for="consulta" class="control-label">Consulta</label>
                        <textarea  name="consulta" id="consulta" value="" class="form-control" rows="8"></textarea>
                     </div>
                     <div class="form-group" align="center">
                         <button type="submit" name="submit" id="btnsubmit" class="btn btn-primary btn-lg" required="required"
									style="padding-left: 20px;" >Solicitar</button>
                         <img id="enviando" src="images/barra.gif" width="100px" height="25px" 
                                style="position: relative; vertical-align:middle; display: none;">
                     </div>
                  </form>
                  <!-- Zona de mensajes -->
                  <div id="mensajes"></div>
                  <!-- FIN Zona de mensajes -->
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-success" data-dismiss="modal" style="padding-left: 12px;">Cerrar</button>
               </div>
            </div>
         </div>
      </div>	
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="gp/js/bootstrap.min.js"></script>
    <script src="gp/js/jquery.prettyPhoto.js"></script>
    <script src="gp/js/jquery.isotope.min.js"></script>
    <script src="gp/js/wow.min.js"></script>
    <script src="gp/js/main.js"></script>
	 <script type="text/javascript" src="js/jquery.simpleslider.js"></script>
	 <script src="js/custom.js"></script>

	<!-- Clic sobre las estrellas -->
	<script>
		$(".rateButton").click(function(){
			window.location.href = 'rating.php?iduser=<?php echo $iduser; ?>';	
		});
	</script>

	<script>
		$('#myModal').on('show.bs.modal', function(e) {
			var id = $(e.relatedTarget).attr('id');
			var iduser = $(e.relatedTarget).attr('data-iduser');
			$('#email').val(id);
			$('#userid').val(iduser);
		});
	</script>         


  </body>
</html>


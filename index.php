<?php 
   session_start();
   session_unset();
	$current = "inicio";
?>
<!DOCTYPE HTML>

<html lang="en-gb" class="no-js">
<head>
  <meta http-equiv="content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <title>Polilegal| Inicio</title>
  <meta name="description" content="">
  <meta name="author" content="adolfo" >
  <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
  <link id="default-css" href="style.css" rel="stylesheet" type="text/css">
  <link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/flip.css" rel="stylesheet" type="text/css">
  <link rel='stylesheet' id='layerslider-css' href="css/layerslider.css" type='text/css' media='all' />
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" />
<!--[if IE 7] -->
  <!--Fonts-->
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <!--jquery-->
  <script src="js/modernizr-2.6.2.min.js"></script>
</head>
<body>
  <!--wrapper starts-->
  <div class="wrapper">
    <!--inner-wrapper starts-->
    <div class="inner-wrapper">
      <!--header starts-->
		<?php include 'header.php'; ?>
            <!--header ends--> 
            <!--main starts-->
            <div id="main"> 
              
              <!--slider starts-->
              <div id="slider">
                <div id="layerslider_2" class="ls-wp-container" style="width:100%;height:660px;margin:0 auto;margin-bottom: 0px;">
                  <div class="ls-slide" data-ls=" transition2d: all;"> <img src="images/img1806198.jpg" class="ls-bg" alt="Slide background" />
                    <h2 class="ls-l caption-1" style="top:385px;left:7px; font-size:20px; padding:15px 30px 15px 25px;white-space: nowrap;" data-ls="offsetxin:-80;delayin:1000;skewxin:-80;"> CONOCIMIENTO, EXPERIENCIA, SERIEDAD, CONFIANZA. </h2>
                    <p class="ls-l caption-1" style="top:452px;left:7px; font-weight:300; font-size:14px; padding:15px 50px 15px 25px; line-height:27px;white-space: nowrap;" data-ls="delayin:2000;skewxin:80;"> ¡CREEMOS EN LAS COSAS BIEN HECHAS! </p>
                    </div>
                    
                    <div class="ls-slide" data-ls="slidedelay:5500; transition2d: all;"> <img src="images/encabezado02.jpg" class="ls-bg" alt="Slide background" style="width: 1500px" /> <img class="ls-l" style="top:200px;left:4px;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:100;durationin:1800;delayin:500;scalexin:0.8;scaleyin:0.8;" src="images/LOGO_BLANCO_TRANSPARENTE.png" alt="" width="550px" height="auto">
                      <h3 class="ls-l" style="top:162px;left:560px;font-family:'Domine', serif; font-weight:bold; font-size:39px; color:#ffffff;white-space: nowrap;" data-ls="delayin:1500;skewxin:80;"> POLILEGAL S.A. </h3>
                      <h2 class="ls-l" style="top:198px;left:560px;font-family:'Domine', serif; font-weight:bold; font-size:26px; color:#ffffff; text-transform:uppercase;white-space: nowrap;" data-ls="offsetxin:-80;delayin:1500;skewxin:-80;"> Asistencia Jurídica </h2>
                      <p class="ls-l" style="top:270px;left:560px;font-family:'Domine', serif; font-size:16px; color:#ffffff; line-height:27px;white-space: nowrap;" data-ls="offsetxin:0;delayin:2000;scalexin:0.8;scaleyin:0.8;"></p>
                      <div class="ls-l caption-list" style="top:350px;left:560px;font-family:'Domine', serif; font-size:16px; font-weight:normal; padding:10px 0px 10px 20px;white-space: nowrap;" data-ls="delayin:3000;rotateyin:180;transformoriginin:Left 50% 0;"><i class="fa fa-picture-o" style="margin-right:15px;"> </i> Oficina matriz en Quito. </div>
                      <div class="ls-l caption-list" style="top:410px;left:560px;font-family:'Domine', serif; font-size:16px; font-weight:normal; padding:10px 0px 10px 20px;white-space: nowrap;" data-ls="delayin:3500;rotateyin:180;transformoriginin:Left 50% 0;"><i class="fa fa-user" style="margin-right:15px;"> </i> Profesionales altamente capacitados. </div>
                      <div class="ls-l caption-list" style="top:470px;left:560px;font-family:'Domine', serif; font-size:16px; font-weight:normal; padding:10px 0px 10px 20px;white-space: nowrap;" data-ls="delayin:4000;rotateyin:180;transformoriginin:Left 50% 0;"><i class="fa fa-briefcase" style="margin-right:15px;"> </i> Cobertura a nivel nacional. </div>
                      <div class="ls-l caption-list" style="top:530px;left:560px;font-family:'Domine', serif; font-size:16px; font-weight:normal; padding:10px 0px 10px 20px;white-space: nowrap;" data-ls="delayin:4500;rotateyin:180;transformoriginin:Left 50% 0;"><i class="fa fa-align-justify" style="margin-right:15px;"> </i> Brindamos las mejores soluciones jurídicas. </div>
                    </div>
                    
                    <div class="ls-slide" data-ls="slidedelay:5000; transition2d: all;">
                     <img src="images/img1806193.jpg" class="ls-bg" alt="Slide background" />
                     <h3 class="ls-l caption-2" style="top:240px;left:245px;font-family:'Domine', serif; font-size:30px; white-space: nowrap; text-transform: none;font-weight: 300; background-color: #767677;" data-ls="delayin:1500;rotateyin:90;skewxin:-60;"> <p align=center style="margin-top: -200px"><span style="color:#1d3e88; text-align: center"><b><center style="color: #ffffff; background-color: #312d2d; opacity: 0.7; ">Conoce nuestros Planes de <br>Asistencia Jurídica POLILEGAL S.A.<br> ¡Los abogados que necesitas, siempre junto a ti! </center></b></span> </p> </h3>
                     <a href="planes-de-asistencia.php" class="ls-l dt-sc-bordered-button" style='top:160px;left:525px;font-family:"Source Sans Pro",sans-serif; font-size:15px; padding:10px 28px; border:1px solid #ffffff; color:#ffffff; display:inline-block; font-weight:600; padding:12px 26px; text-transform:uppercase; white-space: nowrap;' data-ls="delayin:2000;rotateyin:90;skewxin:60;"> Afiliarme </a>
                   </div>
                   
                 </div>
               </div>
               <!--slider ends--> 
               
               <!--primary starts-->
               <section id="primary" class="content-full-width">
                <!--container starts-->
                <div class="container">
                  <h2 class="dt-sc-hr-title"> Servicios </h2>

                  <div class="dt-sc-one-fifth column first">
                    <div class="flip">
                      <div class="front">
                        <div class="dt-sc-ico-content type1">
                          <div class="icon"> <img src="images/practice_icon5.png" alt="" title="" width="50px" height="50px"> </div>
                          <h5><a href="servicios01.php"> ADMINISTRATIVO </a></h5>
                        </div>
                      </div>
                      <div class="back" style="background-color: #1d3e88">
                        <p style="text-align: center; padding-top: 30px">Conforme a la regulación dispuesta en el Código Orgánico General de Procesos...</p>
                        <a href="servicios01.php" class="ls-l dt-sc-bordered-button" style='top:320px;left:560px;font-family:"Source Sans Pro",sans-serif; font-size:12px; padding:10px 28px; border:1px solid #ffffff; color:#ffffff; display:inline-block; font-weight:600; padding:12px 26px; text-transform:uppercase; white-space: nowrap;' data-ls="delayin:4000;rotateyin:90;skewxin:60;"> Leer M&aacute;s </a>
                      </div>
                    </div>
                    <br>
                    <div class="flip">
                      <div class="front">
                        <div class="dt-sc-ico-content type1">
                          <div class="icon"> <img src="images/practice_icon1.png" alt="" title="" width="50px" height="50px"> </div>
                          <h5><a href="servicios01.php"> ADMINISTRATIVO POLICIAL </a></h5>
                        </div>
                      </div>
                      <div class="back" style="background-color: #1d3e88">
                        <p style="text-align: center; padding-top: 30px">Nos encargamos del asesoramiento y defensa...</p>
                        <a href="servicios01.php" class="ls-l dt-sc-bordered-button" style='top:320px;left:560px;font-family:"Source Sans Pro",sans-serif; font-size:12px; padding:10px 28px; border:1px solid #ffffff; color:#ffffff; display:inline-block; font-weight:600; padding:12px 26px; text-transform:uppercase; white-space: nowrap;' data-ls="delayin:4000;rotateyin:90;skewxin:60;"> Leer M&aacute;s </a>
                      </div>
                    </div>

                  </div>
                  
                  <div class="dt-sc-one-fifth column">
                    <div class="flip">
                      <div class="front">
                        <div class="dt-sc-ico-content type1">
                          <div class="icon"> <img src="images/practice_icon6.png" alt="" title="" width="50px" height="50px"> </div>
                          <h5><a href="servicios01.php"> CIVIL Y FAMILIA </a></h5>
                        </div>
                      </div>
                      <div class="back" style="background-color: #1d3e88">
                        <p style="text-align: center; padding-top: 30px">Asesoramos y patrocinamos en cuestiones civiles relacionadas con bienes...</p>
                        <a href="servicios01.php" class="ls-l dt-sc-bordered-button" style='top:320px;left:560px;font-family:"Source Sans Pro",sans-serif; font-size:12px; padding:10px 28px; border:1px solid #ffffff; color:#ffffff; display:inline-block; font-weight:600; padding:12px 26px; text-transform:uppercase; white-space: nowrap;' data-ls="delayin:4000;rotateyin:90;skewxin:60;"> Leer M&aacute;s </a>
                      </div>
                    </div>
                    <br>
                    <div class="flip">
                      <div class="front">
                        <div class="dt-sc-ico-content type1">
                          <div class="icon"> <img src="images/practice_icon3.png" alt="" title="" width="50px" height="50px"> </div>
                          <h5><a href="servicios01.php"> PENAL Y TRÁNSITO </a></h5>
                        </div>
                      </div>
                      <div class="back" style="background-color: #1d3e88">
                        <p style="text-align: center; padding-top: 30px">PoliLegal ofrece sus servicios de asesoría y...</p>
                        <a href="servicios01.php" class="ls-l dt-sc-bordered-button" style='top:320px;left:560px;font-family:"Source Sans Pro",sans-serif; font-size:12px; padding:10px 28px; border:1px solid #ffffff; color:#ffffff; display:inline-block; font-weight:600; padding:12px 26px; text-transform:uppercase; white-space: nowrap;' data-ls="delayin:4000;rotateyin:90;skewxin:60;"> Leer M&aacute;s </a>
                      </div>
                    </div>
                  </div>
                  
                  <div class="dt-sc-one-fifth column">
                    <div class="flip">
                      <div class="front">
                        <div class="dt-sc-ico-content type1">
                          <div class="icon"> <img src="images/practice_icon2.png" alt="" title="" width="50px" height="50px"> </div>
                          <h5><a href="servicios01.php"> CONSTITUCIONAL </a></h5>
                        </div>
                      </div>
                      <div class="back" style="background-color: #1d3e88">
                        <p style="text-align: center; padding-top: 30px">En virtud de las acciones reguladas en la Ley Orgánica de Garantías Jurisdiccionales...</p>
                        <a href="servicios01.php" class="ls-l dt-sc-bordered-button" style='top:320px;left:560px;font-family:"Source Sans Pro",sans-serif; font-size:12px; padding:10px 28px; border:1px solid #ffffff; color:#ffffff; display:inline-block; font-weight:600; padding:12px 26px; text-transform:uppercase; white-space: nowrap;' data-ls="delayin:4000;rotateyin:90;skewxin:60;"> Leer M&aacute;s </a>
                      </div>
                    </div>
                    <br>
                    <div class="flip">
                      <div class="front">
                        <div class="dt-sc-ico-content type1">
                          <div class="icon"> <img src="images/practice_icon7.png" alt="" title="" width="50px" height="50px"> </div>
                          <h5><a href="servicios01.php"> SOCIETARIO Y CORPORATIVO </a></h5>
                        </div>
                      </div>
                      <div class="back" style="background-color: #1d3e88">
                        <p style="text-align: center; padding-top: 30px">Nos encargamos de brindar asesoría a emprendedores...</p>
                        <a href="servicios01.php" class="ls-l dt-sc-bordered-button" style='top:320px;left:560px;font-family:"Source Sans Pro",sans-serif; font-size:12px; padding:10px 28px; border:1px solid #ffffff; color:#ffffff; display:inline-block; font-weight:600; padding:12px 26px; text-transform:uppercase; white-space: nowrap;' data-ls="delayin:4000;rotateyin:90;skewxin:60;"> Leer M&aacute;s </a>
                      </div>
                    </div>
                  </div>
                  
                  <div class="dt-sc-one-fifth column">
                    <div class="flip">
                      <div class="front">
                        <div class="dt-sc-ico-content type1">
                          <div class="icon"> <img src="images/jury.png" alt="" title="" width="50px" height="50px"> </div>
                          <h5><a href="servicios01.php"> ARBITRAJE Y MEDIACIÓN </a></h5>
                        </div>
                      </div>
                      <div class="back" style="background-color: #1d3e88">
                        <p style="text-align: center; padding-top: 30px">En virtud de las acciones reguladas en la Ley Orgánica de Garantías Jurisdiccionales...</p>
                        <a href="servicios01.php" class="ls-l dt-sc-bordered-button" style='top:320px;left:560px;font-family:"Source Sans Pro",sans-serif; font-size:12px; padding:10px 28px; border:1px solid #ffffff; color:#ffffff; display:inline-block; font-weight:600; padding:12px 26px; text-transform:uppercase; white-space: nowrap;' data-ls="delayin:4000;rotateyin:90;skewxin:60;"> Leer M&aacute;s </a>
                      </div>
                    </div>
                    <br>
                    <div class="flip">
                      <div class="front">
                        <div class="dt-sc-ico-content type1">
                          <div class="icon"> <img src="images/agreement.png" alt="" title="" width="50px" height="50px"> </div>
                          <h5><a href="servicios01.php"> COMERCIAL Y REGULATORIO </a></h5>
                        </div>
                      </div>
                      <div class="back" style="background-color: #1d3e88">
                        <p style="text-align: center; padding-top: 30px">Nos encargamos de brindar asesoría a emprendedores...</p>
                        <a href="servicios01.php" class="ls-l dt-sc-bordered-button" style='top:320px;left:560px;font-family:"Source Sans Pro",sans-serif; font-size:12px; padding:10px 28px; border:1px solid #ffffff; color:#ffffff; display:inline-block; font-weight:600; padding:12px 26px; text-transform:uppercase; white-space: nowrap;' data-ls="delayin:4000;rotateyin:90;skewxin:60;"> Leer M&aacute;s </a>
                      </div>
                    </div>
                  </div>

                  <div class="dt-sc-one-fifth column">
                    <div class="dt-sc-ico-content type1" style="background-color: #339933; height:475px">
                      <div class="icon"> <img src="images/practice_icon4.png" alt="" title=""> </div>
                      <h5><a href="plan1.php"> PLANES DE ASISTENCIA JURÍDICA </a></h5><br>
                      <p>Conoce nuestros planes de asistencia para personas naturales, empresas y personal policial...</p><br> 
								<p style="text-align: center;"><a href="planes-de-asistencia.php" style="color: #ffffff;"> Ver más....</a></p>
                    </div>
                  </div>
                  
                  
                  <div class="dt-sc-hr-invisible-large"></div>
                  <h4 class="aligncenter no-transform"> Nuestras soluciones legales son prácticas y eficaces, con un enfoque <br>
                    direccionado 100% al cliente. </h4>
                    <div class="dt-sc-hr-invisible-very-small"></div>
                    <p class="aligncenter" style="text-align: justify;"> Somos un Estudio Jurídico en constante innovación. Hemos rediseñado los servicios legales hacia una óptica diferente, en la cual, además de ofrecer comodidad en nuestros honorarios, brindamos valor agregado en cada servicio que damos, de tal forma que el cliente no solamente tiene el acompañamiento del abogado, sino un personal entero de confianza para la solución íntegra de sus necesidades. </p>
                    
                    <div class="dt-sc-hr-invisible-medium"></div>
                    <div class="dt-sc-hr-image"></div>
                    <div class="dt-sc-hr-invisible"></div>
                    <div class="column dt-sc-one-fifth first">
                      <div class="dt-sc-ico-content type2">
                        <a> <img src="images1/mapa.png"/></a>
                        <h6><a href="#" target="_blank"> Cobertura nacional </a></h6>
                      </div>
                    </div>
                    <div class="column dt-sc-one-fifth">
                      <div class="dt-sc-ico-content type2">
                        <a> <img src="images1/persona.png"/></a>
                        <h6><a href="#" target="_blank"> Atención personalizada </a></h6>
                      </div>
                    </div>
                    <div class="column dt-sc-one-fifth">
                      <div class="dt-sc-ico-content type2">
                        <a> <img src="images1/estrella.png"/></a>
                        <h6><a href="#" target="_blank"> Calidad y excelencia </a></h6>
                      </div>
                    </div>
                    <div class="column dt-sc-one-fifth">
                      <div class="dt-sc-ico-content type2">
                        <a> <img src="images1/foco.png"/></a>
                        <h6><a href="#" target="_blank"> Soluciones óptimas </a></h6>
                      </div>
                    </div>
                    <div class="column dt-sc-one-fifth">
                      <div class="dt-sc-ico-content type2">
                        <a> <img src="images1/libros.png"/></a>
                        <h6><a href="#" target="_blank"> Experiencia y especialización </a></h6>
                      </div>
                    </div>
                    <div class="dt-sc-hr-invisible"></div>
                    <!--dt-sc-one-half starts-->
                    <div class="column dt-sc-one-half first">
                      <h2 class="dt-sc-hr-icon-title"> <span class="fa fa-question-circle"></span> Preguntas frecuentes </h2>
                      <!--dt-sc-toggle-frame-set starts-->
                      <div class="dt-sc-toggle-frame-set faq">
                        <h5 class="dt-sc-toggle-accordion"><a href="#">¿Quiénes somos? </a></h5>
                        <div class="dt-sc-toggle-content">
                          <div class="dt-sc-block" style="text-align: justify"> Somos un amplio equipo de abogados altamente capacitados, especializados en el país y en el extranjero; con profundos conocimientos teórico-prácticos en varias ramas legales, y con una experiencia marcada por la ética y el profesionalismo.  </div>
                        </div>
                        <h5 class="dt-sc-toggle-accordion"><a href="#"> ¿Por qué elegirnos?</a></h5>
                        <div class="dt-sc-toggle-content">
                          <div class="dt-sc-block" style="text-align: justify"> Nuestras soluciones legales son prácticas, eficaces y ágiles, con un enfoque direccionado 100% al cliente, buscando constantemente las formas de comprender, satisfacer, así como detectar y prevenir todas sus necesidades en el ámbito legal.  De esta manera, procuramos superar sus expectativas, pues creemos firmemente que el cliente es la razón de ser de nuestra empresa.  </div>
                        </div>
                        <h5 class="dt-sc-toggle-accordion"><a href="#"> ¿Cómo puedo mantenerme informado de mi caso? </a></h5>
                        <div class="dt-sc-toggle-content">
                          <div class="dt-sc-block" style="text-align: justify"> La transparencia y comunicación son valores que nos caracterizan, y son fundamentales para el éxito de la relación entre Cliente y Abogado.  En PoliLegal, usted podrá acceder a una comunicación directa con nuestros Abogados, quienes le guiarán y le mantendrán informado de cada uno de los avances de su caso, creando un vínculo más cercano de información.   </div>
                        </div>
                        <h5 class="dt-sc-toggle-accordion"><a href="#">¿Cuál será el valor de los honorarios profesionales? </a></h5>
                        <div class="dt-sc-toggle-content">
                          <div class="dt-sc-block" style="text-align: justify"> Con PoliLegal S.A. usted recibirá una atención personalizada y una asesoría de calidad a precios justos y razonables.  Los honorarios profesionales son pactados de forma contractual con cada cliente y de acuerdo a cada caso.  Si contrata uno de nuestros planes de asistencia jurídica, el valor será fijo, de forma mensual.  </div>
                        </div>
                        
                      </div>
                      <!--dt-sc-toggle-frame-set ends--> 
                    </div>
                    <!--dt-sc-one-half ends--> 
                    
                    <!--dt-sc-one-half starts-->
                    <div class="column dt-sc-one-half">
                      <h2 class="dt-sc-hr-icon-title"> <span class="fa fa-file-text"></span> Actualidad </h2>
                      <div class="aligncenter"> <img src="images/blog06.jpg" alt="" title="">
                        <h4 class="dt-sc-simple-hr-title">ÚLTIMAS REFORMAS A LA LEY DE COMPAÑÍAS.</h4>
                        <p style="text-align: justify"> El día de ayer se expidió en el Registro Oficial No. 64, el Reglamento para el Porte y Uso de Armas y Tecnologías No Letales; y Equipos de Protección para las Entidades Complementarias de... </p>
                          <div class="dt-sc-hr-invisible-small"></div>
                          <a href="actualidad.php" class="dt-sc-simple-border-button"> Leer más </a> </div>
                        </div>
                        <div class="dt-sc-hr-invisible-large"></div>
                        
                        <!--container ends-->
                      </section>
                      <!--primary ends--> 
                      
                    </div>
                    <!--main ends--> 
                    
                    <!--footer starts-->
                    <?php include 'footer.php'; ?>
                        <!--footer ends--> 
                      </div>
                      <!--inner-wrapper ends--> 
                    </div>
                    <!--wrapper ends-->
						  <?php include "asistencias.php"; ?>
                    <a href="" title="Go to Top" class="back-to-top"> <span class="fa fa-angle-up"></span> </a> 
                    <!--Java Scripts--> 

                    <script type="text/javascript" src="js/jquery.js"></script> 
                    <script type="text/javascript" src="js/jquery-migrate.min.js"></script> 
                    <script type="text/javascript" src="js/jquery.validate.min.js"></script> 
                    <script type="text/javascript" src="js/jquery-easing-1.3.js"></script> 
                    <script type="text/javascript" src="js/jquery.sticky.js"></script> 
                    <script type="text/javascript" src="js/jquery.nicescroll.min.js"></script> 
                    <script type="text/javascript" src="js/jquery.smartresize.js"></script> 
                    <script type="text/javascript" src="js/shortcodes.js"></script> 
                    <script type="text/javascript" src="js/custom.js"></script> 

                    <!-- Layer Slider --> 
                    <script type="text/javascript" src="js/jquery-transit-modified.js"></script> 
                    <script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script> 
                    <script type='text/javascript' src="js/greensock.js"></script> 
                    <script type='text/javascript' src="js/layerslider.transitions.js"></script> 
                    <script type="text/javascript">
                      var lsjQuery = jQuery;</script><script type="text/javascript"> lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice('layerslider_2','jquery'); } else { lsjQuery("#layerslider_2").layerSlider({responsiveUnder: 1240, layersContainer: 1170, skinsPath: 'js/layerslider/skins/'}) } }); </script>

                      <!--script flip-->
                      <script type='text/javascript' src="js/flip/jquery.flip.min.js"></script> 
                      <script type='text/javascript' src="js/flip/script.js"></script> 

                     <!-- Cerrar Session -->
                     <script>
                        jQuery(document).ready(function(){
                           jQuery("#close_session").click(function(){
                              alert('Su sesión ha sido cerrada');
                              window.location.href='closesession.php';
                           });
                        });
                     </script>


                    </body>
                    </html>

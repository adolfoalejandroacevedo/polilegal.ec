<?php session_start(); ?>
<?php
	header('Content-Type: text/html; charset=utf-8');
	//include_once("db_connect.php");
	$config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
	mysqli_set_charset($conn, "utf8");

	if(!empty($_POST['rating']) && !empty($_POST['iduser'])){
		$userID = $_POST['iduser'];		
		$fullname = mysqli_real_escape_string($conn, $_POST['title']);
		$comment = mysqli_real_escape_string($conn, $_POST["comment"]);
		$sql = "INSERT INTO item_rating (userId, ratingNumber, title, email, ciudad, comments) 
					VALUES ('".$userID."', '".$_POST['rating']."', '".$fullname."', '".$_POST['email']."', '".$_POST['ciudad']."', '".$comment."')";
		if(!mysqli_query($conn, $sql)){
			error_log("Error: " . $sql . "..." . mysqli_error($conn));
			echo "error";
			exit;
		}

		//Datos del abogado
		$sql = "SELECT nombres,
							email
					FROM users
					WHERE id='$userID'";
		if($result = mysqli_query($conn, $sql)){
			$row = mysqli_fetch_assoc($result);
			$nombres = $row['nombres'];
			$email = $row['email'];
		}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

		//Nombre de la ciudad del calificador
		$sql = "SELECT ciudad FROM ciudades WHERE id='$_POST[ciudad]'";
		if($result = mysqli_query($conn, $sql)){
			$row = mysqli_fetch_assoc($result);
			$ciudad = $row['ciudad'];
		}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

		//Enviamos correo
		require 'phpmailer/PHPMailerAutoload.php';
		include("email4.php");
		$message = $cuerpo;
		$subject = "TUABOGADO.EC - Calificación de Usuario";
		$mail = new PHPMailer;
		$mail->CharSet = 'UTF-8';
		$mail->isSMTP();
		//$mail->Host = 'smtp.gmail.com';
		//$mail->Host = 'ssl://md-100.webhostbox.net';
		$mail->Host = $config['general']['mailserver'];
		//$mail->Port = 587;
		$mail->Port = 465;
		$mail->SMTPSecure = 'ssl';
		$mail->SMTPAuth = true;
		//$mail->Username = $email;
		$mail->Username = $config["general"]["username"];
		//$mail->Password = $password;
		$mail->Password = $config["general"]["password"];
		$mail->setFrom('info@tuabogado.ec', 'TUABOGADO.EC');
		$mail->addReplyTo('noreplyto@tuabogado.ec', 'TUABOGADO.EC');
		$mail->addAddress($email);
		$mail->Subject = $subject;
		$mail->msgHTML($message);
		if (!$mail->send()) {
			$error = "Mailer Error: " . $mail->ErrorInfo;
			error_log($error, 0);
		}
		echo "rating saved!";
	}
?>

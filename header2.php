		<?php $src = ($_SESSION['mobile']) ? "img/logo-tuabogadoec-new.png":"img/nuevologo/vector.png"; ?>
      <header id="header">
         <nav class="navbar navbar-fixed-top" role="banner" style="padding-top: 2px;background: #f5f5f5;">
            <div class="container">
            	<div class="row">
	               <div class="navbar-header" align="center">
	                  <a class="navbar-brand" href="index.php" >
								  <img class="logoheader" align="center" src="<?php echo $src; ?>">
							</a>
						</div>
						<center>
							<span class="titulo3">Plataforma Digital de Abogados del Ecuador</span>
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" style="float: none;">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</center>
	               <div class="collapse navbar-collapse navbar-right">
	                  <ul class="nav navbar-nav" style="margin-top: 25px;">
	                     <li><a href="index.php">Inicio</a></li>
	                     <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Directorio<span class="caret"></span></a>
	                        <ul class="dropdown-menu">
	                           <li><a href="lawyers4city.php">Abogados por ciudad</a></li>
	                           <li><a href="lawyers4area.php"> Abogados por área</a></li>
	                        </ul>
	                     </li>
	                     <li><a href="anunciate.php">Regístrate</a></li>
	                     <li><a href="planes.php">Planes</a></li>
	                     <li><a href="blog.php">Blog</a></li>
	                     <li><a href="contactanos.php">Contáctanos</a></li>
	                     <?php
	                        if (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] == true) {
								?>
										<li class="dropdown active">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
	                              aria-expanded="false"><?php echo $_SESSION['email']; ?><span class="caret"></span></a>
											<ul class="dropdown-menu">
	                           		<li><a href="miperfil.php">Mi perfil</a></li>
	                           		<li><a href="closesession.php">Cerrar sesión</a></li>
											</ul>
										</li>
								<?php
	                        }else{
								?>
	                           <li class="dropdown">
	                           	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
											aria-expanded="false">Abogados<span class="caret"></span></a>
	                           	<ul class="dropdown-menu">
	                           		<li><a href="login.php">Acceso Abogados</a></li>
	                           		<li><a href="register.php">Crear cuenta</a></li>
	                           	</ul>
	                           </li>
								<?php
	                        }
	                     ?>
	                  </ul>
	               </div>
            	</div>
            </div>
            <!--/.container-->
         </nav>
         <!--/nav-->
      </header>


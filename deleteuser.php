<?php

   session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");
   if(isset($_SESSION['role']) && $_SESSION['role'] !=='admin') {
      header("Location: index.php");
      exit;
   }

   //Conexion a la BD
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);

	//articulos
	$sql = "DELETE FROM articulos WHERE iduser='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	
	//banners
	$sql = "DELETE FROM banners WHERE iduser='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	
	//banners
	$sql = "DELETE FROM banners WHERE iduser='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	
	//cotizaciones
	$sql = "DELETE FROM cotizaciones WHERE userid='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	
	//cupones
	$sql = "DELETE FROM cupones WHERE iduser='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	
	//datafactura
	$sql = "DELETE FROM datafactura WHERE iduser='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	
	//descriptions
	$sql = "DELETE FROM descriptions WHERE iduser='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	
	//emails
	$sql = "DELETE FROM emails WHERE iduser='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	
	//galeria
	$sql = "DELETE FROM galeria WHERE iduser='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	
	//geolocation
	$sql = "DELETE FROM geolocation WHERE iduser='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	
	//item_rating
	$sql = "DELETE FROM item_rating WHERE userid='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	
	//jobaddress
	$sql = "DELETE FROM jobaddress WHERE iduser='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	
	//logins
	$sql = "DELETE FROM logins WHERE userid='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	
	//phones
	$sql = "DELETE FROM phones WHERE iduser='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	
	//recurrencias
	$sql = "DELETE FROM recurrencias WHERE userid='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	
	//sitioweb
	$sql = "DELETE FROM sitioweb WHERE iduser='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	
	//social
	$sql = "DELETE FROM social WHERE iduser='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	
	//transacciones
	$sql = "DELETE FROM transacciones WHERE iduser='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	
	//users
	$sql = "DELETE FROM users WHERE id='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	
	//usersareas
	$sql = "DELETE FROM usersareas WHERE iduser='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

	header("location: admin.php");
?>

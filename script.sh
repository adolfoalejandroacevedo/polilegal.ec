#!/bin/sh
for filename in $(grep 'header id="header"' *.php | awk -F ":" '{print $1}'); do

	#Obtiene el No de linea de la primera coincidencia
	inicio=$(grep -n 'header id="header"' $filename | awk -F ":" '{ print $1 }')

	#Obtiene el No de linea para la segunda coincidencia
	fin=$(grep -n '</header>' $filename | awk -F ":" '{ print $1 }')

	#Elimina las lineas entre inicio y fin de cada archivo
	sed -i $filename -re "$inicio,$fin d"

	#Inserta una nueva linea despues de la concidencia
	sed -i '/<body class="homepage">/a<?php include "header.php"; ?>' $filename
done

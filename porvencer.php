<?php

//Scrip que busca planes faltando 5 dias por vencer.
//Se envia correo informando al cliente.

header('Content-Type: text/html; charset=utf-8');
session_start();

//Conexion a la BD
$config = require 'config.php';
$conn=mysqli_connect(
		$config['database']['server'],
		$config['database']['username'],
		$config['database']['password'],
		$config['database']['db']
);
if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
mysqli_set_charset($conn, "utf8");

//Buscamos planes por vencer faltando 5 dias.
$sql = "SELECT t1.iduser userid,
					nombres,
					email,
					t3.plan plan,
					fechavence
					FROM transacciones t1
					INNER JOIN users t2 ON t1.iduser=t2.id
					INNER JOIN planes t3 ON t1.concepto=t3.idplan
					WHERE DATE(fechavence) = DATE_ADD(CURRENT_DATE(), INTERVAL 5 DAY) AND t1.estatus='1'
		 ";
if($result = mysqli_query($conn, $sql)){
	while($row = mysqli_fetch_assoc($result)){

		//Enviamos correo de notificacion//
		require 'phpmailer/PHPMailerAutoload.php';
		$config = require 'config.php';
		$subject = "TUABOGADO.EC - AVISO DE VENCIMIENTO";
		$mail = new PHPMailer;
		$mail->CharSet = 'UTF-8';
		$mail->isSMTP();
		//$mail->Host = 'smtp.gmail.com';
		//$mail->Host = 'ssl://md-100.webhostbox.net';
		$mail->Host = $config['general']['mailserver'];
		//$mail->Port = 587;
		$mail->Port = 465;
		$mail->SMTPSecure = 'ssl';
		$mail->SMTPAuth = true;
		//$mail->Username = $email;
		$mail->Username = $config["general"]["username"];
		//$mail->Password = $password;
		$mail->Password = $config["general"]["password"];
		$mail->setFrom('info@tuabogado.ec', 'TUABOGADO.EC');
		$mail->addReplyTo('noreplyto@tuabogado.ec', 'TUABOGADO.EC');
		$mail->addAddress($row['email']);
		$mail->Subject = $subject;
		//Set the body
		$mail->Body = '<b>Estimado Abogado: '.$row['nombres'].'</b><br><br>';
		$mail->Body .= 'Por medio de la presente te informamos que tu plan: '.$row['plan'].', vence en 5 días.';
		$mail->Body .= '<br><br>';
		$mail->Body .= 'Saludos Cordiales,<br>';
		$mail->Body .= 'TUABOGADO.EC';
		$mail->AltBody = 'This is a plain-text message body';
		if (!$mail->send()) {
			$error = "Mailer Error: " . $mail->ErrorInfo;
			error_log($error, 0);
		}

	}
}else rror_log("Error: " . $sql . "..." . mysqli_error($conn));

?>

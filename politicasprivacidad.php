<?php session_start(); ?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Plataforma Digital de Abogados del Ecuador | Pol&iacute;tica de Privacidad de Datos</title>
		<meta name="keywords" content="abogado, ecuador, abogados, quito, manta, portoviejo, esmeraldas, ibarra, cuenca, guayaquil, tributario, visitas, alimentos, juicio, penal, transito, compañia, constituir, negocio, legal">
		<meta name="description" content="TUABOGADO.EC garantiza el respeto y la protección de los datos personales que obtenga por la inscripción voluntaria de los profesionales y estudios jurídicos (Usuarios) interesados en formar parte de nuestra red." />
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <link href="css/animate.min.css" rel="stylesheet">
      <link href="css/prettyPhoto.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="bower_components/datatables.net-dt/css/jquery.dataTables.css">
      <link rel="stylesheet" type="text/css" href="css/bootstrap-markdown.min.css">
      <style>
         .pricingTable{
         text-align: center;
         transition: all 0.5s ease 0s;
         }
         .pricingTable:hover{
         box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
         }
         .pricingTable .pricingTable-header{
         color: #feffff;
         }
         .pricingTable .heading{
         display: block;
         padding-top: 25px;
         }
         .pricingTable .heading > h3{
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .textotitle{
         color: #feffff;
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .pricingTable .subtitle{
         display: block;
         font-size: 13px;
         margin-top: 5px;
         text-transform: capitalize;
         }
         .pricingTable .price-value{
         display: block;
         font-size: 60px;
         font-weight: 700;
         padding-bottom: 25px;
         }
         .pricingTable .price-value span{
         display: block;
         font-size: 14px;
         line-height: 20px;
         text-transform: uppercase;
         }
         .pricingTable .pricingContent{
         /*text-transform: capitalize;*/
         background: #fbfbfb;
         color: #fefeff;
         }
         .pricingTable .pricingContent ul{
         list-style: none;
         padding: 15px 14px 10px;
         margin: 0;
         text-align: left;
         }
         .pricingTable .pricingContent ul li{
         font-size: 14px;
         padding: 12px 0;
         border-bottom: 1px dashed #e1e1e1;
         color: #9da1ad;
         }
         .pricingTable .pricingContent ul li i{
         font-size: 14px;
         float: right;
         }
         .pricingTable .pricingTable-sign-up{
         padding: 20px 0;
         background: #fbfbfb;
         color: #fff;
         text-transform: capitalize;
         }
         .pricingTable .btn-block{
         width: 60%;
         margin: 0 auto;
         font-size: 17px;
         color: #fff;
         text-transform: capitalize;
         border: none;
         border-radius: 5px;
         padding: 10px;
         transition: all 0.5s ease 0s;
         }
         .pricingTable .btn-block:before{
         content: "\f007";
         font-family: 'FontAwesome';
         margin-right: 10px;
         }
         .pricingTable.blue .pricingTable-header,
         .pricingTable.blue .btn-block{
         background: #fce400;
         }
         .pricingTable.pink .pricingTable-header,
         .pricingTable.pink .btn-block{
         background: #c9b600;
         }
         .pricingTable.orange .pricingTable-header,
         .pricingTable.orange .btn-block{
         background: #e0d83a;
         }
         .pricingTable.green .pricingTable-header,
         .pricingTable.green .btn-block{
         background: #e0cd00;
         }
         .pricingTable.blue .btn-block:hover,
         .pricingTable.pink .btn-block:hover,
         .pricingTable.orange .btn-block:hover,
         .pricingTable.green .btn-block:hover{
         background: #e6e6e6;
         color: #939393;
         }
         @media screen and (max-width: 990px){
         .pricingTable{ margin-bottom: 20px; }
         }
      </style>
   </head>
   <body class="homepage">
<?php include "header.php"; ?>
      <!--/header-->
      <section id="about-us">
         <div class="container">
            <div class="skill-wrap clearfix">
               <div class=" wow fadeInDown">
                  <h1 class="center"> <span>Política de Privacidad de Datos</span></h1>
                  <p class="lead" style="text-align: justify;">TUABOGADO.EC garantiza el respeto y la protección de los datos personales que obtenga por la inscripción voluntaria de los profesionales y estudios jurídicos (Usuarios) interesados en formar parte de nuestra red.  </p>
                  <p class="lead"style="text-align: justify;">TUABOGADO.EC cumple en todo momento con las disposiciones normativas vigentes en la República del Ecuador respecto a la protección de datos personales.   </p>
                  <p class="lead"style="text-align: justify;">
                     El Usuario de la página web manifiesta haber leído y comprendido expresamente lo que a continuación se detalla, otorgando su consentimiento y aceptación voluntaria para que los datos personales que transfiera a TUABOGADO.EC sean utilizados conforme a la finalidad y objeto del sitio web.  
                  </p>
                  <p class="lead"style="text-align: justify;">
                     En el momento en el que un Abogado o Estudio Jurídico decide formar parte del directorio web TUABOGADO.EC, deberá completar un formulario en el cual se le solicitará una serie de datos personales, como son nombres y apellidos, correo/s electrónicos, matrícula profesional, así como todos aquellos datos que TUABOGADO.EC considere necesarios para poder prestar los servicios de promoción ofertados en la plataforma. La finalidad de la web es poder satisfacer las necesidades de promoción profesionales en Internet que puedan tener los profesionales del Derecho en Ecuador, a través de un espacio interactivo en el cual el profesional pueda tener su propio perfil profesional y dar a conocer al mercado sus conocimientos y competencias profesionales. 
                  </p>
                  <p class="lead"style="text-align: justify;">
                     <b>FINALIDAD DE TUABOGADO.EC </b> <br>
                     Los Usuarios que procedan a crear sus perfiles en la web, ceden voluntariamente sus datos personales los administradores de la misma con las siguientes finalidades: <br>
                  <ul>
                     <li> Publicitar los servicios y productos de Abogados y Estudios Jurídicos ecuatorianos mediante la creación de un perfil con sus datos de contacto, localización descripción de sus servicios y/o productos, fotografías, en su caso, etc.</li>
                     <li> Favorecer la interacción entre las personas solicitantes de servicios legales y los profesionales cuyo perfil está activo en la web, a través de los canales facilitados por el propio profesional. </li>
                     <li> Crear y potenciar una promoción digital efectiva del profesional, a través de las herramientas que la plataforma incluye, en función del plan escogido.</li>
                     <li> En general, crear una comunidad digital que suponga un método de publicidad y promoción efectivo para el colectivo de abogados del país.</li>
                  </ul>
                  </p>
                  <p class="lead"style="text-align: justify;">
                     <b>CARÁCTER DE LA INFORMACIÓN FACILITADA Y DE SU VERACIDAD </b><br>
                     El Usuario se compromete, con la presente declaración de aceptación, a que todos los datos aportados al formulario de registro o en otros apartados de la web son veraces, lo cual significa que son acordes a la realidad. <br>
                     Corresponde y es obligación del Usuario mantener, en todo momento, sus datos actualizados, siendo el usuario el único responsable de la inexactitud o falsedad de los datos facilitados y de los perjuicios que pueda causar por ello a TUABOGADO.EC o PoliLegal S.A. o a terceros con motivo de la utilización de los servicios ofrecidos por la plataforma.
                  </p>
                  <p class="lead"style="text-align: justify;">
                     <b>CONSENTIMIENTO DEL USUARIO</b><br>
                     El usuario autoriza a TUABOGADO.EC a mostrar y guardar los datos personales suministrados por él mismo, y hacerlos públicos en su perfil y en el sitio web. <br>
                     TUABOGADO.EC implementará y establecerá las medidas de seguridad que crea convenientes para evitar cualquier alteración, pérdida o mal uso de la información bajo su control.<br>
                     Los datos facilitados por el usuario al momento de registrarse en TUABOGADO.EC serán utilizados para la gestión de la relación con los usuarios, y para la ampliación y mejoras de contenidos para el envío de información que TUABOGADO.EC considere de su interés.
                     En ningún caso, TUABOGADO.EC estará autorizado para comercializar o transmitir a terceros los datos personales facilitados por los Usuarios de la plataforma, salvo consentimiento expreso de estos últimos. 
                  </p>
                  <p class="lead"style="text-align: justify;">
                     <b>VIGENCIA</b><br>
                     La Política de Privacidad de TUABOGADO.EC se encuentra vigente y TUABOGADO.EC podrá modificar dicha política en cualquier momento.  Las actualizaciones que se realicen estarán visibles y accesibles al público en el sitio web.
                  </p>
               </div>
               <div class="row team-bar">
                  <div class="first-one-arrow hidden-xs">
                     <hr>
                  </div>
                  <div class="first-arrow hidden-xs">
                     <hr>
                     <i class="fa fa-angle-up"></i>
                  </div>
                  <div class="second-arrow hidden-xs">
                     <hr>
                     <i class="fa fa-angle-down"></i>
                  </div>
                  <div class="third-arrow hidden-xs">
                     <hr>
                     <i class="fa fa-angle-up"></i>
                  </div>
                  <div class="fourth-arrow hidden-xs">
                     <hr>
                     <i class="fa fa-angle-down"></i>
                  </div>
               </div>
               <!--skill_border-->       
            </div>
         </div>
      </section>
      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/markdown.js"></script>
      <script src="js/to-markdown.js"></script>
      <script src="js/bootstrap-markdown.js"></script>
      <script src="js/bootstrap-markdown.es.js"></script>
      <script type="text/javascript" src="js/jquery.simpleslider.js"></script>
      <script type="text/javascript" charset="utf8" src="bower_components/datatables.net/js/jquery.dataTables.min.js"> </script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/jquery.isotope.min.js"></script>
      <script src="js/wow.min.js"></script>
      <script src="js/main.js"></script>
   </body>
</html>



<?php 
   session_start();
   session_unset();
	$current = "nosotros";
?>
<!DOCTYPE HTML>

<html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
  <meta http-equiv="content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
  <title>Polilegal | Nosotros</title>
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
  <link id="default-css" href="style.css" rel="stylesheet" type="text/css">
  <link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" /> 
<!--[if IE 7]>
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lt IE 9]>
<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--Fonts-->
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--jquery-->
<script src="js/jquery-1.10.2.js"></script>
<script src="js/jquery-ui.js"></script>
<script>
  $(function() {
    $( "#datepicker" ).datepicker({ dateFormat : 'dd/mm/yy', });
  });
</script>

<script src="js/modernizr-2.6.2.min.js"></script>
</head>
<body>
	<!--wrapper starts-->
  <div class="wrapper">
    <!--inner-wrapper starts-->
    <div class="inner-wrapper">
      <!--header starts-->
		<?php include 'header.php'; ?>
      <!--header ends-->
          <!--main starts-->
          <div id="main">
            
            <div class="breadcrumb-section">
              <div class="container">
                <h1> ACERCA DE NOSOTROS  </h1>
                <div class="breadcrumb">
                  <a href="index.php"> Inicio </a>
                  <span class="current"> Nosotros </span>
                </div>
              </div>
            </div>
            
            <!--primary starts-->
            <section id="primary" class="content-full-width">
              <!--container starts-->
              <div class="container">
                <p class="alignleft" style="text-align: justify;"><span class="dt-sc-highlight skin-color"> <strong> POLILEGAL S.A.</strong> </span> es un Estudio Jurídico conformado por un grupo de profesionales de cuarto nivel, altamente capacitados, con conocimientos teóricos y prácticos en varias ramas del derecho; quienes a través de ideas innovadoras y prácticas se encargan de brindar las mejores soluciones jurídicas.  ESTAMOS CONVENCIDOS DE QUE EL ÉXITO DE NUESTROS CLIENTES ES NUESTRO PROPIO ÉXITO. </p>
              </div>
              <!--container ends-->    
              <div class="dt-sc-hr-invisible-medium"></div>
              
              <!--fullwidth-background starts-->
              <div class="fullwidth-background dt-sc-parallax-section trust_section">
                <div class="container">
                  <h5> Más de 30 años de experiencia en el sector público y privado. </h5>
                </div>
              </div>
              <!--fullwidth-background ends-->
              
              <!--container starts-->
              <div class="container">
                <div class="dt-sc-one-fourth column first">
                  <div id="div-mision" class="dt-sc-colored-box">
                    <h5> MISIÓN </h5>
                    <div class="dt-sc-colored-box-content" id="mision" style="display: none">
                      <ul class="dt-sc-fancy-list caret">
                        <p class="aligncenter"><i> Ofrecemos servicios jurídicos de calidad, con actuaciones rápidas que se tornan en soluciones viables, priorizando y cuidando el bienestar de nuestros clientes y procurando siempre superar sus expectativas.  </i></p>
                        
                      </ul>
                    </div>
                  </div>
                </div>

                <div class="dt-sc-one-fourth column">
                  <div id="div-vision" class="dt-sc-colored-box">
                    <h5> VISIÓN </h5>
                    <div class="dt-sc-colored-box-content" id="vision" style="display: none">
                      <ul class="dt-sc-fancy-list caret">
                        <p class="aligncenter"><i> A través de un modelo basado en la excelencia y compromiso, POLILEGAL S.A. será la empresa pionera en la prestación de servicios de asistencia jurídica de calidad, convirtiéndonos en un referente en este campo a nivel nacional.    </i></p>
                        
                      </ul>
                    </div>
                  </div>
                </div>
                
                <div class="dt-sc-one-fourth column">
                  <div id="div-experiencia" class="dt-sc-colored-box">
                    <h5> NUESTRA EXPERIENCIA </h5>
                    <div class="dt-sc-colored-box-content" id="experiencia" style="display: none">
                      <p class="aligncenter"><i> Contamos con más de 30 años de experiencia en la práctica jurídica, tanto en el área pública como privada. Esta destacada trayectoria, acompañada del desarrollo tecnológico y operativo con el que cuenta nuestro Estudio, nos permite atender todos los requerimientos con responsabilidad y eficiencia. </i></p>
                      
                    </div>
                  </div>
                </div>
                <div class="dt-sc-one-fourth column">
                  <div id="div-equipo" class="dt-sc-colored-box">
                     <h5 style="background-color: #339933;"><a href="#" style="color: #FFFFFF"> NUESTRO EQUIPO </a></h5> 
                    <div class="dt-sc-colored-box-content" id="equipo" style="display: none">
                     <p class="aligncenter"><i>PoliLegal cuenta con profesionales competentes, de alto nivel, especializados en el país y en el exterior. Adicionalmente, contamos con Abogados y Estudios Jurídicos colaboradores en todas las provincias del país, lo que nos permite estar siempre accesibles para todos nuestros clientes. </i></p>
                   </div>
                 </div>
               </div>

               <div class="dt-sc-hr-invisible-large"></div>
               
               <h5 class="dt-sc-highlight aligncenter"> ¡EL ABOGADO QUE NECESITAS, MÁS CERCA DE TI!  </h5>
               <div class="dt-sc-hr-invisible-very-small"></div>
               <div class="dt-sc-hr-image"></div>
               <div class="dt-sc-hr-invisible-very-small"></div>
               <span class="dt-sc-highlight aligncenter">¿Tiene alguna consulta?</span>
               <div class="dt-sc-hr-invisible-medium"></div>
               <div class="aligncenter">
                <a class="dt-sc-bordered-button" href="contacto.php"> ENVÍENOS UN CORREO  </a>
                <a class="dt-sc-bordered-button" href="#"> LLÁMENOS (+593 2)  3333 533 </a>
              </div>
              <div class="dt-sc-hr-invisible-medium"></div>
              <p class="aligncenter"> En PoliLegal estamos listos para atender y responder a su solicitud  </p>
              <div class="dt-sc-hr-invisible"></div>
              
              <div class="dt-sc-two-third column first">
                <h2 class="dt-sc-bordered-title"> Más sobre nosotros </h2>
                <div class="dt-sc-hr-invisible-very-small"></div>
                <!--dt-sc-toggle-frame-set starts-->
                <div class="dt-sc-toggle-frame-set">
                  <div class="dt-sc-toggle-frame">
                    <h5 class="dt-sc-toggle-accordion active"><a href="#"> ¿Quiénes conforman PoliLegal? </a></h5>
                    <div class="dt-sc-toggle-content">
                      <div class="block" style="text-align: justify;">
                        PoliLegal cuenta con profesionales competentes de una significativa trayectoria, de alto nivel, especializados en el país y en el exterior, lo cual avala un profundo conocimiento del Derecho y garantiza la capacidad para resolver problemas jurídicos. 
                      </div>
                    </div>
                  </div>
                  <div class="dt-sc-toggle-frame">
                    <h5 class="dt-sc-toggle-accordion"><a href="#"> ¿Dónde están ubicados? </a></h5>
                    <div class="dt-sc-toggle-content">
                      <div class="block" style="text-align: justify;">
                        Nuestra oficina matriz se encuentra en Quito, donde contamos con instalaciones propias, tecnología de vanguardia, ubicación estratégica, y un ambiente cómodo y agradable.  Adicionalmente, PoliLegal S.A. cuenta con una red de Abogados a nivel nacional con el fin de brindarle un servicio especializado con una mayor cobertura.    
                      </div>
                    </div>
                  </div>
                  <div class="dt-sc-toggle-frame">
                    <h5 class="dt-sc-toggle-accordion"><a href="#"> Nuestros aliados </a></h5>
                    <div class="dt-sc-toggle-content">
                      <div class="block">
                        <div class="dt-sc-hr-invisible-medium"></div>
                        <div class="dt-sc-hr-invisible"></div>
                        <div class="column dt-sc-one-fifth first">
                          <div class="dt-sc-ico-content type2">
                            <a> <img src="images1/cpn.png"/></a>
                            <h6><a href="#" target="_blank"> Cooperativa Policía Nacional </a></h6>
                          </div>
                        </div>
                        <div class="column dt-sc-one-fifth">
                          <div class="dt-sc-ico-content type2">
                            <a> <img src="images/nic.jpg" width="70px"/></a>
                            <h6><a href="https://www.nic.ec/" target="_blank"> Nic.ec </a></h6>
                          </div>
                        </div>
                        <div class="column dt-sc-one-fifth">
                          <div class="dt-sc-ico-content type2">
                            <a> <img src="images1/big.jpg"/></a>
                            <h6><a href="www.bigdsgn.com" target="_blank"> Big Design </a></h6>
                          </div>
                        </div>
                        <div class="column dt-sc-one-fifth">
                          <div class="dt-sc-ico-content type2">
                            <a> <img src="images1/oibv.jpg"/></a>
                            <h6><a href="#" target="_blank"> Agencia de aduanas OIBV </a></h6>
                          </div>
                        </div>
			<div class="column dt-sc-one-fifth">
                          <div class="dt-sc-ico-content type2">
                            <a> <img src="images/rgs.jpg" width="120px"/></a>
                            <h6><a href="#" target="_blank"> Registro Seguro </a></h6>
                          </div>
                        </div>
                        
                        <div class="dt-sc-hr-invisible"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <!--dt-sc-toggle-frame-set ends-->
              </div>
              
              <div class="dt-sc-one-third column">
                <h2 class="dt-sc-bordered-title" style="text-align: center;"> ¿Por qué PoliLegal?  </h2>
                <div class="dt-sc-hr-invisible-very-small"></div>
                <div class="dt-sc-team">
                  <img src="images/logotransp.png" alt="" title="">
                </div>
                <div class="dt-sc-team-content">
                  
                  <span> <h6>EL ABOGADO QUE NECESITAS, MÁS CERCA DE TI.</h6> </span>

                  <p style="text-align: justify"> Nuestras soluciones legales son prácticas, eficaces y ágiles, con un enfoque direccionado 100% al cliente, buscando constantemente las formas de comprender, satisfacer, así como detectar y prevenir todas sus necesidades en el ámbito legal. </p>
                  <p class="dt-sc-team-phone-no"> <span class="fa fa-phone"></span>  (+593 2)  382 5530 </p>
                </div>
              </div>
            </div>

          </div>
          <!--container ends-->  

          <div class="dt-sc-hr-invisible-large"></div>

          <!--fullwidth-background starts-->
          <div class="fullwidth-background dt-sc-parallax-section benefits_section">
            <div class="container">
              
              <!--dt-sc-one-half starts-->
              <div class="dt-sc-one-half column first" style="margin-left: 25%">
                <h5 style="text-align: center"> Beneficios de la compañ&iacute;a </h5>
                <div class="column dt-sc-one-half first">
                  <div class="dt-sc-ico-content type3">
                    <div class="icon"> 
                      <img src="images/bookmark.png" alt="" title="">
                    </div>
                    <h6><a href="#" target="_blank"> Reconocida trayectoria </a></h6>
                  </div>
                  
                  <div class="dt-sc-ico-content type3">
                    <div class="icon"> 
                      <img src="images/staff.png" alt="" title="">
                    </div>
                    <h6><a href="#" target="_blank"> Asesoría especializada </a></h6>
                  </div>
                </div>
                
                <div class="column dt-sc-one-half">
                  <div class="dt-sc-ico-content type3">
                    <div class="icon"> 
                      <img src="images/chatting.png" alt="" title="">
                    </div>
                    <h6><a href="#" target="_blank"> Alianzas estratégicas </a></h6>
                  </div>
                  
                  <div class="dt-sc-ico-content type3">
                    <div class="icon"> 
                      <img src="images/shield.png" alt="" title="">
                    </div>
                    <h6><a href="#" target="_blank"> Clientes satisfechos </a></h6>
                  </div>
                </div>
                <div class="dt-sc-clear"></div>
              </div>
              <!--dt-sc-one-half ends-->
              
            </div>
            <!--fullwidth-background ends-->
            <div class="dt-sc-hr-invisible-large"></div>
            <!--container starts-->
            <div class="container">
              
              
            </div> 
            <!--container ends-->
            
          </section>
          <!--primary ends-->


        </div>
        <!--main ends-->

        <!--footer starts-->
			<?php include 'footer.php'; ?>
        <!--footer ends-->
       </div>
        <!--inner-wrapper ends-->    
      </div>
      <!--wrapper ends-->

      <!--wrapper ends-->
		<?php include "asistencias.php"; ?>
      <a href="" title="Go to Top" class="back-to-top"> <span class="fa fa-angle-up"></span> </a> 
		<a href="terminos.php"> Términos </a> </li>
                <li> <a href="buzon.php"> Buzón </a> </li>
                <li> <a href="contacto.php">Contacto</a></li<!--Java Scripts--> 
      <!--Java Scripts-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <script type="text/javascript" src="js/jquery-migrate.min.js"></script>
      <script type="text/javascript" src="js/jquery.validate.min.js"></script>
      <script type="text/javascript" src="js/jquery-easing-1.3.js"></script>
      <script type="text/javascript" src="js/jquery.sticky.js"></script>
      <script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
      <script type="text/javascript" src="js/jquery.smartresize.js"></script> 
      <script type="text/javascript" src="js/shortcodes.js"></script>   

      <script type="text/javascript" src="js/custom.js"></script>

      <!-- Layer Slider --> 
      <script type="text/javascript" src="js/jquery-transit-modified.js"></script> 
      <script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script> 
      <script type='text/javascript' src="js/greensock.js"></script> 
      <script type='text/javascript' src="js/layerslider.transitions.js"></script> 

      <script type="text/javascript">var lsjQuery = jQuery;</script><script type="text/javascript"> lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice('layerslider_2','jquery'); } else { lsjQuery("#layerslider_2").layerSlider({responsiveUnder: 1240, layersContainer: 1170, skinsPath: 'js/layerslider/skins/'}) } }); </script>

      <script type="text/javascript">
        /*jQuery( "#div-mision" ).mouseleave(function() {
            $("#mision").show('slide');
          });*/

          jQuery( "#div-mision" )
          .mouseenter(function() {
            jQuery("#mision").show('slide');
          })
          .mouseleave(function() {
            jQuery("#mision").hide('slide');
          });

          jQuery( "#div-vision" )
          .mouseenter(function() {
            jQuery("#vision").show('slide');
          })
          .mouseleave(function() {
            jQuery("#vision").hide('slide');
          });

          jQuery( "#div-experiencia" )
          .mouseenter(function() {
            jQuery("#experiencia").show('slide');
          })
          .mouseleave(function() {
            jQuery("#experiencia").hide('slide');
          });

          jQuery( "#div-equipo" )
          .mouseenter(function() {
            jQuery("#equipo").show('slide');
          })
          .mouseleave(function() {
            jQuery("#equipo").hide('slide');
          });
        </script>

                     <!-- Cerrar Session -->
                     <script>
                        jQuery(document).ready(function(){
                           jQuery("#close_session").click(function(){
                              alert('Su sesión ha sido cerrada');
                              window.location.href='closesession.php';
                           });
                        });
                     </script>
        
      </body>
      </html>

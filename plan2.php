<?php 
   session_start();
   session_unset();
?>
<!DOCTYPE HTML>
<html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
    <meta http-equiv="content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
    <title>Polilegal | Asistencias</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
    <link id="default-css" href="style.css" rel="stylesheet" type="text/css">
    <link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" /> 
<!--[if IE 7]>
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lt IE 9]>
<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--Fonts-->
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--jquery-->
<script src="js/modernizr-2.6.2.min.js"></script>
<style>
    .plan2-text{
        color: rgb(29, 62, 136);
        font-size: 18px;
        font-weight: normal;
    }
</style>
</head>
<body>
	<!--wrapper starts-->
    <div class="wrapper">
        <!--inner-wrapper starts-->
        <div class="inner-wrapper">
            <!--header starts-->
				<?php include 'header.php'; ?>
            <!--header ends-->
              <!--main starts-->
                    <div id="main">
                        
                        <div class="breadcrumb-section">
                            <div class="container">
                                <h1> PLAN DE ASISTENCIA JURÍDICA PARA SOCIOS SP Y CIVILES DE LA CPN </h1>
                                <div class="breadcrumb">
                                    <a href="index.php"> Inicio </a>
                                    <span class="current"> Plan </span>
                                </div>
                            </div>
                        </div>
                        
                        <section id="primary" class="content-full-width">
                            <!--container starts-->
                            <div class="container">
                                <p class="plan2-text" style="text-align: justify;"><span class="dt-sc-highlightt skin-color"> <strong style="color: rgb(43, 155, 15);"> ¡PROTEGE TUS INTERESES!</strong></span><img src="" style="position:absolute; margin-left: 42%; margin-top: -4%; width: 325px;"><br><br> Si eres socio en servicio pasivo o civil de la Cooperativa Policía Nacional, tenemos en PLAN perfecto para ti. A través de la alianza entre la Cooperativa Policía Nacional y PoliLegal Abogados, te damos la bienvenida a nuestro PLAN DE ASISTENCIA JURÍDICA PARA SOCIOS SP Y CIVILES DE LA CPN. <img src="images/caricatura.png" width="200px" height="350px" style="position:absolute; margin-left: 50%; margin-top: 6%;"></p>
                                
                                <!--p class="alignleft plan2-text" style="text-align: justify;"><span class="dt-sc-highlightt skin-color"> <strong> PROTEGE TUS INTERES.<br>Si eres socio civil de la Cooperativa Policía Nacional, tenemos en PLAN perfecto para ti. </strong> </span> A través de la alianza entre la Cooperativa Policía Nacional y PoliLegal Abogados, te damos la bienvenida a nuestro PLAN DE ASISTENCIA JURÍDICA PARA SOCIOS CIVILES.  </p-->
                    
                    <!--primary starts-->
                    <section id="secundary" class="content-full-width">
                      <div class="dt-sc-hr-invisible-small"></div>
                      <!--dt-sc-three-fourth starts-->
                      <div class="dt-sc-three-uno column first">
                        <!--dt-sc-tabs-container starts-->            
                        <div class="dt-sc-tabs-container">
                          <ul class="dt-sc-tabs-frame">
                            <li><a href="#"> Beneficios </a></li>  
                            <li><a href="#"> Cobertura </a></li> 
			                <li><a href="#"> Costo y Forma de Pago </a></li>
                          </ul>
                        <div class="dt-sc-tabs-frame-content">
                         <p class="plan2-text">• 100% de cobertura. </p>
                         <p class="plan2-text">• Atención ilimitada. </p>
                         <p class="plan2-text">• Abogados especializados. </p>
			             <p class="plan2-text">• Cobertura a nivel nacional. </p>
			             <p class="plan2-text">Para el uso de los servicios, el beneficiario deberá comunicarse a nuestro<br> Contact Center (02)3 825530 / (02)3 333533, correo electrónico, página web<br> o redes sociales, con el fin de gestionar sus solicitudes y asignarle a un Abogado.</p>
                       </div>
                       <div class="dt-sc-tabs-frame-content">
			<p><span class="dt-sc-highlightt skin-color"> <strong>Elaboración completa de:</strong></span></p>
                         <p class="plan2-text">• Minutas de compraventa. </p>
                         <p class="plan2-text">• Declaraciones juramentadas. </p>
                         <p class="plan2-text">• Cancelación de hipotecas. </p>
			 <p class="plan2-text">• Procuraciones judiciales. </p>
			 <p class="plan2-text">• Permisos de salida del país de menores. </p>
			 <p class="plan2-text">• Poderes generales y especiales. </p>
			 <p class="plan2-text">• Todo tipo de contratos. </p>
			 <p class="plan2-text">• Peticiones ante entidades públicas. </p>
			 <p class="plan2-text">• Requerimientos judiciales. </p>
			 <p><span class="dt-sc-highlightt skin-color"> <strong>Asesoría integral en:</strong></span></p>
                         <p class="plan2-text">• Asuntos civiles y de familia. </p>
                         <p class="plan2-text">• Procedimientos constitucionales. </p>
                         <p class="plan2-text">• Procedimientos contencioso-administrativos. </p>
			 <p class="plan2-text">• Apertura de negocios y constitución de compañías. </p>
			 <p class="plan2-text">• Asuntos penales y de tránsito. </p>
			 <p class="plan2-text">• Asesoría en cualquier otro asunto legal. </p>
			<p><span class="dt-sc-highlightt skin-color"><img src="images/sello3.png" width="155px" height="155px" style="position:absolute; margin-left: 50%;">
			<strong>Cobertura de consultas:</strong></span></p>
                         <p class="plan2-text">• Consultas personales ilimitadas en nuestras oficinas. </p>
                         <p class="plan2-text">• Consultas a través de la página web y correo electrónico. </p>
                         <p class="plan2-text">• Consultas a través de llamadas telefónicas a nuestro CONTACT CENTER. </p>
                       </div>
			<div class="dt-sc-tabs-frame-content">
                         <p class="plan2-text">• $ 6,99 USD Mensual (tarifa incluye impuestos). </p>
			 <p class="plan2-text">* El socio puede ingresar a cualquiera de nuestros Planes mediante:</p>
			 <p class="plan2-text">• Débito mensual de su cuenta de ahorros de la CPN. </p>
			 <p class="plan2-text">• Débito mensual de su cuenta en otra institución financiera. </p>
                       </div>
                     </div>
                   </div>
                   <!--dt-sc-tabs-container ends-->
                 </div>
                 <!--dt-sc-three-fourth ends-->
                 <!--dt-sc-one-fourth starts-->
                 <!--dt-sc-one-fourth ends-->
                 
                 <div class="dt-sc-clear"></div>
                 <div class="dt-sc-hr-invisible"></div>

               </section>
               <!--primary ends-->
               
                            </div>
                            <!--container ends-->    
                            <div class="dt-sc-hr-invisible-medium"></div>
                            
                            <!--fullwidth-background starts-->
                            <div class="fullwidth-background dt-sc-parallax-section trust_section_civiles">
                                <div class="container">
                                    <h5> Comienza ahora con nosotros </h5>
                                </div>
                            </div>
                            <!--fullwidth-background ends-->
                            
                            <!--container starts-->
                            <div class="container">
                            <div style="width: 100%; margin-left: 12%">    
                                <div class="dt-sc-one-fourth column first">
                                  <div class="dt-sc-colored-box">
                                    <h5> Paso 1 <br> Rellena la información<br> solicitada </h5>
                                </div>
                            </div>
                            <div class="dt-sc-one-fourth column">
                              <div class="dt-sc-colored-box">
                                <h5> Paso 2 <br> Acepta los Términos y Condiciones </h5>
                            </div>
                         </div>
                        </div>
                        
                        <div class="dt-sc-one-fourth column">
                          <div class="dt-sc-colored-box">
                            <h5> Paso 3 <br> Espera nuestro correo o llamada de confirmación </h5>
                        </div>
                    </div>
                </div>
                
                <div class="container"> 
                    <form name="helpform" class="help-form" method="post" action="PHPMailer/examples/gmail.php" accept-charset="UTF-8">
                      <input type="hidden" name="tipo" value="2">
                      <input type="hidden" id="tipo-pago" name="tipo-pago" value="1">
                      <!--primary starts-->
                      <section id="primary" class="content-full-width">

                        <h2 class="dt-sc-hr-title">Paso 1 <br> </h2>
                        <p class="column dt-sc-one-third first">
                          <input id="name" name="txtname" type="text" placeholder="Nombres y Apellidos" required="">
                        </p>
                        <p class="column dt-sc-one-third">
                          <input id="email" name="txtemail" type="email" placeholder="E-mail" required="">
                        </p>
                        <p class="column dt-sc-one-third">
                          <input id="cedula" name="txtcedula" type="text" placeholder="Cédula" required="">
                        </p>
                        <!--p class="column dt-sc-one-third first">
                            <input id="provincia" name="txtprovincia" type="text" placeholder="Provincia" required="">
                        </p-->
                        <p class="column dt-sc-one-third first">
                            <input id="telefono" name="txttelefono" type="text" placeholder="Télefono Móvil" required="">
                        </p>
                        <p class="column dt-sc-one-third">
                            <input id="ciudad" name="txtciudad" type="text" placeholder="Ciudad" required="">
                        </p>
                        <p class="column dt-sc-one-third">
                          <input id="sector" name="txtsector" type="text" placeholder="Sector de Residencia" title="El sector de residencia es necesario para la facturación" required="">
                        </p>

                        <div class="dt-sc-hr-invisible-large"></div>

                        <h2 class="dt-sc-hr-title">Paso 2 </h2>
                        <!--dt-sc-three-fourth starts-->
                        <div class="dt-sc-three-fourth column first" style="margin-left: 12%">
                          <!--dt-sc-tabs-container starts-->            
                          <div class="dt-sc-tabs-container">
                            <!--ul class="dt-sc-tabs-frame">
                              <li id="pestana1"><a href="#"> Débito mensual de la cuenta CPN </a></li> 
                              <li id="pestana2"><a href="#"> Débito mensual de otra institución </a></li>
                            </ul-->
                            <div class="dt-sc-tabs-frame-content" style="margin-top: -60px;">
                               <!--<p class="plan2-text"> <span> La informaci&oacute;n proporcionada debe ser correcta </span> </p>
                               <p>
                                 <input type="text" placeholder="Número de cuenta" class="text_input" name="hf_first_cuenta" id="hf_first_cuenta" required /> 
                               </p>
                               <p>
                				 <select name="hf_first_tipo_cuenta" id="hf_first_tipo_cuenta" class="text_input" required>
                				   <option value="Corriente">Tipo de Cuenta</option>
                				   <option value="Ahorro">Ahorro</option>
                				</select>  
                               </p>-->

                               <p class="plan2-text" style="text-align: center"> <span><a href="terminos.php" target="_blank"> He leído y acepto los Términos y condiciones </a> <input type="checkbox" name="hf_first_condiciones_1" required="" /> </span></p>

                               <div id="ajax_helpform_msg1" style="text-align: center"> </div>
                               <p style="text-align: center">
                                 <input type="submit" value="&nbsp; Enviar">
                                 <img id="enviando1" src="images/loading.gif" width="100px" height="100px" style="position: relative; vertical-align:middle; display: none;">
                               </p>    
                           </div>

                           <!--div class="dt-sc-tabs-frame-content">
                             <p class="plan2-text"> <span> La informaci&oacute;n proporcionada debe ser correcta  </span> </p>
                             <p>
                               <select name="hf_secund_institucion" id="hf_secund_institucion" class="text_input" placeholder="Nombre de la Institución financiera">
                                   <option value="Banco Pichincha">Institución financiera</option>
                				   <option value="Banco Pichincha">Banco Pichincha</option>
                				   <option value="Banco Guayaquil">Banco Guayaquil</option>
                				   <option value="Banco Internacional">Banco Internacional</option>
                				   <option value="Banco del Pacífico">Banco del Pacífico</option>
                				</select>
                             </p>
                             <p>
                               <input type="text" placeholder="Número de cuenta" class="text_input" name="hf_secund_cuenta" id="hf_secund_cuenta" required/>  
                             </p>
                             <p>
				<select name="hf_secund_tipo_cuenta" id="hf_secund_tipo_cuenta" class="text_input" required>
				   <option value="Corriente">Tipo de Cuenta</option>
				   <option value="Corriente">Corriente</option>
				   <option value="Ahorro">Ahorro</option>
				</select>
                             </p>
                             <p class="plan2-text"><a href="terminos.php" target="_blank"> He leído y acepto los Términos y condiciones </a> <input type="checkbox" name="hf_secund_condiciones_2" required=""/> </span></p>

                             <div id="ajax_helpform_msg2"> </div>
                             <p>
                               <input type="submit" value="&#xf002; &nbsp; Enviar">
                               <img id="enviando2" src="images/loading.gif" width="100px" height="100px" style="position: relative; vertical-align:middle; display: none">
                             </p>    
                           </div-->
                         </div>


                         <!--dt-sc-tabs-container ends-->
                       </div>


                     </section>
                     <!--primary ends-->
                   </form>
                 </div>
                 <!--container ends-->
           
           
       </div>
       <!--main ends-->
       
   </section>
   <!--primary ends-->
   
</div>

<!--dt-sc-one-fourth ends-->

<div class="dt-sc-clear"></div>

</section>
<!--container starts-->
<div class="dt-sc-hr-invisible-very-small"></div>
<!--dt-sc-toggle-frame-set starts-->
<div align="center" class="container">
  <div class="column dt-sc-one-fifth first">
      <div class=" type2">
          <div class="icon"> <span class=""> </span> </div>
          <h6><a href="#" target="_blank"> </a></h6>
      </div>
  </div>
  <div class="column dt-sc-one-fifth first">
    <div class="dt-sc-ico-content type2">
      <a> <img src="images1/mapa.png"/></a>
      <h6><a href="#" target="_blank"> Cobertura nacional </a></h6>
  </div>
</div>
<div class="column dt-sc-one-fifth">
    <div class="dt-sc-ico-content type2">
      <a> <img src="images1/libros.png"/></a>
      <h6><a href="#" target="_blank"> Experiencia y Especialización </a></h6>
  </div>
</div>
<div class="column dt-sc-one-fifth">
    <div class="dt-sc-ico-content type2">
      <a> <img src="images1/foco.png"/></a>
      <h6><a href="#" target="_blank"> Soluciones óptimas </a></h6>
  </div>
</div>
</div>

<div class="dt-sc-hr-invisible-large"></div>
<!--container ends-->
<div class="fullwidth-background dt-sc-parallax-section benefits_section">
    <div align="center" class="container">
        
        <!--dt-sc-one-half starts-->
        <div class="dt-sc-one-half textwidget first">
            <h5> EL SERVICIO JURÍDICO QUE ESTABAS ESPERANDO, CON LA MEJOR TARIFA Y COBERTURA, ¡CONTRÁTALO YA! <br><br> EL ABOGADO MÁS CERCA DE TI </h5>
        </div>
        <div class="dt-sc-hr-invisible-large"></div>
        
    </div>
</div>     
<div class="dt-sc-hr-invisible-large"></div>
<!--main ends-->
<div class="container">
    
  
</div> 
<!--footer starts-->
<?php include 'footer.php'; ?>
<!--footer ends-->
</div>
<!--inner-wrapper ends-->    
</div>
<!--wrapper ends-->
<!--wrapper ends-->
<?php include "asistencias.php"; ?>
<a href="" title="Go to Top" class="back-to-top"> <span class="fa fa-angle-up"></span> </a> 
<!--Java Scripts-->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-migrate.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-easing-1.3.js"></script>
<script type="text/javascript" src="js/jquery.sticky.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>


<script type="text/javascript" src="js/jquery.tabs.min.js"></script>
<script type="text/javascript" src="js/jquery.smartresize.js"></script> 
<script type="text/javascript" src="js/shortcodes.js"></script>   

<script type="text/javascript" src="js/custom.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDz1RGAW3gGyDtvmBfnH2_fE2DVVNWq4Eo&callback=initMap" type="text/javascript"></script>
<script src="js/gmap3.min.js"></script>
<!-- Layer Slider --> 
<script type="text/javascript" src="js/jquery-transit-modified.js"></script> 
<script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script> 
<script type='text/javascript' src="js/greensock.js"></script> 
<script type='text/javascript' src="js/layerslider.transitions.js"></script> 

<script type="text/javascript">var lsjQuery = jQuery;</script><script type="text/javascript"> lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice('layerslider_2','jquery'); } else { lsjQuery("#layerslider_2").layerSlider({responsiveUnder: 1240, layersContainer: 1170, skinsPath: 'js/layerslider/skins/'}) } }); </script>

<script type="text/javascript">
  jQuery( "#pestana1" ).click(function() {
    
    jQuery('#hf_secund_institucion option:first-child').attr('selected','selected');
    jQuery('#hf_secund_tipo_cuenta option:first-child').attr('selected','selected');
    jQuery('#hf_secund_cuenta').val('');
    jQuery('#hf_first_tipo_cuenta option:first-child').attr('selected','selected');
    jQuery('#hf_first_cuenta').val('');
      
    jQuery('#hf_secund_institucion').removeAttr('required');
    jQuery('#hf_secund_tipo_cuenta').removeAttr('required');
    jQuery('#hf_secund_cuenta').removeAttr('required');
    jQuery('#hf_first_tipo_cuenta').attr('required','true');
    jQuery('#hf_first_cuenta').attr('required','true');
    jQuery('#tipo-pago').val('1');
  });

  jQuery( "#pestana2" ).click(function() {
    
    jQuery('#hf_first_tipo_cuenta option:first-child').attr('selected','selected');
    jQuery('#hf_first_cuenta').val('');
    jQuery('#hf_secund_institucion option:first-child').attr('selected','selected');
    jQuery('#hf_secund_tipo_cuenta option:first-child').attr('selected','selected');
    jQuery('#hf_secund_cuenta').val('');
      
    jQuery('#hf_first_tipo_cuenta').removeAttr('required');
    jQuery('#hf_first_cuenta').removeAttr('required');
    jQuery('#hf_secund_institucion').attr('required','true');
    jQuery('#hf_secund_tipo_cuenta').attr('required','true');
    jQuery('#hf_secund_cuenta').attr('required','true');
    jQuery('#tipo-pago').val('2');
  });

</script>

                     <!-- Cerrar Session -->
                     <script>
                        jQuery(document).ready(function(){
                           jQuery("#close_session").click(function(){
                              alert('Su sesión ha sido cerrada');
                              window.location.href='closesession.php';
                           });
                        });
                     </script>

</body>
</html>

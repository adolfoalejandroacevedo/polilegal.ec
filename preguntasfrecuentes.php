<?php session_start(); ?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Plataforma Digital de Abogados del Ecuador | Preguntas Frecuentes</title>
		<meta name="keywords" content="abogado, ecuador, abogados, quito, tulcan, santa elena, galapagos, portoviejo, ibarra, ambato, cuenca, divorcio, alimentos, disolucion, union de hecho, manta">
		<meta name="description" content="Encuentra respuestas a tus preguntas aquí" />
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <link href="css/animate.min.css" rel="stylesheet">
      <link href="css/prettyPhoto.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="bower_components/datatables.net-dt/css/jquery.dataTables.css">
      <link rel="stylesheet" type="text/css" href="css/bootstrap-markdown.min.css">
      <style>
         .pricingTable{
         text-align: center;
         transition: all 0.5s ease 0s;
         }
         .pricingTable:hover{
         box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
         }
         .pricingTable .pricingTable-header{
         color: #feffff;
         }
         .pricingTable .heading{
         display: block;
         padding-top: 25px;
         }
         .pricingTable .heading > h3{
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .textotitle{
         color: #feffff;
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .pricingTable .subtitle{
         display: block;
         font-size: 13px;
         margin-top: 5px;
         text-transform: capitalize;
         }
         .pricingTable .price-value{
         display: block;
         font-size: 60px;
         font-weight: 700;
         padding-bottom: 25px;
         }
         .pricingTable .price-value span{
         display: block;
         font-size: 14px;
         line-height: 20px;
         text-transform: uppercase;
         }
         .pricingTable .pricingContent{
         /*text-transform: capitalize;*/
         background: #fbfbfb;
         color: #fefeff;
         }
         .pricingTable .pricingContent ul{
         list-style: none;
         padding: 15px 14px 10px;
         margin: 0;
         text-align: left;
         }
         .pricingTable .pricingContent ul li{
         font-size: 14px;
         padding: 12px 0;
         border-bottom: 1px dashed #e1e1e1;
         color: #9da1ad;
         }
         .pricingTable .pricingContent ul li i{
         font-size: 14px;
         float: right;
         }
         .pricingTable .pricingTable-sign-up{
         padding: 20px 0;
         background: #fbfbfb;
         color: #fff;
         text-transform: capitalize;
         }
         .pricingTable .btn-block{
         width: 60%;
         margin: 0 auto;
         font-size: 17px;
         color: #fff;
         text-transform: capitalize;
         border: none;
         border-radius: 5px;
         padding: 10px;
         transition: all 0.5s ease 0s;
         }
         .pricingTable .btn-block:before{
         content: "\f007";
         font-family: 'FontAwesome';
         margin-right: 10px;
         }
         .pricingTable.blue .pricingTable-header,
         .pricingTable.blue .btn-block{
         background: #fce400;
         }
         .pricingTable.pink .pricingTable-header,
         .pricingTable.pink .btn-block{
         background: #c9b600;
         }
         .pricingTable.orange .pricingTable-header,
         .pricingTable.orange .btn-block{
         background: #e0d83a;
         }
         .pricingTable.green .pricingTable-header,
         .pricingTable.green .btn-block{
         background: #e0cd00;
         }
         .pricingTable.blue .btn-block:hover,
         .pricingTable.pink .btn-block:hover,
         .pricingTable.orange .btn-block:hover,
         .pricingTable.green .btn-block:hover{
         background: #e6e6e6;
         color: #939393;
         }
         @media screen and (max-width: 990px){
         .pricingTable{ margin-bottom: 20px; }
         }
      </style>
   </head>
   <body class="homepage">
<?php include "header.php"; ?>
      <!--/header-->
      <section id="about-us">
      <div class="container">
      <div class="skill-wrap clearfix">
      <div class=" wow fadeInDown">
         <h1 class="center"> <span>Preguntas Frecuentes</span></h1>
         <p class="lead"><strong>¿Cómo buscar el Abogado que necesito?</strong><br></p>
         <p class="lead">Si usted necesita un Abogado, tiene dos opciones de encontrarlo en tuabogado.ec.  La primera, es buscarlo por la ciudad en la cual requiere los servicios; la segunda, es buscar directamente por el tipo de caso que usted posee, para lo cual deberá dirigirse a la búsqueda por áreas legales.<br><br></p>
         <strong>¿Qué ventajas existen al contactar a un Abogado por medio de tuabogado.ec?</strong><br></p><p class="lead">Tuabogado.ec es una plataforma segura y confiable, en la cual, a través de la información brindada, podrá encontrar fácilmente al mejor Abogado que se adapte a sus necesidades.  Entre las principales ventajas están: a) Se puede contactar con el Abogado desde cualquier lugar; b) Se afianza la comunicación entre cliente-Abogado; c) El Abogado estará disponible cuando lo necesite; e) Ahorro de tiempo y dinero en desplazamientos, y muchas ventajas más.<br><br></p>
         <strong>Si soy Abogado o Estudio Jurídico, ¿Cuáles son las ventajas de registrarme en tuabogado.ec?</strong><br></p><p class="lead">Estar en tuabogado.ec es la puerta de entrada al mundo digital.  A través de esta plataforma, usted, además de dar a conocer su profesionalismo y especialización, podrá mostrar contenido de valor académico mediante el desarrollo de sus propios artículos, crear y afianzar vínculos con nuevos clientes; y lo más importante, para permanecer en un entorno digital apropiado para Abogados.<br><br></p>
         <strong>Soy Abogado y quiero contratar el servicio de Directorio, ¿Cómo es el método de pago?</strong><br></p><p class="lead">Tuabogado.ec es una plataforma totalmente confiable.  Trabajamos con una de las empresas más confiables de pagos y transacciones monetarias en el Ecuador como es Paymentez cuyos términos y condiciones se podrán ver siempre en su sitio web www.paymentez.com.ec .  Con este aval, tuabogado.ec receptará pagos con tarjeta de crédito o débito de forma segura.<br><br></p>
      </div>
      <div class="row team-bar">
         <div class="first-one-arrow hidden-xs">
            <hr>
         </div>
         <div class="first-arrow hidden-xs">
            <hr>
            <i class="fa fa-angle-up"></i>
         </div>
         <div class="second-arrow hidden-xs">
            <hr>
            <i class="fa fa-angle-down"></i>
         </div>
         <div class="third-arrow hidden-xs">
            <hr>
            <i class="fa fa-angle-up"></i>
         </div>
         <div class="fourth-arrow hidden-xs">
            <hr>
            <i class="fa fa-angle-down"></i>
         </div>
      </div>
      <!--skill_border-->       
      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/markdown.js"></script>
      <script src="js/to-markdown.js"></script>
      <script src="js/bootstrap-markdown.js"></script>
      <script src="js/bootstrap-markdown.es.js"></script>
      <script type="text/javascript" src="js/jquery.simpleslider.js"></script>
      <script type="text/javascript" charset="utf8" src="bower_components/datatables.net/js/jquery.dataTables.min.js"> </script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/jquery.isotope.min.js"></script>
      <script src="js/wow.min.js"></script>
      <script src="js/main.js"></script>
   </body>
</html>



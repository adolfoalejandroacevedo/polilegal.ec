<?php

	header('Content-Type: text/html; charset=utf-8');
	session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");

   //Conexion a la BD
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
	mysqli_set_charset($conn, "utf8");

	//Si la area esta en blanco regresamos al listado
	if(trim($_POST['area']) == "") header("location: adm_areas.php");

	//Insertamos la area en la base de datos
	$area = mysqli_real_escape_string($conn,$_POST['area']);
	if($_POST['idarea'] == '0'){
		$sql = "INSERT INTO areas (
					area )
					VALUES (
						'$area')";
	}else{
		$sql = "UPDATE areas
					SET 	area='$area'
					WHERE id='$_POST[idarea]'";
	}
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

	//Regresamos al listado
	header("location: adm_areas.php");

?>


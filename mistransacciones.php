<?php 
	header('Content-Type: text/html; charset=utf-8');
	session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");

  $config = require 'config.php';
  $conn=mysqli_connect(
  $config['database']['server'],
  $config['database']['username'],
  $config['database']['password'],
  $config['database']['db']);
  if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
  mysqli_set_charset($conn, "utf8");

  //Proxima fecha de cobro y monto
    $sql = "SELECT amount, DATE_FORMAT(fechaexec,'%d/%m/%Y') AS fecha from recurrencias WHERE userid='82' AND active='1'";
    if ($result = mysqli_query($conn, $sql)){
      $row = mysqli_fetch_assoc($result);
      $monto = $row['amount'];
      $fecha = $row['fecha'];
    }else error_log("Error: " . $sql . "..." . mysqli_error($conn));

?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>
         Tu Abogado:
         Users :: Directorio de Abogados del Ecuador
      </title>
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
      <link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/jquery.dataTables.min.css" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <link href="css/animate.min.css" rel="stylesheet">
      <link href="css/prettyPhoto.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="bower_components/datatables.net-dt/css/jquery.dataTables.css">
		<!-- Javascripts necearios para la tabla -->
		<script src="js/jquery-3.1.1.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.dataTables.min.js"></script>
      <script>
         $(document).ready(function(){
            $('#mitabla').DataTable({
               "order": [[0, "asc"]],
               "language":{
               "lengthMenu": "Mostrar _MENU_ registros por pagina",
               "info": "Mostrando página _PAGE_ de _PAGES_",
                  "infoEmpty": "No hay registros disponibles",
                  "infoFiltered": "(filtrada de _MAX_ registros)",
                  "loadingRecords": "Cargando...",
                  "processing":     "Procesando...",
                  "search": "Buscar:",
                  "zeroRecords":    "No se encontraron registros coincidentes",
                  "paginate": {
                     "next":       "Siguiente",
                     "previous":   "Anterior"
                  },             
               },
               "bProcessing": true,
               "bServerSide": true,
               "sAjaxSource": "server_process4.php"
            });   
         });
         
      </script>

   </head>
   <body class="homepage">
<?php include "header.php"; ?>
      <!--/header-->
      <section id="blog" class="container" >
      <div class="row">
        <div class="col-sm-6 breadcrumb">
          <li><a href="miperfil.php">Mi perfil</a></li>
          <li class="active">Mis transacciones</li>
        </div>
        <div class="col-sm-6 breadcrumb" align="right">
          <li><a href="#">Tu próximo pago será de <?php echo $monto; ?>$ a efectuarse el día <?php echo $fecha; ?> </a></li>
        </div>
      </div>
      <div class="container">
         <div class="row">
            <h2 style="text-align:center;font-size:30px">MIS TRANSACCIONES</h2>
         </div>

         <div class="row">
         </div>

         <br>

         <div class="row table-responsive">
            <table class="display" id="mitabla">
               <thead>
                  <tr>
                     <th>Transacción</th>
                     <th>Concepto</th>
                     <th>Monto</th>
                     <th>Estatus</th>
                     <th>Inicio</th>
                     <th>Vence</th>
							<th>Recurrencia</th>
							<th>Eliminar Recurrencia</th>
                  </tr>
               </thead>

               <tbody>

               </tbody>
            </table>
         </div>
      </div>
      <!-- Modal -->
      <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">

               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="myModalLabel">Eliminar Recurrencia</h4>
               </div>

               <div class="modal-body">
                  ¿Desea eliminar la recurrencia de este registro?. Si Recurrencia = 0, ya es un registro sin recurrencia.
               </div>

               <div class="modal-footer">
                  <div class="barra" align="center" style="padding-top: 10px; display: none">
                     <center><img id="enviando" src="images/barra.gif" \><br>
                     <b>Por favor espere</b></center>
                  </div>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  <a class="btn btn-danger btn-ok">Delete</a>
               </div>
            </div>
         </div>
      </div>
      </section>
      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->

      <!-- Modal Zone -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">

               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="myModalLabel">Mensaje:</h4>
               </div>

               <div class="modal-body">
               </div>

               <div class="modal-footer">
                  <button type="button" class="btn btn-warning" data-dismiss="modal">Aceptar</button>
               </div>
            </div>
         </div>
      </div>


      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="js/markdown.js"></script>
      <script src="js/to-markdown.js"></script>
      <script src="js/bootstrap-markdown.js"></script>
      <script src="js/bootstrap-markdown.es.js"></script>
      <script type="text/javascript" src="js/jquery.simpleslider.js"></script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/jquery.isotope.min.js"></script>
      <script src="js/wow.min.js"></script>
      <script src="js/main.js"></script>
      <script>
         $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            
            $('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
         });
         $( ".btn-ok" ).click(function() {
            $(".barra").show();
         });
      </script>

		<!-- Launch Modal -->
      <script>
         $(document).ready(function(){
            $("#makearticulo").click(function(){
					var iduser = "<?php echo $_SESSION['id']; ?>";
					var plan = "<?php echo $_SESSION['plan']; ?>";
					var parametros = {"iduser":iduser};
					$.ajax({
						data: parametros,
						url: 'countarticulos.php',
						type: 'post',
						error: function (xhr, status, error) {
						 confirm('Algo salió mal!');
						},
						success: function (response) {
							console.log("the response is", response);
							if(responce 
							if(response >= 5 && plan < 3){
								$(".modal-body").empty();
                  		$(".modal-body").append("<p>Solo puede publicar hasta 5 artículos al año. \
														En el plan Gold puede publicar de manera ilimitada. \
														Puede adquirir el plan Gold haciendo click <a href='planes.php'>Aquí.</a></p> ");
								$("#myModal").modal('show');
							}else{
								window.location = "makearticulo.php";
							}
						}
					});
				});
         });
      </script>

   </body>
</html>



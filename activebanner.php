<?php

   session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");
   $config = require 'config.php';

   //Conexion a BD
   $conn=mysqli_connect($config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']);
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);

	//Se activa o desactiva el articulo	
	$sql = "UPDATE banners
				SET active=IF(active='1', '0', '1')
				WHERE idbanner='$_GET[idbanner]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

	header("location: adm_banners.php");

?>

<?php
   session_start();
	header('Content-Type: text/html; charset=utf-8');
	$current = "asistencias";
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
	mysqli_set_charset($conn, "utf8");

   //Id Plan
   $id = $_GET['id'];

	//Buscando el monto
	$sql = "SELECT costo, concepto FROM productos WHERE id='$id'";
	if($result = mysqli_query($conn, $sql)){
		$row = mysqli_fetch_assoc($result);
		$amount = floatval($row['costo']);
	}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
/*
	$cart = $_SESSION['cart'];
	switch($cart['installment']){
		case 0:
			$formapago = "Pago Corriente";
			break;
		case 2:
			$formapago = "Pago Diferido con intereses";
			break;
		case 3:
			$formapago = "Pago Diferido sin intereses";
			break;
	}
	$tipopago = "Pago Recurrente (cargos automáticos)";
	$periodo = ($cart['periodo'] == 1) ? "Pago Recurrente Mensual":"Pago Recurrente Anual";
*/
?>
<!DOCTYPE html>
<html>
   <head>
		<meta http-equiv="content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
		<title>Polilegal | Pago</title>
		<meta name="description" content="">
		<meta name="author" content="adolfo" >
		<link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
		<link id="default-css" href="style.css" rel="stylesheet" type="text/css">
		<link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
		<link href="css/responsive.css" rel="stylesheet" type="text/css">
		<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" /> 
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<link href='https://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' 
		rel='stylesheet' type='text/css'>
      <!-- JQuery Validator and form -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.js"></script>
      <!-- Paymentez -->
      <link href="https://cdn.paymentez.com/js/1.0.1/paymentez.min.css" rel="stylesheet" type="text/css" />
      <script src="https://cdn.paymentez.com/js/1.0.1/paymentez.min.js"></script>

		<!--jquery-->
		<script src="js/modernizr-2.6.2.min.js"></script>

      <style>
         .error{
            color: red;
         }
         .login-form input[type=text].error,
         .login-form input[type=email].error,
         .login-form input[type=password].error{
            padding:15px 18px;
            border:1px solid #FF0000;
         }
         .pricingTable{
         text-align: center;
         transition: all 0.5s ease 0s;
         }
         .pricingTable:hover{
         box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
         }
         .pricingTable .pricingTable-header{
         color: #feffff;
         }
         .pricingTable .heading{
         display: block;
         padding-top: 25px;
         }
         .pricingTable .heading > h3{
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .textotitle{
         color: #feffff;
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .pricingTable .subtitle{
         display: block;
         font-size: 13px;
         margin-top: 5px;
         text-transform: capitalize;
         }
         .pricingTable .price-value{
         display: block;
         font-size: 60px;
         font-weight: 700;
         padding-bottom: 25px;
         }
         .pricingTable .price-value span{
         display: block;
         font-size: 14px;
         line-height: 20px;
         text-transform: uppercase;
         }
         .pricingTable .pricingContent{
         /*text-transform: capitalize;*/
         background: #fbfbfb;
         color: #fefeff;
         }
         .pricingTable .pricingContent ul{
         list-style: none;
         padding: 15px 14px 10px;
         margin: 0;
         text-align: left;
         }
         .pricingTable .pricingContent ul li{
         font-size: 14px;
         padding: 12px 0;
         border-bottom: 1px dashed #e1e1e1;
         color: #9da1ad;
         }
         .pricingTable .pricingContent ul li i{
         font-size: 14px;
         float: right;
         }
         .pricingTable .pricingTable-sign-up{
         padding: 20px 0;
         background: #fbfbfb;
         color: #fff;
         text-transform: capitalize;
         }
         .pricingTable .btn-block{
         width: 60%;
         margin: 0 auto;
         font-size: 17px;
         color: #fff;
         text-transform: capitalize;
         border: none;
         border-radius: 5px;
         padding: 10px;
         transition: all 0.5s ease 0s;
         }
         .pricingTable .btn-block:before{
         content: "\f007";
         font-family: 'FontAwesome';
         margin-right: 10px;
         }
         .pricingTable.blue .pricingTable-header,
         .pricingTable.blue .btn-block{
         background: #fce400;
         }
         .pricingTable.pink .pricingTable-header,
         .pricingTable.pink .btn-block{
         background: #c9b600;
         }
         .pricingTable.orange .pricingTable-header,
         .pricingTable.orange .btn-block{
         background: #e0d83a;
         }
         .pricingTable.green .pricingTable-header,
         .pricingTable.green .btn-block{
         background: #e0cd00;
         }
         .pricingTable.blue .btn-block:hover,
         .pricingTable.pink .btn-block:hover,
         .pricingTable.orange .btn-block:hover,
         .pricingTable.green .btn-block:hover{
         background: #e6e6e6;
         color: #939393;
         }
         @media screen and (max-width: 990px){
         .pricingTable{ margin-bottom: 20px; }
         }
         .glyphicon-trash{
            cursor: pointer;
         }
      </style>
		<style type="text/css">
			/* CSS for Credit Card Payment form */
			.credit-card-box .panel-title {
				display: inline;
				font-weight: bold;
			}

			.credit-card-box .form-control.error {
				border-color: red;
				outline: 0;
				box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6);
			}

			.credit-card-box label.error {
				font-weight: bold;
				color: red;
				padding: 2px 8px;
				margin-top: 2px;
			}

			.credit-card-box .payment-errors {
				font-weight: bold;
				color: red;
				padding: 2px 8px;
				margin-top: 2px;
			}
			.credit-card-box label {
				display: block;
			}

			/* The old "center div vertically" hack */
			.credit-card-box .display-table {
				display: table;
			}

			.credit-card-box .display-tr {
				display: table-row;
			}

			.credit-card-box .display-td {
				display: table-cell;
				vertical-align: middle;
				width: 100%;
			}
			/* Just looks nicer */
			.credit-card-box .panel-heading img {
				min-width: 180px;
			}

			[type="radio"]:checked,
			[type="radio"]:not(:checked) {
				position: absolute;
				left: -9999px;
			}

			.labelbox {
				position: relative;
				padding-left: 28px;
				cursor: pointer;
				line-height: 20px;
				display: inline-block;
				color: #666;
			}
			[type="checkbox"]:checked {
				margin-right: 4px;
			}
			[type="radio"]:checked + label:before,
			[type="radio"]:not(:checked) + label:before {
				content: '';
				position: absolute;
				left: 0;
				top: 0;
				width: 18px;
				height: 18px;
				border: 1px solid #ddd;
				border-radius: 100%;
				background: #fff;
			}
			[type="radio"]:checked + label:after,
			[type="radio"]:not(:checked) + label:after {
				content: '';
				width: 12px;
				height: 12px;
				background: #00a1e4;
				position: absolute;
				top: 4px;
				left: 4px;
				border-radius: 100%;
				-webkit-transition: all 0.2s ease;
				transition: all 0.2s ease;
			}

			[type="radio"]:not(:checked) + label:after {
				opacity: 0;
				-webkit-transform: scale(0);
				transform: scale(0);
			}

			[type="radio"]:checked + label:after {
				opacity: 1;
				-webkit-transform: scale(1);
				transform: scale(1);
			}
			.elemento {
				-webkit-box-shadow: 2px 2px 5px #999;
				-moz-box-shadow: 2px 2px 5px #999;
			}
			.center {
					display: block;
					margin-left: auto;
					margin-right: auto;
					width: 25%;
			}
			.btn-primary:hover, .btn-primary:focus {
				 background: #999;
				 outline: none;
				 box-shadow: none;
			}
			#regresar {
				 font-size: 16px;
				 color: #0E8FAB;
				 font-weight: 700;
			}
			#regresar:hover {
				color: #000000;
			}
		
			@media screen and (max-width: 550px) {
				.caja {
					margin-top: 325px;
				}
			}

		</style>
		<!-- Paymentez -->
		<style>
		 .panel {
			margin: 0 auto;
			background-color: #F5F5F7;
			border: 1px solid #ddd;
			padding: 20px;
			display: block;
			width: 80%;
			border-radius: 6px;
			box-shadow: 0 2px 4px rgba(0,0,0,.1);
		 }
		 .btn {
			background: rgb(140,197,65); /* Old browsers */
			background: -moz-linear-gradient(top, rgba(140,197,65,1) 0%, rgba(20,167,81,1) 100%); /* FF3.6-15 */
			background: -webkit-linear-gradient(top, rgba(140,197,65,1) 0%,rgba(20,167,81,1) 100%); /* Chrome10-25,Safari5.1-6 */
			background: linear-gradient(to bottom, rgba(140,197,65,1) 0%,rgba(20,167,81,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#44afe7', endColorstr='#3198df',GradientType=0 );
			color: #fff;
			display: block;
			width: 100%;
			border: 1px solid rgba(46, 86, 153, 0.0980392);
			border-bottom-color: rgba(46, 86, 153, 0.4);
			border-top: 0;
			border-radius: 4px;
			font-size: 17px;
			text-shadow: rgba(46, 86, 153, 0.298039) 0px -1px 0px;
			line-height: 34px;
			-webkit-font-smoothing: antialiased;
			font-weight: bold;
			display : block;
			margin-top: 20px;
		 }

		 .btn:hover {
			cursor: pointer;
		 }
		</style>
   </head>
   <body class="homepage">
<?php include "header.php"; ?>
      <!--/header-->
					<div class="row">
						<div class="col-md-6 col-md-offset-3 caja">
							<!-- CREDIT CARD FORM STARTS HERE -->
							<div class="elemento panel panel-default credit-card-box"
								style="margin-top: 40px; margin-bottom: 60px;">
								<div class="panel-heading display-table" style="
									padding-top: 10px;
									padding-bottom: 10px;
									background: #ddd;
									">
									<div class="row display-tr">
										<h6 class="panel-title display-td"><?php echo "Monto: $".$amount; ?></h6>
										<div class="display-td">
											<img class="img-responsive pull-right"  src="images/tarjetas.png">
										</div>
									</div>
								</div>
								<div class="panel-body" style="
									padding-top: 15px;
									padding-bottom: 15px;
									padding-left: 30px;
									padding-right: 30px;
									background-color: white;
									">
									<div class=" col-md-12">
										<label style="font-size: 16px; color: #0E8FAB;" class="text-center"><b>Agregar Tarjeta</b></label>
										<br>
									</div>
									<form id="add-card-form">
										<div class="paymentez-form" id="my-card" data-capture-name="true"></div>
										<button class="btn" style="margin-bottom: 0px;">Agregar</button>
										<div align="center" id="messages"></div>
									</form>
									<form id="frmdebitcard" name="frmdebitcard" method="POST" action="debitcard.php">
										<input type="hidden" name='formname' value="frmdebitcard">
										<input type="hidden" name='plan' value="<?php echo $id; ?>">
										<div class="body-form" style="margin-top: 30px;">
											<?php
												include "getcards.php";
												for ($i = 0; $i < $data['result_size']; $i++){
											?>
											<div class='row <?php echo $data['cards'][$i]['token']; ?>' style='margin-left: 0px; '> 
												<div class='col-md-12'> 
													<div class='form-group'>
														<input type='radio' id='<?php echo $data['cards'][$i]['token']; ?>' 
															name='option_select' value='<?php echo $data['cards'][$i]['token']; ?>' checked>
														<label style='font-size: 16px;margin-left: 10px;'
															for='<?php echo $data['cards'][$i]['token']; ?>'>
															<?php echo substr($data['cards'][$i]['bin'],0,4).'-'.substr($data['cards'][$i]['bin'],4,2).'XX-XXXX-XXXX'; ?>
															<img src='img/<?php echo $data['cards'][$i]['type'].'.jpg'; ?>' style='height: 15px;margin-left: 5px;'>
															<a href'#'><span id='<?php echo $data['cards'][$i]['token']; ?>' class='glyphicon glyphicon-trash deltdc'></span></a>
															<img id='img<?php echo $data['cards'][$i]['token']; ?>' src='img/espera.gif' style='display: none;'>
														</label>
													</div>
												</div>
											</div>
											<?php } ?>
										</div>
										<button type="submit" name="submit" class="btn" style="margin-top: 0px;">Pagar</button> 
									</form>
									<div class="row" align="center" style="margin-left: 0px; margin-top: 15px;">
											<a href="planes-de-asistencia.php"><span id="regresar">Regresar</span></a>
									</div>	
									<div class="row" align="center" style="margin-left: 0px;">
										<div id="barra" align="center" style="display: block; width:50%; padding-top: 20px; display: none">
											<center><img id="enviando" src="images/barra.gif" \><br>
											<b>Por favor espere</b></center>
										</div>
									</div>
									<!-- Zona de mensajes -->
									<div id="mensajes"></div>
									<!-- FIN Zona de mensajes -->
								</div>
							</div>
						</div>
						<div class="c4">
						</div>
					</div>
				</div><!-- end grid -->
      <!-- footer -->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery-migrate.min.js"></script>
		<script type="text/javascript" src="js/jquery.validate.min.js"></script>
		<script type="text/javascript" src="js/jquery-easing-1.3.js"></script>
		<script type="text/javascript" src="js/jquery.sticky.js"></script>
		<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>

		<script type="text/javascript" src="js/jquery.tabs.min.js"></script>
		<script type="text/javascript" src="js/jquery.smartresize.js"></script>
		<script type="text/javascript" src="js/shortcodes.js"></script>

		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDz1RGAW3gGyDtvmBfnH2_fE2DVVNWq4Eo&callback=initMap" type="text/javascript"></script>
		<script src="js/gmap3.min.js"></script>

      <script src="js/custom.js"></script>
      <script src="https://cdn.paymentez.com/checkout/1.0.1/paymentez-checkout.min.js"></script>

		<script>
		$(function() {

		  //
		  // EXAMPLE CODE FOR PAYMENTEZ INTEGRATION
		  // ---------------------------------------------------------------------------
		  //
		  //  1.) You need to import the Paymentez JS -> https://cdn.paymentez.com/js/v1.0.1/paymentez.min.js
		  //
		  //  2.) You need to import the Paymentez CSS -> https://cdn.paymentez.com/js/v1.0.1/paymentez.min.css
		  //
		  //  3.) Add The Paymentez Form
		  //  <div class="paymentez-form" id="my-card" data-capture-name="true"></div>
		  //
		  //  3.) Init library
		  //  Replace "PAYMENTEZ_CLIENT_APP_CODE" and "PAYMENTEZ_CLIENT_APP_KEY" with your own Paymentez Client Credentials.
		  //
		  // 4.) Add Card: converts sensitive card data to a single-use token which you can safely pass to your server to charge the user.

		  /**
			* Init library
			*
			* @param env_mode `prod`, `stg`, `local` to change environment. Default is `stg`
			* @param paymentez_client_app_code provided by Paymentez.
			* @param paymentez_client_app_key provided by Paymentez.
			*/
		  Paymentez.init('<?php echo $config['paymentez']['env_mode']; ?>', '<?php echo $config['paymentez']['client_app_code']; ?>', '<?php echo $config['paymentez']['client_app_key']; ?>');
		  
		  var form              = $("#add-card-form");
		  var submitButton            = form.find("button");
		  var submitInitialText = submitButton.text();

		  $("#add-card-form").submit(function(e){
			 var myCard = $('#my-card');
			 $('#messages').text("");
			 var cardToSave = myCard.PaymentezForm('card');
			 if(cardToSave == null){
				$('#messages').text("Tarjeta Invalida");
			 }else{
				submitButton.attr("disabled", "disabled").text("Procesando TDC...");    
				
				/*
				After passing all the validations cardToSave should have the following structure:

				 var cardToSave = {
										  "card": {
											 "number": "5119159076977991",
											 "holder_name": "Martin Mucito",
											 "expiry_month": 9,
											 "expiry_year": 2020,
											 "cvc": "123",
											 "type": "vi"
										  }
										};

				*/

				
				var uid = "<?php echo $_SESSION['id']; ?>";
				var email = "osalas@paymentez.com";
				//var email = "<?php echo $_SESSION['mail']; ?>";
				var cardNumber = myCard.PaymentezForm('cardNumber');
				var cardType = myCard.PaymentezForm('cardType');
				console.log(cardToSave);

				/* Add Card converts sensitive card data to a single-use token which you can safely pass to your server to charge the user.
				 *
				 * @param uid User identifier. This is the identifier you use inside your application; you will receive it in notifications.
				 * @param email Email of the user initiating the purchase. Format: Valid e-mail format.
				 * @param card the Card used to create this payment token
				 * @param success_callback a callback to receive the token
				 * @param failure_callback a callback to receive an error
				 */
				Paymentez.addCard(uid, email, cardToSave, successHandler, errorHandler);

			 }
			 
			 e.preventDefault();
		  });  

		  var successHandler = function(cardResponse) {
			 console.log(cardResponse.card);
			 document.getElementById('add-card-form').reset();
			 var card1 = cardResponse.card.bin.substring(0, 4); 
			 var card2 = cardResponse.card.bin.substring(4, 6);
			 if(cardResponse.card.type === 'vi'){
				var cardtype = "vi.jpg";
			 }else if(cardResponse.card.type === 'mc'){
				var cardtype = "mc.jpg";
			 }else if(cardResponse.card.type === 'dc'){
				var cardtype = "dc.jpg";
			 }else if(cardResponse.card.type === 'ae'){
				var cardtype = "ae.jpg";
			 }
			 if(cardResponse.card.status === 'valid'){
				$('.body-form').append("\
                              <div class='row "+cardResponse.card.token+"' style='margin-left: 0px; '> \
                                 <div class='col-md-12'> \
                                    <div class='form-group'> \
                                       <input type='radio' id='"+cardResponse.card.token+"' name='option_select' value='"+cardResponse.card.token+"' checked> \
                                       <label style='font-size: 16px;margin-left: 10px;' \
														for='"+cardResponse.card.token+"'>"+card1+"-"+card2+"XX-XXXX-XXXX"+"\
														<img src='img/"+cardtype+"' style='height: 15px;margin-left: 5px;'>\
														<a href'#'><span id='"+cardResponse.card.token+"' class='glyphicon glyphicon-trash deltdc'></span></a>\
														<img id='img"+cardResponse.card.token+"' src='img/espera.gif' style='display: none;'>\
													</label> \
                                    </div>\
                                 </div>\
                              </div>\
										");
			 }else{
			 }

			 submitButton.removeAttr("disabled");
			 submitButton.text(submitInitialText);
		  };

		  var errorHandler = function(err) {    
			 console.log(err.error);
			 $('#messages').html(err.error.type);    
			 submitButton.removeAttr("disabled");
			 submitButton.text(submitInitialText);
		  };

		});
</script>

   </body>
</html>


<?php 
	header('Content-Type: text/html; charset=utf-8');
	session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");
   if(!isset($_SESSION['admin'])) header("location: index.php");
   $include = ($_SESSION['admin']) ? "adm_header.php":"header.php";
   $urllevel1 = ($_SESSION['admin']) ? "admin.php":"miperfil.php";
   $titulolevel1 =  ($_SESSION['admin']) ? "Abogados":"Mi Perfil";
   $titulo =  ($_SESSION['admin']) ? "Recomendacones":"Mis Recomendacones";
?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>
         Tu Abogado:
         Users :: Directorio de Abogados del Ecuador
      </title>
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
      <link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/jquery.dataTables.min.css" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <link href="css/animate.min.css" rel="stylesheet">
      <link href="css/prettyPhoto.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="bower_components/datatables.net-dt/css/jquery.dataTables.css">
		<!-- Javascripts necearios para la tabla -->
		<script src="js/jquery-3.1.1.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.dataTables.min.js"></script>
      <script>
         $(document).ready(function(){
            $('#mitabla').DataTable({
               "order": [[0, "asc"]],
               "language":{
               "lengthMenu": "Mostrar _MENU_ registros por pagina",
               "info": "Mostrando página _PAGE_ de _PAGES_",
                  "infoEmpty": "No hay registros disponibles",
                  "infoFiltered": "(filtrada de _MAX_ registros)",
                  "loadingRecords": "Cargando...",
                  "processing":     "Procesando...",
                  "search": "Buscar:",
                  "zeroRecords":    "No se encontraron registros coincidentes",
                  "paginate": {
                     "next":       "Siguiente",
                     "previous":   "Anterior"
                  },             
               },
               "bProcessing": true,
               "bServerSide": true,
               "sAjaxSource": "server_process3.php"
            });   
         });
         
      </script>

   </head>
   <body class="homepage">
<?php include $include; ?>
      <!--/header-->
      <section id="blog" class="container" >
		<ol class="breadcrumb">
         <li><a href="<?php echo $urllevel1; ?>"><?php echo $titulolevel1; ?></a></li>
         <li class="active"><?php echo $titulo; ?></li>
		</ol>
      <div class="container">
         <div class="row">
            <h2 style="text-align:center;font-size:30px"><?php echo mb_strtoupper($titulo); ?></h2>
         </div>

         <br>

         <div class="row table-responsive">
            <table class="display" id="mitabla">
               <thead>
                  <tr>
							<?php if($_SESSION['admin']){ ?><th>ID_Abogado</th><?php } ?>
                     <th>Id</th>
                     <th>Puntaje</th>
                     <th>Usuario</th>
                     <th>Correo</th>
                     <th>Recomendación</th>
                     <th>Visible?</th>
                     <th>Leído?</th>
                     <th>Fecha</th>
                     <th>Visualizar</th>
                     <th>Eliminar</th>
                  </tr>
               </thead>

               <tbody>

               </tbody>
            </table>
         </div>
      </div>
      <!-- Modal -->
      <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">

               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="myModalLabel">Eliminar Recomendación</h4>
               </div>

               <div class="modal-body">
                  ¿Desea eliminar esta recomendación?
               </div>

               <div class="modal-footer">
                  <div class="barra" align="center" style="padding-top: 10px; display: none">
                     <center><img id="enviando" src="images/barra.gif" \><br>
                     <b>Por favor espere</b></center>
                  </div>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  <a class="btn btn-danger btn-ok">Delete</a>
               </div>
            </div>
         </div>
      </div>

      </section>
      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->

      <!-- Modal Zone -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">

               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="myModalLabel">Mensaje:</h4>
               </div>

               <div class="modal-body">
               </div>

               <div class="modal-footer">
                  <button type="button" class="btn btn-warning" data-dismiss="modal">Aceptar</button>
               </div>
            </div>
         </div>
      </div>

      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="js/markdown.js"></script>
      <script src="js/to-markdown.js"></script>
      <script src="js/bootstrap-markdown.js"></script>
      <script src="js/bootstrap-markdown.es.js"></script>
      <script type="text/javascript" src="js/jquery.simpleslider.js"></script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/jquery.isotope.min.js"></script>
      <script src="js/wow.min.js"></script>
      <script src="js/main.js"></script>
      <script>
         $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            
            $('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
         });
         $( ".btn-ok" ).click(function() {
            $(".barra").show();
         });
      </script>

      <!-- Launch Modal -->
      <script>
         $(document).ready(function(){
            $("#makecupon").click(function(){
               var iduser = "<?php echo $_SESSION['id']; ?>";
               var plan = "<?php echo $_SESSION['plan']; ?>";
               var parametros = {"iduser":iduser};
               $.ajax({
                  data: parametros,
                  url: 'countcupones.php',
                  type: 'post',
                  error: function (xhr, status, error) {
                   confirm('Algo salió mal!');
                  },
                  success: function (response) {
                     console.log("the response is", response);
                     if(response >= 6 && plan < 3){
								$(".modal-body").empty();
                        $(".modal-body").append("<p>Solo puede publicar hasta 6 cupones al año. \
                                          En el plan Gold puede publicar de manera ilimitada. \
                                          Puede adquirir el plan Gold haciendo click <a href='planes.php'>Aquí.</a></p> ");
                        $("#myModal").modal('show');
                     }else{
                        window.location = "makecupon.php";
                     }
                  }
               });
            });
         });
      </script>

   </body>
</html>



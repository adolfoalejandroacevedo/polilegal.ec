<section id="primary" class="content-full-width">
 <!--container starts-->

	<p class="plan1-text" style="text-align: justify;"><span class="dt-sc-highlightt skin-color"> <strong style="color: rgb(43, 155, 15);"> ¡PROTEGER TU PROFESIÓN ES MUY IMPORTANTE!</strong></span><img src="" style="position:absolute; margin-left: 24%; margin-top: -4%; width: 325px;"><br><br> A través de la alianza entre la Cooperativa Policía Nacional y PoliLegal Abogados, te damos la bienvenida a nuestro PLAN DE ASISTENCIA JURÍDICA PARA SOCIOS POLICÍAS.</p>

   <section id="secundary" class="content-full-width margintop1">
      <div class="dt-sc-hr-invisible-small"></div>

	<!--dt-sc-toggle-frame-set starts -->
	<div class="dt-sc-toggle-frame-set">
      <div class="dt-sc-toggle-frame">
         <h5 class="dt-sc-toggle-accordion active"><a href="#"> Elaboración De </a></h5>
         <div class="dt-sc-toggle-content">
            <div class="block" style="text-align: left;">
               <p class="plan1-text">• Requerimientos judiciales. </p>
               <p class="plan1-text">• Peticiones ante todos los organismos policiales.</p>
               <p class="plan1-text">• Procuraciones judiciales. </p>
               <p class="plan1-text">• Permisos de salida del país de menores. </p>
               <p class="plan1-text">• Poderes generales y especiales. </p>
               <p class="plan1-text">• Reclamos en el proceso de calificación de ascensos o condecoraciones.</p>
            </div>
         </div>
      </div>
      <div class="dt-sc-toggle-frame">
         <h5 class="dt-sc-toggle-accordion"><a href="#"> Asesoría Integral En </a></h5>
         <div class="dt-sc-toggle-content">
            <div class="block" style="text-align: left;">
               <p class="plan1-text">• Cualquier asunto legal. </p>
				</div>
			</div>
		</div>
      <div class="dt-sc-toggle-frame">
         <h5 class="dt-sc-toggle-accordion"><a href="#"> Descuentos </a></h5>
         <div class="dt-sc-toggle-content">
            <div class="block" style="text-align: left;">
               <p class="plan1-text">• 50% de descuento en todos los servicios no incluidos en la cobertura. </p>
            </div>
			</div>
		</div>
      <div class="dt-sc-toggle-frame">
         <h5 class="dt-sc-toggle-accordion"><a href="#"> Consultas </a></h5>
         <div class="dt-sc-toggle-content">
            <div class="block" style="text-align: left;">
               <p class="plan1-text">• De forma personal en nuestras oficinas. </p>
               <p class="plan1-text">• A través de la página web, correo electrónico y redes sociales. </p>
               <p class="plan1-text">• A través de llamadas telefónicas a nuestro Contact Center. </p>
            </div>
			</div>
		</div>
      <div class="dt-sc-toggle-frame">
         <h5 class="dt-sc-toggle-accordion"><a href="#"> Beneficios </a></h5>
         <div class="dt-sc-toggle-content">
            <div class="block" style="text-align: left;">
               <p class="plan1-text">• 100% de cobertura. </p>
               <p class="plan1-text">• Atención ilimitada. </p>
               <p class="plan1-text">• Abogados especializados. </p>
               <p class="plan1-text">• Cobertura a nivel nacional. </p>
               <p class="plan1-text">Para el uso de los servicios, el beneficiario deberá comunicarse a nuestro<br>
               Contact Center (02)3 825530 / (02)3 333533, correo electrónico, página web<br> o redes sociales,
               con el fin de gestionar sus solicitudes y asignarle a un Abogado.</p>
            </div>
			</div>
		</div>
      <div class="dt-sc-toggle-frame">
         <h5 class="dt-sc-toggle-accordion"><a href="#"> Cobertura </a></h5>
         <div class="dt-sc-toggle-content">
            <div class="block" style="text-align: left;">
               <p><span class="dt-sc-highlightt skin-color"> <strong style="color: rgb(43, 155, 15);">
               Patrocinio completo en procedimientos COESCOP:</strong></span></p>
               <p class="plan1-text">• Procedimiento por faltas leves. </p>
               <p class="plan1-text">• Sumarios administrativos (primera fase). </p>
               <p class="plan1-text">• Ascensos y condecoraciones. </p>
               <p class="plan1-text">• Cupos de Eliminación Anual. </p>
               <p class="plan1-text">• Rehabilitación de faltas disciplinarias. </p>
               <p class="plan1-text">• Reincorporaciones. </p>
               <p class="plan1-text">• Versiones (en Asuntos Internos y Fiscalía). </p>
               <p class="plan1-text">• Reclamos administrativos internos. </p>
               <p><span class="dt-sc-highlightt skin-color"> <strong style="color: rgb(43, 155, 15);">
               Asesoría integral en:</strong></span></p>
               <p class="plan1-text">• Procedimientos contencioso-administrativos. </p>
               <p class="plan1-text">• Procedimientos constitucionales. </p>
               <p class="plan1-text">• Asuntos civiles y de familia. </p>
               <p class="plan1-text">• Asuntos penales y de tránsito. </p>
               <p class="plan1-text">• Asesoría en cualquier otro asunto legal. </p>
               <p><span class="dt-sc-highlightt skin-color"> <strong style="color: rgb(43, 155, 15);">
               Elaboración de:</strong></span></p>
               <p class="plan1-text">• Peticiones ante todos los organismos policiales. </p>
               <p class="plan1-text">• Procuraciones judiciales. </p>
               <p class="plan1-text">• Permisos de salida del país de menores. </p>
               <p class="plan1-text">• Poderes generales y especiales. </p>
               <p><span class="dt-sc-highlightt skin-color">
               <strong style="color: rgb(43, 155, 15);">
               Cobertura de consultas:</strong></span></p>
               <p class="plan1-text">• Consultas personales ilimitadas en nuestras oficinas. </p>
               <p class="plan1-text">• Consultas a través de la página web y correo electrónico. </p>
               <p class="plan1-text">• Consultas a través de llamadas telefónicas a nuestro CONTACT CENTER. </p>
            </div>
			</div>
		</div>
      <div class="dt-sc-toggle-frame">
         <h5 class="dt-sc-toggle-accordion"><a href="#"> Costo y Forma de Pago </a></h5>
         <div class="dt-sc-toggle-content">
            <div class="block" style="text-align: left;">
               <p class="plan1-text">• $ 6,99 USD Mensual (tarifa incluye impuestos). </p>
               <p class="plan1-text">El socio puede ingresar a cualquiera de nuestros Planes mediante: </p>
               <p class="plan1-text">• Débito mensual de su cuenta de ahorros de la CPN. </p>
               <p class="plan1-text">• Débito mensual de su cuenta en otra institución financiera. </p>
            </div>
			</div>
		</div>
		<div class="dt-sc-toggle-frame">
			<h5 class="dt-sc-toggle-accordion"><a href="#"> Defensa En </a></h5>
			<div id="collapse1" class="dt-sc-toggle-content">
				<div class="block" style="text-align: left;">
               <p class="plan1-text">• Acciones previas. </p>
               <p class="plan1-text">• Procedimientos por faltas leves. </p>
               <p class="plan1-text">• Sumarios administrativos (primera fase). </p>
               <p class="plan1-text">• Cupos de Eliminación Anual. </p>
               <p class="plan1-text">• Rehabilitación de faltas disciplinarias. </p>
               <p class="plan1-text">• Versiones (Asuntos Internos y Fiscalía). </p>
				</div>
			</div>
		</div>
	</div>


<!--primary starts
	<section id="secundary" class="content-full-width margintop1">
		<div class="dt-sc-hr-invisible-small"></div>
		 dt-sc-three-fourth starts-->
		<!--
		<div class="dt-sc-three-uno column first">
		-->
			<!--dt-sc-tabs-container starts-->      
<!--      
			<div class="dt-sc-tabs-container">
				<ul class="dt-sc-tabs-frame">
			 		<li><a id="defensatitle" href="#defensa"> Defensa En </a></li>
			 		<li><a href="#"> Elaboración De </a></li>
			 		<li><a href="#"> Asesoría Integral En </a></li>
			 		<li><a href="#"> Descuentos </a></li>
			 		<li><a href="#"> Consultas </a></li>
					<li><a href="#"> Beneficios </a></li>
				 	<li><a href="#"> Cobertura </a></li>
			 		<li><a href="#costo"> Costo y Forma de Pago </a></li>
			  	</ul>
				<div id="defensacontent" class="dt-sc-tabs-frame-content">
			 		<p class="plan1-text">• Acciones previas. </p>
			 		<p class="plan1-text">• Procedimientos por faltas leves. </p>
			 		<p class="plan1-text">• Sumarios administrativos (primera fase). </p>
		 			<p class="plan1-text">• Cupos de Eliminación Anual. </p>
		 			<p class="plan1-text">• Rehabilitación de faltas disciplinarias. </p>
		 			<p class="plan1-text">• Versiones (Asuntos Internos y Fiscalía). </p>
		  		</div>
				<div class="dt-sc-tabs-frame-content">
			 		<p class="plan1-text">• Requerimientos judiciales. </p>
			 		<p class="plan1-text">• Peticiones ante todos los organismos policiales.</p>
			 		<p class="plan1-text">• Procuraciones judiciales. </p>
			 		<p class="plan1-text">• Permisos de salida del país de menores. </p>
			 		<p class="plan1-text">• Poderes generales y especiales. </p>
			 		<p class="plan1-text">• Reclamos en el proceso de calificación de ascensos o condecoraciones.</p>
		  		</div>
				<div class="dt-sc-tabs-frame-content">
			 		<p class="plan1-text">• Cualquier asunto legal. </p>
				</div>
				<div class="dt-sc-tabs-frame-content">
			 		<p class="plan1-text">• 50% de descuento en todos los servicios no incluidos en la cobertura. </p>
				</div>
				<div class="dt-sc-tabs-frame-content">
			 		<p class="plan1-text">• De forma personal en nuestras oficinas. </p>
			 		<p class="plan1-text">• A través de la página web, correo electrónico y redes sociales. </p>
			 		<p class="plan1-text">• A través de llamadas telefónicas a nuestro Contact Center. </p>
				</div>
			 	<div class="dt-sc-tabs-frame-content">
			 		<p class="plan1-text">• 100% de cobertura. </p>
			 		<p class="plan1-text">• Atención ilimitada. </p>
			 		<p class="plan1-text">• Abogados especializados. </p>
		 			<p class="plan1-text">• Cobertura a nivel nacional. </p>
		 			<p class="plan1-text">Para el uso de los servicios, el beneficiario deberá comunicarse a nuestro<br>
					Contact Center (02)3 825530 / (02)3 333533, correo electrónico, página web<br> o redes sociales, 
					con el fin de gestionar sus solicitudes y asignarle a un Abogado.</p>
		  		</div>
				<div class="dt-sc-tabs-frame-content">
					<p><span class="dt-sc-highlightt skin-color"> <strong style="color: rgb(43, 155, 15);">
					Patrocinio completo en procedimientos COESCOP:</strong></span></p>
			 		<p class="plan1-text">• Procedimiento por faltas leves. </p>
			 		<p class="plan1-text">• Sumarios administrativos (primera fase). </p>
					<p class="plan1-text">• Ascensos y condecoraciones. </p>
					<p class="plan1-text">• Cupos de Eliminación Anual. </p>
					<p class="plan1-text">• Rehabilitación de faltas disciplinarias. </p>
					<p class="plan1-text">• Reincorporaciones. </p>
					<p class="plan1-text">• Versiones (en Asuntos Internos y Fiscalía). </p>
					<p class="plan1-text">• Reclamos administrativos internos. </p>
					<p><span class="dt-sc-highlightt skin-color"> <strong style="color: rgb(43, 155, 15);">
					Asesoría integral en:</strong></span></p>
					<p class="plan1-text">• Procedimientos contencioso-administrativos. </p>
			 		<p class="plan1-text">• Procedimientos constitucionales. </p>
					<p class="plan1-text">• Asuntos civiles y de familia. </p>
					<p class="plan1-text">• Asuntos penales y de tránsito. </p>
					<p class="plan1-text">• Asesoría en cualquier otro asunto legal. </p>
					<p><span class="dt-sc-highlightt skin-color"> <strong style="color: rgb(43, 155, 15);">
					Elaboración de:</strong></span></p>
					<p class="plan1-text">• Peticiones ante todos los organismos policiales. </p>
			 		<p class="plan1-text">• Procuraciones judiciales. </p>
			 		<p class="plan1-text">• Permisos de salida del país de menores. </p>
					<p class="plan1-text">• Poderes generales y especiales. </p>
					<p><span class="dt-sc-highlightt skin-color">
					<strong style="color: rgb(43, 155, 15);">
					Cobertura de consultas:</strong></span></p>
					<p class="plan1-text">• Consultas personales ilimitadas en nuestras oficinas. </p>
					<p class="plan1-text">• Consultas a través de la página web y correo electrónico. </p>
					<p class="plan1-text">• Consultas a través de llamadas telefónicas a nuestro CONTACT CENTER. </p>
				</div>
				<div id="costo" class="dt-sc-tabs-frame-content">
			 		<p class="plan1-text">• $ 6,99 USD Mensual (tarifa incluye impuestos). </p>
					<p class="plan1-text">El socio puede ingresar a cualquiera de nuestros Planes mediante: </p>
					<p class="plan1-text">• Débito mensual de su cuenta de ahorros de la CPN. </p>
					<p class="plan1-text">• Débito mensual de su cuenta en otra institución financiera. </p>
		  		</div>
			</div>
	 	</div>
-->
	 	<!--dt-sc-tabs-container ends-->
  		<!--dt-sc-three-fourth ends-->
  		<!--dt-sc-one-fourth starts-->
  		<!--dt-sc-one-fourth ends-->
		<div class="dt-sc-clear"></div>
		<div class="dt-sc-hr-invisible"></div>
	</section>

<?php
session_start();
$_SESSION['mail'] = $_POST['email'];
header('Content-Type: text/html; charset=utf-8');
$config = require 'config.php';
$conn=mysqli_connect(
		$config['database']['server'],
		$config['database']['username'],
		$config['database']['password'],
		$config['database']['db']
);
if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
mysqli_set_charset($conn, "utf8");

//Buscamos el nombre del plan
$sql = 	"SELECT concepto
			FROM productos
			WHERE id='$_POST[plan]'";
if($result = mysqli_query($conn, $sql)){
   while($row = mysqli_fetch_assoc($result)) $plan = $row['concepto'];
}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

//Buscamos el id de la ciudad
$sql = "SELECT id FROM ciudades WHERE ciudad='$_POST[ciudad]'";
if($result = mysqli_query($conn, $sql)){
   while($row = mysqli_fetch_assoc($result)) $ciudad = $row['id'];
}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

//Buscamos medio_pago
$sql = 	"SELECT medio
			FROM medios_pagos
			WHERE id='$_POST[medio_pago]'";
if($result = mysqli_query($conn, $sql)){
   while($row = mysqli_fetch_assoc($result)) $medio_pago = $row['medio'];
}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

$nombres = mysqli_real_escape_string($conn,utf8_decode($_POST['nombres']));
$sector = mysqli_real_escape_string($conn,utf8_decode($_POST['sector']));

//Construimos el query de afiliacion
$sql = "REPLACE INTO afiliacion
			(
				email,
				nombres,
				cedula,
				ciudad,
				sector,
				celular,
				plan,
				medio_pago
			)
			VALUES (
				'$_POST[email]',
				'$nombres',
				$_POST[cedula],
				$ciudad,
				'$sector',
				'$_POST[celular]',
				$_POST[plan],
				$_POST[medio_pago]
			)";

//Guardando el formulrio
if(mysqli_query($conn, $sql)) {
	$_SESSION['id'] = $_POST['email'];
}else{
	error_log("Error: " . $sql . "..." . mysqli_error($conn));
	echo "Error en BD";
	exit;
}

//Devolvemos respuesta
if($_POST['medio_pago'] == 1){

	//Enviando correo al cliente
	$nombres = $_POST['nombres'];
	$cedula = $_POST['cedula'];
	$celular = $_POST['celular'];
	$email = $_POST['email'];	
	$ciudad = $_POST['ciudad'];
	$sector = $_POST['sector'];
	if ($_POST['plan'] == 3) $guia = "PLAN_POLILEGAL_PERSONAL_julio2019.pdf";
	if ($_POST['plan'] == 4) $guia = "PLAN_POLILEGAL_POLICIAL_SERVICIO_ACTIVO_julio2019.pdf";
	if ($_POST['plan'] == 5) $guia = "PLAN_POLILEGAL_POLICIAL_SERVICIO_PASIVO_Y_FAMILIARES_julio2019.pdf";
	if ($_POST['plan'] == 6) $guia = "PLAN_POLILEGAL_POLICIAL_CADETES_julio2019.pdf";
	if ($_POST['plan'] == 7) $guia = "PLAN_POLILEGAL_EMPRESARIAL_julio2019.pdf";
	error_log("Plan = ".$_POST['plan']."| Guia = ".$guia);

	require 'PHPMailer/PHPMailerAutoload.php';
	include("email.php");
	include("email2.php");
	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->CharSet = 'UTF-8';
	$mail->SMTPDebug = 0;
	//$mail->Debugoutput = 'html';
	//$mail->Host = 'smtp.polilegal.ec';
	$mail->Host = $config['general']['mailserver'];
	//$mail->Host = 'localhost';
	$mail->Port = 465;
	$mail->SMTPSecure = 'ssl';
	$mail->SMTPAuth = true;
	$mail->SMTPOptions = array(
	  'ssl' => array(
		 'verify_peer' => false,
		 'verify_peer_name' => false,
		 'allow_self_signed' => true
	  )
	);
	$mail->Username = $config['general']['username'];
	$mail->Password = $config['general']['password'];
	$mail->setFrom('admin@polilegal.ec', 'Polilegal S.A.');
	$mail->addAddress($_POST['email'], '');
	$mail->addReplyTo('noreplyto@polilegal.ec', 'POLILEGAL S.A.');
	$mail->Subject = 'Plan de Asistencia Jurídica de POLILEGAL S.A.';
	$mail->msgHTML($cuerpo);
	//$mail->AddAttachment('Guia_del_usuario_POLILEGAL_2019.pdf');
	if (!$mail->send()) {
		$error = "Mailer Error: " . $mail->ErrorInfo;
		error_log($error);
		 echo "Error al enviar correo";
     exit;
	}

	//Enviando correo a Polilegal
	$mail->ClearAddresses();
	$mail->addAddress("info@polilegal.ec", '');
	$mail->msgHTML($cuerpo2);
	if (!$mail->send()) {
		$error = "Mailer Error: " . $mail->ErrorInfo;
		error_log($error);
		 echo "Error al enviar correo";
	}
	
	echo "1";

}else{
	echo "2";
}

?>

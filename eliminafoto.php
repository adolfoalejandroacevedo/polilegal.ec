<?php

session_start(); 
if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");
$config = require 'config.php';
$info["OK"] = "1";
$info["html"] = "";

//Conectamos con BD
$conn = mysqli_connect($config['database']['server'],$config['database']['username'],
		$config['database']['password'],$config['database']['db']);

//Borramos la imagen de BD
$sql = "DELETE FROM galeria WHERE id='$_POST[code]' AND iduser='$_SESSION[id]'";
if (!mysqli_query($conn, $sql)) {
	error_log("Error: " . $sql . "..." . mysqli_error($conn));
	$info["OK"] = "0";
}

//Seleccionamos las imagenes que quedaron para ser mostradas
$sql = "SELECT * FROM galeria WHERE iduser='$_SESSION[id]'";
if ($result = mysqli_query($conn, $sql)){
	while ($row = mysqli_fetch_assoc($result)) {
		$info["html"] .= "
                    <div class='portfolio-item apps col-xs-12 col-sm-4 col-md-3' id='$row[id]' >
                        <div class='recent-work-wrap' >
                            <img class='img-responsive' src='uploads/$row[img]' alt=''>
                            <div class='overlay'>
                                <div class='recent-work-inner'>
                                    <div class='col-md-6' align='left'>
                                       <a class='preview' href='uploads/$row[img]' rel='prettyPhoto'><i class='fa fa-eye'></i> Ampliar</a>
                                    </div>
                                    <div class='col-md-6' align='right'>
                                       <a class='fotos' href='uploads/$row[img]' 
                                       data-idfoto='$row[id]' ><i class='fa fa-trash-o'></i> Eliminar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
						  ";
	}
}else{
	error_log("Error: " . $sql . "..." . mysqli_error($conn));
}

//Respuesta
if (isset($_GET['callback'])) {
    echo $_GET['callback'] . '( ' . json_encode($info) . ' )';
}else {
    echo 'callbackEjercicio( ' . json_encode($info) . ' )';
}

?>

#!/bin/sh
for filename in *.php; do

	#Se busca el string dentro del archivo
	grep 'session_star' $filename > /dev/null

	#Se guarda el valor de la salida anterior
	valor=$(echo $?)

	#Se valor es 0 eiste, en caso contrario se agrega
	if [ "$valor" -gt 0 ] 
	then
		echo "No existe en $filename"
		sed -i '1s/^/<?php session_start(); ?>\n/' $filename
	fi

	#Inserta una nueva linea despues de la concidencia
	#sed -i '/<body class="homepage">/a<?php include "header.php"; ?>' $filename
done

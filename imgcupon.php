<?php
	session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");
   $info = array();
	$info['pass'] = true;

	//Conexion a BD
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );

   //Se verifica el tipo de archivo
   $arr_file_types = ['image/png', 'image/gif', 'image/jpg', 'image/jpeg'];

	if (!(in_array($_FILES['file']['type'], $arr_file_types))) {
       $info['msg'] = "badextension";
       $info['pass'] = false;
	}

   //Se valida el tamano del archivo
   if ($_FILES["file"]["size"] > 500000) {
      $info['msg'] = "badsize";
      $info['pass'] = false;
   }

   //Si pasa los filtros se continua.
   if($info['pass']){

		if (!file_exists('uploads')) {
			 mkdir('uploads', 0777);
		}

		$file = time() . $_FILES['file']['name'];
		move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/' . $file);

		//Actualizamos la foto del articulo 
		$info['file'] = htmlentities($file);
		$_SESSION['file'] = htmlentities($file);
		//$file = mysqli_real_escape_string($conn, $file);

		/* Anteriormente se guardaba o actualizaba
		$file = htmlentities($file);
		if ($_POST['idcupon'] != NULL){
			$sql = "UPDATE cupones SET img='$file' WHERE id='$_POST[idcupon]'";
			if(mysqli_query($conn, $sql)) {
				$info['id'] = $_POST['idcupon'];
			}else{
				error_log("Error: " . $sql . "..." . mysqli_error($conn));
				$info['error'] =  "Error en sql de BD";
			}
		}else{
			$sql = "INSERT INTO cupones (iduser,img) VALUES ('$_SESSION[id]','$file')";
			if(mysqli_query($conn, $sql)) {
				$info['id'] = mysqli_insert_id($conn);	
			}else{
				error_log("Error: " . $sql . "..." . mysqli_error($conn));
				$info['error'] =  "Error en sql de BD";
			}
		}
		*/
		$info['msg'] = "good";
	}

	//Respuesta
	if (isset($_GET['callback'])) {
		 echo $_GET['callback'] . '( ' . json_encode($info) . ' )';
	}else {
		 echo 'callbackEjercicio( ' . json_encode($info) . ' )';
	}

?>


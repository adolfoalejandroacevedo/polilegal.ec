<?php
   session_start();
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Plataforma Digital de Abogados del Ecuador | Encuentra un Abogado</title>
		<meta name="keywords" content="abogado, ecuador, abogados, quito, salinas, portoviejo, babahoyo, guayaquil">
		<meta name="description" content="La forma mas fácil de encontrar un abogado. Busca por ciudad, área o palabra clave." />
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <link href="css/animate.min.css" rel="stylesheet">
      <link href="css/prettyPhoto.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="bower_components/datatables.net-dt/css/jquery.dataTables.css">
      <link rel="stylesheet" type="text/css" href="css/bootstrap-markdown.min.css">
      <style>
         .pricingTable{
         text-align: center;
         transition: all 0.5s ease 0s;
         }
         .pricingTable:hover{
         box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
         }
         .pricingTable .pricingTable-header{
         color: #feffff;
         }
         .pricingTable .heading{
         display: block;
         padding-top: 25px;
         }
         .pricingTable .heading > h3{
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .textotitle{
         color: #feffff;
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .pricingTable .subtitle{
         display: block;
         font-size: 13px;
         margin-top: 5px;
         text-transform: capitalize;
         }
         .pricingTable .price-value{
         display: block;
         font-size: 60px;
         font-weight: 700;
         padding-bottom: 25px;
         }
         .pricingTable .price-value span{
         display: block;
         font-size: 14px;
         line-height: 20px;
         text-transform: uppercase;
         }
         .pricingTable .pricingContent{
         /*text-transform: capitalize;*/
         background: #fbfbfb;
         color: #fefeff;
         }
         .pricingTable .pricingContent ul{
         list-style: none;
         padding: 15px 14px 10px;
         margin: 0;
         text-align: left;
         }
         .pricingTable .pricingContent ul li{
         font-size: 14px;
         padding: 12px 0;
         border-bottom: 1px dashed #e1e1e1;
         color: #9da1ad;
         }
         .pricingTable .pricingContent ul li i{
         font-size: 14px;
         float: right;
         }
         .pricingTable .pricingTable-sign-up{
         padding: 20px 0;
         background: #fbfbfb;
         color: #fff;
         text-transform: capitalize;
         }
         .pricingTable .btn-block{
         width: 60%;
         margin: 0 auto;
         font-size: 17px;
         color: #fff;
         text-transform: capitalize;
         border: none;
         border-radius: 5px;
         padding: 10px;
         transition: all 0.5s ease 0s;
         }
         .pricingTable .btn-block:before{
         content: "\f007";
         font-family: 'FontAwesome';
         margin-right: 10px;
         }
         .pricingTable.blue .pricingTable-header,
         .pricingTable.blue .btn-block{
         background: #fce400;
         }
         .pricingTable.pink .pricingTable-header,
         .pricingTable.pink .btn-block{
         background: #c9b600;
         }
         .pricingTable.orange .pricingTable-header,
         .pricingTable.orange .btn-block{
         background: #e0d83a;
         }
         .pricingTable.green .pricingTable-header,
         .pricingTable.green .btn-block{
         background: #e0cd00;
         }
         .pricingTable.blue .btn-block:hover,
         .pricingTable.pink .btn-block:hover,
         .pricingTable.orange .btn-block:hover,
         .pricingTable.green .btn-block:hover{
         background: #e6e6e6;
         color: #939393;
         }
         @media screen and (max-width: 990px){
         .pricingTable{ margin-bottom: 20px; }
         }
         #keyword {
            text-transform:lowercase;
         }
      </style>
   </head>
   <body class="homepage">
<?php include "header.php"; ?>
      <!--/header-->
      <section id="blog" class="container">
         <ol class="breadcrumb">
            <li><a href="index.php">Inicio</a></li>
            <li class="active">Buscar Abogados</li>
         </ol>
         <div class="jumbotron" style="padding-bottom: 0px;">
            <div class="container">
               <form method="post" accept-charset="utf-8" class="form-inline center wow fadeInDown" role="form" action="findabogado.php">
                  <div style="display:none;"><input type="hidden" name="_method" class="form-control"  value="POST" /></div>
                  <div class="form-group text"><input type="text" name="keyword" class="form-control form-control input-lg"  
							placeholder="Palabra clave"  id="titulo" value="" onkeyup="return forceLower(this);" /></div>
                  <div class="form-group select">
                     <select name="ciudad" class="form-control form-control input-lg"  id="ciudad">
                        <option value="" selected="selected">Buscar en todas las ciudades</option>
								<?php
									$sql = "SELECT * FROM ciudades WHERE importancia='1' ORDER BY ciudad";
									if ($result = mysqli_query($conn, $sql)){
										while ($row = mysqli_fetch_assoc($result)) {
											echo "<option value='$row[id]'>".utf8_encode($row['ciudad'])."</option>";
										}
									}else{
										error_log("Error: " . $sql . "..." . mysqli_error($conn));
									}
								?>
                     </select>
                  </div>
                  <div class="form-group select">
                     <select name="area" class="form-control form-control input-lg"  id="area">
                        <option value="" selected="selected">Buscar en todas las áreas</option>
								<?php
									$sql = "SELECT * FROM areas ORDER BY area";
									if ($result = mysqli_query($conn, $sql)){
										while ($row = mysqli_fetch_assoc($result)) {
											echo "<option value='$row[id]'>".utf8_encode($row['area'])."</option>";
										}
									}else{
										error_log("Error: " . $sql . "..." . mysqli_error($conn));
									}
								?>
                     </select>
                  </div>
						<div class="row">
                  	<div class="form-group">
								<input type="submit" class="btn btn-success btn-lg" value="Buscar" style="margin-top: 30px;">
							</div>
						</div>
               </form>
            </div>
         </div>
      </section>
      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/markdown.js"></script>
      <script src="js/to-markdown.js"></script>
      <script src="js/bootstrap-markdown.js"></script>
      <script src="js/bootstrap-markdown.es.js"></script>
      <script type="text/javascript" src="js/jquery.simpleslider.js"></script>
      <script type="text/javascript" charset="utf8" src="bower_components/datatables.net/js/jquery.dataTables.min.js"> </script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/jquery.isotope.min.js"></script>
      <script src="js/wow.min.js"></script>
      <script src="js/main.js"></script>
      <script>
         function forceLower(strInput) {
            strInput.value=strInput.value.toLowerCase();
         }
      </script>

   </body>
</html>



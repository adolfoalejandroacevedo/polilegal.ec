<?php

$config = require 'config.php';

//error_log("Se tiende al llamado", 0);

//Retrieve form data. 
//GET - user submitted data using AJAX
//POST - in case user does not support javascript, we'll use POST instead
$nombres = ($_GET['nombres']) ? trim($_GET['nombres']) : trim($_POST['nombres']);
$cedula = ($_GET['cedula']) ? trim($_GET['cedula']) : trim($_POST['cedula']);
$email = ($_GET['email']) ?$_GET['email'] : $_POST['email'];
$email2 = ($_GET['email2']) ?$_GET['email2'] : $_POST['email2'];
$passwd = ($_GET['passwd']) ?$_GET['passwd'] : $_POST['passwd'];
$passwd2 = ($_GET['passwd2']) ?$_GET['passwd2'] : $_POST['passwd2'];
$error_encontrado="";

//Abriendo conexion a BD
$conn=mysqli_connect($config['database']['server'],$config['database']['username'],$config['database']['password'],$config['database']['db']);

//Validando si el usuario existe
$sql = "select * from users where email='$email' and active='1'";
$result = mysqli_query($conn, $sql);
$numero_filas = mysqli_num_rows($result);
if ($numero_filas > '0') $errors[count($errors)] = '5';

//if the errors array is empty, send the mail
if (!$errors) {

    //Generando hash
    $hash = md5( rand(0,1000) ); // Generate random 32 character hash and assign it to a local variable.
                                 // Example output: f4552671f8909587cf485ea990207f3b

    //Escapando caraceres especiales;
    $first_name = mysqli_real_escape_string($conn, $nombres);
    $last_name = mysqli_real_escape_string($conn, $cedula);
    $mail = mysqli_real_escape_string($conn, $email);
    $password = mysqli_real_escape_string($conn, md5($passwd));
    $hash = mysqli_real_escape_string($conn, $hash);

    //Actualizando usuario
    $sql = "select * from users where email='$email'";
    $result = mysqli_query($conn, $sql);
    $numero_filas = mysqli_num_rows($result);
    if ($numero_filas > '0') {
    	$sql = "update users set nombres='$first_name', cedula='$last_name', password='$password', 
		hash='$hash' where email='$mail'";
	if (!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
    }else{
    	//Insertando usuario
   	$sql = "insert into users (nombres, cedula, email, password, hash, active, role) 
            	values ('$first_name', '$last_name', '$mail', '$password', '$hash', '0','user')";
    	if (!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
    }

    //Creando el URL de validacion
    $url = $config["general"]["url"];
    $url = "$url/verify.php?email=$email&hash=$hash";

    //Prfeparando el envio del correo    
    require 'phpmailer/PHPMailerAutoload.php';
    include("email.php");
    include("config.php");

    $message = $cuerpo;

    $subject = "POLILEGAL.EC - Signup | Verificacion";

    $mail = new PHPMailer;

    $mail->isSMTP();

    //$mail->Host = 'smtp.gmail.com';
    //$mail->Host = 'smtp.polilegal.ec';
    $mail->Host = $config['general']['mailserver'];

    //$mail->Port = 587;
    $mail->Port = 465;

    $mail->SMTPSecure = 'ssl';

	$mail->SMTPOptions = array(
	  'ssl' => array(
		'verify_peer' => false,
		'verify_peer_name' => false,
		'allow_self_signed' => true
	  )
	);

    $mail->SMTPAuth = true;

    //$mail->Username = $email;
    $mail->Username = $config['general']['username'];

    //$mail->Password = $password;
    $mail->Password = $config['general']['password'];

    $mail->setFrom('admin@polilegal.ec', 'Polilegal S.A.');

    //$mail->addReplyTo();

    $mail->addAddress($email);

    $mail->Subject = $subject;

    $mail->msgHTML($message);

    if (!$mail->send()) {
       $error = "Mailer Error: " . $mail->ErrorInfo;
       error_log($error, 0);
       echo "0";
       return 0;
    }else {
        //echo '<script>alert("Message sent!");</script>';
	$errors['1'] = '1';
        return $errors['1'];
    }

//if the errors array has values
} else {
	//display the errors message
	for ($i=0; $i<count($errors); $i++) {
		echo $errors[$i];
		return $errors[$i];
	}
}

function validar_clave($clave,&$error_clave){
   if(strlen($clave) < 6){
      $error_clave = "La clave debe tener al menos 6 caracteres";
      return false;
   }
   if(strlen($clave) > 16){
      $error_clave = "La clave no puede tener más de 16 caracteres";
      return false;
   }
   if (!preg_match('`[a-z]`',$clave)){
      $error_clave = "La clave debe tener al menos una letra minúscula";
      return false;
   }
   if (!preg_match('`[A-Z]`',$clave)){
      $error_clave = "La clave debe tener al menos una letra mayúscula";
      return false;
   }
   if (!preg_match('`[0-9]`',$clave)){
      $error_clave = "La clave debe tener al menos un caracter numérico";
      return false;
   }
   $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/';
   if (!preg_match($pattern, $clave)) {
      $error_clave = "La clave debe tener al menos un caracter especial";
      return false;
   }
   $error_clave = "";
   return true;
} 

?>

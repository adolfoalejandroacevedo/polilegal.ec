<?php session_start(); ?>
<?php 

header('Content-Type: text/html; charset=utf-8');
$config = require 'config.php';
$param = array();

$param['mes'] = (isset($_GET['mes'])) ? $_GET['mes']:"";
$param['year'] = (isset($_GET['year'])) ? $_GET['year']:"";
$param['busqueda'] = (isset($_GET['busqueda'])) ? $_GET['busqueda']:"";



//Abriendo conexion a BD
$conn=mysqli_connect($config['database']['server'],
                $config['database']['username'],
                $config['database']['password'],
                $config['database']['db']);
if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
mysqli_set_charset($conn, "utf8");

//Cantidad de resultados por página (debe ser INT, no string/varchar)
$param['cantidad_resultados_por_pagina'] = 3;

//Comprueba si está seteado el GET de HTTP
if (isset($_GET["pagina"])) {
	if ($_GET["pagina"] == 0) { 
		$param['pagina'] = 1;
	}else{
		$param['pagina'] = $_GET["pagina"];
	}
} else { //Si el GET de HTTP no está seteado, lleva a la primera página
	$param['pagina'] = 1;
}

//Define el número 0 para empezar a paginar multiplicado por la cantidad de resultados por página
$param['empezar_desde'] = ($param['pagina']-1) * $param['cantidad_resultados_por_pagina'];

//Obtiene TODO de la tabla
if(isset($_GET['busqueda'])){
	$wordsAry = explode(" ", $_GET['busqueda']);
	$wordsCount = count($wordsAry);
	$queryCondition = "";
	for($i=0;$i<$wordsCount;$i++) {
		$queryCondition .= "titulo LIKE '%".$wordsAry[$i]."%' OR contenido LIKE '%".$wordsAry[$i]."%'";
		if($i!=$wordsCount-1) {
			$queryCondition .= " OR ";
		}
	}
	$sql = "SELECT * FROM articulos WHERE ".$queryCondition;
}else if(isset($_GET['mes']) && isset($_GET['year'])) {
	$sql = "SELECT * 
				FROM articulos 
				WHERE YEAR(fechanew)='$_GET[year]' AND MONTH(fechanew)='$_GET[mes]'";
}else $sql = "SELECT * FROM articulos";
if($result = mysqli_query($conn, $sql)){
	//Cuenta el número total de registros
	$param['total_registros'] = mysqli_num_rows($result);
	//Obtiene el total de páginas existentes
	$param['total_paginas'] = ceil($param['total_registros'] / $param['cantidad_resultados_por_pagina']);
}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

//Valor de page_before
if($param['pagina'] == 1){
	$param['page_before_class'] = 'disabled';
	$param['page_before_href'] = "#";
	$param['page_before'] = 0;
}else{
	$param['page_before'] = $param['pagina'] - 1;
	$param['page_before_class'] = 'enabled';
	$param['page_before_href'] = "blog.php?pagina=".$param['page_before']."&mes=".$param['mes']."&year=".$param['year']."&busqueda=".$param['busqueda'];
}

//Valor de page_after
if($param['pagina'] == $param['total_paginas']){
	$param['page_after'] = $param['total_paginas'];
	$param['page_after_class'] = 'disabled';
	$param['page_after_href'] = "#";
}else{
	$param['page_after'] = $param['pagina'] + 1;
	$param['page_after_class'] = 'enabled';
	$param['page_after_href'] = "blog.php?pagina=".$param['page_after']."&mes=".$param['mes']."&year=".$param['year']."&busqueda=".$param['busqueda'];
}

//Realiza la consulta en el orden de ID descendente
//Limitada por la cantidad de cantidad por página
if(isset($queryCondition)){
	$sql = "SELECT t1.id idart,
						iduser,
						titulo,
						contenido,
						img,
						t1.fechanew fecha,
						nombres,
						email 
			FROM articulos t1
			INNER JOIN users t2
			ON t2.id = iduser
			WHERE t1.active='1' AND ( $queryCondition )
			ORDER BY t1.id DESC LIMIT $param[empezar_desde], $param[cantidad_resultados_por_pagina]";
}else if(isset($_GET['mes']) && isset($_GET['year'])) {
   $sql = "SELECT t1.id idart,
                  iduser,
                  titulo,
                  contenido,
                  img,
                  t1.fechanew fecha,
                  nombres,
                  email 
         FROM articulos t1
         INNER JOIN users t2
         ON t2.id = iduser
         WHERE t1.active='1' AND YEAR(t1.fechanew)='$_GET[year]' AND MONTH(t1.fechanew)='$_GET[mes]' 
         ORDER BY t1.id DESC LIMIT $param[empezar_desde], $param[cantidad_resultados_por_pagina]";
}else{
	$sql = "SELECT t1.id idart,
						iduser,
						titulo,
						contenido,
						img,
						t1.fechanew fecha,
						nombres,
						email 
			FROM articulos t1
			INNER JOIN users t2
			ON t2.id = iduser
			WHERE t1.active='1'
			ORDER BY t1.id DESC LIMIT $param[empezar_desde], $param[cantidad_resultados_por_pagina]";
}
if(!$result = mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

function mes($mes){
	switch($mes){
		case '01':
			$mes = "ene.";
			break;
		case '02':
			$mes = "feb.";
			break;
		case '03':
			$mes = "mar.";
			break;
		case '04':
			$mes = "abr.";
			break;
		case '05':
			$mes = "may.";
			break;
		case '06':
			$mes = "jun.";
			break;
		case '07':
			$mes = "jul.";
			break;
		case '08':
			$mes = "ago.";
			break;
		case '09':
			$mes = "sep.";
			break;
		case '10':
			$mes = "oct.";
			break;
		case '11':
			$mes = "nov.";
			break;
		case '12':
			$mes = "dic.";
			break;
	}
	return $mes;
}

function mescompleto($mes){
	switch($mes){
		case '01':
			$mes = "Enero";
			break;
		case '02':
			$mes = "Febrero";
			break;
		case '03':
			$mes = "Marzo";
			break;
		case '04':
			$mes = "Abril";
			break;
		case '05':
			$mes = "Mayo";
			break;
		case '06':
			$mes = "Junio";
			break;
		case '07':
			$mes = "Julio";
			break;
		case '08':
			$mes = "Agosto";
			break;
		case '09':
			$mes = "Septiembre";
			break;
		case '10':
			$mes = "Octubre";
			break;
		case '11':
			$mes = "Noviembre";
			break;
		case '12':
			$mes = "Diciembre";
			break;
	}
	return $mes;
}

?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Plataforma Digital de Abogados del Ecuador | Blog </title>
		<meta name="keywords" content="abogado, abogados, ecuador, quito, machala, bolivar, esmeraldas">
		<meta name="description" content="Encuentra árticulos y noticias de interes de abogados del Ecuador" />
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <link href="css/animate.min.css" rel="stylesheet">
      <link href="css/prettyPhoto.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="bower_components/datatables.net-dt/css/jquery.dataTables.css">
      <link rel="stylesheet" type="text/css" href="css/bootstrap-markdown.min.css">
      <style>
         .pricingTable{
         text-align: center;
         transition: all 0.5s ease 0s;
         }
         .pricingTable:hover{
         box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
         }
         .pricingTable .pricingTable-header{
         color: #feffff;
         }
         .pricingTable .heading{
         display: block;
         padding-top: 25px;
         }
         .pricingTable .heading > h3{
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .textotitle{
         color: #feffff;
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .pricingTable .subtitle{
         display: block;
         font-size: 13px;
         margin-top: 5px;
         text-transform: capitalize;
         }
         .pricingTable .price-value{
         display: block;
         font-size: 60px;
         font-weight: 700;
         padding-bottom: 25px;
         }
         .pricingTable .price-value span{
         display: block;
         font-size: 14px;
         line-height: 20px;
         text-transform: uppercase;
         }
         .pricingTable .pricingContent{
         /*text-transform: capitalize;*/
         background: #fbfbfb;
         color: #fefeff;
         }
         .pricingTable .pricingContent ul{
         list-style: none;
         padding: 15px 14px 10px;
         margin: 0;
         text-align: left;
         }
         .pricingTable .pricingContent ul li{
         font-size: 14px;
         padding: 12px 0;
         border-bottom: 1px dashed #e1e1e1;
         color: #9da1ad;
         }
         .pricingTable .pricingContent ul li i{
         font-size: 14px;
         float: right;
         }
         .pricingTable .pricingTable-sign-up{
         padding: 20px 0;
         background: #fbfbfb;
         color: #fff;
         text-transform: capitalize;
         }
         .pricingTable .btn-block{
         width: 60%;
         margin: 0 auto;
         font-size: 17px;
         color: #fff;
         text-transform: capitalize;
         border: none;
         border-radius: 5px;
         padding: 10px;
         transition: all 0.5s ease 0s;
         }
         .pricingTable .btn-block:before{
         content: "\f007";
         font-family: 'FontAwesome';
         margin-right: 10px;
         }
         .pricingTable.blue .pricingTable-header,
         .pricingTable.blue .btn-block{
         background: #fce400;
         }
         .pricingTable.pink .pricingTable-header,
         .pricingTable.pink .btn-block{
         background: #c9b600;
         }
         .pricingTable.orange .pricingTable-header,
         .pricingTable.orange .btn-block{
         background: #e0d83a;
         }
         .pricingTable.green .pricingTable-header,
         .pricingTable.green .btn-block{
         background: #e0cd00;
         }
         .pricingTable.blue .btn-block:hover,
         .pricingTable.pink .btn-block:hover,
         .pricingTable.orange .btn-block:hover,
         .pricingTable.green .btn-block:hover{
         background: #e6e6e6;
         color: #939393;
         }
         @media screen and (max-width: 990px){
         .pricingTable{ margin-bottom: 20px; }
         }
      </style>
   </head>
   <body class="homepage">
<?php include "header.php"; ?>
      <!--/header-->
      <section id="blog" class="container">
         <div class="center">
            <h2>Blog</h2>
            <p class="lead">Artículos de Abogados</p>
         </div>
         <div class="blog">
            <div class="row">
               <div class="col-md-9">
					<?php
						while($row = mysqli_fetch_array($result)) {
							$day = date('d', strtotime($row['fecha']));
							$month = date('m', strtotime($row['fecha']));
							$mes = mes($month);
					?>	
                  <div class="blog-item">
                     <div class="row">
                        <div class="col-xs-12 col-sm-3 text-center">
                           <div class="entry-meta">
                              <span id="publish_date"><?php echo $day.' '.$mes; ?></span>
                              <span><i class="fa fa-user"></i> <a href="perfil.php?iduser=<?php echo $row['iduser']; ?>"><a href="perfil.php?iduser=<?php echo $row['iduser']; ?>"><?php echo $row['nombres']; ?></a></a></span>
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-9 blog-content">
                           <a href="articulo.php?idart=<?php echo $row['idart']; ?>"><img class="img-responsive img-blog" src="uploads/<?php echo $row['img']; ?>" width="100%" alt="" /></a>
                           <h2><a href="articulo.php?idart=<?php echo $row['idart']; ?>"><?php echo $row['titulo']; ?></a></h2>
                           <h3>
                              <p><?php echo str_replace("\n","</p><p>",substr(($row['contenido']),0,400)).'...'; ?></p>
                           </h3>
                           <a class="btn btn-primary readmore" href="articulo.php?idart=<?php echo $row['idart']; ?>">Leer más <i class="fa fa-angle-right"></i></a>
                        </div>
                     </div>
                  </div>
                  <!--/.blog-item-->
					<?php } ?>
                  <div class="paginator">
                     <ul class="pagination">
                        <li class="<?php echo $param['page_before_class']; ?>"><a href="<?php echo $param['page_before_href']; ?>">&lt; anterior</a></li>
                        <li class="<?php echo $param['page_after_class']; ?>"><a href="<?php echo $param['page_after_href']; ?>">siguiente &gt;</a></li>
                     </ul>
                     <p>Página <?php echo $param['pagina']; ?> de <?php echo $param['total_paginas']; ?>, mostrando <?php echo $param['cantidad_resultados_por_pagina']; ?> registros de <?php echo $param['total_registros']; ?> en total</p>
                  </div>
               </div>
               <!--/.col-md-8-->
               <aside class="col-md-3">
                  <div class="widget search">
                     <form role="form" method="GET" action="blog.php">
                        <input type="text" name="busqueda" class="form-control search_box" autocomplete="off" placeholder="Buscar">
                     </form>
                  </div>
                  <!--/.search-->
                  <div class="widget archieve">
                     <h3>Archivo</h3>
                     <div class="row">
                        <div class="col-sm-12">
                           <ul class="blog_archieve">
										<?php
											$sql = "SELECT COUNT(id) totalreg, 
																YEAR(fechanew) year, 
																MONTH(fechanew) mes 
																FROM articulos 
																GROUP BY YEAR(fechanew), MONTH(fechanew)";
											if(!$result = mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
											while($row = mysqli_fetch_array($result)) {
										?>
                              <li><a href="blog.php?mes=<?php echo $row['mes']; ?>&year=<?php echo $row['year']; ?>"><i class="fa fa-angle-double-right"></i> <?php echo mescompleto($row['mes'])." ".$row['year']; ?> <span class="pull-right">(<?php echo $row['totalreg']; ?>)</span></a></li>
										<?php } ?>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <!--/.archieve-->
               </aside>
            </div>
            <!--/.row-->
         </div>
      </section>
      <!--/#blog-->
      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/markdown.js"></script>
      <script src="js/to-markdown.js"></script>
      <script src="js/bootstrap-markdown.js"></script>
      <script src="js/bootstrap-markdown.es.js"></script>
      <script type="text/javascript" src="js/jquery.simpleslider.js"></script>
      <script type="text/javascript" charset="utf8" src="bower_components/datatables.net/js/jquery.dataTables.min.js"> </script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/jquery.isotope.min.js"></script>
      <script src="js/wow.min.js"></script>
      <script src="js/main.js"></script>
   </body>
</html>



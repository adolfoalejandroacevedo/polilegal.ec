<?php
   session_start();
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
   $sql = "SELECT users.id id, nombres, ciudad, planes.plan plan 
           FROM users 
           INNER JOIN planes 
           ON users.plan=idplan 
           WHERE users.id='59'";
   if ($result = mysqli_query($conn, $sql)){
      $row = mysqli_fetch_assoc($result);
   }else error_log("Error: " . $sql . "..." . mysqli_error($conn));
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
	 <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Gp Bootstrap Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
   <link rel="stylesheet" href="gp/css/font-awesome.min.css">
   <link href="gp/css/animate.min.css" rel="stylesheet">
    <link href="gp/css/prettyPhoto.css" rel="stylesheet">      
   <link href="css/main.css" rel="stylesheet">
    <link href="gp/css/responsive.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

	 <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       

      <style>
         .file_drag_area
         {
            width:600px;
            height:105px;
            border:2px dashed #ccc;
            /*line-height:30px;*/
            text-align:center;
            font-size:16px;
            margin:0 auto;
         }
         .file_drag_area p{
            margin: 0 0 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
            margin-left: 0px;
         }
         .file_drag_over{
            color:#000;
            border-color:#000;
         }
         .multiple_files{
            cursor: pointer;
         }
			hr {
				 display:block;
				 border:0px;
				 height:19px;
				 background-image:url('salique/images/separators.png');
				 margin-top: 0px;
			}
			#portfolio .row {
				margin-left: 0px;
				margin-right: 0px;
				margin-bottom: 10px;
			}
			.lista {
				font-size: 17px;
			}
      </style>    
 
  </head>
  <body class="homepage">   
<?php include "header.php"; ?>
      <!--/header-->

		<section id="portfolio">
			<div class="container wow fadeInDown">
				<ol class="breadcrumb" align="center" style="margin-bottom: 0px">
					<li class="active" id="bottom">
						<h3><?php echo strtoupper($row['nombres']); ?></h3>
					</li>
				</ol>
				<div class="separador">
					<hr>
				</div>
				<div class="row">
					<div class="col-md-4 col-md-offset-4">
						<div class="single-profile-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
							<a href="#"><img class="thumbnail" width= "100%" src="images/man1.jpg" alt=""></a>
							<!--/.media -->
						</div>
					</div>
					<!--/.col-lg-4 -->
				</div>
				<div>
				<div class="row">
					<div class="col-md-6">
						<h1 class="maintitle" style="color: #000">
							<span>Areas de Práctica</span>
						</h1>
						<ul>
							<li class="lista">Penal</li>
							<li class="lista">Civil</li>
						</ul>
					</div>
               <div class="col-md-6">
						<h1 class="maintitle" style="color: #000">
							<span>Contactos</span>
						</h1>
                  <ul>
                     <li class="lista">adolfoale@yahoo.es</li>
                  </ul>
               </div>
            </div>
			</div>
		</section>
		<!--/#portfolio-item-->

      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
	
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="gp/js/jquery.js"></script>
    <script src="gp/js/bootstrap.min.js"></script>
    <script src="gp/js/jquery.prettyPhoto.js"></script>
    <script src="gp/js/jquery.isotope.min.js"></script>
    <script src="gp/js/wow.min.js"></script>
    <script src="gp/js/main.js"></script>
	 <script src="js/eliminafoto.js"></script>


	<!-- Upload images -->
	<script>

	$(document).ready(function(){
	  
		/*Drag and Drop*/
		$('.file_drag_area').on('dragover', function(){  
			$(this).addClass('file_drag_over');  
			return false;  
		});  
		$('.file_drag_area').on('dragleave', function(){  
			$(this).removeClass('file_drag_over');  
			return false;  
		});  
		$('.file_drag_area').on('drop', function(e){  
			e.preventDefault();  
			$(this).removeClass('file_drag_over');  
			var formData = new FormData();  
			var files_list = e.originalEvent.dataTransfer.files;  
			var error_images = '';
			//console.log(files_list);  
         if (files_list.length > 10) {
				error_images += "<p>* No puede seleccionar mas de 30 imágenes</p>";
         } else {
				for(var i=0; i<files_list.length; i++)  
				{  
					formData.append('file[]', files_list[i]);  
					var name = files_list[i].name;
					var size = files_list[i].size;
					var type = files_list[i].type;
					if(size > 1024*1024){
						error_images += "<p>* El archivo "+name+" supera el maximo permitido 1MB</p>";
					}else if(type != 'image/jpeg' && type != 'image/jpg' && type != 'image/png' && type != 'image/gif'){
						error_images += "<p>* El archivo "+name+" no es del tipo de imagen permitido</p>";
					}
				}  
			}
			//console.log(formData);  
			if (error_images == '') {
				$.ajax({  
					url:"upload.php",  
					method:"POST",  
					data:formData,  
					contentType:false,  
					cache: false,  
					processData: false,
               beforeSend: function () {
                  $('#mensaje').empty();
                  $('#mensaje').append("<img id='enviando' src='images/barra.gif' width='100px' height='25px' \
                                        style='position: relative; vertical-align:middle'>");
               },  
					success:function(data){  
						$('#enviando').hide();
						if(data=="limite"){
                     $('#mensaje').append("\
                           <div class='alert alert-danger alert-dismissible fade in' id='successbox' style='margin-bottom: 10px;'> \
                              <a class='close' id='closediv' data-dismiss='alert' aria-label='close'>×</a> \
                              <p>Se a alcanzado el limite de imágenes permitidas = 30</p> \
                           </div>");
						}else{
							$('#portafolio').empty();
							$('#portafolio').html(data);
							location.reload(true);
						}
					}  
				})  
			}else{
            $('#mensaje').append("\
                           <div class='alert alert-danger alert-dismissible fade in' id='successbox' style='margin-bottom: 10px;'> \
                              <a class='close' id='closediv' data-dismiss='alert' aria-label='close'>×</a> \
                              <p>"+error_images+"</p> \
                           </div>");
				return false;
			}
		});

		/*Upload multiple*/
		$('#multiple_files').change(function () {
			var error_images = '';
			var form_data = new FormData();
			var files = $('#multiple_files')[0].files;
			if (files.length > 10) {
				error_images += '<p>* No puede seleccionar mas de 10 imágenes</p>';
			} else {
				for (var i = 0; i < files.length; i++) {
					var name = document.getElementById("multiple_files").files[i].name;
					var ext = name.split('.').pop().toLowerCase();
					if (jQuery.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
						error_images += '<p>* El archivo '+name+' no es del tipo de imagen permitido</p>';
					}
					var oFReader = new FileReader();
					oFReader.readAsDataURL(document.getElementById("multiple_files").files[i]);
					var f = document.getElementById("multiple_files").files[i];
					var fsize = f.size || f.fileSize;
					if (fsize > 2000000) {
						error_images += '* El archivo '+name+' supera el maximo permitido 1MB';
					} else {
						form_data.append("file[]", document.getElementById('multiple_files').files[i]);
					}
				}
			}
			if (error_images == '') {
				$.ajax({
					url: "upload.php",
					method: "POST",
					data: form_data,
					contentType: false,
					cache: false,
					processData: false,
					beforeSend: function () {
						$('#mensaje').empty();
						$('#mensaje').append("<img id='enviando' src='images/barra.gif' width='100px' height='25px' \
													 style='position: relative; vertical-align:middle'>");
					},
					success: function (data) {
                  $('#enviando').hide();
                  if(data=="limite"){
            			$('#mensaje').append("\
                           <div class='alert alert-danger alert-dismissible fade in' id='successbox' style='margin-bottom: 10px;'> \
                              <a class='close' id='closediv' data-dismiss='alert' aria-label='close'>×</a> \
                              <p>Se a alcanzado el limite de imágenes permitidas = 30</p> \
                           </div>");
                  }else{
                     $('#portafolio').empty();
                     $('#portafolio').html(data);
                     location.reload(true);
                  }
					}
				});
			} else {
				$('#mensaje').append("\
									<div class='alert alert-danger alert-dismissible fade in' id='successbox' style='margin-bottom: 10px;'> \
										<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>×</a> \
										<p>"+error_images+"</p> \
									</div>");
				return false;
			}
		});
	}); 
	</script>

  </body>
</html>

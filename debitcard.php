<?php
header('Content-Type: text/html; charset=utf-8');
session_start();
$config = require 'config.php';
$conn=mysqli_connect(
		$config['database']['server'],
		$config['database']['username'],
		$config['database']['password'],
		$config['database']['db']
);
if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
mysqli_set_charset($conn, "utf8");

if(!isset($_POST['option_select'])){
	echo "notoken";
	exit;
}

//Obtenemos el id del plan
$idplan = $_POST['plan'];

//Buscando el monto
$sql = "SELECT id, costo, concepto FROM productos WHERE id='$idplan'";
if($result = mysqli_query($conn, $sql)){
	$row = mysqli_fetch_assoc($result);
	$amount = floatval($row['costo']);
}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

//Buscando el iva
$sql = "SELECT costo FROM productos WHERE id='2'";
if($result = mysqli_query($conn, $sql)){
	$row = mysqli_fetch_assoc($result);
	$tax_percentage = floatval($row['costo']);
}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

//Obteniendo los datos del cliente
$sql =  "SELECT
				nombres,
				cedula,
				celular,
				email,
				sector,
				t2.ciudad ciudad,
				t1.plan planid,
				t3.concepto plan,
				t4.medio medio_pago
			FROM afiliacion t1 
			INNER JOIN ciudades t2 ON t1.ciudad = t2.id
			INNER JOIN productos t3 ON t1.plan = t3.id
			INNER JOIN medios_pagos t4 ON t1.medio_pago = t4.id
			WHERE email='$_SESSION[id]'
		  ";
if($result = mysqli_query($conn, $sql)){
	$row = mysqli_fetch_assoc($result);
	$nombres = $row['nombres'];
	$description = $row['plan'];
	$descriptionId = $row['planid'];
  $correo = $row['email'];
}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

// ===================
// Set de variables
// ===================
$userId = $_SESSION['id'];
$token = $_POST['option_select'];
$auth_timestamp = time();
$application_code = $config['paymentez']['server_app_code'];
$appkey = $config['paymentez']['server_app_key'];
$uniq_token_string = $appkey.$auth_timestamp;
$uniq_token_hash = hash('sha256', $uniq_token_string);
$auth_token = base64_encode($application_code.';'.$auth_timestamp.';'.$uniq_token_hash);
$param = array(
				"user" => array(
								"id" => "$userId",
								"email" => "osalas@paymentez.com"),
								//"email" => "$correo"),
				"order" => array(
								"amount" => $amount,
								"description" => "$description",
								"dev_reference" => "polilegal-".time(),
								"vat" => round(($amount/(1+$tax_percentage/100))*$tax_percentage/100,2),
								"taxable_amount" => round($amount/(1+$tax_percentage/100),2),
								"installments_type" => 0,
								"tax_percentage" => $tax_percentage
								),
				"card" => array(
								"token" => "$token")
				);
$payload = json_encode($param);
error_log($payload);

// ===============================
// Ejecucion de metodo refund
// ===============================

        //Lo primerito, creamos una variable iniciando curl, pasándole la url
        $ch = curl_init($config['paymentez']['environment']."/v2/transaction/debit/");

        //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
        curl_setopt ($ch, CURLOPT_POST, 1);

        //le decimos qué paramáetros enviamos (pares nombre/valor, también acepta un array)
        curl_setopt ($ch, CURLOPT_POSTFIELDS, $payload);

        //Pasamos la cabecera
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","Auth-Token: $auth_token"));

        //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

        //recogemos la respuesta
        $respuesta = curl_exec ($ch);

        //o el error, por si falla
        $error = curl_error($ch);

        //y finalmente cerramos curl
        curl_close ($ch);

        //Convertir json string en array
        $data = json_decode($respuesta, TRUE);
       //print_r($data);
        //Retornamos si la respuesta fue "success" o "failure"
        //if (isset($data["status"])){
        //        return $data["status"];
        //}else{
        //        return true;
        //}

        //echo $respuesta;
        //echo $error;
		  error_log($respuesta);


if (isset($data['transaction']['status']) && $data['transaction']['status'] == 'success'){

	//Variables de trabajo
	$df = $data['transaction']['id'];
	$amount = $param['order']['amount'];
	$token = $data['card']['token'];
	$installments_type = 0;
	$periodo = 1;
	
	//Insertamos transaccion en recurrencias
	$sql = "INSERT INTO recurrencias (
					df,
					userid,
					token,
					amount,
					description,
					installments_type,
					tax_percentage,
					periodo,
					fechaexec)
				VALUES (
					'$df',
					'$_SESSION[id]',
					'$token',
					'$amount',
					'$descriptionId',
					'$installments_type',
					'$tax_percentage',
					'$periodo',
					DATE_ADD(NOW(), INTERVAL $periodo MONTH))";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

	//Enviamos correo
	sendmail($df,$amount,$param['order']['vat'],$nombres,$data['transaction']['authorization_code'],$description,$param['order']['taxable_amount'],$row);

	echo "success";
}else{
	error_log(serialize($data));
	echo "nopay";
}

function sendmail($df,$order_amount,$order_vat,$nombres,$txtCod,$order_description,$sub_total,$row){
	$config = require 'config.php';

   //Enviando comprobnte de pago al cliente
   require 'PHPMailer/PHPMailerAutoload.php';
   include("emailpayplan.php");
   $mail = new PHPMailer;
   $mail->isSMTP();
   $mail->CharSet = 'UTF-8';
   $mail->SMTPDebug = 0;
   //$mail->Debugoutput = 'html';
   //$mail->Host = 'smtp.polilegal.ec';
   $mail->Host = $config['general']['mailserver'];
   //$mail->Host = 'localhost';
   $mail->Port = 465;
   $mail->SMTPSecure = 'ssl';
   $mail->SMTPAuth = true;
   $mail->SMTPOptions = array(
     'ssl' => array(
       'verify_peer' => false,
       'verify_peer_name' => false,
       'allow_self_signed' => true
     )
   );
   $mail->Username = $config['general']['username'];
   $mail->Password = $config['general']['password'];
   $mail->setFrom('admin@polilegal.ec', 'Polilegal S.A.');
   $mail->addAddress($_SESSION['id'], '');
   $mail->addReplyTo('noreplyto@polilegal.ec', 'POLILEGAL S.A.');
   $mail->Subject = 'POLILEGAL.EC - Afiliación';
   $mail->msgHTML($cuerpo);
   //$mail->AddAttachment('Guia_del_usuario_POLILEGAL_2019.pdf');
   if (!$mail->send()) {
      $error = "Mailer Error: " . $mail->ErrorInfo;
      error_log($error);
       echo "Error al enviar correo";
   }

	//Enviando correo de bienvenida
   if ($_POST['plan'] == 3) $guia = "PLAN_POLILEGAL_PERSONAL_julio2019.pdf";
   if ($_POST['plan'] == 4) $guia = "PLAN_POLILEGAL_POLICIAL_SERVICIO_ACTIVO_julio2019.pdf";
   if ($_POST['plan'] == 5) $guia = "PLAN_POLILEGAL_POLICIAL_SERVICIO_PASIVO_Y_FAMILIARES_julio2019.pdf";
   if ($_POST['plan'] == 6) $guia = "PLAN_POLILEGAL_POLICIAL_CADETES_julio2019.pdf";
   if ($_POST['plan'] == 7) $guia = "PLAN_POLILEGAL_EMPRESARIAL_julio2019.pdf";
   error_log("Plan = ".$_POST['plan']."| Guia = ".$guia);
   include("email.php");
   $mail->msgHTML($cuerpo);
   if (!$mail->send()) {
      $error = "Mailer Error: " . $mail->ErrorInfo;
      error_log($error);
       echo "Error al enviar correo";
   }

   //Enviando correo a Polilegal
   $nombres = $row['nombres'];
   $cedula = $row['cedula'];
   $celular = $row['celular'];
   $email = $row['email'];
   $sector = $row['sector'];
   $ciudad = $row['ciudad'];
   $plan = $row['plan'];
   $medio_pago = $row['medio_pago'];


	include("email2.php");
	$mail->ClearAddresses();
   $mail->addAddress("info@polilegal.ec", '');
   $mail->msgHTML($cuerpo2);
   if (!$mail->send()) {
      $error = "Mailer Error: " . $mail->ErrorInfo;
      error_log($error);
       echo "Error al enviar correo";
   }



}


?>

jQuery(document).ready(function($){
	
	"use strict";
		
	var isMobile = (navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i)) || (navigator.userAgent.match(/Android/i)) || (navigator.userAgent.match(/Blackberry/i)) || (navigator.userAgent.match(/Windows Phone/i)) ? true : false;
	var currentWidth = window.innerWidth || document.documentElement.clientWidth;
		
		if( currentWidth <= 319 ){
			$("div.fixed-help-form").animate( { right: -200 } );
			$(".fixed-help-form-icon").click(function(e){
				var $this = $(this);	
				if($this.hasClass('form-open')){
					$("div.fixed-help-form").animate({right: -200},function(){$this.removeClass('form-open'); $this.parents('.fixed-help-form').removeClass('helpform-wrapper-open');});
				}else{
					$("div.fixed-help-form").animate({right: 0},function(){$this.addClass('form-open'); $this.parents('.fixed-help-form').addClass('helpform-wrapper-open');});
				}
				e.preventDefault();
			});
		} else {
			$("div.fixed-help-form").animate( { right: -300 } );
			$(".fixed-help-form-icon").click(function(e){
				var $this = $(this);	
				if($this.hasClass('form-open')){
					$("div.fixed-help-form").animate({right: -300},function(){$this.removeClass('form-open'); $this.parents('.fixed-help-form').removeClass('helpform-wrapper-open');});
				}else{
					$("div.fixed-help-form").animate({right: 0},function(){$this.addClass('form-open'); $this.parents('.fixed-help-form').addClass('helpform-wrapper-open');});
				}
				e.preventDefault();
			});
		}
		
		
     	$('.comment-form').validate();
		$('.qtyplus').click(function(e){
			e.preventDefault();
			var currentVal = parseInt($('input[name="quantity"]').val());
			if (!isNaN(currentVal)) {
				$('input[name="quantity"]').val(currentVal + 1);
			} else {
				$('input[name="quantity"]').val(0);
			}
		});
	
		$(".qtyminus").click(function(e) {
	
			e.preventDefault();
			var currentVal = parseInt($('input[name="quantity"]').val());
			if (!isNaN(currentVal) && currentVal > 0) {
				$('input[name="quantity"]').val(currentVal - 1);
			} else {
				$('input[name="quantity"]').val(0);
			}
		});
	
	
	//Our practices page content toggle
	$("a.dt-sc-toggle-practice").click(function( event ){
		event.preventDefault();
		var $this = $(this);
		$this.parents('.column').find( "div.dt-sc-practice-content" ).slideToggle( "slow", function() {
			$this.toggleClass('expanded');
			if($this.hasClass('expanded')) {
				$this.html(' Contraer <span class="fa fa-angle-double-up"></span>');
			} else {
				$this.html(' Expandir <span class="fa fa-angle-double-down"></span>');
			}
		});
	});
	
	$("html").niceScroll({zindex:99999,cursorborder:"1px solid #424242"});
	//STICKY MENU...
	$("#menu-container").sticky({ topSpacing: 0 });
	//Mobile Menu
	$("#dt-menu-toggle").click(function( event ){
		event.preventDefault();
		var $menu = $("nav#main-menu").find("ul.menu:first");
		$menu.slideToggle(function(){
			$menu.css('overflow' , 'visible');
			$menu.toggleClass('menu-toggle-open');
		});
	});

	$(".dt-menu-expand").click(function(event){
		event.preventDefault();
		if( $(this).hasClass("dt-mean-clicked") ){
			$(this).text("+");
			if( $(this).prev('ul').length ) {
				$(this).prev('ul').slideUp(400);
			} else {
				$(this).prev('.megamenu-child-container').find('ul:first').slideUp(600);
			}
		} else {
			$(this).text("-");
			if( $(this).prev('ul').length ) {
				$(this).prev('ul').slideDown(400);
			} else{
				$(this).prev('.megamenu-child-container').find('ul:first').slideDown(2000);
			}
		}
		
		$(this).toggleClass("dt-mean-clicked");
		return false;
	});
  
	/*Menu */
	function megaMenu() {
		var screenWidth = $(document).width(),
		containerWidth = $("#header .container").width(),
		containerMinuScreen = (screenWidth - containerWidth)/2;
		
		if( containerWidth == screenWidth ){
							
			$("li.menu-item-megamenu-parent .megamenu-child-container").each(function(){
				var ParentLeftPosition = $(this).parent("li.menu-item-megamenu-parent").offset().left,
				MegaMenuChildContainerWidth = $(this).width();				
				
				if( (ParentLeftPosition + MegaMenuChildContainerWidth) > screenWidth ){
					var SwMinuOffset = screenWidth - ParentLeftPosition;
					var marginFromLeft = MegaMenuChildContainerWidth - SwMinuOffset;
					marginFromLeftActual = (marginFromLeft) + 25;
					marginLeftFromScreen = "-"+marginFromLeftActual+"px";
					$(this).css('left',marginLeftFromScreen);
			  }
			});		
				
		} else {
		
		
			$("li.menu-item-megamenu-parent .megamenu-child-container").each(function(){
				var ParentLeftPosition = $(this).parent("li.menu-item-megamenu-parent").offset().left,
				MegaMenuChildContainerWidth = $(this).width();
				
				if( (ParentLeftPosition + MegaMenuChildContainerWidth) > containerWidth ){
				
					var marginFromLeft = ( ParentLeftPosition + MegaMenuChildContainerWidth ) - screenWidth;
					var marginLeftFromContainer = containerMinuScreen + marginFromLeft + 20;
					
					if( MegaMenuChildContainerWidth > containerWidth ){
						
						var MegaMinuContainer	= ( (MegaMenuChildContainerWidth - containerWidth)/2 ) + 10; 			
						var marginLeftFromContainerVal = marginLeftFromContainer - MegaMinuContainer;
						marginLeftFromContainerVal = "-"+marginLeftFromContainerVal+"px";
						$(this).css('left',marginLeftFromContainerVal);
						
					} else {
						
						marginLeftFromContainer = "-"+marginLeftFromContainer+"px";
						$(this).css('left',marginLeftFromContainer);
					
					}
				
				}
			});
		
		}
	}
	
	
	megaMenu();
	$(window).smartresize(function(){
		megaMenu();
	});

	//Menu Hover Start
	function menuHover() {
		$("li.menu-item-depth-0,li.menu-item-simple-parent ul li" ).hover(
			function(){
				if( $(this).find(".megamenu-child-container").length  ){
					$(this).find(".megamenu-child-container").stop().fadeIn('fast');
				} else {
					$(this).find("> ul.sub-menu").stop().fadeIn('fast');
				}
			},
			function(){
				if( $(this).find(".megamenu-child-container").length ){
					$(this).find(".megamenu-child-container").stop(true, true).hide();
				} else {
					$(this).find('> ul.sub-menu').stop(true, true).hide(); 
				}
			}
		);
	}//Menu Hover End

	if( !isMobile ){
		if( currentWidth > 767 ){
			menuHover();
		}
	}
	
		
	//MAIN MENU...
	$("#main-menu ul li:has(ul)").each(function(){
		$(this).addClass("hasSubmenu");
	});
	
	//GOOGLE MAPS...
	var $map = $('#map');
	if( $map.length ) {
		$map.gmap3({
    marker: {
      values: [{
        latLng: [-0.180222,-78.479192],
        options: {
            icon: "img/marker.png"
        }
    }, ],
    },
    map:{
      options:{
        zoom:18,
        mapTypeControl: true,
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
        },
        navigationControl: false,
        scrollwheel: false,
        center: [-0.180222,-78.479192],
        streetViewControl: true,
        styles: [{
          featureType: "all",
          elementType: "all",
//          stylers: [
//            {"saturation":-100},{"lightness":25},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}
//          ]
        }]
      }
    }
  });
	}
	 
	//GO TO TOP...
	var offset = 220;
	var duration = 500;
	$(window).scroll(function() {
		if ($(this).scrollTop() > offset) {
			$('.back-to-top').fadeIn(duration);
		} else {
			$('.back-to-top').fadeOut(duration);
		}
	});
	
	$('.back-to-top').click(function(event) {
		event.preventDefault();
		$('html, body').animate({scrollTop: 0}, duration);
		return false;
	});
	
	//NEWSLETTER AJAX SUBMIT...
	$('form[name="frmnewsletter"]').submit(function () {
		
		var This = $(this);
		if($(This).valid()) {
			var action = $(This).attr('action');

			var data_value = unescape($(This).serialize());
			$.ajax({
				 type: "POST",
				 url:action,
				 data: data_value,
				 error: function (xhr, status, error) {
					 confirm('Algo salió mal!');
				   },
				  success: function (response) {
					$('#ajax_subscribe_msg').html(response);
					$('#ajax_subscribe_msg').slideDown('slow');
					if (response.match('success') != null) $(This).slideUp('slow');
				 }
			});
		}
		return false;
		
    });
	$('form[name="frmnewsletter"]').validate({
		rules: { 
			mc_email: { required: true, email: true }
		},
		errorPlacement: function(error, element) { }
	});

	//CONTACT FORM VALIDATION & MAIL SENDING....
	$('form[name="frmcontact"]').submit(function () {
		
		var This = $(this);
		if($(This).valid()) {
			var action = $(This).attr('action');
			var id = $("input[name='plan']").val();;
			$('#enviando').show('slow');
			var data_value = unescape($(This).serialize());
			$.ajax({
				 type: "POST",
				 url:action,
				 data: data_value,
				 error: function (xhr, status, error) {
					 confirm('Algo salió mal!');
				   },
				  success: function (response) {
					console.log(response);

					//$('#ajax_contact_msg').html(response);
					//$('#ajax_contact_msg').slideDown('slow');
					if(response=="1"){
						$('#enviando').hide();
						$('#mensajes').html("\
								  <div class='alert alert-success alert-dismissible fade in' id='successbox2' \
										style='display: none; margin-bottom: 30px; margin-left: 30px; margin-right: 30px'>\
										<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
										<span>Bienvenido, a tu correo electrónico y/o WhatsApp te llegará la Guía de Usuario del Plan POLILEGAL S.A.</span>\
								  </div>\
								  ");
						$('#successbox2').slideDown('slow');
					}else if(response=="2"){
						window.location.href = "addcardpci.php?id="+id;
          }else if(response=="Correo enviado"){
            confirm('!Sugerencia enviada!');
            $('#enviando').hide();
					}else if(response=="Error al enviar correo"){
                  $('#enviando').hide();
                  $('#mensajes').html("\
                          <div class='alert alert-danger alert-dismissible fade in' id='successbox2' \
                              style='display: none; margin-bottom: 30px; margin-left: 30px; margin-right: 30px'>\
                              <a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
                              <span>Error al enviar correo. Por favor intente mas tarde.</span>\
                          </div>\
                          ");
                  $('#successbox2').slideDown('slow');
          }else if(response=="Error en BD"){
                  $('#enviando').hide();
                  $('#mensajes').html("\
                          <div class='alert alert-danger alert-dismissible fade in' id='successbox2' \
                              style='display: none; margin-bottom: 30px; margin-left: 30px; margin-right: 30px'>\
                              <a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
                              <span>A ourrido un error inesperado. Por favor intente mas tarde.</span>\
                          </div>\
                          ");
                  $('#successbox2').slideDown('slow');
					}
				 }
			});
		}
		return false;
		
    });
	$('form[name="frmcontact"]').validate({
		rules: { 
			nombres: { required: true },
			cedula: { required: true,},
			sector: { required: true },
			celular: { required: true },
			email: { 
				required: true, 
				email: true,
				/*
            remote: {
               url: "checkemail.php",
               type: "post",
               dataFilter: function(data) {
                  console.log(data);
                  if(data == 'Exists'){
                     return false;
                  }else{
                     return true;
                  }
               }
            },
				*/
			},
			tos: { required: true },
			medio_pago: { required: true },
			banco: { required: true },
			cuenta: { required: true },
			numero_cuenta: { required: true },
			/*
			ciudad: { 
				required: true,
            remote: {
               url: "checkciudad.php",
               type: "post",
               dataFilter: function(data) {
						console.log(data);
                  if(data == 'Exists'){
                     return true;
                  }else{
                     return false;
                  }
               }
            },
			},
			*/
			plan: { required: true },
		},
		errorPlacement: function(error, element) { }
	});
	
	//CONTACT FORM VALIDATION & MAIL SENDING....
	$('form[name="frmcontactpolicial"]').submit(function () {
		
		var This = $(this);
		if($(This).valid()) {
			var action = $(This).attr('action');
			var id = $("input[name='plan']").val();;
			$('#enviando').show('slow');
			var data_value = unescape($(This).serialize());
			$.ajax({
				 type: "POST",
				 url:action,
				 data: data_value,
				 error: function (xhr, status, error) {
					 confirm('Algo salió mal!');
				   },
				  success: function (response) {
					console.log(response);
					//$('#ajax_contact_msg').html(response);
					//$('#ajax_contact_msg').slideDown('slow');
					if(response=="1"){
						$('#enviando').hide();
						$('#mensajes').html("\
								  <div class='alert alert-success alert-dismissible fade in' id='successbox2' \
										style='display: none; margin-bottom: 30px; margin-left: 30px; margin-right: 30px'>\
										<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
										<span>Bienvenido, a tu correo electrónico y/o WhatsApp te llegará la Guía de Usuario del Plan POLILEGAL S.A.</span>\
								  </div>\
								  ");
						$('#successbox2').slideDown('slow');
					}else if(response=="2"){
						window.location.href = "addcardpci.php?id="+id;
					}else if(response=="Error al enviar correo"){
                  $('#enviando').hide();
                  $('#mensajes').html("\
                          <div class='alert alert-danger alert-dismissible fade in' id='successbox2' \
                              style='display: none; margin-bottom: 30px; margin-left: 30px; margin-right: 30px'>\
                              <a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
                              <span>Error al enviar correo. Por favor intente mas tarde.</span>\
                          </div>\
                          ");
                  $('#successbox2').slideDown('slow');
               }else if(response=="Error en BD"){
                  $('#enviando').hide();
                  $('#mensajes').html("\
                          <div class='alert alert-danger alert-dismissible fade in' id='successbox2' \
                              style='display: none; margin-bottom: 30px; margin-left: 30px; margin-right: 30px'>\
                              <a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
                              <span>A ourrido un error inesperado. Por favor intente mas tarde.</span>\
                          </div>\
                          ");
                  $('#successbox2').slideDown('slow');
					}
				 }
			});
		}
		return false;
		
    });
	$('form[name="frmcontactpolicial"]').validate({
		rules: { 
			nombres: { required: true },
			cedula: { required: true,},
			sector: { required: true },
			celular: { required: true },
			email: { 
				required: true, 
				email: true,
				/*
            remote: {
               url: "checkemail.php",
               type: "post",
               dataFilter: function(data) {
                  console.log(data);
                  if(data == 'Exists'){
                     return false;
                  }else{
                     return true;
                  }
               }
            },
				*/
			},
			tos: { required: true },
			medio_pago: { required: true },
			banco: { required: true },
			cuenta: { required: true },
			numero_cuenta: { required: true },
			ciudad: { 
				required: true,
            remote: {
               url: "checkciudad.php",
               type: "post",
               dataFilter: function(data) {
						console.log(data);
                  if(data == 'Exists'){
                     return true;
                  }else{
                     return false;
                  }
               }
            },
			},
			plan: { required: true },
		},
		errorPlacement: function(error, element) { }
	});

	//FACTURA
	$('form[name="frmfactura"]').submit(function () {
		
		var This = $(this);
		if($(This).valid()) {
			var action = $(This).attr('action');
			$('#enviando').show('slow');
			var data_value = unescape($(This).serialize());
			$.ajax({
				 type: "POST",
				 url:action,
				 data: data_value,
				 dataType: 'jsonp',
				 cache: false,
	             error: function (data, textStatus, errorThrown)
    	         {
                 	confirm(data, textStatus, errorThrown);
            	 },
				 /*error: function (xhr, status, error) {
					 confirm('Algo salió mal!');
				   },*/
				 success: function(data, textStatus, jqXHR) {
					console.log("the response is", data);
					$('#mensajes').show();
					if(data.OK=="nofound"){
						$('#mensajes').html("\
								  <div class='alert alert-danger alert-dismissible fade in' id='successbox' style='display: none'>\
										<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
										<span>No se encontró factura para el período seleccionado.</span>\
								  </div>\
								  ");
						$('#successbox').slideDown('slow');
						$('#enviando').hide();
					}
                    if(data.OK=="found"){
                        $('#mensajes').html("\
                                  <div class='alert alert-success alert-dismissible fade in' id='successbox' style='display: none'>\
                                        <a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
                                        <span>Haga click </span><a href='" + data.file + "' download>Aquí</a><span> para descargar su factura.</span>\
                                  </div>\
                                  ");
                        $('#successbox').slideDown('slow');
                        $('#enviando').hide();
                    }
				 }
			});
		}
		return false;
    });
	$('form[name="frmfactura"]').validate({
		rules: { 
			date: { required: true },
		},
		errorPlacement: function(error, element) { }
	});

	//LOGIN
	$('form[name="frmlogin"]').submit(function () {
		
		var This = $(this);
		if($(This).valid()) {
			var action = $(This).attr('action');
			$('#enviando').show('slow');
			var data_value = unescape($(This).serialize());
			$.ajax({
				 type: "POST",
				 url:action,
				 data: data_value,
				 error: function (xhr, status, error) {
					 confirm('Algo salió mal!');
				   },
				  success: function (response) {
					console.log("the response is", response);
					$('#mensajes').show();
					if(response==""){
						$('#mensajes').html("\
								  <div class='alert alert-danger alert-dismissible fade in' id='successbox' style='display: none'>\
										<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
										<span>Correo o Contraseña incorrecto.</span>\
								  </div>\
								  ");
						$('#successbox').slideDown('slow');
						$('#enviando').hide();
					}else{
						parent.document.location.href="factura.php";
                    }
				 }
			});
		}
		return false;
    });
	$('form[name="frmlogin"]').validate({
		rules: { 
			email: { required: true, email: true },
			passwd: { required: true },
		},
		errorPlacement: function(error, element) { }
	});

	//CONTACT FORM LOGIN
	$('form[name="frmolvidopasswd"]').submit(function () {
		
		var This = $(this);
		if($(This).valid()) {
			var action = $(This).attr('action');
			$('#enviando').show('slow');
			var data_value = unescape($(This).serialize());
			$.ajax({
				 type: "POST",
				 url:action,
				 data: data_value,
				 error: function (xhr, status, error) {
					 confirm('Algo salió mal!');
				   },
				  success: function (response) {
					console.log("the response is", response);
					$('#mensajes').show();
					if(response=="No existe correo"){
						$('#mensajes').html("\
								  <div class='alert alert-danger alert-dismissible fade in' id='successbox' style='display: none'>\
										<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
										<span>El correo ingresado no se encuentra registrado o la cuenta esta inactiva.</span>\
								  </div>\
								  ");
						$('#successbox').slideDown('slow');
						$('#enviando').hide();
					}
                    if(response=="Correo Enviado"){
                        $('#mensajes').html("\
                                  <div class='alert alert-success alert-dismissible fade in' id='successbox' style='display: none'>\
                                        <a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
                                        <span>Se ha enviado un link a su dirección de correo para cambiar su contraseña.</span>\
                                  </div>\
                                  ");
                        $('#successbox').slideDown('slow');
                        $('#enviando').hide();
                    }
				 }
			});
		}
		return false;
    });
	$('form[name="frmolvidopasswd"]').validate({
		rules: { 
			email: { required: true, email: true },
		},
		errorPlacement: function(error, element) { }
	});

//CAMBIO DE PASSWORD
	$('form[name="frmcambiopasswd"]').submit(function () {
		
		var This = $(this);
		if($(This).valid()) {
			var action = $(This).attr('action');
			$('#enviando').show('slow');
			var data_value = unescape($(This).serialize());
			$.ajax({
				 type: "POST",
				 url:action,
				 data: data_value,
				 error: function (xhr, status, error) {
					 confirm('Algo salió mal!');
				   },
				  success: function (response) {
					console.log("the response is", response);
					$('#mensajes').show();
					if(response=="Passwords Diferentes"){
						$('#mensajes').html("\
								  <div class='alert alert-danger alert-dismissible fade in' id='successbox' style='display: none'>\
										<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a><span>Las contraseñas deben ser iguales</span>\
								  </div>\
								  ");
						$('#successbox').slideDown('slow');
						$('#enviando').hide();
					}
					if(response=="Password Cambiado"){
						$('#mensajes').html("\
								  <div class='alert alert-success alert-dismissible fade in' id='successbox' style='display: none'>\
										<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
										<span>Su contrseña ha sido cambiada. Inicie sesión haciendo click </span>\
										<a href='login.php'>Aquí</a>\
								  </div>\
								  ");
						$('#successbox').slideDown('slow');
						$('#enviando').hide();
					}
                    if(response=="Error en Password"){
                        $('#mensajes').html("\
                                  <div class='alert alert-danger alert-dismissible fade in' id='successbox' style='display: none'>\
                                        <a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
                                        <p>La contraseña debe cumplir con las siguientes características:</p>\
                                        <ul>\
                                            <li>La clave debe tener al menos 6 caracteres</li>\
                                            <li>La clave no puede tener más de 16 caracteres</li>\
                                            <li>La clave debe tener al menos una letra minúscula</li>\
                                            <li>La clave debe tener al menos una letra mayúscula</li>\
                                            <li>La clave debe tener al menos un caracter numérico</li>\
                                            <li>La clave debe tener al menos un caracter especial</li>\
                                        </ul>\
                                  </div>\
                                  ");
                        $('#successbox').slideDown('slow');
                        $('#enviando').hide();
                    }
				 }
			});
		}
		return false;
		
    });
	$('form[name="frmcambiopasswd"]').validate({
		rules: { 
			passwd1: { required: true },
			passwd2: { required: true },
		},
		errorPlacement: function(error, element) { }
	});

	//CONTACT FORM VALIDATION & MAIL SENDING....
	$('form[name="frmregistro"]').submit(function () {
		
		var This = $(this);
		if($(This).valid()) {
			var action = $(This).attr('action');
			$('#enviando').show('slow');
			var data_value = unescape($(This).serialize());
			$.ajax({
				 type: "POST",
				 url:action,
				 data: data_value,
				 error: function (xhr, status, error) {
					 confirm('Algo salió mal!');
				   },
				  success: function (response) {
					console.log("the response is", response);
					$('#mensajes').show();
					if(response=="Correos Diferentes"){
						$('#mensajes').html("\
								  <div class='alert alert-danger alert-dismissible fade in' id='successbox' style='display: none'>\
										<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a><span>Los correos deben ser iguales</span>\
								  </div>\
								  ");
						$('#successbox').slideDown('slow');
						$('#enviando').hide();
					}
					if(response=="Passwords Diferentes"){
						$('#mensajes').html("\
								  <div class='alert alert-danger alert-dismissible fade in' id='successbox' style='display: none'>\
										<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a><span>Las contraseñas deben ser iguales</span>\
								  </div>\
								  ");
						$('#successbox').slideDown('slow');
						$('#enviando').hide();
					}
					if(response=="Correo Existe"){
						$('#mensajes').html("\
								  <div class='alert alert-danger alert-dismissible fade in' id='successbox' style='display: none'>\
										<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a><span>El correo ya se encuentra registrdo</span>\
								  </div>\
								  ");
						$('#successbox').slideDown('slow');
						$('#enviando').hide();
					}
					if(response=="Cedula Existe"){
						$('#mensajes').html("\
								  <div class='alert alert-danger alert-dismissible fade in' id='successbox' style='display: none'>\
										<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a><span>La cédula ya se encuentra registrada</span>\
								  </div>\
								  ");
						$('#successbox').slideDown('slow');
						$('#enviando').hide();
					}
					if(response=="Error en Password"){
						$('#mensajes').html("\
								  <div class='alert alert-danger alert-dismissible fade in' id='successbox' style='display: none'>\
										<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
										<p>La contraseña debe cumplir con las siguientes características:</p>\
										<ul>\
											<li>La clave debe tener al menos 6 caracteres</li>\
											<li>La clave no puede tener más de 16 caracteres</li>\
											<li>La clave debe tener al menos una letra minúscula</li>\
											<li>La clave debe tener al menos una letra mayúscula</li>\
											<li>La clave debe tener al menos un caracter numérico</li>\
											<li>La clave debe tener al menos un caracter especial</li>\
										</ul>\
								  </div>\
								  ");
						$('#successbox').slideDown('slow');
						$('#enviando').hide();
					}
					if(response=="Error al enviar correo"){
						$('#mensajes').html("\
								  <div class='alert alert-danger alert-dismissible fade in' id='successbox' style='display: none'>\
										<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
										<span>Error al enviar correo, por favor intente mas tarde.</span>\
								  </div>\
								  ");
						$('#successbox').slideDown('slow');
						$('#enviando').hide();
					}
					if(response=="Correo enviado"){
						$('#mensajes').html("\
								  <div class='alert alert-success alert-dismissible fade in' id='successbox' style='display: none'>\
										<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
										<span>Se ha enviado un link a su direccion de correo.</span>\
								  </div>\
								  ");
						$('#successbox').slideDown('slow');
						$('#enviando').hide();
					}
				 }
			});
		}
		return false;

    });
	$('form[name="frmregistro"]').validate({
		rules: { 
			txtname: { required: true },
			txtemail: { required: true, email: true },
			txtsubject: { required: true },
			txtmessage: { required: true },
		},
		errorPlacement: function(error, element) { }
	});


//CONTACT FORM VALIDATION & MAIL SENDING....
	$('form[name="frmupdate"]').submit(function () {
		
		var This = $(this);
		if($(This).valid()) {
			var action = $(This).attr('action');
			$('#enviando').show('slow');
			var data_value = unescape($(This).serialize());
			$.ajax({
				 type: "POST",
				 url:action,
				 data: data_value,
				 error: function (xhr, status, error) {
					 confirm('Algo salió mal!');
				   },
				  success: function (response) {
					console.log("the response is", response);
					$('#mensajes').show();
					if(response=="Cedula Existe"){
						$('#mensajes').html("\
								  <div class='alert alert-danger alert-dismissible fade in' id='successbox' style='display: none'>\
										<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a><span>Esta cédula ya se encuentra registrada</span>\
								  </div>\
								  ");
						$('#successbox').slideDown('slow');
						$('#enviando').hide();
					}
					if(response=="Datos Actualizados"){
						$('#mensajes').html("\
								  <div class='alert alert-success alert-dismissible fade in' id='successbox' style='display: none'>\
										<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
										<span>Sus datos fueron actualizados con exito.</span>\
								  </div>\
								  ");
						$('#successbox').slideDown('slow');
						$('#enviando').hide();
					}
				 }
			});
		}
		return false;

    });
	$('form[name="frmupdate"]').validate({
		rules: { 
			nombres: { required: true },
			cedula: { required: true },
		},
		errorPlacement: function(error, element) { }
	});


	//CONSULTATION REQUEST & MAIL SENDING....
	$('#direct_consult, #phone_consult').click(function() {
			
		var This = $(this).parents('.consultation-form');
		if($(This).valid()) {
			var action = $(This).attr('action');

			var data_value = unescape($(This).serialize())+ "&consult_submit="+ $(this).attr("value");
			$.ajax({
				 type: "POST",
				 url:action,
				 data: data_value,
				 error: function (xhr, status, error) {
					 confirm('Algo salió mal!');
				   },
				  success: function (response) {
					$('#ajax_consultation_msg').html(response);
					$('#ajax_consultation_msg').slideDown('slow');
				 }
			});
		}
		return false;
		
    });
	$('form[name="frmconsultation"]').validate({
		rules: { 
			name: { required: true },
			emailid: { required: true, email: true },
			phone: { required: true }
		},
		errorPlacement: function(error, element) { }
	});

	//HAVE A QUESTION & MAIL SENDING....
	$('form[name="frmquestions"]').submit(function () {
				
		var This = $(this);
		if($(This).valid()) {
			var action = $(This).attr('action');

			var data_value = unescape($(This).serialize());
			$.ajax({
				 type: "POST",
				 url:action,
				 data: data_value,
				 error: function (xhr, status, error) {
					 confirm('Algo salió mal!');
				   },
				  success: function (response) {
					$('#ajax_questions_msg').html(response);
					$('#ajax_questions_msg').slideDown('slow');
				 }
			});
		}
		return false;
		
    });
	$('form[name="frmquestions"]').validate({
		rules: { 
			name: { required: true },
			emailid: { required: true, email: true },
			phone: { required: true }
		},
		errorPlacement: function(error, element) { }
	});
	
	//HELP FORM & MAIL SENDING....
	$('form[name="helpform"]').submit(function () {
				
		var This = $(this);
		if($(This).valid()) {
			var action = $(This).attr('action');
			$('#enviando1').show('slow');
			$('#enviando2').show('slow');
			var data_value = unescape($(This).serialize());
			$.ajax({
				 type: "POST",
				 url:action,
				 data: data_value,
				 error: function (xhr, status, error) {
					 confirm('Algo salió mal!');
				   },
				  success: function (response) {
					$('#ajax_helpform_msg1').html(response);
					$('#ajax_helpform_msg1').slideDown('slow');
					$('#enviando1').hide();

					$('#ajax_helpform_msg2').html(response);
					$('#ajax_helpform_msg2').slideDown('slow');
					$('#enviando2').hide();
				 }
			});
		}
		return false;
		
    });
	$('form[name="helpform"]').validate({
		rules: { 
			hf_first_name: { required: true },
			hf_last_name: { required: true },
		},
		errorPlacement: function(error, element) { }
	});


	//SUGERENCIA
	$('form[name="buzon"]').on('submit',(function(e) {
	    var This = $(this);
	    var action = $(This).attr('action');
		$('#enviando').show('slow');
		
		if($(This).valid()) {
            e.preventDefault();
            $.ajax({
                url: action,
                type: "POST",
                data:  new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                error: function (xhr, status, error) {
			        confirm('Algo salió mal!');
				},
				success: function (response) {
          if(response=="noespdf"){
            alert("!Solo se permiten arhivos PDF!");          
          }else if(response=="muygrande"){
            alert("!El archivo excede el límite máximo permitido de 1000000 Mb!");          
          }else if(response=="error"){
            alert("!Error al subir el archvio!");
          }else{
            confirm('!Su solicitud ha sido enviada!');
          }
          $('#enviando').hide();
				}         
      });
		}else{
      $('#enviando').hide();
    }
    }));

	$('form[name="buzon"]').validate({
		rules: { 
			nombres: { required: true },
			cedula: { required: true,},
			sector: { required: true },
			celular: { required: true },
			email: { 
				required: true, 
				email: true,
				/*
            remote: {
               url: "checkemail.php",
               type: "post",
               dataFilter: function(data) {
                  console.log(data);
                  if(data == 'Exists'){
                     return false;
                  }else{
                     return true;
                  }
               }
            },
				*/
			},
			tos: { required: true },
			medio_pago: { required: true },
			banco: { required: true },
			cuenta: { required: true },
			numero_cuenta: { required: true },
			/*
			ciudad: { 
				required: true,
            remote: {
               url: "checkciudad.php",
               type: "post",
               dataFilter: function(data) {
						console.log(data);
                  if(data == 'Exists'){
                     return true;
                  }else{
                     return false;
                  }
               }
            },
			},
			*/
			plan: { required: true },
		},
		errorPlacement: function(error, element) { }
	});
	
	//CONTACTO FORM & MAIL SENDING....
	$('form[name="contactoform"]').on('submit',(function(e) {
	    var This = $(this);
	    var action = $(This).attr('action');
		$('#enviando2').show('slow');
		
		if($(This).valid()) {
      e.preventDefault();
      $.ajax({
        url: action,
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        error: function (xhr, status, error) {
			    confirm('Algo salió mal!');
				},
				success: function (response) {
            if(response=="noespdf"){
              alert("!Solo se permiten arhivos PDF!");
            }else if(response=="muygrande"){
              alert("!El archivo excede el límite máximo permitido de 1000000 Mb!");
            }else if(response=="error"){
              alert("!Error al subir el archvio!");
            }else{
             confirm('!Su solicitud ha sido enviada!');
            }
            $('#enviando2').hide();
          }
      });
		}else{
      $('#enviando2').hide();
    }
  }));

	/*$('form[name="contactoform"]').submit(function () {
				
		var This = $(this);
		if($(This).valid()) {
			var action = $(This).attr('action');
			$('#enviando2').show('slow');
			var data_value = unescape($(This).serialize());
			$.ajax({
				 type: "POST",
				 url:action,
				 data:  data_value,
                 contentType: false,
                 cache: false,
                 processData: false,
				 error: function (xhr, status, error) {
					 confirm('Algo salió mal!');
				   },
				  success: function (response) {
					$('#ajax_contactoform_msg').html(response);
					$('#ajax_contactoform_msg').slideDown('slow');
					$('#enviando2').hide();
				 }
			});
		}
		return false;
		
    });*/
	$('form[name="contactoform"]').validate({
		rules: { 
			hf_tree_name: { required: true },
			hf_tree_email: { required: true },
			hf_tree_telefono: { required: true },
			hf_tree_mensaje: { required: true },
		},
		errorPlacement: function(error, element) { }
	});

	//CONTACTO GENERAL
	$('form[name="frmcontactogeneral"]').on('submit',(function(e) {
	  var This = $(this);
	  var action = $(This).attr('action');
		$('#espiral').show('slow');
		
		if($(This).valid()) {
            e.preventDefault();
            $.ajax({
                url: action,
                type: "POST",
                data:  new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                error: function (xhr, status, error) {
			            confirm('Algo salió mal!')
                  $('#espiral').hide();
				        },
                success: function (response) {
                  confirm('!Su consulta ha sido enviada!');
                  $('#espiral').hide();
                }        
            });
		}else{
      $('#espiral').hide();
    }
    return false;
    }));

	$('form[name="frmcontactogeneral"]').validate({
		rules: { 
			txtname: { required: true },
			txtemail: { required: true },
			txtsubject: { required: true },
			txtmessage: { required: true },
		},
		errorPlacement: function(error, element) { }
	});

	
	//Parallax Sections...
	$('.dt-sc-parallax-section').each(function(){
		$(this).bind('inview', function (event, visible) {
			if(visible == true) {
				$(this).parallax("50%", 0.3, true);
			} else {
				$(this).css('background-position','');
			}
		});
	});
	
	$("select[name=cause]").change(function(event){
		var total_items = $('.attorney_list .attorney_content').length;
		if($(this).val() == 'all') {
			$('.attorney_list .attorney_content').slideDown();
			$('.attorney_list .result-count').html('Showing ' + total_items + ' of ' + total_items);
		} else {
			var sorted_items = $('.attorney_list .'+$(this).val()).length;
			$('.attorney_list .attorney_content').slideUp();
			$('.attorney_list .'+$(this).val()).slideDown();
			$('.attorney_list .result-count').html('Showing ' + sorted_items + ' of ' + total_items);
		}
	});	

	$('#medio_pago').on('change',function(){
		var medio_pago = $(this).val();
		if(medio_pago == 2){
			$('.oculto').slideDown('slow');
		}else if(medio_pago == 1){
			$('.oculto').hide();
		}
	});

   //Captura el evento de borrar TDC
   $(document).on("click",".deltdc",function(){
      var token = $(this).attr('id');
      var parametros = {"token":token};
      $('#img'+token).show();
      $.ajax({
         data: parametros,
         url: 'deletecards.php',
         type: 'post',
         error: function (xhr, status, error) {
          confirm('Algo salió mal!');
         },
         success: function (response) {
            console.log("the response is", response);
            if(response=="card deleted"){
               $('.'+token).remove();
            }else{
               alert('No se pudo remover la tarjeta');
            }
         }
      });
   });

   //Pago con debidcard
   $('form[name="frmdebitcard"]').submit(function () {
      var This = $(this);
      var action = $(This).attr('action');
      $('#barra').show();
      var data_value = unescape($(This).serialize());
      $.ajax({
          type: "POST",
          url:action,
          data: data_value,
          error: function (xhr, status, error) {
             $('#barra').hide();
             confirm('Algo salió mal!');
            },
           success: function (response) {
            console.log("the response is", response);
            $('#barra').hide();
            if(response == 'notoken'){
               $('#mensajes').html("\
                       <div class='alert alert-danger alert-dismissible fade in' id='successbox' \
                           style='display: none; margin-bottom: 30px; margin-top: 15px; padding-right: 35px; padding-left: 35px; text-align: center;'>\
                           <a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
                           <span>Agregue una TDC para pagar.</span>\
                       </div>\
                       ");
               $('#successbox').slideDown();
            }else if(response == 'nopay'){
               $('#mensajes').html("\
                       <div class='alert alert-danger alert-dismissible fade in' id='successbox' \
                           style='display: none; margin-bottom: 30px; margin-top: 15px; padding-right: 35px; padding-left: 35px; text-align: center;'>\
                           <a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
                           <span>Su pago no pudo ser procesado.</span>\
                       </div>\
                       ");
               $('#successbox').slideDown();
            }else if(response == 'success'){
               window.location.replace("paysuccess.php");
            }
          }
      });
      return false;
   });

	
});

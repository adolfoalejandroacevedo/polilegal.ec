<!DOCTYPE HTML>
<html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
    <meta http-equiv="content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
    <title>Polilegal |Contacto</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link id="default-css" href="style.css" rel="stylesheet" type="text/css">
    <link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" /> 
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!--[if IE 7]>
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--Fonts-->
<link href='http://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--jquery-->
<script src="js/modernizr-2.6.2.min.js"></script>
</head>
<body>
	<!--wrapper starts-->
    <div class="wrapper">
        <!--inner-wrapper starts-->
        <div class="inner-wrapper">
            <!--header starts-->
            <header>
                <!--top-bar starts-->
                <div class="top-bar">
                    <div class="container">
                        <ul class="dt-sc-social-icons">
                            <li><a href="https://www.facebook.com/polilegal" title="Facebook"><span class="fa fa-facebook"></span></a></li>
                            <li><a href="https://www.linkedin.com/company/27118661/" title="Linkedin"><span class="fa fa-linkedin"></span></a></li>
                        </ul>
                        <div class="dt-sc-contact-number"> <span class="fa fa-phone"> </span> Contact Center: (02)3 825530<!--br><span class="telefonoLogo" style="margin-left: 65%;">(765453)</span--> </div>
                    </div>
                </div>
                
                <!--menu-container starts-->
                <div id="menu-container">
                    <div id="logo" style="width: 200px; height: 70px; position: absolute; left: 73%;">
                        <img src="images/logopolilegal.png" width="200px" height="60px" style="margin-top: 5px;"/>
                    </div>
                    <div class="container">
                        <!--nav starts-->
                        <nav id="main-menu">
                            <div class="dt-menu-toggle" id="dt-menu-toggle">Menu<span class="dt-menu-toggle-icon"></span></div>
                            <ul id="menu-main-menu" class="menu">
                                <li> <a href="index.html"> Inicio </a> </li>
                                <li> <a href="nosotros.html">Nosotros </a> </li>
                                <li class="menu-item-simple-parent menu-item-depth-0"> <a href="servicios01.html"> servicios </a> 
                                    <ul class="sub-menu">
                                        <li> <a href="servicios01.html"> Nuestros servicios </a> </li>
                                        <li> <a href="plan.html"> Plan de Asistencia Jurídica </a>  </li>
                                        <a class="dt-menu-expand">+</a>  
                                    </li>   
                                </ul>
                                <li><a href="actualidad.html" title="">Actualidad</a></li>
                		<li class="menu-item-simple-parent menu-item-depth-0">
                  		  <a href="plan.html"> Asistencias </a>
                  		  <ul class="sub-menu">
                    		    <li class="menu-item-simple-parent menu-item-depth-0">
                      		      <a href="plan.html"> Asistencia jurídica socios CPN </a> 
                      		      <ul class="sub-menu"> 
                          		<li> <a href="plan1.html"> Plan de Asistencia Jurídica para socios policías de la CPN </a> </li>
                          		<li> <a href="plan2.html"> Plan de Asistencia Jurídica para socios SP y civiles de la CPN </a> </li>
                          		<a class="dt-menu-expand">+</a>
                      		      </ul>
                    		    </li>
                    		    <a class="dt-menu-expand">+</a>
                  		  </ul>
                		</li>
                            <li class="current_page_item"> <a href="buzon.html"> Buzón </a> </li>
                            <li> <a href="contacto.html"> Contacto </a> </li>
                        <li class="menu-item-simple-parent menu-item-depth-0">
                            <a href=""> Socios </a>
                            <ul class="sub-menu">
                                <li> <a href="login.php"> Descargar Factura </a></li>
                            </ul>
                        </li>
                        </nav>
                        
                    </header>
                    <!--header ends-->
                    <!--main starts-->
                    <div id="main">
                        
                        <div class="breadcrumb-section">
                            <div class="container">
                                <h1> REGISTRO DE USUARIO </h1>
                                <div class="breadcrumb">
                                    <a href="index.html"> Inicio </a>
                                    <span class="current"> Registro </span>
                                </div>
                            </div>
                        </div>
                        
                        <!--container starts-->
                        <div class="container">	
                            
                            <!--primary starts-->
                            <section id="primary" class="content-full-width" style="margin-top: 10px;">
                                
                                <!--dt-sc-three-fourth starts-->
                                 <!--dt-sc-tabs-container ends-->
                             <!--dt-sc-three-fourth ends-->
                             <!--dt-sc-one-fourth starts-->
                            <!--dt-sc-one-fourth ends-->
                            
                  	  <div class="dt-sc-three-fourth column first">
                    	    <br>
                    	    <form name="frmregistro" method="post" class="contact-form" action="PHPMailer/examples/gmail.php" enctype="multipart/form-data">                                               <input type="hidden" name="tipo" value="6">
                      	      <p><input type="text" placeholder="Nombres y Apellidos" class="text_input" name="nombres" required /></p>
                      	      <p><input type="number" placeholder="Número de cédula" class="text_input" name="cedula" maxlength="15"required /></p>
                              <p><input type="email" placeholder="E-mail" class="text_input" name="email" required /></p>
                              <p><input type="email" placeholder="Repetir E-mail" class="text_input" name="email2" required /></p>
                              <p><input type="password" placeholder="Password" class="text_input" name="passwd" required /></p>
                              <p><input type="password" placeholder="Repetir Password" class="text_input" name="passwd2" required /></p>
							  <!-- Zona de mensajes -->
							  <div id="mensajes"></div>
							  <!-- Fin zona de mensajes -->
                              <p class="aligncenter">
                                <input name="submit" type="submit" id="submit" class="dt-sc-bordered-button" value="REGISTRAR">
                                <img id="enviando" src="images/loading.gif" width="100px" height="100px" 
									style="position: relative; vertical-align:middle; display: none;">
                              </p>
                            </form>
                          </div>

                            <div class="dt-sc-hr-invisible-large"></div>
                            
                            
                            
                        </section>
                        <!--primary ends-->
                        
                    </div>
                    <!--container ends-->
                    
                    
                </div>
                <!--main ends-->
                
                <!--footer starts-->
                <!--footer starts-->
                <footer>
                    <!--footer-widgets-wrapper starts-->
                    <div class="footer-widgets-wrapper">
                         <!--container starts-->
                        <div class="container">
                          <div class="footer_top"> </div>
                          <div class="column dt-sc-one-sixth first">
                            <aside class="widget widget_text">
                              <div class="widget_text_logo"> <img src="#" alt="" title="">
                                
                                <div class="dt-sc-hr-invisible-very-small"></div>
                                <p> <img src="images/logopl.png" <span class="fa fa-angle-double-right"></span> </strong> </a> </div>
                                </aside>
                              </div>
                              <div class="column dt-sc-two-sixth">
                                <aside class="widget widget_text">
                                  <h3 class="widgettitle">Detalles de contacto</h3>
                                  <div class="textwidget">
                                    <p class="dt-sc-contact-info"><span><i class="fa fa-print"></i> Contact Center: </span> (02)3 825530</p>
                                    <p class="dt-sc-contact-info"><span><i class="fa fa-phone"></i> Teléfono: </span>  (02) 3333533 </p>
                                    <p class="dt-sc-contact-info"><span><i class="fa fa-envelope"></i> E-mail: </span><a href="mailto:yourname@somemail.com"> info@polilegal.ec </a></p>
                                    <p class="dt-sc-contact-info"><i class="fa fa-location-arrow"></i>Pasaje El Jardín No. 168 y Av. 6 de Diciembre <br>
                                      (frente al Megamaxi) <br>
                                      Edificio Century Plaza I, piso 5, Of. 14. <br>
                                      Quito, Ecuador</p>
                                    </div>
                                  </aside>
                                </div>
                                <div class="column dt-sc-one-sixth">
                                  <aside class="widget widget_text">
                                    <h3 class="widgettitle"> Servicios </h3>
                                    <ul>
                                      <li> <a href="servicios01.html"> ADMINISTRATIVOS </a> </li>
                                      <li> <a href="servicios01.html"> ADMINISTRATIVOS POLICIALES </a> </li>
                                      <li> <a href="servicios01.html"> CONSTITUCIONAL </a> </li>
                                      <li> <a href="servicios01.html"> CIVIL Y DE FAMILIA </a> </li>
                                      <li> <a href="servicios01.html"> PENAL Y TRÁNSITO </a> </li>
                                      <li> <a href="servicios01.html"> SOCIETARIO Y CORPORATIVO </a> </li>
                                      <li> <a href="servicios01.html"> ARBITRAJE Y MEDIACIÓN </a> </li>
                                      <li> <a href="servicios01.html"> ASUNTOS REGULATORIOS </a> </li>
                                    </ul>
                                  </aside>
                                </div>
                                <div class="column dt-sc-two-sixth">
                                  <aside class="widget widget_text">
                                    <h3 class="widgettitle"> VIDEOS </h3>
                                    <iframe width="300" height="200" src="https://www.youtube.com/embed/6IEDnTE9DQw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> 
                                 <!--   <video class="motionpicture" width="300" height="200" autobuffer controls preload="auto">
                                        <source src="VideoPolilegal.mp4" />
                                        Your browser does not appear to support HTML5 media. Try updating
                                        your browser or (if you are not already) using an open source browser like Firefox.
                                    </video>
                                -->
                                  </aside>
                                </div>
                              </div>
                              <!--container ends--> 
                </div>
                <!--footer-widgets-wrapper ends-->  
                <div class="copyright">
                    <div class="container">
                        <div class="copyright-info">Polilegal © 2017 <a href="http://polilegal.ec./" target="_blank"> www.polilegal.ec </a> Todos los derechos reservados. </div>
                        
                    </ul>
                    <p> Design by <a href="http://themeforest.net/user/buddhathemes" target="_blank" title="">BigDesign</a> </p>
                </div>
            </div>  
        </footer>
        <!--footer ends-->
    </div>
    <!--inner-wrapper ends-->    
</div>
<!--wrapper ends-->
<div class="fixed-help-form">
  <div class="fixed-help-form-icon"> <img src="images/fixed-help-form-icon.png" alt="" title=""> </div>
  <h4> Afiliate </h4>
  <p> Comienza el proceso de afiliación llenando los siguientes datos </p>
  <p> <span> Gracias por confiar en nosotros </span> </p>
  <form name="helpform" method="post" class="help-form" action="php/helpform.php">
    <p>
      <input type="text" placeholder="Nombres" class="text_input" name="hf_first_name" required />
  </p>
  <p>
      <input type="text" placeholder="Apellidos" class="text_input" name="hf_last_name" required />
  </p>
  <p>
      <input type="text" placeholder="Teléfono" class="text_input" name="hf_last_name" required />
  </p>

<div id="ajax_helpform_msg"> </div>
<p>
  <input type="submit" value="&#xf002; &nbsp; Enviar">
</p>
</form>
</div>
<a href="" title="Go to Top" class="back-to-top"> <span class="fa fa-angle-up"></span> </a>    <!--Java Scripts-->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-migrate.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-easing-1.3.js"></script>
<script type="text/javascript" src="js/jquery.sticky.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>


<script type="text/javascript" src="js/jquery.tabs.min.js"></script>
<script type="text/javascript" src="js/jquery.smartresize.js"></script> 
<script type="text/javascript" src="js/shortcodes.js"></script>   

<script type="text/javascript" src="js/custom.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDz1RGAW3gGyDtvmBfnH2_fE2DVVNWq4Eo&callback=initMap" type="text/javascript"></script>
<script src="js/gmap3.min.js"></script>
<!-- Layer Slider --> 
<script type="text/javascript" src="js/jquery-transit-modified.js"></script> 
<script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script> 
<script type='text/javascript' src="js/greensock.js"></script> 
<script type='text/javascript' src="js/layerslider.transitions.js"></script> 

<script type="text/javascript">var lsjQuery = jQuery;</script><script type="text/javascript"> lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice('layerslider_2','jquery'); } else { lsjQuery("#layerslider_2").layerSlider({responsiveUnder: 1240, layersContainer: 1170, skinsPath: 'js/layerslider/skins/'}) } }); </script>

<script>
	jQuery(document).ready(function() {
		jQuery("#mensajes").click(function(){
			jQuery("#mensajes").hide();
		});
	});
</script>

</body>
</html>

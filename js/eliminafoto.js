
// Elimina foto
$(document).ready(function() {
	
	// Si el div fotos que contiene la palabra Eliminar el clicked
    $('.fotos').on('click',(function () {
	
		//Obtiene el id del div de clase fotos		
        var id = $(this).attr('data-idfoto');

        //start the ajax:
        $.ajax({
            //this is the php file that processes the data and sends email
            url: "eliminafoto.php",

            //GET method is used
            type: "POST",

            //pass the data         
            data: {code: id},

            //Tipo de datos
            dataType: 'jsonp',

            //Do not cache the page
            cache: false,

            //Timeout
            timeout: 60000,

            //success
            success: function(data, textStatus, jqXHR) {

                console.log("the response is", data);
				if (data.OK == "0") {
					alert("Ocurrio un error inesperado al momento de borrar la imagen");
				}else {
					//Vacia el div de imagenes	
					$('#portafolio').empty();
					//Llena el div de imagenes con las fotos
					$('#portafolio').html(data.html);
					//Refrescamos la pagina
					location.reload(true);
				}
            },
            error: function (data, textStatus, errorThrown)
            {
                console.warn(data, textStatus, errorThrown);
            }
        });

        //cancel the submit button default behaviours
        return false;
    }));
});

                    <footer> 
                      <!--footer-widgets-wrapper starts-->
                      <div class="footer-widgets-wrapper"> 
                        <!--container starts-->
                        <div class="container">
                          <div class="footer_top"> </div>
                          <div class="column dt-sc-two-sixth first">
                            <aside class="widget widget_text">
                              <div class="widget_text_logo"> <img src="#" alt="" title="">
                                
                                <div class="dt-sc-hr-invisible-very-small"></div>
                                <p> <img src="images/logoblancotransp.png" /></strong> </a> </div>
                                </aside>
                              </div>
                              <div class="column dt-sc-one-sixth">
                                <aside class="widget widget_text">
                                  <h3 class="widgettitle">Detalles de contacto</h3>
                                  <div class="textwidget">
                                    <p class="dt-sc-contact-info"><span><i class="fa fa-print"></i> Contact Center: </span> (02)3 825530</p>
                                    <p class="dt-sc-contact-info"><span><i class="fa fa-phone"></i> Tel&eacute;fono: </span>  (02) 3333533 </p>
                                    <p class="dt-sc-contact-info"><span><i class="fa fa-envelope"></i> E-mail: </span><a href="mailto:yourname@somemail.com"> info@polilegal.ec </a></p>
                                    <p class="dt-sc-contact-info"><i class="fa fa-location-arrow"></i>Pasaje El Jardín No. 168 y Av. 6 de Diciembre <br>
                                      (frente al Megamaxi) <br>
                                      Edificio Century Plaza I, piso 5, Of. 14. <br>
                                      Quito, Ecuador</p>
                                    </div>
                                  </aside>
											<aside class="widget widget_text">
												<h3 class="widgettitle" style="margin-bottom: 8px;">Facturación Electrónica</h3>
												<ul>
													<li> <a href="login.php"> INGRESO AFILIADOS </a> </li>
													<?php
														if (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] == 1) {	
															echo '<li> <a href="closesession.php" id="close_session"> CERRAR SESION </a> </li>';
														}
													?>
												</ul>
											</aside>
                                </div>
                                <div class="column dt-sc-one-sixth">
                                  <aside class="widget widget_text">
                                    <h3 class="widgettitle"> Servicios </h3>
                                    <ul>
                                      <li> <a href="servicios01.php"> ADMINISTRATIVO </a> </li>
                                      <li> <a href="servicios01.php"> CONSTITUCIONAL </a> </li>
                                      <li> <a href="servicios01.php"> CIVIL Y FAMILIA </a> </li>
                                      <li> <a href="servicios01.php"> PENAL Y TRÁNSITO </a> </li>
                                      <li> <a href="servicios01.php"> ADMINISTRATIVO POLICIAL </a> </li>
                                      <li> <a href="servicios01.php"> SOCIETARIO Y CORPORATIVO </a> </li>
                                      <li> <a href="servicios01.php"> ARBITRAJE Y MEDIACIÓN </a> </li>
                                      <li> <a href="servicios01.php"> ASUNTOS REGULATORIOS </a> </li>
                                    </ul>
                                  </aside>
                                </div>
                                <div class="column dt-sc-two-sixth">
                                  <aside class="widget widget_text">
                                    <h3 class="widgettitle"> VIDEOS 
                                  </aside>
											<video width="300px" height="auto" controls>
											  <source src="images/video1.webm" type="video/webm">
												Your browser does not support the video tag.
											</video>
                                </div>
                              </div>
                              <!--container ends--> 
                            </div>
                            <!--footer-widgets-wrapper ends-->
                            <div class="copyright">
                              <div class="container">
                                <div class="copyright-info">Polilegal © 2017 <a href="http://polilegal.ec./" target="_blank"> www.polilegal.ec </a> Todos los derechos reservados. </div>
                              </ul>
                              <p> Design by <a href="http://themeforest.net/user/buddhathemes" target="_blank" title="">BigDesign</a> </p>
                            </div>
                          </div>
                        </footer>

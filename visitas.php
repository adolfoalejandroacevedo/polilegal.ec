<?php
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);

   //Contador de visitas general
	$sql = "SELECT COUNT(id) AS visitas FROM visitas WHERE iduser='0'";
   if($result = mysqli_query($conn, $sql)){
      $row = mysqli_fetch_assoc($result);
      $visitas = $row['visitas'];
   }else error_log("Error: " . $sql . "..." . mysqli_error($conn));

	//Contador de visitas del abogado
	if(isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'])
   	$sql = "SELECT COUNT(id) AS visitas FROM visitas WHERE iduser='$_SESSION[id]'";
   if($result = mysqli_query($conn, $sql)){
      $row = mysqli_fetch_assoc($result);
      $visitasabogado = $row['visitas'];
   }else error_log("Error: " . $sql . "..." . mysqli_error($conn));
?>

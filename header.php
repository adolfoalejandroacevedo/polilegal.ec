      <?php include "detect_mobile.php"; ?>
      <header>
        <!--top-bar starts-->
        <div class="top-bar" style="background-color: #f2f2f2;">
          <div class="container">
            <ul class="dt-sc-social-icons">
              <li><a href="https://www.facebook.com/polilegal" title="Facebook"><span class="fa fa-facebook" style="color: #000000;"></span></a></li>
              <li><a href="https://www.linkedin.com/company/27118661/" title="Linkedin"><span class="fa fa-linkedin" style="color: #000000;"></span></a></li>
            </ul>
            <div class="dt-sc-contact-number" style="color: #000000;"> <span class="fa fa-phone" style="color: #000000;"> </span> Contact Center: (02)3 825530<!--br><span class="telefonoLogo" style="margin-left: 65%;">(765453)</span--> </div>
          </div>
        </div>
        <!--top-bar ends--> 

        <!--menu-container starts-->
        <div id="menu-container">
          <div id="logo" class="logodiv">
            <img src="images/logoblancotransp2.png" class="logoheader"/>
          </div>
          <div class="container"> 
            <!--nav starts-->
            <nav id="main-menu">
              <div class="dt-menu-toggle" id="dt-menu-toggle">Menu<span class="dt-menu-toggle-icon"></span></div>
              <ul id="menu-main-menu" class="menu">
                <li <?php echo (isset($current) && $current == "inicio") ? 'class="current_page_item"':''; ?>> <a href="index.php"> Inicio </a> </li>
                <li <?php echo (isset($current) && $current == "nosotros") ? 'class="current_page_item"':''; ?>> <a href="nosotros.php">Nosotros </a> </li>
                <li <?php echo (isset($current) && $current == "servicios") ? 'class="current_page_item"':''; ?>><a href="servicios01.php"> servicios </a>
                </li>
                <li <?php echo (isset($current) && $current == "actualidad") ? 'class="current_page_item"':''; ?>><a href="actualidad.php" title="">Actualidad</a></li>
                <li class="menu-item-simple-parent menu-item-depth-0 <?php echo (isset($current) && $current == "asistencias") ? 'current_page_item':''; ?>">
                  <a href="#"> Asistencias </a>
                  <?php
                    if ($_SESSION['mobile']){ ?>
                      <a href="plan-polilegal-personal.php"> Plan Polilegal PERSONAL </a>
                      <a href="planes-policiales.php"> Plan Polilegal POLICIAL </a>
                      <a href="plan-polilegal-empresarial.php"> Plan Polilegal EMPRESARIAL </a>
                      <a href="plan-polilegal-emprendedor.php"> Plan Polilegal EMPRENDEDOR </a>
                  <?php } ?> 
                  <ul class="sub-menu">
		    		 		<li class="menu-item-simple-parent menu-item-depth-0">
                  		<a href="plan-polilegal-personal.php"> Plan Polilegal PERSONAL </a> 
                  		<a href="planes-policiales.php"> Plan Polilegal POLICIAL </a> 
                  		<a href="plan-polilegal-empresarial.php"> Plan Polilegal EMPRESARIAL </a> 
                  		<a href="plan-polilegal-emprendedor.php"> Plan Polilegal EMPRENDEDOR </a> 
								<!--
		      	 			<ul class="sub-menu"> 
                  			<li> <a href="planes-de-asistencia.php"> Plan de Asistencia Jurídica policías y familiares </a> </li>
		          				<a class="dt-menu-expand">+</a>
		      	 			</ul>
								-->
		    				</li>
                    	<a class="dt-menu-expand">+</a>
                  </ul>
                </li>
                <li <?php echo (isset($current) && $current == "buzon") ? 'class="current_page_item"':''; ?>> <a href="buzon.php"> Buzón </a> </li>
                <li <?php echo (isset($current) && $current == "contacto") ? 'class="current_page_item"':''; ?>> <a href="contacto.php">Contacto</a></li>
              </nav>
            </header>

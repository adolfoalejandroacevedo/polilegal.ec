<?php session_start(); ?>
<?php
	header('Content-Type: text/html; charset=utf-8');
	$iduser = $_GET['iduser'];
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
	mysqli_set_charset($conn, "utf8");
/*
   $sql = "SELECT users.id id, nombres, ciudad, planes.plan plan 
           FROM users 
           INNER JOIN planes 
           ON users.plan=idplan 
           WHERE users.id='$_SESSION[id]'";
   if ($result = mysqli_query($conn, $sql)){
      $row = mysqli_fetch_assoc($result);
   }else error_log("Error: " . $sql . "..." . mysqli_error($conn));
*/
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
	 <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Gp Bootstrap Template</title>
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
    	<!-- Bootstrap -->
    	<link href="css/bootstrap.min.css" rel="stylesheet">
   	<link rel="stylesheet" href="gp/css/font-awesome.min.css">
   	<link href="gp/css/animate.min.css" rel="stylesheet">
    	<link href="gp/css/prettyPhoto.css" rel="stylesheet">      
   	<link href="css/main.css" rel="stylesheet">
    	<link href="gp/css/responsive.css" rel="stylesheet">
      <!-- JQuery Validator and form -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.js"></script>


	 <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       

	<style>
		.btn-primary {
			background: #c4b504;
		}
		/* Color rojo para el texto de error de los campos */
		.error{
			color: red;
		}
		/* Borde rojo y grosor de linea de los inputs */
		.infogeneral-form input[type=email].error,
		.infogeneral-form input[type=text].error{
			padding:15px 18px;
			border:1px solid #FF0000;
		}
		.infogeneral-form select.error{
			padding:15px 18px;
			border:1px solid #FF0000;
		}
		textarea.error {
      	border:1px solid #FF0000;
      }
	</style>

  </head>
  <body class="homepage" style="background: #f9f9f9">   
		<?php include "header.php"; ?>
      <!--/header-->

	<section id="portfolio">
		<div class="container wow fadeInDown">
		<div class="row">
			<div class="col-sm-12">
				<form id="ratingForm" class="infogeneral-form" method="POST">
					<div class="form-group">
						<h4>Calificación del Abogado</h4>
						<button type="button" class="btn btn-warning btn-sm rateButton" aria-label="Left Align">
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						</button>
						<button type="button" class="btn btn-default btn-grey btn-sm rateButton" aria-label="Left Align">
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						</button>
						<button type="button" class="btn btn-default btn-grey btn-sm rateButton" aria-label="Left Align">
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						</button>
						<button type="button" class="btn btn-default btn-grey btn-sm rateButton" aria-label="Left Align">
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						</button>
						<button type="button" class="btn btn-default btn-grey btn-sm rateButton" aria-label="Left Align">
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						</button>
						<input type="hidden" class="form-control" id="rating" name="rating" value="1">
						<input type="hidden" class="form-control" id="iduser" name="iduser" value="<?php echo $iduser; ?>">
					</div>
					<div class="form-group">
						<label for="usr">Nombre completo*</label>
						<input type="text" class="form-control" id="title" name="title" required>
					</div>
					<div class="form-group">
						 <label>Email *</label>
						 <input type="email" name="email" class="form-control" required="required">
					</div>
					<div class="form-group select">
						<label>Ciudad *</label>
						<select name="ciudad" class="form-control"  id="ciudad">
							<option value="">--Selccionar una ciudad --</option>
								<?php
									$sql = "SELECT * FROM ciudades WHERE importancia='1' ORDER BY ciudad";
									if ($result = mysqli_query($conn, $sql)){
										while ($row = mysqli_fetch_assoc($result)) {
											echo "<option value='$row[id]'>".$row['ciudad']."</option>";
										}
									}else{
										error_log("Error: " . $sql . "..." . mysqli_error($conn));
									}
								?>
						</select>
					</div>
					<div class="form-group">
						<label for="comment">Comentario* (maximo 340 caracteres)</label>
						<textarea class="form-control" rows="5" id="comment" name="comment" maxlength="340" required></textarea>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-lg" id="saveReview">Enviar</button> <button type="button" class="btn btn-primary btn-lg" id="cancelReview">Cancelar</button>
					 	<img id="enviando" src="images/barra.gif" width="100px" height="25px" 
							  style="position: relative; vertical-align:middle; display: none;">
						<!-- Zona de mensajes -->
						<div id="mensajes"></div>
						<!-- FIN Zona de mensajes -->
					</div>
				</form>
			</div>
		</div>
		</div>
    </section><!--/#portfolio-item-->

      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
	
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="gp/js/bootstrap.min.js"></script>
    <script src="gp/js/jquery.prettyPhoto.js"></script>
    <script src="gp/js/jquery.isotope.min.js"></script>
    <script src="gp/js/wow.min.js"></script>
    <script src="gp/js/main.js"></script>
	 <script src="js/eliminafoto.js"></script>

	<script>
		$(function() {
			// rating form hide/show
			$( "#rateProduct" ).click(function() {
				$("#ratingDetails").hide();
				$("#ratingSection").show();
			});
			$( "#cancelReview" ).click(function() {
				window.location.href = 'perfil.php?iduser=<?php echo $iduser; ?>';
			});
			// implement start rating select/deselect
			$( ".rateButton" ).click(function() {
				if($(this).hasClass('btn-grey')) {
					$(this).removeClass('btn-grey btn-default').addClass('btn-warning star-selected');
					$(this).prevAll('.rateButton').removeClass('btn-grey btn-default').addClass('btn-warning star-selected');
					$(this).nextAll('.rateButton').removeClass('btn-warning star-selected').addClass('btn-grey btn-default');
				} else {
					$(this).nextAll('.rateButton').removeClass('btn-warning star-selected').addClass('btn-grey btn-default');
				}
				$("#rating").val($('.star-selected').length);
			});
			// save review using Ajax
			$('#ratingForm').on('submit', function(event){
				event.preventDefault();
				var This = $(this);
				if($(This).valid()) {
					$('#enviando').show();
					var formData = $(this).serialize();
					$.ajax({
						type : 'POST',
						url : 'saveRating.php',
						data : formData,
						success:function(response){
							console.log("la respuesta es: "+response);
							$('#enviando').hide();
							if(response=="rating saved!"){
								$('#mensajes').html("\
										  <div class='alert alert-success alert-dismissible fade in' id='successbox2' \
												style='display: none; margin-bottom: 30px; margin-left: 30px; margin-right: 30px'>\
												<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
												<span>Su calificación ha sido enviada satisfactoriamente.</span>\
										  </div>\
										  ");
								$('#successbox2').slideDown();
							}else{
								$('#mensajes').html("\
										  <div class='alert alert-danger alert-dismissible fade in' id='successbox2' \
												style='display: none; margin-bottom: 30px; margin-left: 30px; margin-right: 30px'>\
												<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
												<span>Error Inesperado. Por favor intente mas tarde.</span>\
										  </div>\
										  ");
								$('#successbox2').slideDown();
							}
							$("#ratingForm")[0].reset();
							window.setTimeout(function(){window.location.reload()},3000)
						}
					});
				}
				return false;
			});

			jQuery("#ratingForm").validate({
				rules: {
					title: "required",
					email: {
						required: true,
						email: true
					},
					ciudad: "required",
					comment: "required",
				},
				messages: {
					title: "Este campo es requerido",
					email: {
						required: "Este campo es requerido",
						email: "Introduzca un correo valido"
					},
					ciudad: "Este campo es requerido",
					comment: "Este campo es requerido"
				},
			});

		});
	</script>

  </body>
</html>

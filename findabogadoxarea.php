<?php session_start(); ?>
<?php
	header('Content-Type: text/html; charset=utf-8');
	include "detect_mobile.php";
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
	mysqli_set_charset($conn, "utf8");

	//Variables del post
	$keyword = (isset($_POST['keyword'])) ? $_POST['keyword']:"";
	$ciudad	= (isset($_POST['ciudad'])) ? $_POST['ciudad']:"";
	$area = (isset($_POST['area'])) ? $_POST['area']:"";

	/* Las posibles 8 consultas
		Las posibles consultas
		clave ciudad   area
		0  	0  		0
		1  	0  		0
		0  	1  		0
		1  	1  		0
		0  	0  		1
		1  	0  		1
		0  	1  		1
		1  	1  		1
	*/

	if($keyword == "" && $ciudad == "" && $area == ""){
		$sql = "SELECT t1.id iduser, nombres, t1.ciudad ciudadid, t2.ciudad ciudadname, foto 
					FROM users t1 
					INNER JOIN ciudades t2
					ON t1.ciudad=t2.id";
	}
   if($keyword == "" && $ciudad == "" && $area != ""){
      $sql = "SELECT t1.id iduser, nombres, t1.ciudad ciudadid, t2.ciudad ciudadname, foto 
               FROM users t1 
               INNER JOIN ciudades t2
               ON t1.ciudad=t2.id
               WHERE t1.id IN (
						SELECT iduser 
						FROM usersareas 
						WHERE idarea='$area'
					)";
   }

	if(!$users = mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));	

?>

<!DOCTYPE html>
<html lang="en">
  <head>
	 <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Plataforma Digital de Abogados del Ecuador | Abogados por &Aacute;rea</title>
      <meta name="keywords" content="abogado, abogada, abogados, abogadas, area, busqueda, buscar, necesito, encontrar, abogados, areas, contratar, ecuador, precio, costo, bajo, contacto, seriedad, materia, juridico, legal, administrativo, civil, legal, constitucional, arbitraje,
                                    penal, financiero, familia, migratorio, mediación, mercantil, comercial, aeronáutico, ambiental, antipirateria, bancario, seguros, comercio, electrónico, concursal, consultoria, contratacion, publica, corporativo, económico, energético, industrial, internacional, inversiones, marítimo, laboral, mercado, valores, policial, propiedad, intelectual, industrial, tributario, tecnología, quito, guayaquil, portoviejo, babahoyo, puyo, tena" />
      <meta name="description" content="Búsqueda de abogados en Ecuador por área. Seleccione una área de especialización para encontrar un abogado." />
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
   <link rel="stylesheet" href="gp/css/font-awesome.min.css">
   <link href="gp/css/animate.min.css" rel="stylesheet">
    <link href="gp/css/prettyPhoto.css" rel="stylesheet">      
   <link href="css/main.css" rel="stylesheet">
    <link href="gp/css/responsive.css" rel="stylesheet">

	 <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       

      <style>
         .file_drag_area
         {
            width:600px;
            height:105px;
            border:2px dashed #ccc;
            /*line-height:30px;*/
            text-align:center;
            font-size:16px;
            margin:0 auto;
         }
         .file_drag_area p{
            margin: 0 0 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
            margin-left: 0px;
         }
         .file_drag_over{
            color:#000;
            border-color:#000;
         }
         .multiple_files{
            cursor: pointer;
         }
			.tempforSize {display: inline-block !important;}
      </style>    
 
  </head>
  <body class="homepage">   
<?php include "header.php"; ?>
      <!--/header-->

	<section id="portfolio" style="padding-bottom: 100px; padding-top: 80px;">
        <div class="container">
            <div class="center" style="padding-bottom: 0px;">
					<?php
						$sql = "SELECT area
									FROM areas
									WHERE id='$area'";
						if ($result = mysqli_query($conn, $sql)){
							$row2 = mysqli_fetch_assoc($result);
						}else error_log("Error: " . $sql . "..." . mysqli_error($conn)); 
						$areaname = ($area == "") ? "Todas":$row2['area'];
					?>
               <h2>Abogados por área: "<?php echo $areaname; ?>"</h2>
            </div>

			<?php if(!$_SESSION['mobile']) { ?>
				<!--/#portfolio-filter-->
            <ul class="portfolio-filter text-center" style="margin-bottom: 30px;">
                <li><a class="btn btn-default active" href="#" data-filter="*">Todas las ciudades</a></li>
						<?php 
							$sql = "SELECT id,
												ciudad
										FROM ciudades
										WHERE importancia='1'
										ORDER BY ciudad";
							if ($result = mysqli_query($conn, $sql)){
								while($row2 = mysqli_fetch_assoc($result)){
					 	?>
                				<li><a class="btn btn-default" href="#" data-filter=".<?php echo $row2['id']; ?>"><?php echo $row2['ciudad']; ?></a></li>
						<?php }
							}else error_log("Error: " . $sql . "..." . mysqli_error($conn)); 
						?>
						
            </ul><!--/#portfolio-filter-->
			<?php } ?>

            <!-- Select Filter (Sirve para seleccionar otras ciudades, para activar cambie display:none por display:blocked en la siguiente linea) -->
            <div class="row" style="margin-bottom: 25px; display:none">
               <div class="col-md-4 col-md-offset-4" align="center">
                  <form id="frmfindciudad" class="infogeneral-form" name="frmfindciudad" method="post">
                      <input type="hidden" name='keyword' value="">
                      <input type="hidden" name='area' value="">
                      <select name="ciudad" class="form-control ddList centerSelect" id="filter-select">
                        <option value="" SELECTED>-- <?php echo ($_SESSION['mobile']) ? "SELECCIONE UNA CIUDAD":"OTRAS CIUDADES"; ?> --</option>
                        <?php
									$ids = " ";
                           $sql = "SELECT * FROM ciudades ORDER BY ciudad";
                           if ($result = mysqli_query($conn, $sql)){
                              while ($row2 = mysqli_fetch_assoc($result)) {
                                 echo "<option value='.$row2[id]'>".$row2['ciudad']."</option>";
											$ids = $ids.$row2['id']." ";
                              }
                           }else{
                              error_log("Error: " . $sql . "..." . mysqli_error($conn));
                           }
                        ?>
                     </select>
                  </form>
               </div>
            </div>
            <!-- End Select Filter -->

            <div class="row">
                <div class="portfolio-items" id="portfolio-items">
							<?php
								while($row = mysqli_fetch_assoc($users)) {
									$sql = "SELECT idarea, area 
												FROM usersareas 
												INNER JOIN areas 
												ON idarea=areas.id 
												WHERE iduser='$row[iduser]'";
									if ($result = mysqli_query($conn, $sql)){
										$areasid = "";
										$areasname = "";
										$cont = 1;
										while($row2 = mysqli_fetch_assoc($result)){
											$areasid .= $row2['idarea']." ";
											if($cont < 8){
												$areasname .= "* ".$row2['area']." ";
											}else if ($cont == 8){
												$areasname .= "* Mas áreas dentro del perfil......";
											}
											$cont += 1;
										}
									}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
							?>
								  <div class="portfolio-item <?php echo $row['ciudadid']; ?> col-xs-12 col-sm-4 col-md-3">
										<div class="recent-work-wrap">
											<?php if(is_null($row['foto'])) $foto = "images/human.jpg"; else $foto = "uploads/$row[foto]"; ?>
											 <img class="img-responsive" src="<?php echo $foto; ?>" alt="">
											 <div class="overlay">
												  <div class="recent-work-inner">
														<h3><a href="perfil.php?iduser=<?php echo $row['iduser']; ?>">
														<?php echo $row['nombres']; ?></a></h3>
														<p>Ciudad: <?php echo $row['ciudadname']; ?></p>
														<p>Areas: <?php echo trim($areasname); ?></p>
														<a class="preview" href="<?php echo $foto; ?>" rel="prettyPhoto"><i class="fa fa-eye"></i> Ampliar</a>
												  </div>
											 </div>
										</div>
								  </div><!--/.portfolio-item-->
							<?php } ?>

                    <div class="portfolio-item <?php echo $ids; ?>col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="img/encontacto.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#">Sigue en Contacto</a></h3>
                                    <p>Tuabogado.ec crece cada día con nuevos profesionales calificados, sigue en contacto.</p>
                                    <a class="preview" href="img/encontacto.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i> Ampliar</a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
                </div>
            </div>
            <div class="row" align="center">
					<p>NOTA: Si existe una ciudad que no aparezca en esta página, por favor ponerse en contacto con nosotros.</p>
				</div>
        </div>

    </section><!--/#portfolio-item-->

      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
	
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="gp/js/jquery.js"></script>
    <script src="gp/js/bootstrap.min.js"></script>
    <script src="gp/js/jquery.prettyPhoto.js"></script>
    <script src="gp/js/jquery.isotope.min.js"></script>
    <script src="gp/js/wow.min.js"></script>
    <script src="gp/js/main.js"></script>

	<!-- Centrado del Select -->
	<script>
		function getTextWidth(txt) {
		  var $elm = $('<span class="tempforSize">'+txt+'</span>').prependTo("body");
		  var elmWidth = $elm.width();
		  $elm.remove();
		  return elmWidth;
		}
		function centerSelect($elm) {
			 var optionWidth = getTextWidth($elm.children(":selected").html())
			 var emptySpace =   $elm.width()- optionWidth;
			 $elm.css("text-indent", (emptySpace/2) - 10);// -10 for some browers to remove the right toggle control width
		}
		// on start 
		$('.centerSelect').each(function(){
		  centerSelect($(this));
		});
		// on change
		$('.centerSelect').on('change', function(){
		  centerSelect($(this));
		});
	</script>

	<!-- Filtering del Select -->
	<script type="text/javascript">
		$( function () {
		  var $container = $('#portfolio-items');
		  $container.isotope({})
		  $('#filter-select').change( function() {
			 $container.isotope({
				filter: this.value
			 });
		  });
		});
	</script>

  </body>
</html>

<?php session_start(); ?>
<?php
header('Content-Type: text/html; charset=utf-8');
$config = require 'config.php';

//Abriendo conexion a BD
$conn=mysqli_connect($config['database']['server'],
					 $config['database']['username'],
					 $config['database']['password'],
					 $config['database']['db']);
if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
mysqli_set_charset($conn, "utf8");

//Variables del POST
$username = $_POST['username'];
$name = mysqli_real_escape_string($conn,utf8_decode($_POST['name']));
$email = $_POST['email'];
$phone = mysqli_real_escape_string($conn,utf8_decode($_POST['phone']));
$consulta = mysqli_real_escape_string($conn,utf8_decode($_POST['consulta']));
$iduser = $_POST['iduser'];

//Guardamos la info en la BD
$sql = "INSERT INTO cotizaciones (
				userid,
				nombre,
				email,
				telefono,
				consulta)
			VALUES (
			'$iduser',
			'$name',
			'$email',
			'$phone',
			'$consulta')";
if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

//Buscamos el nombre y apellido del abogado
$sql = "select nombres from users where email='$username'";
if($result = mysqli_query($conn, $sql)){
	while ($row = mysqli_fetch_assoc($result)) {
		$nombres = $row['nombres'];
	}
}else {
	error_log("Error: " . $sql . "..." . mysqli_error($conn));
	echo "Error Inesperado";
	exit;
}

//Enviamos correo
require 'phpmailer/PHPMailerAutoload.php';
include("email3.php");

$message = $cuerpo;

$subject = "TUABOGADO.EC - Solicitud de Cotización";

$mail = new PHPMailer;

$mail->CharSet = 'UTF-8';

$mail->isSMTP();

//$mail->Host = 'smtp.gmail.com';
//$mail->Host = 'ssl://md-100.webhostbox.net';
$mail->Host = $config['general']['mailserver'];

//$mail->Port = 587;
$mail->Port = 465;

$mail->SMTPSecure = 'ssl';

$mail->SMTPAuth = true;

//$mail->Username = $email;
$mail->Username = $config["general"]["username"];

//$mail->Password = $password;
$mail->Password = $config["general"]["password"];

$mail->setFrom('info@tuabogado.ec', 'TUABOGADO.EC');

$mail->addReplyTo('noreplyto@tuabogado.ec', 'TUABOGADO.EC');

$mail->addAddress($username);

$mail->Subject = $subject;

$mail->msgHTML($message);

if (!$mail->send()) {
   $error = "Mailer Error: " . $mail->ErrorInfo;
   error_log($error, 0);
   echo "Error Inesperado";
}else {
	echo "Enviado";
}

?>

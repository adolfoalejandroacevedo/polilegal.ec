<?php 
   session_start();
   session_unset();
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
   mysqli_set_charset($conn, "utf8");
?>
<!DOCTYPE HTML>
<html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
  <meta http-equiv="content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
  <title>Polilegal | Pol&iacute;ticas de Privacidad de Datos</title>
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
  <link id="default-css" href="style.css" rel="stylesheet" type="text/css">
  <link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" /> 
  <link rel="stylesheet" href="css/style.css">
  <style>
	   .tdwhite {
      background: white;
   }
  </style>
<!--[if IE 7]>
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lt IE 9]>
<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--Fonts-->
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--jquery-->
<script src="js/modernizr-2.6.2.min.js"></script>
</head>
<body>
  <!--wrapper starts-->
  <div class="wrapper">
    <!--inner-wrapper starts-->
    <div class="inner-wrapper">
      <!--header starts-->
		<?php include 'header.php'; ?>	
      <!--header ends-->
            <!--main starts-->
            <div id="main">

              <div class="breadcrumb-section">
                <div class="container">
                  <h1> POLÍTICA DE PRIVACIDAD DE DATOS </h1>
                  <div class="breadcrumb">
                    <a href="index.php"> Inicio </a>
                    <a href="terminos.php">Pol&iacute;ticas de Privacidad de Datos</a>
                  </div>
                </div>
              </div>

              <!--container starts-->
              <div class="container"> 

                <!--primary starts-->
                <section id="primary" class="content-full-width">
						<p> Polilegal S.A. garantiza el respeto y la protección de sus datos personales.  Usted no se encuentra obligado a entregar información alguna al ingresar a nuestro sitio web.  No obstante, puede optar por afiliarse a cualquiera de nuestros planes de asistencia, para lo cual deberá completar el formulario disponible en la sección final de cada Plan de Asistencia de nuestro sitio web, en donde se le solicitará datos como: tipo de plan, nombres y apellidos, número de cédula, ciudad, sector de residencia, celular, email, y el medio de pago de su preferencia. 
						</p>
                  <p> Sus datos serán utilizados única y exclusivamente para finalidad y objeto de los Planes de Asistencia Jurídica, e ingresarán a nuestra base de datos de afiliados.  Polilegal S.A. también podrá enviarle información importante sobre nuestros servicios, eventos y comunicaciones que consideremos que puedan ser de su interés. 
						</p>
						<p> Su información será protegida con altos estándares de seguridad y en ningún caso, Polilegal S.A. comercializará o transmitirá a terceros los datos personales facilitados por los afiliados a través de nuestra plataforma, salvo consentimiento expreso de éstos, o salvo alguna disposición legal u orden judicial. 
						</p>
						<p> La política de privacidad de Polilegal S.A. se encuentra vigente y podrá ser modificada en cualquier momento.  Todas las actualizaciones estarán siempre visibles y accesibles al público en este sitio web.
						</p>
					</section>
				 </div>
			  </div>
			</div>

            <!--dt-sc-toggle-frame-set ends-->
          </div>     

          <!--primary ends-->

        </div>
        <!--container ends-->

        <div class="dt-sc-hr-invisible-large"></div>     

      </div>
      <!--main ends-->
      
      <!--footer starts-->
		<?php include 'footer.php'; ?>
      <!--footer ends-->
      </div>
      <!--inner-wrapper ends-->    
    </div>
    <!--wrapper ends-->
    <!--wrapper ends-->
	<?php include "asistencias.php"; ?>
    <a href="" title="Go to Top" class="back-to-top"> <span class="fa fa-angle-up"></span> </a> 
    <!--Java Scripts--> 
    <!--Java Scripts-->
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/jquery-easing-1.3.js"></script>
    <script type="text/javascript" src="js/jquery.sticky.js"></script>
    <script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
    <script type="text/javascript" src="js/jquery.smartresize.js"></script> 
    <script type="text/javascript" src="js/shortcodes.js"></script>   

    <script type="text/javascript" src="js/custom.js"></script>

    <!-- Layer Slider --> 
    <script type="text/javascript" src="js/jquery-transit-modified.js"></script> 
    <script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script> 
    <script type='text/javascript' src="js/greensock.js"></script> 
    <script type='text/javascript' src="js/layerslider.transitions.js"></script> 

    <script type="text/javascript">var lsjQuery = jQuery;</script><script type="text/javascript"> lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice('layerslider_2','jquery'); } else { lsjQuery("#layerslider_2").layerSlider({responsiveUnder: 1240, layersContainer: 1170, skinsPath: 'js/layerslider/skins/'}) } }); </script>

                     <!-- Cerrar Session -->
                     <script>
                        jQuery(document).ready(function(){
                           jQuery("#close_session").click(function(){
                              alert('Su sesión ha sido cerrada');
                              window.location.href='closesession.php';
                           });
                        });
                     </script>

  </body>
  </html>

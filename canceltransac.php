<?php

   session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
	if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);

	//Eliminamos transaccion del abogado
	$sql = "UPDATE transacciones
				SET estatus='2'
				WHERE df='$_GET[df]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

	//Eliminamos transaccion de paymentez
	//Buscamos si exite en el callback y verificamos si esta Approved
	$sql = "SELECT status
				FROM transactions
				WHERE transaction_id='$_GET[df]'";
	if($result = mysqli_query($conn, $sql)){
		$row = mysqli_fetch_assoc($result);
		if($row['status'] == 1) $aprobado = true;
	}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

	if(isset($aprobado) && $aprobado == true){

		//Procedemos a reversar el DF
		// ===================
		// Set de variables
		// ===================
		$transaction_id = $_GET['df'];
		$auth_timestamp = time();
		$application_code = $config['paymentez']['server_app_code'];
		$appkey = $config['paymentez']['server_app_key'];
		$uniq_token_string = $appkey.$auth_timestamp;
		$uniq_token_hash = hash('sha256', $uniq_token_string);
		$auth_token = base64_encode($application_code.';'.$auth_timestamp.';'.$uniq_token_hash);
		$payload = json_encode(array("transaction"=>array("id"=>$transaction_id)));

		// ===============================
		// Ejecucion de metodo refund
		// ===============================

		//Lo primerito, creamos una variable iniciando curl, pasándole la url
		$ch = curl_init($config['paymentez']['environment']."/v2/transaction/refund/");

		//especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
		curl_setopt ($ch, CURLOPT_POST, 1);

		//le decimos qué paramáetros enviamos (pares nombre/valor, también acepta un array)
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $payload);

		//Pasamos la cabecera
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","Auth-Token: $auth_token"));

		//le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

		//recogemos la respuesta
		$respuesta = curl_exec ($ch);

		//o el error, por si falla
		$error = curl_error($ch);

		//y finalmente cerramos curl
		curl_close ($ch);

		//Convertir json string en array
		$data = json_decode($respuesta, TRUE);
	}

	header("location: adm_transac_abogados.php");


?>

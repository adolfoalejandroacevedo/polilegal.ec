<?php
   session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");
   switch ($_GET['error']) {
         case "1":
               $txterror = "Verification required, please see Verification section";
               break;
         case "3":
               $txterror = "Paid";
               break;
         case "6":
               $txterror = "Fraud";
               break;
         case "7":
               $txterror = "Refund";
               break;
         case "8":
               $txterror = "Chargeback";
               break;
         case "9":
               $txterror = "Rejected by carrier";
               break;
         case "10":
               $txterror = "System error";
               break;
         case "11":
               $txterror = "Paymentez fraud";
               break;
         case "12":
               $txterror = "Paymentez blacklist";
               break;
         case "13":
               $txterror = "Time tolerance";
               break;
         case "19":
               $txterror = "Invalid Authorization Code";
               break;
         case "20":
               $txterror = "Authorization code expired";
               break;
         case "21":
               $txterror = "Paymentez Fraud - Pending refund";
               break;
         case "22":
               $txterror = "Invalid AuthCode - Pending refund";
               break;
         case "23":
               $txterror = "AuthCode expired - Pending refund";
               break;
         case "24":
               $txterror = "Paymentez Fraud - Refund requested";
               break;
         case "25":
               $txterror = "Invalid AuthCode - Refund requested";
               break;
         case "26":
               $txterror = "AuthCode expired - Refund requested";
               break;
         case "27":
               $txterror = "Merchant - Pending refund";
               break;
         case "28":
               $txterror = "Merchant - Refund requested";
               break;
      default:
         $txterror = "Error inesperado";
   }

	$plan = (isset($_GET['plan'])) ? $_GET['plan']:0;

	if ($plan == 2){
      //Actualiza transaccion
      $sql = "INSERT INTO transacciones (iduser, concepto, status) VALUES ('$_SESSION[id]', '2', '0')";
      if (!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	}

	if ($plan == 3){
      //Actualiza transaccion
      $sql = "INSERT INTO transacciones (iduser, concepto, status) VALUES ('$_SESSION[id]', '3', '0')";
      if (!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	}

?>

<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>
         Tu Abogado:
         Users :: Directorio de Abogados del Ecuador
      </title>
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <link href="css/animate.min.css" rel="stylesheet">
      <link href="css/prettyPhoto.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="bower_components/datatables.net-dt/css/jquery.dataTables.css">
      <link rel="stylesheet" type="text/css" href="css/bootstrap-markdown.min.css">
      <style>
         .pricingTable{
         text-align: center;
         transition: all 0.5s ease 0s;
         }
         .pricingTable:hover{
         box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
         }
         .pricingTable .pricingTable-header{
         color: #feffff;
         }
         .pricingTable .heading{
         display: block;
         padding-top: 25px;
         }
         .pricingTable .heading > h3{
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .textotitle{
         color: #feffff;
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .pricingTable .subtitle{
         display: block;
         font-size: 13px;
         margin-top: 5px;
         text-transform: capitalize;
         }
         .pricingTable .price-value{
         display: block;
         font-size: 60px;
         font-weight: 700;
         padding-bottom: 25px;
         }
         .pricingTable .price-value span{
         display: block;
         font-size: 14px;
         line-height: 20px;
         text-transform: uppercase;
         }
         .pricingTable .pricingContent{
         /*text-transform: capitalize;*/
         background: #fbfbfb;
         color: #fefeff;
         }
         .pricingTable .pricingContent ul{
         list-style: none;
         padding: 15px 14px 10px;
         margin: 0;
         text-align: left;
         }
         .pricingTable .pricingContent ul li{
         font-size: 14px;
         padding: 12px 0;
         border-bottom: 1px dashed #e1e1e1;
         color: #9da1ad;
         }
         .pricingTable .pricingContent ul li i{
         font-size: 14px;
         float: right;
         }
         .pricingTable .pricingTable-sign-up{
         padding: 20px 0;
         background: #fbfbfb;
         color: #fff;
         text-transform: capitalize;
         }
         .pricingTable .btn-block{
         width: 60%;
         margin: 0 auto;
         font-size: 17px;
         color: #fff;
         text-transform: capitalize;
         border: none;
         border-radius: 5px;
         padding: 10px;
         transition: all 0.5s ease 0s;
         }
         .pricingTable .btn-block:before{
         content: "\f007";
         font-family: 'FontAwesome';
         margin-right: 10px;
         }
         .pricingTable.blue .pricingTable-header,
         .pricingTable.blue .btn-block{
         background: #fce400;
         }
         .pricingTable.pink .pricingTable-header,
         .pricingTable.pink .btn-block{
         background: #c9b600;
         }
         .pricingTable.orange .pricingTable-header,
         .pricingTable.orange .btn-block{
         background: #e0d83a;
         }
         .pricingTable.green .pricingTable-header,
         .pricingTable.green .btn-block{
         background: #e0cd00;
         }
         .pricingTable.blue .btn-block:hover,
         .pricingTable.pink .btn-block:hover,
         .pricingTable.orange .btn-block:hover,
         .pricingTable.green .btn-block:hover{
         background: #e6e6e6;
         color: #939393;
         }
         @media screen and (max-width: 990px){
         .pricingTable{ margin-bottom: 20px; }
         }
      </style>
   </head>
   <body class="homepage">
<?php include "header.php"; ?>
      <!--/header-->
      <section id="blog" class="container">
         <div class="container">
            <div class="alert alert-danger alert-dismissible fade in" role="alert" align="center" style="margin-bottom: 2px">
               <h1 class="alert-heading" style="color: #b45c5a">Su pago no pudo ser procesado.</h1>
               <p>El sistema de pago ha generado el siguiente mensaje de error: "<?php echo $txterror; ?>".</p>
               <p>Por favor intente de nuevo. Si el problema persiste, contactenos.</p>
            </div>
         </div>
      </section>
      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/markdown.js"></script>
      <script src="js/to-markdown.js"></script>
      <script src="js/bootstrap-markdown.js"></script>
      <script src="js/bootstrap-markdown.es.js"></script>
      <script type="text/javascript" src="js/jquery.simpleslider.js"></script>
      <script type="text/javascript" charset="utf8" src="bower_components/datatables.net/js/jquery.dataTables.min.js"> </script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/jquery.isotope.min.js"></script>
      <script src="js/wow.min.js"></script>
      <script src="js/main.js"></script>
   </body>
</html>



<?php session_start(); ?>
<?php
   header('Content-Type: text/html; charset=utf-8');
   include "detect_mobile.php";
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
   mysqli_set_charset($conn, "utf8");

	//Variables del post
	$keyword = (isset($_POST['keyword'])) ? $_POST['keyword']:"";
	$ciudad	= (isset($_POST['ciudad'])) ? $_POST['ciudad']:"";
	$area = (isset($_POST['area'])) ? $_POST['area']:"";

	/* Las posibles 8 consultas
		Las posibles consultas
		clave ciudad   area
		0  	0  		0
		1  	0  		0
		0  	1  		0
		1  	1  		0
		0  	0  		1
		1  	0  		1
		0  	1  		1
		1  	1  		1
	*/

	if($keyword == "" && $ciudad == "" && $area == ""){
		$sql = "SELECT t1.id iduser, nombres, t1.ciudad ciudadid, t2.ciudad ciudadname, foto 
					FROM users t1 
					INNER JOIN ciudades t2
					ON t1.ciudad=t2.id";
	}
	if($keyword != "" && $ciudad == "" && $area == ""){
		$keyword = $keyword;
      $sql = "SELECT t1.id iduser, nombres, t1.ciudad ciudadid, t2.ciudad ciudadname, foto 
               FROM users t1 
               INNER JOIN ciudades t2
               ON t1.ciudad=t2.id
					WHERE nombres LIKE '%$keyword%'";
	}
   if($keyword == "" && $ciudad != "" && $area == ""){
		echo "<form name='redirect' method='POST' action='findabogadoxciudad.php'>
					<input type='hidden' name='ciudad' value='$ciudad'>
				</form>
				<script type='text/javascript'>
					document.redirect.submit();
				</script>";
			exit;
   }
   if($keyword != "" && $ciudad != "" && $area == ""){
		$keyword = $keyword;
      $sql = "SELECT t1.id iduser, nombres, t1.ciudad ciudadid, t2.ciudad ciudadname, foto 
               FROM users t1 
               INNER JOIN ciudades t2
               ON t1.ciudad=t2.id
               WHERE nombres LIKE '%$keyword%' AND t1.ciudad='$ciudad'";
   }
   if($keyword == "" && $ciudad == "" && $area != ""){
		echo "<form name='redirect' method='POST' action='findabogadoxarea.php'>
					<input type='hidden' name='area' value='$area'>
				</form>
				<script type='text/javascript'>
					document.redirect.submit();
				</script>";
				exit;
   }
   if($keyword != "" && $ciudad == "" && $area != ""){
		$keyword = $keyword;
      $sql = "SELECT t1.id iduser, nombres, t1.ciudad ciudadid, t2.ciudad ciudadname, foto 
               FROM users t1 
               INNER JOIN ciudades t2
               ON t1.ciudad=t2.id
               WHERE nombres LIKE '%$keyword%' AND t1.id IN (
                  SELECT iduser 
                  FROM usersareas 
                  WHERE idarea='$area'
					)";
   }
   if($keyword == "" && $ciudad != "" && $area != ""){
      $sql = "SELECT t1.id iduser, nombres, t1.ciudad ciudadid, t2.ciudad ciudadname, foto 
               FROM users t1 
               INNER JOIN ciudades t2
               ON t1.ciudad=t2.id
               WHERE t1.ciudad='$ciudad' AND t1.id IN (
                  SELECT iduser 
                  FROM usersareas 
                  WHERE idarea='$area'
               )";
   }
   if($keyword != "" && $ciudad != "" && $area != ""){
		$keyword = $keyword;
      $sql = "SELECT t1.id iduser, nombres, t1.ciudad ciudadid, t2.ciudad ciudadname, foto 
               FROM users t1 
               INNER JOIN ciudades t2
               ON t1.ciudad=t2.id
               WHERE nombres LIKE '%$keyword%'  AND t1.ciudad='$ciudad' AND t1.id IN (
                  SELECT iduser 
                  FROM usersareas 
                  WHERE idarea='$area'
               )";
   }


	if(!$users = mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));	


/*
Las posibles consultas
clave	ciudad	area
0	0	0
1	0	0
0	1	0
1	1	0
0	0	1
1	0	1
0	1	1
1	1	1
*/

?>

<!DOCTYPE html>
<html lang="en">
  <head>
	 <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
         Tu Abogado:
          Directorio de Abogados del Ecuador
	</title>
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
   <link rel="stylesheet" href="gp/css/font-awesome.min.css">
   <link href="gp/css/animate.min.css" rel="stylesheet">
    <link href="gp/css/prettyPhoto.css" rel="stylesheet">      
   <link href="css/main.css" rel="stylesheet">
    <link href="gp/css/responsive.css" rel="stylesheet">

	 <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       

      <style>
         .file_drag_area
         {
            width:600px;
            height:105px;
            border:2px dashed #ccc;
            /*line-height:30px;*/
            text-align:center;
            font-size:16px;
            margin:0 auto;
         }
         .file_drag_area p{
            margin: 0 0 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
            margin-left: 0px;
         }
         .file_drag_over{
            color:#000;
            border-color:#000;
         }
         .multiple_files{
            cursor: pointer;
         }
			.tempforSize {display: inline-block !important;}
      </style>    
 
  </head>
  <body class="homepage">   
<?php include "header.php"; ?>
      <!--/header-->

	<section id="portfolio" style="padding-bottom: 100px; padding-top: 80px;">
        <div class="container">
            <div class="center" style="padding-bottom: 0px;">
               <h2>Abogados</h2>
            </div>

         <?php if(!$_SESSION['mobile']) { ?>
            <!--/#portfolio-filter-->
            <ul class="portfolio-filter text-center">
                <li><a class="btn btn-default active" href="#" data-filter="*">Todas la áreas</a></li>
						<?php 
							$sql = "SELECT id, area FROM areas ORDER BY area";
								if ($result = mysqli_query($conn, $sql)){
									$ids = " ";
									while($row2 = mysqli_fetch_assoc($result)){
										$ids .= $row2['id']." ";
					 	?>
                				<li><a class="btn btn-default" href="#" data-filter=".<?php echo $row2['id']; ?>"><?php echo $row2['area']; ?></a></li>
						<?php }
							}else error_log("Error: " . $sql . "..." . mysqli_error($conn)); 
						?>
            </ul><!--/#portfolio-filter-->
			<?php }else { ?>

            <!-- Select Filter -->
            <div class="row" style="margin-bottom: 25px;">
               <div class="col-md-4 col-md-offset-4" align="center">
                  <form id="frmareas" class="infogeneral-form" name="frmareas" method="post">
                      <input type="hidden" name='keyword' value="">
                      <input type="hidden" name='area' value="">
                      <select name="area" class="form-control ddList centerSelect" id="filter-select">
                        <option value="" SELECTED>-- SELECCIONE UN ÁREA --</option>
                        <?php
                           $ids = " ";
                           $sql = "SELECT * FROM areas ORDER BY area";
                           if ($result = mysqli_query($conn, $sql)){
                              while ($row2 = mysqli_fetch_assoc($result)) {
                                 echo "<option value='.$row2[id]'>".$row2['area']."</option>";
                                 $ids = $ids.$row2['id']." ";
                              }
                           }else{
                              error_log("Error: " . $sql . "..." . mysqli_error($conn));
                           }
                        ?>
                     </select>
                  </form>
               </div>
            </div>
            <!-- End Select Filter -->
         <?php } ?>

            <div class="row">
                <div class="portfolio-items" id="portfolio-items">
							<?php
								while($row = mysqli_fetch_assoc($users)) {
									$sql = "SELECT idarea, area 
												FROM usersareas 
												INNER JOIN areas 
												ON idarea=areas.id 
												WHERE iduser='$row[iduser]'";
									if ($result = mysqli_query($conn, $sql)){
										$areasid = "";
										$areasname = "";
										$cont = 1;
										while($row2 = mysqli_fetch_assoc($result)){
											$areasid .= $row2['idarea']." ";
											if($cont < 8){
												$areasname .= "* ".$row2['area']." ";
											}else if ($cont == 8){
												$areasname .= "* Mas áreas dentro del perfil......";
											}
											$cont += 1;
										}
									}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
							?>
								  <div class="portfolio-item <?php echo trim($areasid); ?> col-xs-12 col-sm-4 col-md-3">
										<div class="recent-work-wrap">
											 <?php if(is_null($row['foto'])) $foto = "images/human.jpg"; else $foto = "uploads/$row[foto]"; ?>
											 <img class="img-responsive" src="<?php echo $foto; ?>" alt="">
											 <div class="overlay">
												  <div class="recent-work-inner">
														<h3><a href="perfil.php?iduser=<?php echo $row['iduser']; ?>">
														<?php echo $row['nombres']; ?></a></h3>
														<p>Ciudad: <?php echo $row['ciudadname']; ?></p>
														<p>Areas: <?php echo trim($areasname); ?></p>
														<a class="preview" href="<?php echo $foto; ?>" rel="prettyPhoto"><i class="fa fa-eye"></i> Ampliar</a>
												  </div>
											 </div>
										</div>
								  </div><!--/.portfolio-item-->
							<?php } ?>

                    <div class="portfolio-item<?php echo $ids; ?>col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="img/encontacto.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#">Sigue en Contacto</a></h3>
                                    <p>Tuabogado.ec crece cada día con nuevos profesionales calificados, sigue en contacto.</p>
                                    <a class="preview" href="img/encontacto.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i> Ampliar</a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
                </div>
            </div>
            <div class="row" align="center">
               <p>NOTA: Si existe un área que no aparezca en esta página, por favor ponerse en contacto con nosotros.</p>
            </div>
        </div>

    </section><!--/#portfolio-item-->

      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
	
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="gp/js/jquery.js"></script>
    <script src="gp/js/bootstrap.min.js"></script>
    <script src="gp/js/jquery.prettyPhoto.js"></script>
    <script src="gp/js/jquery.isotope.min.js"></script>
    <script src="gp/js/wow.min.js"></script>
    <script src="gp/js/main.js"></script>

   <!-- Centrado del Select -->
   <script>
      function getTextWidth(txt) {
        var $elm = $('<span class="tempforSize">'+txt+'</span>').prependTo("body");
        var elmWidth = $elm.width();
        $elm.remove();
        return elmWidth;
      }
      function centerSelect($elm) {
          var optionWidth = getTextWidth($elm.children(":selected").html())
          var emptySpace =   $elm.width()- optionWidth;
          $elm.css("text-indent", (emptySpace/2) - 10);// -10 for some browers to remove the right toggle control width
      }
      // on start 
      $('.centerSelect').each(function(){
        centerSelect($(this));
      });
      // on change
      $('.centerSelect').on('change', function(){
        centerSelect($(this));
      });
   </script>

   <!-- Filtering del Select -->
   <script type="text/javascript">
      $( function () {
        var $container = $('#portfolio-items');
        $container.isotope({})
        $('#filter-select').change( function() {
          $container.isotope({
            filter: this.value
          });
        });
      });
   </script>


  </body>
</html>

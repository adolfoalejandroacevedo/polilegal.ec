<?php 
   session_start();
   session_unset();
	$current = "contacto";
?>
<!DOCTYPE HTML>
<html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
    <meta http-equiv="content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
    <title>Polilegal |Contacto</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link id="default-css" href="style.css" rel="stylesheet" type="text/css">
    <link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" /> 
<!--[if IE 7]>
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lt IE 9]>
<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--Fonts-->
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--jquery-->
<script src="js/modernizr-2.6.2.min.js"></script>
</head>
<body>
	<!--wrapper starts-->
    <div class="wrapper">
        <!--inner-wrapper starts-->
        <div class="inner-wrapper">
            <!--header starts-->
				<?php include 'header.php'; ?>
            <!--header ends-->
                    <!--main starts-->
                    <div id="main">
                        
                        <div class="breadcrumb-section">
                            <div class="container">
                                <h1> Contáctenos </h1>
                                <div class="breadcrumb">
                                    <a href="index.php"> Inicio </a>
                                    <span class="current"> Contacto </span>
                                </div>
                            </div>
                        </div>
                        
                        <!--container starts-->
                        <div class="container">	
                            
                            <!--primary starts-->
                            <section id="primary" class="content-full-width">
                                
                                
                                
                                <div class="dt-sc-hr-invisible-small"></div>
                                
                                <!--dt-sc-three-fourth starts-->
                                <div class="dt-sc-three-fourth column first">
                                    <!--dt-sc-tabs-container starts-->            
                                    <div class="dt-sc-tabs-container">
                                        <ul class="dt-sc-tabs-frame">
                                            <li><a href="#"> Ubicación </a></li> 
                                            <li><a href="#"> Detalles de contacto </a></li>
                                            <li><a href="#"> Trabaje con nosotros </a></li>
                                        </ul>
                                        <div class="dt-sc-tabs-frame-content">
                                            <div>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2525.056897430557!2d-78.48114177167457!3d-0.180519140260444!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMMKwMTAnNDguOCJTIDc4wrAyOCc0NS4xIlc!5e0!3m2!1ses!2sec!4v1489091634720" allowfullscreen="" height="350" style="border:0" width="550" frameborder="0"></iframe>
</div>
                                        </div>
                                        <div class="dt-sc-tabs-frame-content">
                                            
                                            <div class="dt-sc-hr-invisible-very-small"></div>
                                            <div class="dt-sc-one-half column first">
                                                <p class="dt-sc-contact-info"><span><i class="fa fa-phone"></i> Teléfono: </span> (02)3 825530 </p>
                                                <p class="dt-sc-contact-info"><span><i class="fa fa-print"></i> Otro: </span> (+593 2) 3333 533</p>
                                                <p class="dt-sc-contact-info"><i class="fa fa-location-arrow"></i>Pasaje El Jardín No. 168 y Av. 6 de Diciembre <br> (frente al Megamaxi) <br>Edificio Century Plaza I, piso 5, Of. 14. <br> Quito, Ecuador</p>
                                            </div>
                                            <div class="dt-sc-one-half column">
                                                <p class="dt-sc-contact-info"><span><i class="fa fa-envelope"></i> E-mail: </span><a href="mailto:yourname@somemail.com"> info@polilegal.ec</a></p>
                                                <p class="dt-sc-contact-info"><span><i class="fa fa-globe"></i> Web: </span><a href="#"> www.polilegal.ec</a></p>
                                            </div>
                                        </div>
                                        <div class="dt-sc-tabs-frame-content">
                                         <p> <span> Te contactaremos </span> </p>
                                         <form name="contactoform" method="post" class="help-form" action="PHPMailer/examples/gmail.php" enctype="multipart/form-data">
					     <input type="hidden" name="tipo" value="4">
                                             <p>
                                                 <input type="text" placeholder="Nombres y Apellidos" class="text_input" name="hf_tree_name" required />	
                                             </p>
                                             <p>
                                                 <input type="text" placeholder="E-mail" class="text_input" name="hf_tree_email" required />	
                                             </p>
                                             <p>
                                                 <input type="text" placeholder="Teléfono móvil" class="text_input" name="hf_tree_telefono" required />	
                                             </p>
                                             <p>
                                                 <textarea placeholder="Mensaje" class="text_input" name="hf_tree_mensaje" required rows="30" cols="40" /></textarea>
                                             </p>
                                             <p>
                                                 Adjuntar CV: <input type="file" placeholder="Adjuntar CV" class="text_input" name="hf_file_cv" required />
                                             </p>
                                             
                                             <div id="ajax_contactoform_msg"> </div>
                                             <p>
                                                 <input type="submit" value="&#xf002; &nbsp; Enviar">
						<img id="enviando2" src="images/loading.gif" width="100px" height="100px" style="position: relative; vertical-align:middle; display: none;">
                                             </p>    
                                         </form>
                                     </div>
                                 </div>
                                 
                                 
                                 <!--dt-sc-tabs-container ends-->
                             </div>
                             <!--dt-sc-three-fourth ends-->
                             <!--dt-sc-one-fourth starts-->
                             <div class="dt-sc-one-fourth column">
                                <h2 class="dt-sc-bordered-title" style="text-align: center;"> Horario de Atenci&oacute;n </h2>
                                
                                <div class="dt-sc-business-hours">
                                    <p> Lunes <span> : 0900 - 18h00 </span> </p>
                                    <p> Martes <span> : 0900 - 18h00 </span> </p>
                                    <p> Miércoles <span> : 0900 - 18h00 </span> </p>
                                    <p> Jueves <span> : 0900 - 18h00 </span> </p>
                                    <p> Viernes <span> : 0900 - 18h00 </span> </p>
                                    <p> Sábado <span> Previa Cita </span> </p>
                                </div>
                            </div>
                            <!--dt-sc-one-fourth ends-->
                            
                            <div class="dt-sc-clear"></div>
                            <div class="dt-sc-hr-invisible"></div>
                            
                            <form name="frmcontactogeneral" class="contact-form" method="post" action="PHPMailer/examples/gmail.php">
                                <input type="hidden" name="tipo" value="3">
                                <h2 class="dt-sc-hr-title">¿Tiene alguna consulta?</h2>
                                <p class="column dt-sc-one-third first">
                                    <input id="name" name="txtname" type="text" placeholder="Nombres y Apellidos" required="">
                                </p>
                                <p class="column dt-sc-one-third">
                                    <input id="email" name="txtemail" type="email" placeholder="E-mail" required="">
                                </p>
                                <p class="column dt-sc-one-third">
                                    <input id="subject" name="txtsubject" type="text" placeholder="Asunto" required="">
                                </p>
                                <p>
                                    <textarea id="comment" name="txtmessage" placeholder="Mensaje"></textarea>
                                </p>
                                <div id="ajax_contact_msg"> </div>
                                <p class="aligncenter">
                                    <input name="submit" type="submit" id="submit" class="dt-sc-bordered-button" value="Enviar E-mail">
                                    <img id="espiral" src="images/loading.gif" width="100px" height="100px" style="position: relative; vertical-align:middle; display: none;">
                                </p>
                            </form>
                            <div class="dt-sc-hr-invisible-large"></div>
                            
                            
                            
                        </section>
                        <!--primary ends-->
                        
                    </div>
                    <!--container ends-->
                    
                    
                </div>
                <!--main ends-->
                
                <!--footer starts-->
					 <?php include 'footer.php'; ?>
			       <!--footer ends-->
    </div>
    <!--inner-wrapper ends-->    
</div>
<!--wrapper ends-->
<?php include "asistencias.php"; ?>
<a href="" title="Go to Top" class="back-to-top"> <span class="fa fa-angle-up"></span> </a>    <!--Java Scripts-->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-migrate.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-easing-1.3.js"></script>
<script type="text/javascript" src="js/jquery.sticky.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>


<script type="text/javascript" src="js/jquery.tabs.min.js"></script>
<script type="text/javascript" src="js/jquery.smartresize.js"></script> 
<script type="text/javascript" src="js/shortcodes.js"></script>   

<script type="text/javascript" src="js/custom.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDz1RGAW3gGyDtvmBfnH2_fE2DVVNWq4Eo&callback=initMap" type="text/javascript"></script>
<script src="js/gmap3.min.js"></script>
<!-- Layer Slider --> 
<script type="text/javascript" src="js/jquery-transit-modified.js"></script> 
<script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script> 
<script type='text/javascript' src="js/greensock.js"></script> 
<script type='text/javascript' src="js/layerslider.transitions.js"></script> 

<script type="text/javascript">var lsjQuery = jQuery;</script><script type="text/javascript"> lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice('layerslider_2','jquery'); } else { lsjQuery("#layerslider_2").layerSlider({responsiveUnder: 1240, layersContainer: 1170, skinsPath: 'js/layerslider/skins/'}) } }); </script>

                     <!-- Cerrar Session -->
                     <script>
                        jQuery(document).ready(function(){
                           jQuery("#close_session").click(function(){
                              alert('Su sesión ha sido cerrada');
                              window.location.href='closesession.php';
                           });
                        });
                     </script>

</body>
</html>

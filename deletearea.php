<?php

   header('Content-Type: text/html; charset=utf-8');
   session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");
	error_log($_SESSION['role']);
	if(isset($_SESSION['role']) && $_SESSION['role'] !=='admin') {
		header("Location: index.php");
		exit;
	}

   //Conexion a la BD
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);

	//Eliminamos la ciudad
	$sql = "DELETE FROM areas WHERE id='$_GET[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

	//Regresamos
	header("location: adm_areas.php");

?>

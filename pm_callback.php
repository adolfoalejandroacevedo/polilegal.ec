<?php
header('Content-Type: text/html; charset=utf-8');
$data = json_decode(file_get_contents('php://input'), true);
$config = require 'config.php';
$conn = mysqli_connect($config['database']['server'],$config['database']['username'],
        $config['database']['password'],$config['database']['db']);
if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
mysqli_set_charset($conn, "utf8");


if(!isset($_POST)){
	exit(404);
}
/*
$file = fopen("logs.txt", "a");
fwrite($file, date("d m Y H:m:s")." "."Se ha ejecutado el archivo".PHP_EOL);
fclose($file);
*/
session_start();

$sql = "SELECT * FROM transactions WHERE transaction_id='".$data['transaction']['id']."'";
if ($result = mysqli_query($conn, $sql)){
	$numero_filas = mysqli_num_rows($result);
	if ($numero_filas > '0'){
		$sql = "UPDATE transactions SET 
				status='".$data['transaction']['status']."', 
				order_description='".$data['transaction']['order_description']."', 
				authorization_code='".$data['transaction']['authorization_code']."',
				dev_reference='".$data['transaction']['dev_reference']."',
				carrier_code='".$data['transaction']['carrier_code']."',
				status_detail='".$data['transaction']['status_detail']."',
				installments='".$data['transaction']['installments']."',
				amount='".$data['transaction']['amount']."',
				paid_date='".$data['transaction']['paid_date']."',
				date='".$data['transaction']['date']."',
				message='".$data['transaction']['message']."',
				stoken='".$data['transaction']['stoken']."',
				transaction_id='".$data['transaction']['id']."',
				application_code='".$data['transaction']['application_code']."',
				user_id='".$data['user']['id']."',
				email='".$data['user']['email']."',
				card_bin='".$data['card']['bin']."',
				card_origin='".$data['card']['origin']."',
				holder_name='".$data['card']['holder_name']."',
				card_type='".$data['card']['type']."',
				card_number='".$data['card']['number']."'
				WHERE transaction_id='".$data['transaction']['id']."'";
		if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	}else{
		$sql = "INSERT INTO transactions (
						status,
						order_description,
						authorization_code,
						dev_reference,
						carrier_code,
						status_detail,
						installments,
						amount,
						paid_date,
						date,
						message,
						stoken,
						transaction_id,
						application_code,
						user_id,
						email,
						card_bin,
						card_origin,
						holder_name,
						card_type,
						card_number) 
				VALUES (
						'".$data['transaction']['status']."', 
						'".$data['transaction']['order_description']."', 
						'".$data['transaction']['authorization_code']."',
						'".$data['transaction']['dev_reference']."',
						'".$data['transaction']['carrier_code']."',
						'".$data['transaction']['status_detail']."',
						'".$data['transaction']['installments']."',
						'".$data['transaction']['amount']."',
						'".$data['transaction']['paid_date']."',
						'".$data['transaction']['date']."',
						'".$data['transaction']['message']."',
						'".$data['transaction']['stoken']."',
						'".$data['transaction']['id']."',
						'".$data['transaction']['application_code']."',
						'".$data['user']['id']."',
						'".$data['user']['email']."',
						'".$data['card']['bin']."',
						'".$data['card']['origin']."',
						'".$data['card']['holder_name']."',
						'".$data['card']['type']."',
						'".$data['card']['number']."'
						)";
		if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
	}
}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

mysqli_close($conn);

?>

<?php
	header('Content-Type: text/html; charset=utf-8');
   session_start();
   $config = require 'config.php';
	include "detect_mobile.php";
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
	mysqli_set_charset($conn, "utf8");

	//Obtencion de los precios de los planes
	$plan = array();
	$sql = "SELECT plan,
						costo
				FROM planes";
	if ($result = mysqli_query($conn, $sql)){
		while ($row = mysqli_fetch_assoc($result)) {
			array_push($plan,$row);
		}
	}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Plataforma Digital de Abogados del Ecuador | Planes y Tarifas</title>
		<meta name="keywords" content="abogado, ecuador, abogados, quito, guayaquil, cuenca, macas, loja, ibarra, ambato, riobamba, divorcios, deudas, cobro, penal, transito">
		<meta name="description" content="Seleccione el plan que mas se adapte a sus necesidades." />
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <link href="css/animate.min.css" rel="stylesheet">
      <link href="css/prettyPhoto.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="bower_components/datatables.net-dt/css/jquery.dataTables.css">
      <link rel="stylesheet" type="text/css" href="css/bootstrap-markdown.min.css">
      <style>
         .pricingTable{
         text-align: center;
         transition: all 0.5s ease 0s;
         }
         .pricingTable:hover{
         box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
         }
         .pricingTable .pricingTable-header{
         color: #feffff;
         }
         .pricingTable .heading{
         display: block;
         padding-top: 25px;
         }
         .pricingTable .heading > h3{
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .textotitle{
         color: #feffff;
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .pricingTable .subtitle{
         display: block;
         font-size: 13px;
         margin-top: 5px;
         text-transform: capitalize;
         }
         .pricingTable .price-value{
         display: block;
         font-size: 60px;
         font-weight: 700;
         padding-bottom: 25px;
         }
         .pricingTable .price-value span{
         display: block;
         font-size: 14px;
         line-height: 20px;
         text-transform: uppercase;
         }
         .pricingTable .pricingContent{
         /*text-transform: capitalize;*/
         background: #fbfbfb;
         color: #fefeff;
         }
         .pricingTable .pricingContent ul{
         list-style: none;
         padding: 15px 14px 10px;
         margin: 0;
         text-align: left;
         }
         .pricingTable .pricingContent ul li{
         font-size: 14px;
         padding: 12px 0;
         border-bottom: 1px dashed #e1e1e1;
         color: #9da1ad;
         }
         .pricingTable .pricingContent ul li i{
         font-size: 14px;
         float: right;
         }
         .pricingTable .pricingTable-sign-up{
         padding: 20px 0;
         background: #fbfbfb;
         color: #fff;
         text-transform: capitalize;
         }
         .pricingTable .btn-block{
         width: 60%;
         margin: 0 auto;
         font-size: 17px;
         color: #fff;
         text-transform: capitalize;
         border: none;
         border-radius: 5px;
         padding: 10px;
         transition: all 0.5s ease 0s;
         }
         .pricingTable .btn-block:before{
         content: "\f007";
         font-family: 'FontAwesome';
         margin-right: 10px;
         }
         .pricingTable.blue .pricingTable-header,
         .pricingTable.blue .btn-block{
         background: #fce400;
         }
         .pricingTable.pink .pricingTable-header,
         .pricingTable.pink .btn-block{
         background: #c9b600;
         }
         .pricingTable.orange .pricingTable-header,
         .pricingTable.orange .btn-block{
         background: #D4AF37;
         }
         .pricingTable.green .pricingTable-header,
         .pricingTable.green .btn-block{
         background: #C0C0C0;
         }
         .pricingTable.blue .btn-block:hover,
         .pricingTable.pink .btn-block:hover,
         .pricingTable.orange .btn-block:hover,
         .pricingTable.green .btn-block:hover{
         background: #e6e6e6;
         color: #939393;
         }
         @media screen and (max-width: 990px){
         .pricingTable{ margin-bottom: 20px; }
         }
         #keyword {
            text-transform:lowercase;
         }
      </style>
   </head>
   <body class="homepage">
<?php include "header.php"; ?>
      <!--/header-->
      <section id="blog" class="container" style="margin-top: 0px;padding-bottom: 0px;padding-left: 0px;padding-right: 0px;">
          <div class="container" style="padding-left: 0px; padding-right: 0px; margin-right: 0px;">
				<div class="skill-wrap clearfix" style="margin-bottom: 20px;">
					<div class="">
						<h1 class="center"> <span>PLANES Y TARIFAS</span></h1>
						<div class="container">
							<div class="row" style="margin-right: 0px; margin-left: 0px;">
								<div class="col-md-4 col-sm-6">
									<div class="pricingTable blue">
										<div class="pricingTable-header">
											<span class="heading">
												<h3 class="textotitle">Plan <?php echo ucwords($plan[0]['plan']); ?></h3>
												<br>
												<!--span class="subtitle">Lorem ipsum dolor sit</span-->
											</span>
											<span class="price-value"> GRATIS <br><span><br><br></span></span><br>
										</div>
										<div class="panel-group" id="accordion1">
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1">
														Perfil legal profesional
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapseOne1" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/www.png" style="width: 160px;">
															</div>
															<div class="media-body">
																<p>Obtienes tu presencia online.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse2">
														Informaci&oacute;n general
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse2" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/infognral.png" style="width: 95px; height: 95px">
															</div>
															<div class="media-body">
																<p>Nombres y Apellidos / Estudio Jurídico / Ciudad.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse3">
														Contacto
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse3" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/email.png" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>Correo electrónico.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse4">
														&Aacute;reas de pr&aacute;ctica 
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse4" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/areas.png" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>Podrás incluir 1 área de práctica.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- /  CONTENT BOX-->
										<div class="pricingTable-sign-up">
											<a class="btn btn-block"
												data-plan='1' data-planuser="<?php echo (isset($_SESSION['plan'])) ? $_SESSION['plan']:'0'; ?>"> SELECCIONAR </a>
										</div>
										<!-- BUTTON BOX-->
									</div>
								</div>
								<div class="col-md-4 col-sm-6">
									<div class="pricingTable green">
										<div class="pricingTable-header">
											<span class="heading">
												<h3 class="textotitle">Plan <?php echo ucwords($plan[1]['plan']); ?></h3>
												<br>
												<!--span class="subtitle">Lorem ipsum dolor sit</span-->
											</span>
											<span class="price-value">$<?php echo $plan[1]['costo']; ?> <br><span><br>AL MES (impuestos incluidos)</span></span><br>
										</div>
										<div class="panel-group" id="accordion2">
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse21">
														Perfil legal profesional
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse21" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/www.png" style="width: 160px;">
															</div>
															<div class="media-body">
																<p>Obtienes tu presencia online.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse22">
														Informaci&oacute;n general
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse22" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/infognral.png" style="width: 95px; height: 95px">
															</div>
															<div class="media-body">
																<p>Nombres y Apellidos / Estudio Jurídico / Ciudad.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse24">
														Contacto
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse24" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/email.png" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>1 correo electrónico y 1 número telefónico.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse25">
														&Aacute;reas de pr&aacute;ctica 
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse25" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/areas.png" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>Podrás incluir hasta 5 áreas de práctica.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse23">
														Imagen personalizada
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse23" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/imgpersonal.png" style="width: 95px; height: 95px">
															</div>
															<div class="media-body">
																<p>La foto de perfil que tú elijas.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse26">
														Galería de imágenes
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse26" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/images.png" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>Podrás mostrar hasta 5 imágenes en tu página de perfil profesional.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse27">
														Tu descripci&oacute;n
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse27" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/personal.png" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>Una reseña de 15 líneas sobre ti.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse28">
														Direcci&oacute;n de lugar de trabajo
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse28" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/address.png" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>Dirección exacta de tu lugar de trabajo.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse29">
														<i class="fa fa-angle-right pull-right"></i>
												<?php if($_SESSION['mobile']) { ?>
														Cupones de descuento 
												<?php }else { ?>
														Cupones de descuento para clientes
												<?php } ?>
														</a>
													</h3>
												</div>
												<div id="collapse29" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/cupones.png" style="width: 125px; height: auto;">
															</div>
															<div class="media-body">
																<p>Podrás subir a la página web hasta 6 cupones de descuentos de tus servicios al año. Cada publicación de cupón tendrá una duración máxima de 1 mes.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse210">
														Elaboraci&oacute;n de artículos
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse210" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/articles.png" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>Podrás escribir hasta 5 artículos al año sobre cualquier tema legal que prefieras para el blog de la página que será visible para todo público.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse211">
														<i class="fa fa-angle-right pull-right"></i>
													<?php if($_SESSION['mobile']) { ?>
														Posicionamiento destacado
													<?php }else{ ?>
														Posicionamiento destacado (opcional)
													<?php } ?>
														</a>
													</h3>
												</div>
												<div id="collapse211" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/posicionamiento.png" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>Opción adicional en este paquete: Estar en Destacados en tuabogado.ec por USD 2 (al mes).</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse212">
														Descuento Especial
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse212" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/10descuento.gif" style="width: 160px;">
															</div>
															<div class="media-body">
																<p>Recibe un descuento del 10% por 1 año de suscripción a este plan.</p>
															</div>
														</div>
													</div>
												</div>
											</div>


										</div>
										<!-- /  CONTENT BOX-->
										<div class="pricingTable-sign-up" style="padding-bottom: 0px;">
											<a class="btn btn-block"
												data-plan='2' data-planuser="<?php echo (isset($_SESSION['plan'])) ? $_SESSION['plan']:'0'; ?>"> SELECCIONAR </a>
										</div>
										<!-- BUTTON BOX
										<div class="pricingContent">
											<ul style="padding-top: 0px;">
												<li>Opci&oacute;n adicional en este paquete: Estar en Destacados en tuabogado.ec por USD 2 (al mes). <i class="fa fa-check"></i></li>
											</ul>
										</div> -->
									</div>
								</div>
								<div class="col-md-4 col-sm-6">
									<div class="pricingTable orange">
										<div class="pricingTable-header">
											<span class="heading">
												<h3 class="textotitle">Plan <?php echo ucwords($plan[2]['plan']); ?></h3>
												<br>
												<!--span class="subtitle">Lorem ipsum dolor sit</span-->
											</span>
											<span class="price-value">$<?php echo $plan[2]['costo']; ?> <br><span><br>AL MES (impuestos incluidos)</span></span><br>
										</div>
										<div class="panel-group" id="accordion3">
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse31">
														Perfil legal profesional
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse31" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/www.png" style="width: 160px;">
															</div>
															<div class="media-body">
																<p>Obtienes tu presencia online.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse32">
														Informaci&oacute;n general
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse32" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/infognral.png" style="width: 95px; height: 95px">
															</div>
															<div class="media-body">
																<p>Nombres y Apellidos / Estudio Jurídico / Ciudad.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse34">
														Contacto
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse34" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/email.png" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>Números de teléfono y cuentas de correo electrónico ilimitados, para que el cliente te contacte fácilmente.</p>
															</div>
														</div>
													</div>
												</div>
											</div>	
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse35">
														&Aacute;reas de pr&aacute;ctica 
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse35" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/areas.png" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>Ilimitado.  Podrás incluir las áreas que quieras.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse33">
														Imagen personalizada
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse33" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/imgpersonal.png" style="width: 95px; height: 95px">
															</div>
															<div class="media-body">
																<p>La foto de perfil que tú elijas.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse36">
														Galería de imágenes
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse36" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/images.png" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>Podrás mostrar hasta 30 imágenes en tu página de perfil profesional.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse37">
														Tu descripci&oacute;n
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse37" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/personal.png" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>Una reseña sobre ti. Ilimitado.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse38">
														Direcci&oacute;n de lugar de trabajo
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse38" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/address.png" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>Dirección exacta de tu lugar de trabajo.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse312">
														<i class="fa fa-angle-right pull-right"></i>
												<?php if($_SESSION['mobile']) { ?>
														Cupones de descuento
												<?php }else{ ?>
														Cupones de descuento para clientes
												<?php } ?>
														</a>
													</h3>
												</div>
												<div id="collapse312" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/cupones.png" style="width: 125px; height: auto">
															</div>
															<div class="media-body">
																<p>Podrás subir a la página web 12 cupones de descuento de tus servicios. Cada publicación de cupón tendrá una duración máxima de 1 mes.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse311">
														<i class="fa fa-angle-right pull-right"></i>
												<?php if($_SESSION['mobile']) { ?>
														Elaboraci&oacute;n de artículos
												<?php }else{ ?>
														Elaboraci&oacute;n de artículos ilimitados
												<?php } ?>
														</a>
													</h3>
												</div>
												<div id="collapse311" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/articles.png" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>Podrás escribir artículos ilimitados sobre cualquier tema legal que prefieras para el blog de la página que será visible para todo público.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse39">
														<i class="fa fa-angle-right pull-right"></i>
													<?php if($_SESSION['mobile']) { ?>
														Geolocalización en Google
													<?php }else{ ?>
														Geolocalización en Google Maps
													<?php } ?>
														</a>
													</h3>
												</div>
												<div id="collapse39" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/geolocation.png" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>Tu ubicación en Google Maps para que los clientes puedan encontrarte con más facilidad.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse310">
														Calificación del servicio
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse310" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/starrate.png" style="width: 160px;">
															</div>
															<div class="media-body">
																<p>Tu propio ranking de atención al cliente, por estrellas y por comentarios de clientes, previa revisión en tu perfil.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse313">
														<i class="fa fa-angle-right pull-right"></i>
													<?php if($_SESSION['mobile']) { ?>
														Bot&oacute;n de cotizaci&oacute;n
													<?php }else{ ?>
														Bot&oacute;n de cotizaci&oacute;n de servicios
													<?php } ?>
														</a>
													</h3>
												</div>
												<div id="collapse313" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/cotizacion.png" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>Los clientes podrán tener una mayor interacción solicitando directamente cotizaciones de tus servicios.  Las cotizaciones te llegarán automáticamente al correo electrónico de tu preferencia.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse314">
														<i class="fa fa-angle-right pull-right"></i>
												<?php if($_SESSION['mobile']) { ?>
														Enlace a tu web o redes sociales
												<?php }else{ ?>
														Enlace a tu sitio web o redes sociales
												<?php } ?>
														</a>
													</h3>
												</div>
												<div id="collapse314" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/socialnetworks.png" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>Si tienes página web o redes sociales, tendrás un enlace directo que dirigirá a los visitantes a tu sitio web, o redes sociales donde podrán conocer más sobre tus servicios.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse315">
														Banner publicitario
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse315" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/bannerpublicitario.png" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>Podrás subir un banner publicitario a la página tuabogado.ec a fin de que sea visible para todo público en la portada principal.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse316">
														Posicionamiento destacado
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse316" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/posicionamiento.png" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>Tendrás un posicionamiento en los primeros lugares, el cual será visible en la portada principal del sitio web. </p>
															</div>
														</div>
													</div>
												</div>
											</div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading">
                                       <h3 class="panel-title">
                                          <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse317">
                                          Compartir en redes sociales
                                          <i class="fa fa-angle-right pull-right"></i>
                                          </a>
                                       </h3>
                                    </div>
                                    <div id="collapse317" class="panel-collapse collapse">
                                       <div class="panel-body">
                                          <div class="media accordion-inner">
                                             <div class="pull-left">
                                                <img class="img-responsive" src="images/shared.jpg" style="height: 95px;">
                                             </div>
                                             <div class="media-body">
                                                <p>Tú o cualquier persona podrá compartir tus artículos y tus cupones de descuento en las redes sociales. </p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse317">
														Un mes de prueba gratis
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse317" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/mesgratis.jpg" style="height: 95px;">
															</div>
															<div class="media-body">
																<p>El primer mes será completamente gratuito.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse318">
														Descuento Especial
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse318" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="pull-left">
																<img class="img-responsive" src="images/10descuento.gif" style="width: 160px;">
															</div>
															<div class="media-body">
																<p>Recibe un descuento del 10% por el pago anual de suscripción a este plan.</p>
															</div>
														</div>
													</div>
												</div>
											</div>

										</div>
										<!-- /  CONTENT BOX-->
										<div class="pricingTable-sign-up">
											<a class="btn btn-block"
												data-plan='3' data-planuser="<?php echo (isset($_SESSION['plan'])) ? $_SESSION['plan']:'0'; ?>"> SELECCIONAR </a>
										</div>
										<!-- BUTTON BOX-->
									</div>
								</div>
							</div>
						</div>
						
						<div class="row team-bar" style="margin-top: 100px;">
							<div class="first-one-arrow hidden-xs">
								<hr>
							</div>
							<div class="first-arrow hidden-xs">
								<hr>
								<i class="fa fa-angle-up"></i>
							</div>
							<div class="second-arrow hidden-xs">
								<hr>
								<i class="fa fa-angle-down"></i>
							</div>
							<div class="third-arrow hidden-xs">
								<hr>
								<i class="fa fa-angle-up"></i>
							</div>
							<div class="fourth-arrow hidden-xs">
								<hr>
								<i class="fa fa-angle-down"></i>
							</div>
						</div>
						<!--skill_border-->
					</div>
				</div>
			</div>

      </section>
      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/markdown.js"></script>
      <script src="js/to-markdown.js"></script>
      <script src="js/bootstrap-markdown.js"></script>
      <script src="js/bootstrap-markdown.es.js"></script>
      <script type="text/javascript" src="js/jquery.simpleslider.js"></script>
      <script type="text/javascript" charset="utf8" src="bower_components/datatables.net/js/jquery.dataTables.min.js"> </script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/jquery.isotope.min.js"></script>
      <script src="js/wow.min.js"></script>
      <script src="js/main.js"></script>
      <script>
         function forceLower(strInput) {
            strInput.value=strInput.value.toLowerCase();
         }
      </script>
		<script>
			$(document).ready(function(){
				$(".btn-block").click(function(){
					var plan = $(this).attr("data-plan");
					var planuser = $(this).attr("data-planuser");
					if(planuser=='0'){
						window.location.href = 'login.php';
					}else if(plan != planuser && plan=='1'){
						if (confirm("Esta seguro que desea bajar de plan?")){
							window.location.href = 'selectplanbasico.php';
						}
					}else if(plan != planuser && plan != '1'){
						window.location.href = 'datafact.php?plan='+plan;
					}else{
						alert('Usted ya se encuentra registrado a este plan.');
					}
				});
			});
		</script>
   </body>
</html>



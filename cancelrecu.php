<?php

session_start(); 
if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");
$config = require 'config.php';

//Conectamos con BD
$conn = mysqli_connect($config['database']['server'],$config['database']['username'],
		$config['database']['password'],$config['database']['db']);

//Actualizamos recurrencia
$sql = "UPDATE transacciones SET recu='0' WHERE df='$_GET[df]'";
if (mysqli_query($conn, $sql)) {
	if(isset($_SESSION['role']) && $_SESSION['role'] == 'admin'){
		header("location: adm_transac_abogados.php");
	}else{
		header("location: mistransacciones.php");
	}
}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

?>

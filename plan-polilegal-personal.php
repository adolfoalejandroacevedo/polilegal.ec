<?php 
   session_start();
   session_unset();
	header('Content-Type: text/html; charset=utf-8');
	$current = "asistencias";
	$config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
   mysqli_set_charset($conn, "utf8");

	//Obtniendo el valor de los planes
	$sql = "SELECT id, costo FROM productos";
   if($result = mysqli_query($conn, $sql)){
     while( $row = mysqli_fetch_assoc($result)){
			$productos[] = array(
				'id' => $row['id'],
				'costo' => $row['costo']
			);
		}
   }else error_log("Error: " . $sql . "..." . mysqli_error($conn));

?>
<!DOCTYPE HTML>
<html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
  <meta http-equiv="content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
  <title>Polilegal | Landing</title>
  <meta name="description" content="">
  <meta name="author" content="adolfo" >
  <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
  <link id="default-css" href="style.css" rel="stylesheet" type="text/css">
  <link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/pricingtable.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="css/main.css" rel="stylesheet">
  <link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" /> 
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
<!--[if lt IE 9]>
  <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--Fonts-->
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--jquery-->
<script src="js/modernizr-2.6.2.min.js"></script>
<style>
		body {font-size: 16px;}
      .contact-form input[type=checkbox].error{
         outline: 1px solid #FF0000;
      }
    .plan1-text{
        color: rgb(29, 62, 136);
        font-size: 18px;
        font-weight: normal;
    }
	.plan2-text{
        color: rgb(29, 62, 136);
        font-size: 18px;
        font-weight: normal;
    }
	 .columnas {float: left; margin: 0px 0px 0px 4%; width: 44%}
	.margintop1 {margin-top: 110px;}
	.margintop2{margin-top: 60px;}
	p {text-align: left;}
	.dt-sc-toggle-frame h5 a {font-size: 21px;}
	.subtitulo {
		text-align: center; 
		margin-bottom: 20px; 
		margin-top: 0px;
	}
	.tdwhite {
		background: white;
	}
	ul.beneficios {
	  list-style-image: url("img/check20x20.png");
	  margin: 30px;
	  padding: 0px;
	}
	@media (max-width: 600px) {
	  .columnas {
		 width: 92%;
	  }
		.logodiv {
			position: relative;
			width: auto;
			height: auto;
			left: 0%;
		}
		.logoheader{margin-left: 0px; height: 140px;}
		.breadcrumb-section{
			padding-top: 50px;
			padding-right: 0px;
			padding-bottom: 25px;
			padding-left: 0px;
		}
		#primary{
			margin-top: 50px;
    		margin-right: 0px;
    		margin-bottom: 0px;
    		margin-left: 0px
		}
		.margintop1 {margin-top: 0px;}
		.margintop2{margin-top: 0px;}
		h5.dt-sc-toggle-accordion a, 
		.dt-sc-toggle-frame h5.dt-sc-toggle-accordion a, 
		.dt-sc-toggle-frame h5.dt-sc-toggle a, 
		h5.dt-sc-toggle-accordion, 
		h5.dt-sc-toggle, 
		h5.dt-sc-toggle-accordion a, 
		h5.dt-sc-toggle a {
			font-size: 19px;
		}
		.subtitulo {
			text-align: center; 
			margin-bottom: 20px; 
			margin-top: 40px;
		}
	}
</style>
</head>
<body>
	<!--wrapper starts-->
  <div class="wrapper">
    <!--inner-wrapper starts-->
    <div class="inner-wrapper">
      <!--header starts-->
		<?php include 'header.php'; ?>
      <!--header ends-->

	 <div class="main">
	 	<section id="submenu" class="content-full-width">
			<div class="breadcrumb-section" style="padding-top: 25px; padding-bottom: 10px;">
				  <div class="container">
						<h1> PLANE POLILEGAL PERSONAL</h1>
						<div class="breadcrumb" style="margin: 0px 0px 0px; font-size: 14px; background-color: #ffffff;">
							 <a href="index.php">Inicio</a>
							 <span class="current"> Plan Polilegal Personal</span>
						</div>
				  </div>
			 </div>
		</section>
		<section id="primary" class="content-full-width">
			<div class="container">
					<div class="row" style="margin-right: 0px; margin-left: 0px;">
						<div class="col-md-4 col-md-offset-4" style="margin-bottom: 40px;">
							<div class="pricingTable green">
								<div class="pricingTable-header">
									<span class="heading">
										<h3 class="textotitle"></h3>
										<br>
										<!--span class="subtitle">Lorem ipsum dolor sit</span-->
									</span>
									<span class="price-value"><br><span><br><br></span></span><br><br>
								</div>
			               <div class="tituloplan">
			                  <h3>Plan Polilegal PERSONAL</h3>
			               </div>
								<div class="panel-group" id="accordion2">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">
												<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse21">
												Ver Detalles
												<i class="fa fa-angle-right pull-right"></i>
												</a>
											</h3>
										</div>
										<div id="collapse21" class="panel-collapse collapse">
											<div class="panel-body">
												<div class="media accordion-inner">
													<div class="media-body">
														<ul class="beneficios" style="text-align: left; margin-top: 10px;">
														<?php
															$sql = "SELECT beneficio FROM beneficios WHERE idproducto='3' ORDER BY orden";
															if($result = mysqli_query($conn, $sql)){
															  while( $row = mysqli_fetch_assoc($result)){
																	echo "<li>$row[beneficio]</li>"; 
																}
															}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
														?>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
									<?php
										$precio = explode(".",$productos[2]['costo']);
									?>
									<div class="sello-precio-circulo-ancho anual" style="margin-left: 50px;margin-top: 20px;">
										<div class="sello-precio-circulo-fino">
											<p class="ahora">Por solo</p>
											<div class="sello-precio-caja">
												<p class="entero"><?php echo $precio[0]; ?></p>
												<p class="resto"><span class="decimales">´<?php echo $precio[1]; ?></span><span class="moneda">$/mes</span></p>
											</div>
											<p class="antes">Servicio Anual</p>
										</div>
									</div>
									<button type="button" class="btn btn-success btn-margin" onclick="window.location.href='membershipform.php?id=3'">SELECCIONAR</button>
			
								</div>
								<!-- /  CONTENT BOX-->
								<!--
								<div class="pricingTable-sign-up" style="padding-bottom: 0px;">
									<a class="btn btn-block" style="background: #1d3e88;"
										data-plan='2' data-planuser="<?php echo (isset($_SESSION['plan'])) ? $_SESSION['plan']:'0'; ?>"> SELECCIONAR </a>
								</div>
								-->
								<!-- BUTTON BOX
								<div class="pricingContent">
									<ul style="padding-top: 0px;">
										<li>Opci&oacute;n adicional en este paquete: Estar en Destacados en tuabogado.ec por USD 2 (al mes). <i class="fa fa-check"></i></li>
									</ul>
								</div> -->
							</div>
						</div>
					</div>
			</div>
						<!--skill_border-->
		</section>
		<section id="primary" class="content-full-width" style="background: #f2f2f2; margin-top: 20px;">
        <div class="container">
           <div class="center wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
                <h2>Beneficios</h2>
            </div>
            <div class="row">
                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" 
								style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap">
                            <i class="fa fa-check"></i>
                            <h3 class="beneficios">100% de cobertura</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" 
								style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap">
                            <i class="fa fa-check"></i>
                            <h3 class="beneficios">Servicios ilimitados</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" 
								style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap">
                            <i class="fa fa-check"></i>
                            <h3 class="beneficios">Personal altamente calificado</h3>
                        </div>
                    </div><!--/.col-md-4-->
                
                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" 
								style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap">
                            <i class="fa fa-check"></i>
                            <h3 class="beneficios">Atención inmediata a los requerimientos</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" 
								style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap">
                            <i class="fa fa-check"></i>
                            <h3 class="beneficios">Garantía de una gestión ágil y eficiente</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" 
								style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap">
                            <i class="fa fa-check"></i>
                            <h3 class="beneficios">Entrega de información oportuna y pormenorizada del trabajo realizado en cada caso</h3>
                        </div>
                    </div><!--/.col-md-4-->
	
                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" 
								style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap">
                            <i class="fa fa-check"></i>
                            <h3 class="beneficios">Acceso a una gran red de abogados a nivel nacional</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" 
								style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap">
                            <i class="fa fa-check"></i>
                            <h3 class="beneficios">Adquisición de un servicio legal de prestigio a un costo razonable</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" 
								style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap">
                            <i class="fa fa-check"></i>
                            <h3 class="beneficios">Acceso a grandes descuentos en servicios no incluidos en las coberturas</h3>
                        </div>
                    </div><!--/.col-md-4-->
               	<!--/.services-->
            </div><!--/.row-->    
			</div><!--/.container-->
    	</section>
		<section id="primary" class="content-full-width" style="margin-top: 75px">
			<div class="container">
				<div class="row">
					<div class="col-md-4 wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
						<div class="clients-comments text-center">
							<img src="images/client11.png" class="img-circle" alt="">
							<h3>Tengo contratado el Plan Polilegal Personal y lo recomiendo.</h3>
							<h4><span>-Andrés Veloz /</span>  Coordinador de Calidad en Ormedic</h4>
						</div>
					</div>
					<div class="col-md-4 wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
						<div class="clients-comments text-center">
							<img src="images/client33.png" class="img-circle" alt="">
							<h3>Hago uso del Plan Polilegal Policial (SP) y la atención es rápida y eficiente.</h3>
							<h4><span>-Guillermo Ramos /</span>  Coronel de Policía de E.M. (SP)</h4>
						</div>
					</div>
					<div class="col-md-4 wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
						<div class="clients-comments text-center">
							<img src="images/client22.png" class="img-circle" alt="">
							<h3>Con el Plan Polilegal he ahorrado dinero y los servicios son de calidad.</h3>
							<h4><span>-Freddy Velasco /</span>  Negocios</h4>
						</div>
					</div>
				</div>
			</div>
		</section>
	 </div>
      <!--footer starts-->
		<?php include 'footer.php'; ?>
      <!--footer ends-->
    <!--inner-wrapper ends-->    
  <!--wrapper ends-->
  <!--wrapper ends-->
	<?php include "asistencias.php"; ?>
  <a href="" title="Go to Top" class="back-to-top"> <span class="fa fa-angle-up"></span> </a> 
  <!--Java Scripts-->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate.min.js"></script>
  <script type="text/javascript" src="js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="js/jquery-easing-1.3.js"></script>
  <script type="text/javascript" src="js/jquery.sticky.js"></script>
  <script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>


  <script type="text/javascript" src="js/jquery.tabs.min.js"></script>
  <script type="text/javascript" src="js/jquery.smartresize.js"></script> 
  <script type="text/javascript" src="js/shortcodes.js"></script>   

  <script type="text/javascript" src="js/custom.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDz1RGAW3gGyDtvmBfnH2_fE2DVVNWq4Eo&callback=initMap" type="text/javascript"></script>
  <script src="js/gmap3.min.js"></script>
  <!-- Layer Slider --> 
  <script type="text/javascript" src="js/jquery-transit-modified.js"></script> 
  <script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script> 
  <script type='text/javascript' src="js/greensock.js"></script> 
  <script type='text/javascript' src="js/layerslider.transitions.js"></script> 

  <script type="text/javascript">var lsjQuery = jQuery;</script><script type="text/javascript"> lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice('layerslider_2','jquery'); } else { lsjQuery("#layerslider_2").layerSlider({responsiveUnder: 1240, layersContainer: 1170, skinsPath: 'js/layerslider/skins/'}) } }); </script>

  <script type="text/javascript">
	  
      jQuery( "#pestana1" ).click(function() {
          
        jQuery('#hf_secund_institucion option:first-child').attr('selected','selected');
        jQuery('#hf_secund_tipo_cuenta option:first-child').attr('selected','selected');
        jQuery('#hf_secund_cuenta').val('');
        jQuery('#hf_first_tipo_cuenta option:first-child').attr('selected','selected');
        jQuery('#hf_first_cuenta').val(''); 
          
        jQuery('#hf_secund_institucion').removeAttr('required');
        jQuery('#hf_secund_tipo_cuenta').removeAttr('required');
        jQuery('#hf_secund_cuenta').removeAttr('required');
        jQuery('#hf_first_tipo_cuenta').attr('required','true');
        jQuery('#hf_first_cuenta').attr('required','true');
        jQuery('#tipo-pago').val('1');
      });

      jQuery( "#pestana2" ).click(function() {
          
        jQuery('#hf_first_tipo_cuenta option:first-child').attr('selected','selected');
        jQuery('#hf_first_cuenta').val('');
        jQuery('#hf_secund_institucion option:first-child').attr('selected','selected');
        jQuery('#hf_secund_tipo_cuenta option:first-child').attr('selected','selected');
        jQuery('#hf_secund_cuenta').val('');
        
        jQuery('#hf_first_tipo_cuenta').removeAttr('required');
        jQuery('#hf_first_cuenta').removeAttr('required');
        jQuery('#hf_secund_institucion').attr('required','true');
        jQuery('#hf_secund_tipo_cuenta').attr('required','true');
        jQuery('#hf_secund_cuenta').attr('required','true');
        jQuery('#tipo-pago').val('2');
      });
      
    </script>

	<!-- Cerrar Session -->
	<script>
		jQuery(document).ready(function(){
			jQuery("#close_session").click(function(){
				alert('Su sesión ha sido cerrada');
				window.location.href='closesession.php';
			});
		});
	</script>

	<script>
		jQuery("#defensatitle").click(function() {
			 jQuery('html,body').animate({
				  scrollTop: $("#defensacontent").offset().top},
				  'slow');
		});
	</script>

</body>
</html>

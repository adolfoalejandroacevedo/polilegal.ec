<?php
	header('Content-Type: text/html; charset=utf-8');
   session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");
   if(!isset($_SESSION['admin'])) header("location: index.php");
   $include = ($_SESSION['admin']) ? "adm_header.php":"header.php";
   $urlorigen = ($_SESSION['admin']) ? "admin.php":"miperfil.php";
   $urltitulo =  ($_SESSION['admin']) ? "Abogados":"Mi Perfil";
   $titulo =  ($_SESSION['admin']) ? "Banners":"Mis Banners";
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
	mysqli_set_charset($conn, "utf8");
	if(isset($_GET['idbanner'])){
		$sql = "SELECT idbanner, banner FROM banners WHERE idbanner='$_GET[idbanner]'";
	}else{
		$sql = "SELECT idbanner, banner FROM banners WHERE iduser='$_SESSION[id]'";
	}
	if ($result = mysqli_query($conn, $sql)) {
		$row = mysqli_fetch_assoc($result); 
		$idbanner = (isset($row['idbanner'])) ? $row['idbanner']:'0';
	}
	else error_log("Error: " . $sql . "..." . mysqli_error($conn));
?>

<!DOCTYPE html>
<html>
   <head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>
         Tu Abogado:
         Anuncios :: Directorio de Abogados del Ecuador
      </title>
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <link href="css/animate.min.css" rel="stylesheet">
      <link href="css/prettyPhoto.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="bower_components/datatables.net-dt/css/jquery.dataTables.css">
      <link rel="stylesheet" type="text/css" href="css/bootstrap-markdown.min.css">
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/markdown.js"></script>
      <script src="js/to-markdown.js"></script>
      <script src="js/bootstrap-markdown.js"></script>
      <script src="js/bootstrap-markdown.es.js"></script>
      <script type="text/javascript" src="js/jquery.simpleslider.js"></script>
      <script type="text/javascript" charset="utf8" src="bower_components/datatables.net/js/jquery.dataTables.min.js"> </script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/jquery.isotope.min.js"></script>
      <script src="js/wow.min.js"></script>
      <script src="js/main.js"></script>
      <script src="js/custom.js"></script>
      <!-- JQuery Validator and form -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.js"></script>
      <style>
			.nota p{
				margin-bottom: 15px;
				font-size: 14px;
				font-weight: 600;
				margin-left: 30px;
				margin-right: 30px;
				text-align: center;
			}
         /* Color rojo para el texto de error de los campos */
         .error{
            color: red;
         }
         /* Borde rojo y grosor de linea de los inputs */
         .frmarticulo input[type=text].error {
            padding:15px 18px;
            border:1px solid #FF0000;
         }
			textarea.error {
				border:1px solid #FF0000;	
			}
			.panel-body .media .row .form-group{
			margin-left: 45px;
			margin-right: 45px;
			}
         .pricingTable{
         text-align: center;
         transition: all 0.5s ease 0s;
         }
         .pricingTable:hover{
         box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
         }
         .pricingTable .pricingTable-header{
         color: #feffff;
         }
         .pricingTable .heading{
         display: block;
         padding-top: 25px;
         }
         .pricingTable .heading > h3{
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .textotitle{
         color: #feffff;
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .pricingTable .subtitle{
         display: block;
         font-size: 13px;
         margin-top: 5px;
         text-transform: capitalize;
         }
         .pricingTable .price-value{
         display: block;
         font-size: 60px;
         font-weight: 700;
         padding-bottom: 25px;
         }
         .pricingTable .price-value span{
         display: block;
         font-size: 14px;
         line-height: 20px;
         text-transform: uppercase;
         }
         .pricingTable .pricingContent{
         /*text-transform: capitalize;*/
         background: #fbfbfb;
         color: #fefeff;
         }
         .pricingTable .pricingContent ul{
         list-style: none;
         padding: 15px 14px 10px;
         margin: 0;
         text-align: left;
         }
         .pricingTable .pricingContent ul li{
         font-size: 14px;
         padding: 12px 0;
         border-bottom: 1px dashed #e1e1e1;
         color: #9da1ad;
         }
         .pricingTable .pricingContent ul li i{
         font-size: 14px;
         float: right;
         }
         .pricingTable .pricingTable-sign-up{
         padding: 20px 0;
         background: #fbfbfb;
         color: #fff;
         text-transform: capitalize;
         }
         .pricingTable .btn-block{
         width: 60%;
         margin: 0 auto;
         font-size: 17px;
         color: #fff;
         text-transform: capitalize;
         border: none;
         border-radius: 5px;
         padding: 10px;
         transition: all 0.5s ease 0s;
         }
         .pricingTable .btn-block:before{
         content: "\f007";
         font-family: 'FontAwesome';
         margin-right: 10px;
         }
         .pricingTable.blue .pricingTable-header,
         .pricingTable.blue .btn-block{
         background: #fce400;
         }
         .pricingTable.pink .pricingTable-header,
         .pricingTable.pink .btn-block{
         background: #c9b600;
         }
         .pricingTable.orange .pricingTable-header,
         .pricingTable.orange .btn-block{
         background: #e0d83a;
         }
         .pricingTable.green .pricingTable-header,
         .pricingTable.green .btn-block{
         background: #e0cd00;
         }
         .pricingTable.blue .btn-block:hover,
         .pricingTable.pink .btn-block:hover,
         .pricingTable.orange .btn-block:hover,
         .pricingTable.green .btn-block:hover{
         background: #e6e6e6;
         color: #939393;
         }
         @media screen and (max-width: 990px){
         .pricingTable{ margin-bottom: 20px; }
         }
			.breadcrumb>.active{
			color: #c1ab09;
    		font-weight: 600;
			}
			.glyphicon-trash{
				cursor: pointer;
			}
			/* Para la imagen personalizada */
          #drop_file_zone {
              background-color: #EEE;
              border: #999 5px dashed;
              width: 100%;
              height: 100px;
              padding-left: 0px;
              padding-right: 0px;
              padding-top: 0px;
              padding-bottom: 0px;
              font-size: 18px;
          }
          #drag_upload_file {
              width:100%;
              margin:0 auto;
          }
          #drag_upload_file p {
              text-align: center;
				  font-family: 'Open Sans', Arial, sans-serif;
				  font-size: 16px;
				  margin-bottom: 5px;
          }
          #drag_upload_file #selectfile {
              display: none;
          }
          .elimina {
              cursor: pointer;
          }
      </style>
   </head>
   <body class="homepage">
		<?php include $include; ?>
      <!--/header-->
      <section id="blog" class="container">
         <ol class="breadcrumb">
         <li><a href="<?php echo $urlorigen; ?>"><?php echo $urltitulo; ?></a></li>
			<?php if($_SESSION['admin']) { ?>
         	<li><a href="adm_banners.php"><?php echo $titulo; ?></a></li>
			<?php } ?>
         <li class="active">Banner Publicitario</li>
         </ol>
         <div class="jumbotron">
            <div class="container">
               <div class="center">
                  <h2> Crear Banner</h2>
               </div>
               <div class="row">
						<div class="nota">
							<p> Arrastre y suelte su imagen publicitaria aquí. </p>
						</div>
						<div id="drop_file_zone" ondrop="upload_file(event)" ondragover="return false" align="center">
							<img id="enviando" src="images/barra.gif" width="100px" height="25px" 
							  style="position: relative; vertical-align:middle; display: none; margin-top: 100px">
						</div>
							<div id="imagen" style="margin-top: 30px;">
							<?php if(isset($row['banner'])) { ?>
								<img src='<?php echo (isset($row['banner'])) ? "uploads/".$row['banner']:''; ?>' style='height: auto; width: 100%' \>
							<?php } ?>
							</div>
							<div id="drag_upload_file" style="margin-top: 10px;">
							  <p><input type="button" value="Seleccione" onclick="file_explorer();"></p>
							  <input type="file" id="selectfile">
							</div>
							<div class="nota" id='delimgbanner' data-idreg="<?php echo $_SESSION['id']; ?>">
								<p class="elimina">ELIMINAR</p>
							</div>
						</div>
               </div>
               <div class="center" style="padding-bottom: 10px;">
                  <h2 style="margin-top: 25px;"> Notas para la imagen</h2>
               </div>
					<div class="nota">
						<ul>
							<li>Se recomienda que la imagen tenga un ancho de 1669px y un alto de 607px.</li>
						</ul>
					</div>
            </div>
         </div>
			<!-- Zona de mensajes -->
			<div id="mensajes"></div>
			<!-- FIN Zona de mensajes -->
      </section>
      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
		<!-- Subida de imagen personalizada -->
      <script type="text/javascript">
          var fileobj;
          function upload_file(e) {
              e.preventDefault();
              fileobj = e.dataTransfer.files[0];
              ajax_file_upload(fileobj);
          }
       
          function file_explorer() {
              document.getElementById('selectfile').click();
              document.getElementById('selectfile').onchange = function() {
                  fileobj = document.getElementById('selectfile').files[0];
                  ajax_file_upload(fileobj);
              };
          }
       
          function ajax_file_upload(file_obj) {
              if(file_obj != undefined) {
						var idart = $("input[name='idart']").val();
                  var form_data = new FormData();                  
                  form_data.append('file', file_obj);
                  form_data.append('idbanner', <?php echo $idbanner; ?>);
						$("#enviando").show();
                  $.ajax({
                      type: 'POST',
                      url: 'imgbanner.php',
                      contentType: false,
                      processData: false,
                      data: form_data,
							 dataType: 'jsonp',
                      success: function(data, textStatus, jqXHR) {
									console.log(data);
                          $('#imagen').html("<img src='uploads/" + data.file + "' style='height: auto; width: 100%' \>"); 
                          $('#imagen').show();
                          $('#selectfile').val('');
								  $("#enviando").hide();
								  $("#idart").attr("value",data.id);
                      },
							 error: function (data, textStatus, errorThrown)
							 {
								console.warn(data, textStatus, errorThrown);
							 }
                  });
              }
          }
      </script>

		<!-- Cambia el cursor cuando pasa por div fotos -->
		<script type="text/javascript">
			$(".fotos").hover(function() {
				$(this).css('cursor','pointer');
			}, function() {
				$(this).css('cursor','auto');
			});
		</script>
   </body>
</html>

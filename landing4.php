<?php 
   session_start();
   session_unset();
	header('Content-Type: text/html; charset=utf-8');
	$config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
   mysqli_set_charset($conn, "utf8");

	//Obteniendo el valor de la afiliacion
	$sql = "SELECT costo FROM productos WHERE id='1'";
	if($result = mysqli_query($conn, $sql)){
		$row = mysqli_fetch_assoc($result);
		$amount = $row['costo'];
	}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
?>
<!DOCTYPE HTML>
<html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
  <meta http-equiv="content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
  <title>Polilegal | Landing</title>
  <meta name="description" content="">
  <meta name="author" content="adolfo" >
  <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
  <link id="default-css" href="style.css" rel="stylesheet" type="text/css">
  <link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/pricingtable.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="css/main.css" rel="stylesheet">
  <link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" /> 
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
<!--[if lt IE 9]>
  <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--Fonts-->
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--jquery-->
<script src="js/modernizr-2.6.2.min.js"></script>
<style>
		body {font-size: 16px;}
      .contact-form input[type=checkbox].error{
         outline: 1px solid #FF0000;
      }
    .plan1-text{
        color: rgb(29, 62, 136);
        font-size: 18px;
        font-weight: normal;
    }
	.plan2-text{
        color: rgb(29, 62, 136);
        font-size: 18px;
        font-weight: normal;
    }
	 .columnas {float: left; margin: 0px 0px 0px 4%; width: 44%}
	.margintop1 {margin-top: 110px;}
	.margintop2{margin-top: 60px;}
	p {text-align: left;}
	.dt-sc-toggle-frame h5 a {font-size: 21px;}
	.subtitulo {
		text-align: center; 
		margin-bottom: 20px; 
		margin-top: 0px;
	}
	.tdwhite {
		background: white;
	}
	ul.beneficios {
	  list-style-image: url("img/check20x20.png");
	  margin: 30px;
	  padding: 0px;
	}
	@media (max-width: 600px) {
	  .columnas {
		 width: 92%;
	  }
		.logodiv {
			position: relative;
			width: auto;
			height: auto;
			left: 0%;
		}
		.logoheader{margin-left: 0px; height: 140px;}
		.breadcrumb-section{
			padding-top: 50px;
			padding-right: 0px;
			padding-bottom: 25px;
			padding-left: 0px;
		}
		#primary{
			margin-top: 50px;
    		margin-right: 0px;
    		margin-bottom: 0px;
    		margin-left: 0px
		}
		.margintop1 {margin-top: 0px;}
		.margintop2{margin-top: 0px;}
		h5.dt-sc-toggle-accordion a, 
		.dt-sc-toggle-frame h5.dt-sc-toggle-accordion a, 
		.dt-sc-toggle-frame h5.dt-sc-toggle a, 
		h5.dt-sc-toggle-accordion, 
		h5.dt-sc-toggle, 
		h5.dt-sc-toggle-accordion a, 
		h5.dt-sc-toggle a {
			font-size: 19px;
		}
		.subtitulo {
			text-align: center; 
			margin-bottom: 20px; 
			margin-top: 40px;
		}
	}
</style>
</head>
<body>
	<!--wrapper starts-->
  <div class="wrapper">
    <!--inner-wrapper starts-->
    <div class="inner-wrapper">
      <!--header starts-->
		<?php include 'header.php'; ?>
      <!--header ends-->

      <section id="blog" class="container" style="margin-top: 150px;">
          <div class="container" style="padding-left: 0px; padding-right: 0px; margin-right: 0px;">
				<div class="skill-wrap clearfix" style="margin-bottom: 20px;">
					<div class="">
						<div class="container">
							<div class="row" style="margin-right: 0px; margin-left: 0px;">
								<div class="col-md-4 col-sm-6">
									<div class="pricingTable green">
										<div class="pricingTable-header">
											<span class="heading">
												<h3 class="textotitle"></h3>
												<br>
												<!--span class="subtitle">Lorem ipsum dolor sit</span-->
											</span>
											<span class="price-value"><br><span><br><br></span></span><br><br>
										</div>
                              <div class="tituloplan">
                                 <h3>Plan Polilegal PERSONAL</h3>
                              </div>
										<div class="panel-group" id="accordion2">
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse21">
														Ver Detalles
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse21" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="media-body">
																<ul class="beneficios" style="text-align: left; margin-top: 10px;">
																  <li>Asesoría en cualquier asunto legal</li>
																  <li>50% de descuento en todos los servicios no incluidos en la cobertura</li>
																  <li>Escritos de procuración judicial</li>
																  <li>Escritos para permiso de salida del país de menores</li>
																  <li>Poderes generales y especiales</li>
																  <li>Requerimientos judiciales (ej: escritos de actualización SUPA, extinción de 
																  pensión de alimentos, petición de salida de vehículo retenido, etc)</li>
																  <li>Minuta de compraventa de bienes inmuebles</li>
																  <li>Declaraciones juramentadas</li>
																  <li>Cancelación de hipotecas</li>
																  <li>Elaboración de todo tipo de contratos</li>
																  <li>Peticiones ante entidades públicas</li>
																  <li>Posesión efectiva</li>
																  <li>Extinción de patrimonio familiar</li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="sello-precio-circulo-ancho anual" style="margin-left: 50px;margin-top: 20px;">
												<div class="sello-precio-circulo-fino">
													<p class="ahora">Por solo</p>
													<div class="sello-precio-caja">
														<p class="entero">9</p>
														<p class="resto"><span class="decimales">´99</span><span class="moneda">$/mes</span></p>
													</div>
													<p class="antes">Servicio Anual</p>
												</div>
											</div>
											<button type="button" class="btn btn-success" onclick="window.location.href='membershipform.php?id=3'"
											style="margin-top: 75px;margin-left: 10px;">SELECCIONAR</button>

										</div>
										<!-- /  CONTENT BOX-->
										<!--
										<div class="pricingTable-sign-up" style="padding-bottom: 0px;">
											<a class="btn btn-block" style="background: #1d3e88;"
												data-plan='2' data-planuser="<?php echo (isset($_SESSION['plan'])) ? $_SESSION['plan']:'0'; ?>"> SELECCIONAR </a>
										</div>
										-->
										<!-- BUTTON BOX
										<div class="pricingContent">
											<ul style="padding-top: 0px;">
												<li>Opci&oacute;n adicional en este paquete: Estar en Destacados en tuabogado.ec por USD 2 (al mes). <i class="fa fa-check"></i></li>
											</ul>
										</div> -->
									</div>
								</div>
								<div class="col-md-4 col-sm-6">
									<div class="pricingTable blue">
										<div class="pricingTable-header">
											<span class="heading">
												<h3 class="textotitle"></h3>
												<br>
												<!--span class="subtitle">Lorem ipsum dolor sit</span-->
											</span>
											<span class="price-value"><br><span><br><br></span></span><br><br>
										</div>
										<div class="tituloplan">
											<h3>Plan Polilegal POLICIAL</h3>
										</div>
										<div class="panel-group" id="accordion1">
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1">
														Ver Detalles
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapseOne1" class="panel-collapse collapse">
													<div class="panel-body">
														<div class="media accordion-inner">
															<div class="media-body">
                                                <ul class="beneficios" style="text-align: left; margin-top: 10px;">
                                                  <li><a href="landing5.php">Policías en servicio activo</a></li>
                                                  <li><a href="landing5.php">Policías en servicio pasivo y familiares</a></li>
                                                  <li><a href="landing5.php">Cadetes</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="sello-precio-circulo-ancho anual" style="margin-left: 50px;margin-top: 20px;">
												<div class="sello-precio-circulo-fino">
													<p class="ahora">Por solo</p>
													<div class="sello-precio-caja">
														<p class="entero">6</p>
														<p class="resto"><span class="decimales">´99</span><span class="moneda">$/mes</span></p>
													</div>
													<p class="antes">Servicio Anual</p>
												</div>
											</div>
											<button type="button" class="btn btn-success" onclick="window.location.href='planes-policiales.php'"
											style="margin-top: 75px;margin-left: 10px;">SELECCIONAR</button>
										</div>
										<!-- /  CONTENT BOX-->
										<!--
										<div class="pricingTable-sign-up">
											<a class="btn btn-block" style="background: #1d3e88;" 
												data-plan='1' data-planuser="<?php echo (isset($_SESSION['plan'])) ? $_SESSION['plan']:'0'; ?>"> SELECCIONAR </a>
										</div>
										-->
										<!-- BUTTON BOX-->
									</div>
								</div>
								
								<div class="col-md-4 col-sm-6">
									<div class="pricingTable orange">
										<div class="pricingTable-header">
											<span class="heading">
												<h3 class="textotitle"></h3>
												<br>
												<!--span class="subtitle">Lorem ipsum dolor sit</span-->
											</span>
											<span class="price-value"><br><span><br><br></span></span><br><br>
										</div>
                              <div class="tituloplan">
                                 <h3>Plan Polilegal EMPRESARIAL</h3>
                              </div>
										<div class="panel-group" id="accordion3">
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse31">
														Ver Detalles
														<i class="fa fa-angle-right pull-right"></i>
														</a>
													</h3>
												</div>
												<div id="collapse31" class="panel-collapse collapse">
													<div class="panel-group" id="accordion32">
														<div class="panel panel-default">
															<div class="panel-heading" style="background-color: white;">
																<ul class="beneficios" style="text-align: left; margin-top: 0px; margin-bottom: 0px;">
																<li><h3 class="panel-title">
																	<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion32" 
																		href="#collapse321" aria-expanded="false">
																	Reforma de estatutos sociales
																	</a>
																</h3></li>
															</div>
															<div id="collapse321" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
																<div class="panel-body">
																	<div class="media accordion-inner">
																		<div class="media-body">
																			<p>Cambio de domicilio, aumento o reducción de capital, cambios en el objeto o denominación
																			social y todo lo relacionado con modificaciones estatutarias.</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="panel panel-default">
															<div class="panel-heading" style="background-color: white;">
																<ul class="beneficios" style="text-align: left; margin-top: 0px; margin-bottom: 0px;">
																<li><h3 class="panel-title">
																	<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion32" 
																		href="#collapse322" aria-expanded="false">
																	Elaboración de convocatorias y actas  de juntas de los socios
																	</a>
																</h3></li>
															</div>
															<div id="collapse322" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
																<div class="panel-body">
																	<div class="media accordion-inner">
																		<div class="media-body">
																			<p>Elaboración de convocatorias para la celebración de juntas ordinarias o extraordinarias, de
																			conformidad las disposiciones legales y estatutarias, así como supervisión legal de la asamblea
																			(constatar el quórum requerido, lectura del orden del día, etc.) y elaboración formal de las
																			actas donde se detalla el desarrollo de la asamblea.</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="panel panel-default">
															<div class="panel-heading" style="background-color: white;">
																<ul class="beneficios" style="text-align: left; margin-top: 0px; margin-bottom: 0px;">
																<li><h3 class="panel-title">
																	<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion32" 
																		href="#collapse323" aria-expanded="false">
																	Elaboración o renovación de nombramientos e inscripción en el Registro Mercantil
																	</a>
																</h3></li>
															</div>
															<div id="collapse323" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
																<div class="panel-body">
																	<div class="media accordion-inner">
																		<div class="media-body">
																			<p>Elaboramos el documento en el cual se posesiona o renueva el cargo de los cargos con
																			relevancia societaria, y la posterior tramitación e inscripción de los mismos en el Registro
																			Mercantil
																			</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="panel panel-default">
															<div class="panel-heading" style="background-color: white;">
																<ul class="beneficios" style="text-align: left; margin-top: 0px; margin-bottom: 0px;">
																<li><h3 class="panel-title">
																	<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion32" 
																		href="#collapse324" aria-expanded="false">
																	Elaboración de contratos de compraventa
																	</a>
																</h3></li>
															</div>
															<div id="collapse324" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
																<div class="panel-body">
																	<div class="media accordion-inner">
																		<div class="media-body">
																			<p>
																				En los casos en que la empresa desee comprar o vender algún bien mueble o inmueble
																			</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="panel panel-default">
															<div class="panel-heading" style="background-color: white;">
																<ul class="beneficios" style="text-align: left; margin-top: 0px; margin-bottom: 0px;">
																<li><h3 class="panel-title">
																	<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion32" 
																		href="#collapse325" aria-expanded="false">
																	Elaboración de contratos con empleados, proveedores y clientes
																	</a>
																</h3></li>
															</div>
															<div id="collapse325" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
																<div class="panel-body">
																	<div class="media accordion-inner">
																		<div class="media-body">
																			<p>
																				Elaboramos contratos para los empleados de la empresa y proveedores externos, así como
																				también para tus clientes, bajo la modalidad que más convenga.
																			</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="panel panel-default">
															<div class="panel-heading" style="background-color: white;">
																<ul class="beneficios" style="text-align: left; margin-top: 0px; margin-bottom: 0px;">
																<li><h3 class="panel-title">
																	<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion32" 
																		href="#collapse326" aria-expanded="false">
																	Patrocinio en juicios por cobro de dinero
																	</a>
																</h3></li>
															</div>
															<div id="collapse326" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
																<div class="panel-body">
																	<div class="media accordion-inner">
																		<div class="media-body">
																			<p>
																				Te patrocinamos en los casos en que quieras cobrar una deuda, a través de los procedimientos
																				judiciales oportunos; y, te defendemos en caso de que tengas un juicio en tu contra. Máximo
																				3 eventos por año.
																			</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="panel panel-default">
															<div class="panel-heading" style="background-color: white;">
																<ul class="beneficios" style="text-align: left; margin-top: 0px; margin-bottom: 0px;">
																<li><h3 class="panel-title">
																	<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion32" 
																		href="#collapse327" aria-expanded="false">
																	Elaboración de peticiones ante entidades públicas
																	</a>
																</h3></li>
															</div>
															<div id="collapse327" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
																<div class="panel-body">
																	<div class="media accordion-inner">
																		<div class="media-body">
																			<p>
																				Escritos encaminados a solicitar información, gestión o trámite en cualquier institución del
																				Estado
																			</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="panel panel-default">
															<div class="panel-heading" style="background-color: white;">
																<ul class="beneficios" style="text-align: left; margin-top: 0px; margin-bottom: 0px;">
																<li><h3 class="panel-title">
																	<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion32" 
																		href="#collapse328" aria-expanded="false">
																	Elaboración de declaraciones juramentadas
																	</a>
																</h3></li>
															</div>
															<div id="collapse328" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
																<div class="panel-body">
																	<div class="media accordion-inner">
																		<div class="media-body">
																			<p>
																				Sobre cualquier tema relacionado a la empresa
																			</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="panel panel-default">
															<div class="panel-heading" style="background-color: white;">
																<ul class="beneficios" style="text-align: left; margin-top: 0px; margin-bottom: 0px;">
																<li><h3 class="panel-title">
																	<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion32" 
																		href="#collapse329" aria-expanded="false">
																	Elaboración de poderes generales y especiales
																	</a>
																</h3></li>
															</div>
															<div id="collapse329" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
																<div class="panel-body">
																	<div class="media accordion-inner">
																		<div class="media-body">
																			<p>
																			</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="panel panel-default">
															<div class="panel-heading" style="background-color: white;">
																<ul class="beneficios" style="text-align: left; margin-top: 0px; margin-bottom: 0px;">
																<li><h3 class="panel-title">
																	<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion32" 
																		href="#collapse330" aria-expanded="false">
																	Revisión de documentos relacionados con la empresa
																	</a>
																</h3></li>
															</div>
															<div id="collapse330" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
																<div class="panel-body">
																	<div class="media accordion-inner">
																		<div class="media-body">
																			<p>
																				Revisamos cualquier tipo de documento de tu empresa en beneficio de tus intereses
																			</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="panel panel-default">
															<div class="panel-heading" style="background-color: white;">
																<ul class="beneficios" style="text-align: left; margin-top: 0px; margin-bottom: 0px;">
																<li><h3 class="panel-title">
																	<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion32" 
																		href="#collapse331" aria-expanded="false">
																	Asesoría ilimitada en asuntos legales relacionados con el giro de negocio
																	</a>
																</h3></li>
															</div>
															<div id="collapse331" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
																<div class="panel-body">
																	<div class="media accordion-inner">
																		<div class="media-body">
																			<p>
																				Asesoría en temas societarios, tributarios, laborales y otros que tengan que ver estrictamente
																				con la compañía.
																			</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="panel panel-default">
															<div class="panel-heading" style="background-color: white;">
																<ul class="beneficios" style="text-align: left; margin-top: 0px; margin-bottom: 0px;">
																<li><h3 class="panel-title">
																	<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion32" 
																		href="#collapse332" aria-expanded="false">
																	50% de descuento en servicios legales no incluidos en la cobertura
																	</a>
																</h3></li>
															</div>
															<div id="collapse332" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
																<div class="panel-body">
																	<div class="media accordion-inner">
																		<div class="media-body">
																			<p>
																			</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="sello-precio-circulo-ancho anual" style="margin-left: 50px;margin-top: 20px;">
												<div class="sello-precio-circulo-fino">
													<p class="ahora">Por solo</p>
													<div class="sello-precio-caja">
														<p class="entero">49</p>
														<p class="resto"><span class="decimales">´99</span><span class="moneda">$/mes</span></p>
													</div>
													<p class="antes">Servicio Anual</p>
												</div>
											</div>
											<button type="button" class="btn btn-success" onclick="window.location.href='membershipform.php?id=7'"
											style="margin-top: 75px;margin-left: 10px;">SELECCIONAR</button>
										</div>
										<!-- /  CONTENT BOX-->
										<!--
										<div class="pricingTable-sign-up">
											<a class="btn btn-block" style="background: #1d3e88;"
												data-plan='3' data-planuser="<?php echo (isset($_SESSION['plan'])) ? $_SESSION['plan']:'0'; ?>"> SELECCIONAR </a>
										</div>
										-->
										<!-- BUTTON BOX-->
									</div>
								</div>
						
						<!--skill_border-->
					</div>
				</div>
			</div>

      </section>		

		<section id="feature">
        <div class="container">
           <div class="center wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
                <h2>Beneficios</h2>
            </div>

            <div class="row">
                <div class="features">
                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" 
								style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap">
                            <i class="fa fa-check"></i>
                            <h3 class="beneficios">100% de cobertura</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" 
								style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap">
                            <i class="fa fa-check"></i>
                            <h3 class="beneficios">Servicio ilimitado</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" 
								style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap">
                            <i class="fa fa-check"></i>
                            <h3 class="beneficios">Personal altamente calificado</h3>
                        </div>
                    </div><!--/.col-md-4-->
                
                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" 
								style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap">
                            <i class="fa fa-check"></i>
                            <h3 class="beneficios">Atención inmediata a los requerimientos</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" 
								style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap">
                            <i class="fa fa-check"></i>
                            <h3 class="beneficios">Garantía de una gestión ágil y eficiente</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" 
								style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap">
                            <i class="fa fa-check"></i>
                            <h3 class="beneficios">Entrega de información oportuna y pormenorizada del trabajo realizado en cada caso</h3>
                        </div>
                    </div><!--/.col-md-4-->
	
                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" 
								style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap">
                            <i class="fa fa-check"></i>
                            <h3 class="beneficios">Acceso a una gran red de abogados a nivel nacional</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" 
								style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap">
                            <i class="fa fa-check"></i>
                            <h3 class="beneficios">Adquisición de un servicio legal de prestigio a un costo razonable</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" 
								style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap">
                            <i class="fa fa-check"></i>
                            <h3 class="beneficios">Acceso al 50% de descuento en otros servicios no incluidos en la cobertura</h3>
                        </div>
                    </div><!--/.col-md-4-->

                </div><!--/.services-->
            </div><!--/.row-->    
				        	</div><!--/.container-->
    	</section>
		<section id="testimoniales" style="margin-top: 75px">
			<div class="container">
				<div class="row">
					<div class="col-md-4 wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
						<div class="clients-comments text-center">
							<img src="images/client1.png" class="img-circle" alt="">
							<h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</h3>
							<h4><span>-John Doe /</span>  Director of corlate.com</h4>
						</div>
					</div>
					<div class="col-md-4 wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
						<div class="clients-comments text-center">
							<img src="images/client2.png" class="img-circle" alt="">
							<h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</h3>
							<h4><span>-John Doe /</span>  Director of corlate.com</h4>
						</div>
					</div>
					<div class="col-md-4 wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
						<div class="clients-comments text-center">
							<img src="images/client3.png" class="img-circle" alt="">
							<h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</h3>
							<h4><span>-John Doe /</span>  Director of corlate.com</h4>
						</div>
					</div>
				</div>
			</div>
		</section>

      <!--footer starts-->
		<?php include 'footer.php'; ?>
      <!--footer ends-->
    </div>
    <!--inner-wrapper ends-->    
  </div>
  <!--wrapper ends-->
  <!--wrapper ends-->
	<?php include "asistencias.php"; ?>
  <a href="" title="Go to Top" class="back-to-top"> <span class="fa fa-angle-up"></span> </a> 
  <!--Java Scripts-->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate.min.js"></script>
  <script type="text/javascript" src="js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="js/jquery-easing-1.3.js"></script>
  <script type="text/javascript" src="js/jquery.sticky.js"></script>
  <script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>


  <script type="text/javascript" src="js/jquery.tabs.min.js"></script>
  <script type="text/javascript" src="js/jquery.smartresize.js"></script> 
  <script type="text/javascript" src="js/shortcodes.js"></script>   

  <script type="text/javascript" src="js/custom.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDz1RGAW3gGyDtvmBfnH2_fE2DVVNWq4Eo&callback=initMap" type="text/javascript"></script>
  <script src="js/gmap3.min.js"></script>
  <!-- Layer Slider --> 
  <script type="text/javascript" src="js/jquery-transit-modified.js"></script> 
  <script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script> 
  <script type='text/javascript' src="js/greensock.js"></script> 
  <script type='text/javascript' src="js/layerslider.transitions.js"></script> 

  <script type="text/javascript">var lsjQuery = jQuery;</script><script type="text/javascript"> lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice('layerslider_2','jquery'); } else { lsjQuery("#layerslider_2").layerSlider({responsiveUnder: 1240, layersContainer: 1170, skinsPath: 'js/layerslider/skins/'}) } }); </script>

  <script type="text/javascript">
	  
      jQuery( "#pestana1" ).click(function() {
          
        jQuery('#hf_secund_institucion option:first-child').attr('selected','selected');
        jQuery('#hf_secund_tipo_cuenta option:first-child').attr('selected','selected');
        jQuery('#hf_secund_cuenta').val('');
        jQuery('#hf_first_tipo_cuenta option:first-child').attr('selected','selected');
        jQuery('#hf_first_cuenta').val(''); 
          
        jQuery('#hf_secund_institucion').removeAttr('required');
        jQuery('#hf_secund_tipo_cuenta').removeAttr('required');
        jQuery('#hf_secund_cuenta').removeAttr('required');
        jQuery('#hf_first_tipo_cuenta').attr('required','true');
        jQuery('#hf_first_cuenta').attr('required','true');
        jQuery('#tipo-pago').val('1');
      });

      jQuery( "#pestana2" ).click(function() {
          
        jQuery('#hf_first_tipo_cuenta option:first-child').attr('selected','selected');
        jQuery('#hf_first_cuenta').val('');
        jQuery('#hf_secund_institucion option:first-child').attr('selected','selected');
        jQuery('#hf_secund_tipo_cuenta option:first-child').attr('selected','selected');
        jQuery('#hf_secund_cuenta').val('');
        
        jQuery('#hf_first_tipo_cuenta').removeAttr('required');
        jQuery('#hf_first_cuenta').removeAttr('required');
        jQuery('#hf_secund_institucion').attr('required','true');
        jQuery('#hf_secund_tipo_cuenta').attr('required','true');
        jQuery('#hf_secund_cuenta').attr('required','true');
        jQuery('#tipo-pago').val('2');
      });
      
    </script>

	<!-- Cerrar Session -->
	<script>
		jQuery(document).ready(function(){
			jQuery("#close_session").click(function(){
				alert('Su sesión ha sido cerrada');
				window.location.href='closesession.php';
			});
		});
	</script>

	<script>
		jQuery("#defensatitle").click(function() {
			 jQuery('html,body').animate({
				  scrollTop: $("#defensacontent").offset().top},
				  'slow');
		});
	</script>

</body>
</html>

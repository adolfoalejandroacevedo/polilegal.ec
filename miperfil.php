<?php 
	session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");
	include "detect_mobile.php";
	include "visitas.php";
	$_SESSION['admin'] = false;
	header('Content-Type: text/html; charset=utf-8');
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
	mysqli_set_charset($conn, "utf8");

	//Calculo de cotizaciones sin leer
	$sql = "SELECT count(*) cotiza FROM cotizaciones WHERE userid='$_SESSION[id]' AND leido='No'";
   if ($result = mysqli_query($conn, $sql)){
      $row = mysqli_fetch_assoc($result);
		$cotiza = $row['cotiza'];
   }else error_log("Error: " . $sql . "..." . mysqli_error($conn));

	//Calculo de recomendaciones
	$sql = "SELECT count(*) recomen FROM item_rating WHERE userId='$_SESSION[id]' AND leido='No'";
   if ($result = mysqli_query($conn, $sql)){
      $row = mysqli_fetch_assoc($result);
      $recomen = $row['recomen'];
   }else error_log("Error: " . $sql . "..." . mysqli_error($conn));
	
	//Registro del usuario
	$sql = "SELECT users.id id, nombres, ciudad, planes.plan plan, users.plan planid, matricula 
			  FROM users 
			  INNER JOIN planes 
			  ON users.plan=idplan 
			  WHERE users.id='$_SESSION[id]'";
	if ($result = mysqli_query($conn, $sql)){
		$row = mysqli_fetch_assoc($result);
	}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

?>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>
         Tu Abogado:
         Users :: Directorio de Abogados del Ecuador
      </title>
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <link href="css/animate.min.css" rel="stylesheet">
      <link href="css/prettyPhoto.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="bower_components/datatables.net-dt/css/jquery.dataTables.css">
      <!-- JQuery Validator and form -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.js"></script>

      <style>
			.nota p{
				margin-bottom: 15px;
				font-size: 14px;
				font-weight: 600;
				margin-left: 30px;
				margin-right: 30px;
				text-align: center;
			}
         /* Color del mensaje de error
         del checkbox y posicion debajo del cuadro */
         .errorMsq {
            color: red;
            display: block;
          }
          /* Color rojo para el texto de error de los campos */
         .error{
            color: red;
         }
         /* Borde rojo y grosor de linea de los inputs */
         .email-form input[type=email].error,
         .email-form input[type=text].error,
         .infogeneral-form input[type=text].error,
         .infogeneral-form input[type=password].error{
            padding:15px 18px;
            border:1px solid #FF0000;
         }
			.infogeneral-form select.error{
				padding:15px 18px;
    			border:1px solid #FF0000;
			}			
         /* Grosor y color de linea del checkbox en modo error */
         .reg-form input[type=checkbox].error{
            outline: 1px solid #FF0000;
         }
			.panel-body .media .row .form-group{
			margin-left: 45px;
			margin-right: 45px;
			}
         .pricingTable{
         text-align: center;
         transition: all 0.5s ease 0s;
         }
         .pricingTable:hover{
         box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
         }
         .pricingTable .pricingTable-header{
         color: #feffff;
         }
         .pricingTable .heading{
         display: block;
         padding-top: 25px;
         }
         .pricingTable .heading > h3{
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .textotitle{
         color: #feffff;
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .pricingTable .subtitle{
         display: block;
         font-size: 13px;
         margin-top: 5px;
         text-transform: capitalize;
         }
         .pricingTable .price-value{
         display: block;
         font-size: 60px;
         font-weight: 700;
         padding-bottom: 25px;
         }
         .pricingTable .price-value span{
         display: block;
         font-size: 14px;
         line-height: 20px;
         text-transform: uppercase;
         }
         .pricingTable .pricingContent{
         /*text-transform: capitalize;*/
         background: #fbfbfb;
         color: #fefeff;
         }
         .pricingTable .pricingContent ul{
         list-style: none;
         padding: 15px 14px 10px;
         margin: 0;
         text-align: left;
         }
         .pricingTable .pricingContent ul li{
         font-size: 14px;
         padding: 12px 0;
         border-bottom: 1px dashed #e1e1e1;
         color: #9da1ad;
         }
         .pricingTable .pricingContent ul li i{
         font-size: 14px;
         float: right;
         }
         .pricingTable .pricingTable-sign-up{
         padding: 20px 0;
         background: #fbfbfb;
         color: #fff;
         text-transform: capitalize;
         }
         .pricingTable .btn-block{
         width: 60%;
         margin: 0 auto;
         font-size: 17px;
         color: #fff;
         text-transform: capitalize;
         border: none;
         border-radius: 5px;
         padding: 10px;
         transition: all 0.5s ease 0s;
         }
         .pricingTable .btn-block:before{
         content: "\f007";
         font-family: 'FontAwesome';
         margin-right: 10px;
         }
         .pricingTable.blue .pricingTable-header,
         .pricingTable.blue .btn-block{
         background: #fce400;
         }
         .pricingTable.pink .pricingTable-header,
         .pricingTable.pink .btn-block{
         background: #c9b600;
         }
         .pricingTable.orange .pricingTable-header,
         .pricingTable.orange .btn-block{
         background: #e0d83a;
         }
         .pricingTable.green .pricingTable-header,
         .pricingTable.green .btn-block{
         background: #e0cd00;
         }
         .pricingTable.blue .btn-block:hover,
         .pricingTable.pink .btn-block:hover,
         .pricingTable.orange .btn-block:hover,
         .pricingTable.green .btn-block:hover{
         background: #e6e6e6;
         color: #939393;
         }
         @media screen and (max-width: 990px){
         .pricingTable{ margin-bottom: 20px; }
         }
			.breadcrumb>.active{
			color: #c1ab09;
    		font-weight: 600;
			}
			.glyphicon-trash{
				cursor: pointer;
			}
			/* Para la imagen personalizada */
          #drop_file_zone {
              background-color: #EEE;
              border: #999 5px dashed;
              width: 260px;
              height: 260px;
              padding-left: 0px;
              padding-right: 0px;
              padding-top: 0px;
              padding-bottom: 0px;
              font-size: 18px;
          }
          #drag_upload_file {
              width:50%;
              margin:0 auto;
          }
          #drag_upload_file p {
              text-align: center;
				  font-family: 'Open Sans', Arial, sans-serif;
				  font-size: 16px;
				  margin-bottom: 5px;
          }
          #drag_upload_file #selectfile {
              display: none;
          }
			 #delimg, #misarticulos, #makearticulo, #destacarplan, #miscotizaciones,
			 #miscupones, #misrecomendaciones, #makecupon, #portfolio, #makebanner {
			 	  cursor: pointer;
			 }
			.notification {
			/*
			  background-color: #fff;
			  color: white;
			  text-decoration: none;
			  padding: 15px 26px;
			  position: relative;
			  display: inline-block;
			  border-radius: 2px;
			  margin-left: 30px;
			*/
			}

			.notification:hover {
			  background: #fff;
			}

			.notification .badge {
			  /*position: absolute;*/
			  top: -10px;
			  right: -10px;
			  padding: 5px 10px;
			  border-radius: 50%;
			  background-color: red;
			  color: white;
			}
			.badge {
				position: static;
			}
      </style>
   </head>
   <body class="homepage">
<?php include "header.php"; ?>
      <!--/header-->
      <section id="blog" class="container" style="padding-top: 40px">
			<div class="row">
				<div class="col-sm-6 breadcrumb">
					<li class="active" style="font-size: 25px"><i>PLAN: <?php echo mb_strtoupper($row['plan']); ?></i></li>
				</div>
				<div class="col-sm-6 breadcrumb" align="right">
					<li class="active" style="font-size: 18px"><i>Veces que han visto mi perfil: <?php echo $visitasabogado; ?></i></li>
				</div>
			</div>
         <div class="jumbotron wow fadeInDown">
         	<div class="container">
            	<div class="center" style="padding-bottom: 20px">
               	<h2>
                  	<span class="full_name"><?php echo $row['nombres']; ?></span>      
               	</h2>
            	</div>
            	<div class="col-md-8">
            		<div class="row">
							<div class="panel-group" id="accordion1">
								<div class="panel panel-default">
									<div class="panel-heading active">
										<h3 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1">
												Información General
												<i class="fa fa-angle-right pull-right"></i>
											</a>
										</h3>
									</div>
									<div id="collapseOne1" class="panel-collapse collapse in">
										<div class="panel-body">
											<div class="media accordion-inner">
												<div class="row contact-wrap">
													<form id="frminfogeneral" class="infogeneral-form" name="frminfogeneral" method="post" action="update.php">
														<input type="hidden" name='formname' value="frminfogeneral">
														<div class="form-group">
															 <label>Nombres y Apellidos / Estudio Jurídico *</label>
														 	<input type="text" name="nombres" class="form-control" required="required"
														 	value="<?php echo $row["nombres"]; ?>">
														</div>
														<div class="form-group">
														 	<label>Matrícula Profesional *</label>
														 	<input type="text" name="matricula" class="form-control" required="required"
															placeholder="Si eres Estudio Jurídico coloca el número de Matrícula Profesional de cualquiera de sus integrantes"
														 	value="<?php echo $row["matricula"]; ?>">
														</div>
														<div class="form-group">
															<label>Ciudad *</label>
														 	<select name="ciudad" class="form-control"  id="ciudad">
															<option value="" SELECTED>-- Seleccione una ciudad --</option>
															<?php
																$sql = "SELECT * FROM ciudades WHERE importancia='1' ORDER BY ciudad";
																if ($result = mysqli_query($conn, $sql)){
																	while ($row2 = mysqli_fetch_assoc($result)) {
																		if ($row['ciudad'] == $row2['id']){
																			 echo "<option value='$row2[id]' SELECTED>".$row2['ciudad']."</option>";
																		}else{
																			 echo "<option value='$row2[id]'>".$row2['ciudad']."</option>";
																		}
																	}
																}else{
																	error_log("Error: " . $sql . "..." . mysqli_error($conn));
																}
															?>
															</select>
														</div>
														<div class="form-group" align="center">
														 	<button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Guardar</button>
														 	<img id="enviando" src="images/barra.gif" width="100px" height="25px" 
																  style="position: relative; vertical-align:middle; display: none;">
														</div>
													</form>
												</div>
												<!-- Zona de mensajes -->
												<div id="mensajes"></div>
												<!-- FIN Zona de mensajes -->
											</div>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo1">
											Contacto
											<i class="fa fa-angle-right pull-right"></i>
											</a>
										</h3>
									</div>
									<div id="collapseTwo1" class="panel-collapse collapse">
										<div class="panel-body">
											<div class="media accordion-inner">
												<div class="row contact-wrap">
												<?php
													if ($row['planid'] < 3){

														//Mostramos correo para los planes 1 y 2
														$sql = "SELECT * FROM emails WHERE iduser='$row[id]' ORDER BY id DESC LIMIT 1";
														if ($result = mysqli_query($conn, $sql)){
															$row3 = mysqli_fetch_assoc($result);
														}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
														$sql = "SELECT phone FROM phones WHERE iduser='$row[id]'";
														if ($result = mysqli_query($conn, $sql)){
															$row31 = mysqli_fetch_assoc($result);
														}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
												?>				
													<form id="frmemail" class="email-form" name="frmemail" method="post" action="update.php">
														<input type="hidden" name='formname' value="frmemail">
														<input type="hidden" name='iduser' value="<?php echo $row['id']; ?>">
														<div class="form-group">
															 <label>Email *</label>
															 <input type="email" name="email" class="form-control" required="required"
															 value="<?php echo $row3["email"]; ?>">
														</div>
												<?php
														if ($row['planid'] == '2'){
												?>
														<div class="form-group">
															 <label>Teléfono *</label>
															 <input type="text" name="phone" class="form-control" 
															 value="<?php echo $row31["phone"]; ?>">
														</div>
												<?php } ?>
														<div class="form-group" align="center">
															 <button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Guardar</button>
															 <img id="enviandoemail" src="images/barra.gif" width="100px" height="25px" 
																	  style="position: relative; vertical-align:middle; display: none;">
														</div>
													</form>
												<?php 
													}else{
												?>
													<form id="frmemails" class="email-form" name="frmemails" method="post" action="update.php">
														<input type="hidden" name='formname' value="frmemails">
														<input type="hidden" name='iduser' value="<?php echo $row['id']; ?>">
														<div class="form-group">
															 <label>Email *</label>
															 <input type="email" id="inputemail" name="email" class="form-control" placeholder="Correo" required="required">
														</div>
														<div id='lstcorreos' class="correos">
															<?php
																$sql = "SELECT * FROM emails WHERE iduser='$row[id]'";
																if ($result = mysqli_query($conn, $sql)){
																	while($row4 = mysqli_fetch_assoc($result)){
																		echo "<div class='form-group'>";
																		echo "<a href'#'><span value='$row4[email]' id='$row[id]' 
																				data-idreg='$row4[id]' class='glyphicon glyphicon-trash emails'></span></a> 
																				<span id='idcontact$row[id]$row4[id]'> $row4[email]</span>";
																		echo "</div>";
																	}
																}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
															?>
														</div>
														<div class="form-group" align="center">
															 <button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Guardar</button>
															 <img id="enviandoemail2" src="images/barra.gif" width="100px" height="25px" 
																	  style="position: relative; vertical-align:middle; display: none;">
														</div>
													</form>
													<form id="frmphones" class="email-form" name="frmphones" method="post" action="update.php">
														<input type="hidden" name='formname' value="frmphones">
														<input type="hidden" name='iduser' value="<?php echo $row['id']; ?>">
														<input type="hidden" name='plan' value="<?php echo $row['planid']; ?>">
														<div class="form-group">
															 <label>Teléfono *</label>
															 <input type="text" id="inputphone" name="phone" class="form-control" placeholder="Teléfono" required="required">
														</div>
														<div id='lstphones' class="lstphones">
															<?php
																$sql = "SELECT * FROM phones WHERE iduser='$row[id]'";
																if ($result = mysqli_query($conn, $sql)){
																	while($row5 = mysqli_fetch_assoc($result)){
																		echo "<div class='form-group'>";
																		echo "<a href'#'><span value='$row5[phone]' id='$row[id]' 
																				data-idreg='$row5[id]' class='glyphicon glyphicon-trash phones'></span></a> 
																				<span id='idphones$row[id]$row5[id]'> $row5[phone]</span>";
																		echo "</div>";
																	}
																}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
															?>
														</div>
														<div class="form-group" align="center">
															 <button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Guardar</button>
															 <img id="enviandophone" src="images/barra.gif" width="100px" height="25px" 
																	  style="position: relative; vertical-align:middle; display: none;">
														</div>
													</form>
												<?php	} ?>
												</div>
												<!-- Zona de mensajes -->
												<div id="msgemail2"></div>
												<!-- FIN Zona de mensajes -->
												<?php
													if ($row['planid'] == 1 OR $row['planid'] == 2){
												?>
												<div class="nota">
													<p> NOTA: El plan Gold cuenta con números de teléfono y cuentas de correo electrónico ilimitados. 
														 Puedes adquirir el plan Gold haciendo click <a href="planes.php">Aquí</a>.
													</p>
												</div>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse3">
											Áreas de práctica
											<i class="fa fa-angle-right pull-right"></i>
											</a>
										</h3>
									</div>
									<div id="collapse3" class="panel-collapse collapse">
										<div class="panel-body">
											<div class="media accordion-inner">
												<div class="row contact-wrap">
													<form id="frmareas" class="email-form" name="frmareas" method="post" action="update.php">
														<input type="hidden" name='formname' value="frmareas">
														<input type="hidden" name='iduser' value="<?php echo $row['id']; ?>">
														<div class="form-group">
															 <label>Áreas *</label>
																 <select name="area_id" class="form-control"  id="area_id">
																	 <option value="">-- Seleccione un área --</option>
																		 <?php
																			 $sql = "SELECT * FROM areas ORDER BY area";
																			 if ($result = mysqli_query($conn, $sql)){
																				 while ($row6 = mysqli_fetch_assoc($result)) {
																					echo "<option value='$row6[id]'>".$row6['area']."</option>";
																				 }
																			 }else{
																				 error_log("Error: " . $sql . "..." . mysqli_error($conn));
																			 }
																		 ?>
																 </select>
														</div>
														<div id='lstareas' class="lstareas">
															<?php
																$sql = "SELECT usersareas.id as id, iduser, idarea, area 
																		  FROM usersareas INNER JOIN areas ON usersareas.idarea = areas.id 
																		  WHERE iduser='$row[id]'";
																if ($result = mysqli_query($conn, $sql)){
																	while($row7 = mysqli_fetch_assoc($result)){
																		echo "<div class='form-group'>";
																		echo "<a href'#'><span value='$row7[area]' id='$row[id]' 
																				data-idreg='$row7[id]' class='glyphicon glyphicon-trash areas'></span></a> 
																				<span id='idareas$row[id]$row7[id]'> ".$row7['area']."</span>";
																		echo "</div>";
																	}
																}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
															?>
														</div>   
														<div class="form-group" align="center">
															 <button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Guardar</button>
															 <img id="enviandoarea" src="images/barra.gif" width="100px" height="25px" 
																	  style="position: relative; vertical-align:middle; display: none;">
														</div>
														<div class="nota">
															 <p>NOTA: Si existe un área que no aparezca en esta página, por favor ponte en contacto con nosotros.</p>
														</div>
													</form>
												</div>
												<!-- Zona de mensajes -->
												<div id="msg-area"></div>
												<!-- FIN Zona de mensajes -->
											</div>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse4">
											Imagen Personalizada
											<i class="fa fa-angle-right pull-right"></i>
											</a>
										</h3>
									</div>
									<div id="collapse4" class="panel-collapse collapse">
										<div class="panel-body" align="center">
										<?php 
											if($_SESSION['plan'] > 1){
										?>
											<div class="nota">
												<p> Arrastra y suelta tu imagen aquí. Se recomienda que sea de 250x250 y extensión JPG. </p>
											</div>
											<div id="drop_file_zone" ondrop="upload_file(event)" ondragover="return false" align="center">
												 <img id="enviando" src="images/barra.gif" width="100px" height="auto" 
												  style="position: relative; vertical-align:middle; display: none; margin-top: 100px">
											<?php
													$sql = "SELECT foto FROM users WHERE id='$_SESSION[id]'";
													if($result = mysqli_query($conn, $sql)){
														if(mysqli_num_rows($result) > 0) {
															$row7 = mysqli_fetch_assoc($result);
															if(!is_null($row7['foto'])){
																$foto = true;
												?>
																<div id="imagen">
																	<img src='uploads/<?php echo $row7['foto']; ?>' style='width: 250px; height: 250px;' \>
																</div>
													<?php	}else{ $foto = false; ?>
																<div id="imagen" style="display: none; "></div>
													<?php }
														}
													}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
											?>
											</div>
												 <div id="drag_upload_file" style="margin-top: 10px;">
													  <p><input type="button" value="Seleccione" onclick="file_explorer();"></p>
													  <input type="file" id="selectfile">
												 </div>
													<div class="nota" id='delimg' data-idreg="<?php echo $_SESSION['id']; ?>">
														<p class="elimina">ELIMINAR</p>
													</div>
										<?php
											}else{
										?>
											<div class="nota">
												<p> NOTA: En los planes Silver y Gold podras colocar tu imagen personalizada.
													 Puedes adquirir el plan Silver o Gold haciendo click <a href="planes.php">Aquí</a>.
												</p>	
											</div>
										<?php	} ?>
											<!-- Zona de mensajes -->
											<div id="msg-upload-img"></div>
											<!-- FIN Zona de mensajes -->
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse5">
											Tu descripción
											<i class="fa fa-angle-right pull-right"></i>
											</a>
										</h3>
									</div>
									<div id="collapse5" class="panel-collapse collapse">
										<div class="panel-body">
											<div class="media accordion-inner">
												<div class="row contact-wrap">
												<?php 
													if($row['planid'] == 1){
												?>
													<div class="nota">
														<p>Sólo disponible en los planes "Silver" y "Gold". 
														Puedes adquirir el plan Silver o Gold haciendo click <a href="planes.php">Aquí</a>.</p>
													</div>
												<?php }else{ ?>
													<form id="frmdescription" class="infogeneral-form" name="frmdescription" method="post" action="update.php">
														<input type="hidden" name='formname' value="frmdescription">
														<div class="form-group">
															<label>Descripción *</label>
															<?php
																$sql = "SELECT description FROM descriptions WHERE iduser='$_SESSION[id]'";
																if($result = mysqli_query($conn, $sql)){
																	while ($row8 = mysqli_fetch_assoc($result)) $description = $row8['description'];
																}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
															?>
															<textarea name="description" class="form-control" 
															rows="5" 
															maxlength="<?php echo ($row['planid'] == 2) ? '335':''; ?>"
															><?php echo (isset($description)) ? $description:NULL; ?></textarea>
														</div>
														<div class="form-group" align="center">
															<button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Guardar</button>
															<img id="send-description" src="images/barra.gif" width="100px" height="25px" 
																style="position: relative; vertical-align:middle; display: none;">
														</div>
													</form>
													<!-- Zona de mensajes -->
													<div id="msg-description"></div>
													<!-- FIN Zona de mensajes -->
													<?php } 
														if($row['planid'] == 2){
													?>
													<div class="nota">
														<p> NOTA: En el plan Gold puedes tener una descripción ilimitada.
															 Puedes adquirir el Gold haciendo click <a href="planes.php">Aquí</a>.
														</p>
													</div>
													<?php } ?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse6">
											<i class="fa fa-angle-right pull-right"></i>
									<?php if($_SESSION['mobile']) { ?>
											Dirección de trabajo
									<?php }else{ ?>
											Dirección de lugar de trabajo
									<?php } ?>
											</a>
										</h3>
									</div>
									<div id="collapse6" class="panel-collapse collapse">
										<div class="panel-body">
											<div class="media accordion-inner">
												<div class="row contact-wrap">
												<?php 
													if($row['planid'] == 1){
												?>
													<div class="nota">
														<p>Sólo disponible en los planes "Silver" y "Gold". 
														Puedes adquirir el plan Silver o Gold haciendo click <a href="planes.php">Aquí</a>.</p>
													</div>
												<?php } else { ?>
													<form id="frmjobaddress" class="infogeneral-form" name="frmjobaddress" method="post" action="update.php">
														<input type="hidden" name='formname' value="frmjobaddress">
														<div class="form-group">
															<label>Dirección de trabajo *</label>
															<?php
																$sql = "SELECT address FROM jobaddress WHERE iduser='$_SESSION[id]'";
																if($result = mysqli_query($conn, $sql)){
																	while ($row9 = mysqli_fetch_assoc($result)) $address = $row9['address'];
																}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
															?>
															<textarea name="address" class="form-control" 
															rows="5"><?php echo (isset($address)) ? $address:NULL; ?></textarea>
														</div>
														<div class="form-group" align="center">
															<button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Guardar</button>
															<img id="send-address" src="images/barra.gif" width="100px" height="25px" 
																style="position: relative; vertical-align:middle; display: none;">
														</div>
													</form>
													<!-- Zona de mensajes -->
													<div id="msg-address"></div>
													<!-- FIN Zona de mensajes -->
												<?php } ?>
												</div>
											</div>
										</div>
									</div>
								</div>							
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse7">
											Geolocalización
											<i class="fa fa-angle-right pull-right"></i>
											</a>
										</h3>
									</div>
									<div id="collapse7" class="panel-collapse collapse">
										<div class="panel-body">
											<div class="media accordion-inner">
												<div class="row contact-wrap">
												<?php 
													if($row['planid'] < 3){
												?>
													<div class="nota">
														<p>Sólo disponible en el plan "Gold". 
														Puedes adquirir el plan Gold haciendo click <a href="planes.php">Aquí</a>.</p>
													</div>
												<?php } else { ?>
													<form id="frmgeo" class="infogeneral-form" name="frmgeo" method="post" action="update.php">
														<input type="hidden" name='formname' value="frmgeo">
														<div class="form-group">
															<label>Dirección de google maps *</label>
															<?php
																$sql = "SELECT geo FROM geolocation WHERE iduser='$_SESSION[id]'";
																if($result = mysqli_query($conn, $sql)){
																	while ($row10 = mysqli_fetch_assoc($result)) $geo = $row10['geo'];
																}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
															?>
															<textarea name="geo" class="form-control" 
															rows="2"><?php echo (isset($geo)) ? $geo:NULL; ?></textarea>
														</div>
														<div class="form-group" align="center">
															<button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Guardar</button>
															<img id="send-geo" src="images/barra.gif" width="100px" height="25px" 
																style="position: relative; vertical-align:middle; display: none;">
														</div>
													</form>
													<!-- Zona de mensajes -->
													<div id="msg-geo"></div>
													<!-- FIN Zona de mensajes -->
													<div class="nota" style="margin-left: 34px; margin-right: 34px">
														<p><b>Geolocalización:</b> Url de google maps de su lugar de trabajo. Para obtener el Url, sigue estos pasos:</p>
														<ul class="icons" style="padding-right: 10px">
															<li><i class="icon-double-angle-right"></i> Ve a <a href="https://www.google.co.ve/maps/" 
																	 target="_blank">Google Maps</a></li>
															<li><i class="icon-double-angle-right"></i> Ubica la dirección de tu oficina. </li>
															<li><i class="icon-double-angle-right"></i> Haz click en "Compartir". </li>
															<li><i class="icon-double-angle-right"></i> Haz click en donde dice "Insertar o Incorporar un mapa".</li>
															<li><i class="icon-double-angle-right"></i> Haz click en "COPIAR HTML".</li>
															<li><i class="icon-double-angle-right"></i> Finalmente pega la dirección haciendo click con el botón derecho del mouse
																										y selecciona "Pegar" sobre el campo "Geolocalización".</li>
														</ul>
													</div>
													<div class="nota" style="margin-left: 50px; margin-right: 50px">
														<h3>Vista Previa</h3>
														<?php
															$url = "http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=disney+paris&amp;
																	aq=&amp;sll=37.0625,-95.677068&amp;sspn=39.371738,86.572266&amp;ie=UTF8&amp;hq=disney&amp;
																	hnear=Paris,+%C3%8Ele-de-France,+France&amp;t=m&amp;fll=48.881877,2.535095&amp;
																	fspn=0.512051,1.352692&amp;st=103241701817924407489&amp;rq=1&amp;ev=zo&amp;split=1&amp;
																	ll=49.027964,2.772675&amp;spn=0.315159,0.585022&amp;z=10&amp;iwloc=D&amp;output=embed";
															$urlmap = (isset($geo)) ? $geo:$url;
														?>
														<iframe class="gmap" id="geomap" src="<?php echo $urlmap; ?>" style="width: 100%; height: 254px"></iframe>
													</div>
												<?php } ?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse8">
											Sitio web y redes sociales
											<i class="fa fa-angle-right pull-right"></i>
											</a>
										</h3>
									</div>
									<div id="collapse8" class="panel-collapse collapse">
										<div class="panel-body">
											<div class="media accordion-inner">
												<div class="row contact-wrap">
												<?php 
													if($row['planid'] < 3){
												?>
													<div class="nota">
														<p>Sólo disponible en el plan "Gold". 
														Puedes adquirir el plan Gold haciendo click <a href="planes.php">Aquí</a>.</p>
													</div>
												<?php } else {
													$sql = "SELECT url FROM sitioweb WHERE iduser='$row[id]'";
													if ($result = mysqli_query($conn, $sql)){
														$row2 = mysqli_fetch_assoc($result);
													} else error_log("Error: " . $sql . "..." . mysqli_error($conn));		
												?>
													<form id="frmweb" class="email-form" name="frmweb" method="post" action="update.php">
														<input type="hidden" name='formname' value="frmweb">
														<input type="hidden" name='iduser' value="<?php echo $row['id']; ?>">
														<div class="form-group">
															 <label>http:// *</label>
															 <input type="text" name="web" class="form-control"
															 value="<?php echo $row2["url"]; ?>">
														</div>
														<div class="form-group" align="center">
															 <button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Guardar</button>
															 <img id="enviandourl" src="images/barra.gif" width="100px" height="25px" 
																	  style="position: relative; vertical-align:middle; display: none;">
														</div>
													</form>
													<!-- Zona de mensajes -->
													<div id="msg-url"></div>
													<!-- FIN Zona de mensajes -->
													<form id="frmsocial" class="email-form" name="frmsocial" method="post" action="update.php">
														<input type="hidden" name='formname' value="frmsocial">
														<input type="hidden" name='iduser' value="<?php echo $row['id']; ?>">
														<div class="form-group">
															 <label>Redes sociales *</label>
																 <select name="social_id" class="form-control"  id="social_id">
																	 <option value="">-- Seleccione --</option>
																		 <?php
																			 $sql = "SELECT * FROM socialnetworks ORDER BY name";
																			 if ($result = mysqli_query($conn, $sql)){
																				 while ($row6 = mysqli_fetch_assoc($result)) {
																					echo "<option value='$row6[id]'>".utf8_encode($row6['name'])."</option>";
																				 }
																			 }else{
																				 error_log("Error: " . $sql . "..." . mysqli_error($conn));
																			 }
																		 ?>
																 </select>
														</div>
														<div class="form-group">
															<label>Url *</label>
															<input type="text" name="url" class="form-control" required="required">
														</div>
														<div id='lstsocial' class="lstsocial">
															<?php
																$sql = "SELECT t1.id id, t2.name red, t1.url url 
																		  FROM social t1 INNER JOIN socialnetworks t2 ON t1.idsocial=t2.id 
																		  WHERE t1.iduser='$_SESSION[id]'";
																if ($result = mysqli_query($conn, $sql)){
																	while($row7 = mysqli_fetch_assoc($result)){
																		echo "<div class='form-group'>";
																		echo "<a href'#'><span value='$row7[red]' id='$row[id]' 
																				data-idreg='$row7[id]' class='glyphicon glyphicon-trash socials'></span></a> 
																				<span id='idsocial$row[id]$row7[id]'> $row7[red]: $row7[url]</span>";
																		echo "</div>";
																	}
																}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
															?>
														</div>
														<div class="form-group" align="center">
															 <button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Guardar</button>
															 <img id="enviandosocial" src="images/barra.gif" width="100px" height="25px" 
																	  style="position: relative; vertical-align:middle; display: none;">
														</div>
													</form>
												<?php } ?>
												</div>
												<!-- Zona de mensajes -->
												<div id="msg-social"></div>
												<!-- FIN Zona de mensajes -->
											</div>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<?php
										$sql = "SELECT nombres,
															identificacion,
															telefono,
															direccion,
															correo
													FROM datafactura
													WHERE iduser='$_SESSION[id]'";
                                       if ($result = mysqli_query($conn, $sql)){
                                          $row2 = mysqli_fetch_assoc($result);
                                       } else error_log("Error: " . $sql . "..." . mysqli_error($conn));

									?>
									<div class="panel-heading">
										<h3 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne9">
												Datos de Facturación
												<i class="fa fa-angle-right pull-right"></i>
											</a>
										</h3>
									</div>
									<div id="collapseOne9" class="panel-collapse collapse">
										<div class="panel-body">
											<div class="media accordion-inner">
												<div class="row contact-wrap">
													<form id="frmfactura" class="factura-form" name="frmfactura" method="post" action="update.php">
														<input type="hidden" name='formname' value="frmfactura">
														<input type="hidden" name='ambiente' value="miperfil">
														<div class="form-group">
															<label for="nombres">Nombres *</label>
														 	<input type="text" name="nombres" class="form-control" 
															value="<?php echo (isset($row2['nombres'])) ? $row2['nombres']:NULL; ?>" required="required">
														</div>
														<div class="form-group">
															<label for="doc_nr">Cédula o RUC *</label>
															<input type="text" class="form-control" id="doc_nr" name="doc_nr" 
															value="<?php echo (isset($row2['identificacion'])) ? $row2['identificacion']:NULL; ?>" required="required">
														</div>
														<div class="form-group">
															<label for="phone">Teléfono *</label>
															<input type="text" class="form-control" id="phone" name="phone" 
															value="<?php echo (isset($row2['telefono'])) ? $row2['telefono']:NULL; ?>" required="required">
														</div>
														<div class="form-group">
															<label for="address">Dirección *</label>
														 	<input type="text" name="address" class="form-control" 
															value="<?php echo (isset($row2['direccion'])) ? $row2['direccion']:NULL; ?>" required="required">
														</div>
														<div class="form-group">
															<label for="email">Correo electrónico *</label>
														 	<input type="text" name="email" class="form-control" 
															value="<?php echo (isset($row2['correo'])) ? $row2['correo']:NULL; ?>" required="required">
														</div>
														<div class="form-group" align="center">
														 	<button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Guardar</button>
														 	<img id="barra" src="images/barra.gif" width="100px" height="25px" 
																  style="position: relative; vertical-align:middle; display: none;">
														</div>
													</form>
												</div>
												<!-- Zona de mensajes -->
												<div id="msg-datafact"></div>
												<!-- FIN Zona de mensajes -->
											</div>
										</div>
									</div>
								</div>

						</div>
						<!--/#accordion1-->
               </div>
            </div>
            <div class="col-md-3 col-md-offset-1">
            	<div class="row">
                 	<ul class="list-group">
            	     	<li class="list-group-item"><a id="misarticulos">Mis Artículos</a></li>
                    	<!-- <li class="list-group-item"><a id="makearticulo">Crear un Artículos</a></li> -->
                 	</ul>
                 	<ul class="list-group">
                    	<li class="list-group-item"><a id="miscupones">Mis Cupones</a></li>
                    	<!-- <li class="list-group-item"><a id="makecupon">Crear un Cupón</a></li> -->
                 	</ul>
                 	<ul class="list-group">
                    	<li class="list-group-item"><a id="misrecomendaciones">Mis Recomendaciones</a>
								<a href="#" class="notification">
								  <span class="badge"><?php echo ($recomen == 0) ? "":$recomen; ?></span>
								</a>
							</li>
                    	<!-- <li class="list-group-item"><a id="makecupon">Crear un Cupón</a></li> -->
                 	</ul>
                 	<ul class="list-group">
                    	<li class="list-group-item"><a href="mistransacciones.php">Mis Transacciones</a></li>
                    	<!-- <li class="list-group-item"><a id="makecupon">Crear un Cupón</a></li> -->
                 	</ul>
						<ul class="list-group">
                    	<li class="list-group-item"><a id="miscotizaciones">Mis Cotizaciones</a>
                        <a href="#" class="notification">
                          <span class="badge"><?php echo ($cotiza == 0) ? "":$cotiza; ?></span>
                        </a>
							</li>
                    	<!-- <li class="list-group-item"><a id="makecupon">Crear un Cupón</a></li> -->
                 	</ul>
                 	<ul class="list-group">
                    	<li class="list-group-item"><a id="portfolio">Galeria de imágenes</a></li>
                 	</ul>
                 	<ul class="list-group">
                    	<li class="list-group-item"><a id="makebanner">Banner Publicitario</a></li>
                 	</ul>              
						<?php if($_SESSION['plan'] == 2) { ?>
                 	<ul class="list-group">
                    	<li class="list-group-item"><a id="destacarplan">Posicionamiento Destacado</a></li>
                 	</ul>
						<?php } ?>
                 	<ul class="list-group">
                    	<li class="list-group-item"><a href="frmchangeuser.php" >Cambiar Usuario</a></li>
                 	</ul>
                 	<ul class="list-group">
                    	<li class="list-group-item"><a href="request-reset-password.php" >Cambiar Contraseña</a></li>
                 	</ul>
						<?php if($_SESSION['role'] == 'admin') { ?>
                 	<ul class="list-group">
                    	<li class="list-group-item"><a href="admin.php" >Administrar</a></li>
                 	</ul>
						<?php } ?>
               </div>
            </div>
         </div>
			</div>      
      </section>
      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->

      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="js/bootstrap.min.js"></script>
      <script src="js/markdown.js"></script>
      <script src="js/to-markdown.js"></script>
      <script src="js/bootstrap-markdown.js"></script>
      <script src="js/bootstrap-markdown.es.js"></script>
      <script type="text/javascript" src="js/jquery.simpleslider.js"></script>
      <script type="text/javascript" charset="utf8" src="bower_components/datatables.net/js/jquery.dataTables.min.js"> </script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/jquery.isotope.min.js"></script>
      <script src="js/wow.min.js"></script>
      <script src="js/main.js"></script>
      <script src="js/custom.js"></script>
		<!-- Subida de imagen personalizada -->
      <script type="text/javascript">
          var fileobj;
          function upload_file(e) {
              e.preventDefault();
              fileobj = e.dataTransfer.files[0];
              ajax_file_upload(fileobj);
          }
       
          function file_explorer() {
              document.getElementById('selectfile').click();
              document.getElementById('selectfile').onchange = function() {
                  fileobj = document.getElementById('selectfile').files[0];
                  ajax_file_upload(fileobj);
              };
          }
       
          function ajax_file_upload(file_obj) {
              if(file_obj != undefined) {
                  var form_data = new FormData();                  
                  form_data.append('file', file_obj);
                  form_data.append('iduser', <?php echo $_SESSION['id']; ?>);
						$("#enviando").show();
                  $.ajax({
                      type: 'POST',
                      url: 'imgperfil.php',
                      contentType: false,
                      processData: false,
                      data: form_data,
                      success:function(response) {
								console.log("la respuesta es ", response);
							 	if(response=="badextension"){
									$('#msg-upload-img').html("\
										  <div class='alert alert-danger alert-dismissible fade in' id='successbox2' \
												style='display: none; margin-bottom: 30px; margin-left: 30px; margin-right: 30px'>\
												<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
												<span>Solo se permiten imagenes con extensiones: png, gif, jpg, y jpeg.</span>\
										  </div>\
										  ");
									$('#successbox2').slideDown();
								}else if(response=="badsize"){
									$('#msg-upload-img').html("\
										  <div class='alert alert-danger alert-dismissible fade in' id='successbox2' \
												style='display: none; margin-bottom: 30px; margin-left: 30px; margin-right: 30px'>\
												<a class='close' id='closediv' data-dismiss='alert' aria-label='close'>&times;</a>\
												<span>El tamaño de la imagen no debe ser mayor a 500Kb.</span>\
										  </div>\
										  ");
									$('#successbox2').slideDown();
								}else{
                          $('#imagen').html("<img src='uploads/" + response + "' style='width: 250px; height: 250px;' \>"); 
                          $('#imagen').show();
                          $('#selectfile').val('');
								  $("#enviando").hide();
								}
                      }
                  });
              }
          }
      </script>

		<!-- Cambia el cursor cuando pasa por div fotos -->
		<script type="text/javascript">
			$(".fotos").hover(function() {
				$(this).css('cursor','pointer');
			}, function() {
				$(this).css('cursor','auto');
			});
		</script>

      <!-- Modal Zone -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">

               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="myModalLabel">Mensaje:</h4>
               </div>

               <div class="modal-body">
               </div>

               <div class="modal-footer">
                  <button type="button" class="btn btn-warning" data-dismiss="modal">Aceptar</button>
               </div>
            </div>
         </div>
      </div>


      <!-- Launch Modal -->
      <script>
			$(document).ready(function(){
				$("#misarticulos").click(function(){
					var plan = "<?php echo $_SESSION['plan']; ?>";
					if(plan=="1"){
						$(".modal-body").empty();
						$(".modal-body").append("<p>Sólo disponible para los planes Silver y Gold. \
														 Puedes adquirir estos planes haciendo click <a href='planes.php'>Aquí.</a></p> ");
						$("#myModal").modal('show');
					}else{
						 window.location = "misarticulos.php";
					}
           });
			});
      </script>
      <script>
			$(document).ready(function(){
				$("#makearticulo").click(function(){
					var plan = "<?php echo $_SESSION['plan']; ?>";
					if(plan=="1"){
						$(".modal-body").empty();
						$(".modal-body").append("<p>Sólo disponible para los planes Silver y Gold. \
														 Puedes adquirir estos planes haciendo click <a href='planes.php'>Aquí.</a></p> ");
						$("#myModal").modal('show');
					}else{
						 window.location = "makearticulo.php";
					}
           });
			});
      </script>
      <script>
			$(document).ready(function(){
				$("#miscupones").click(function(){
					var plan = "<?php echo $_SESSION['plan']; ?>";
					if(plan=="1"){
						$(".modal-body").empty();
						$(".modal-body").append("<p>Sólo disponible para los planes Silver y Gold. \
														 Puedes adquirir estos planes haciendo click <a href='planes.php'>Aquí.</a></p> ");
						$("#myModal").modal('show');
					}else{
						 window.location = "miscupones.php";
					}
           });
			});
      </script>
      <script>
			$(document).ready(function(){
				$("#misrecomendaciones").click(function(){
					var plan = "<?php echo $_SESSION['plan']; ?>";
					if(plan=="1" || plan=="2"){
						$(".modal-body").empty();
						$(".modal-body").append("<p>Sólo disponible para el plan Gold. \
														 Puedes adquirir este plan haciendo click <a href='planes.php'>Aquí.</a></p> ");
						$("#myModal").modal('show');
					}else{
						 window.location = "misrecomendaciones.php";
					}
           });
			});
      </script>
      <script>
			$(document).ready(function(){
				$("#miscotizaciones").click(function(){
					var plan = "<?php echo $_SESSION['plan']; ?>";
					if(plan < 3 ){
						$(".modal-body").empty();
						$(".modal-body").append("<p>Sólo disponible para el plan Gold. \
														 Puedes adquirir este plan haciendo click <a href='planes.php'>Aquí.</a></p> ");
						$("#myModal").modal('show');
					}else{
						 window.location = "miscotizaciones.php";
					}
           });
			});
      </script>
      <script>
			$(document).ready(function(){
				$("#makecupon").click(function(){
					var plan = "<?php echo $_SESSION['plan']; ?>";
					if(plan=="1"){
						$(".modal-body").empty();
						$(".modal-body").append("<p>Sólo disponible para los planes Silver y Gold. \
														 Puedes adquirir estos planes haciendo click <a href='planes.php'>Aquí.</a></p> ");
						$("#myModal").modal('show');
					}else{
						 window.location = "makecupon.php";
					}
           });
			});
      </script>
      <script>
			$(document).ready(function(){
				$("#portfolio").click(function(){
					var plan = "<?php echo $_SESSION['plan']; ?>";
					if(plan=="1"){
						$(".modal-body").empty();
						$(".modal-body").append("<p>Sólo disponible para los planes Silver y Gold. \
														 Puedes adquirir estos planes haciendo click <a href='planes.php'>Aquí.</a></p> ");
						$("#myModal").modal('show');
					}else{
						 window.location = "portfolio.php";
					}
           });
			});
      </script>
      <script>
			$(document).ready(function(){
				$("#makebanner").click(function(){
					var plan = "<?php echo $_SESSION['plan']; ?>";
					if(plan < 3){
						$(".modal-body").empty();
						$(".modal-body").append("<p>Sólo disponible en el plan Gold. \
														 Puedes adquirir este plan haciendo click <a href='planes.php'>Aquí.</a></p> ");
						$("#myModal").modal('show')
					}else{
						 window.location = "makebanner.php";
					}
           });
			});
      </script>
      <script>
			$(document).ready(function(){
				$("#destacarplan").click(function(){
					var plandestacado = "<?php echo $_SESSION['plandestacado']; ?>";
					if(plandestacado=="1"){
						$(".modal-body").empty();
						$(".modal-body").append("<p>Su plan ya se encuentra en destacados.</p> ");
						$("#myModal").modal('show');
					}else{
						 window.location = "datafact.php?plan=4";
					}
           });
			});
      </script>
<?php
	if(isset($_GET['action']) && $_GET['action'] == "changeuser") {
?>
		<script>
			$(window).on('load',function(){
				$(".modal-body").empty();
				$(".modal-body").append("<p>Su usario ha sido cambiado con exito.</p> ");
				$("#myModal").modal('show');
			});
		</script>
<?php } ?>
   </body>
</html>



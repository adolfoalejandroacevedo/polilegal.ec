<?php

	header('Content-Type: text/html; charset=utf-8');
	session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");

   //Conexion a la BD
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
	mysqli_set_charset($conn, "utf8");

	//Si la ciudad esta en blanco regresamos al listado
	if(trim($_POST['ciudad']) == "") header("location: adm_ciudades.php");

	//Insertamos la ciudad en la base de datos
	$ciudad = strtr(strtoupper($_POST['ciudad']),"àèìòùáéíóúçñäëïöü","ÀÈÌÒÙÁÉÍÓÚÇÑÄËÏÖÜ");
	$ciudad = mysqli_real_escape_string($conn,$ciudad);
	if($_POST['idciudad'] == '0'){
		$sql = "INSERT INTO ciudades (
					ciudad,
					importancia)
					VALUES (
						'$ciudad',
						'$_POST[importancia]')";
	}else{
		$sql = "UPDATE ciudades
					SET 	ciudad='$ciudad',
							importancia='$_POST[importancia]'
					WHERE id='$_POST[idciudad]'";
	}
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

	//Regresamos al listado
	header("location: adm_ciudades.php");

?>


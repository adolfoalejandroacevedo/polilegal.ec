<?php

	session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");

	//Conexion a BD
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );

	//Se verifica el tipo de archivo
	$arr_file_types = ['image/png', 'image/gif', 'image/jpg', 'image/jpeg'];

	if (!(in_array($_FILES['file']['type'], $arr_file_types))) {
		 echo "badextension";
		 return;
	}

	//Se valida el tamano del archivo
	if ($_FILES["file"]["size"] > 500000) {
		echo "badsize";
		return;
	}

	if (!file_exists('uploads')) {
		 mkdir('uploads', 0777);
	}

	$file = time() . $_FILES['file']['name'];
	move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/' . $file);

	//Actualizamos la foto del abogado
	$file = htmlentities($file);
	$sql = "UPDATE users SET foto='$file' WHERE id='$_SESSION[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

	echo "$file";
?>


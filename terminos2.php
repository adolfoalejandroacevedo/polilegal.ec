<?php 
   session_start();
   session_unset();
?>
<!DOCTYPE HTML>
<html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
    <meta http-equiv="content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
    <title>Polilegal | terminos y condiciones</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
    <link id="default-css" href="style.css" rel="stylesheet" type="text/css">
    <link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" /> 
<!--[if IE 7]>
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lt IE 9]>
<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--Fonts-->
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--jquery-->
<script src="js/modernizr-2.6.2.min.js"></script>
</head>
<body>
	<!--wrapper starts-->
    <div class="wrapper">
        <!--inner-wrapper starts-->
        <div class="inner-wrapper">
            <!--header starts-->
				<?php include 'header.php'; ?>
            <!--header ends-->
                    <!--main starts-->
                    <div id="main">

              <div class="breadcrumb-section">
                <div class="container">
                  <h1> TÉRMINOS Y CONDICIONES </h1>
                  <div class="breadcrumb">
                    <a href="index.php"> Inicio </a>
                    <a href="terminos.php">Terminos y Condiciones </a>
                  </div>
                </div>
              </div>

              <!--container starts-->
              <div class="container"> 

                <!--primary starts-->
                <section id="primary" class="content-full-width">
                  <p>Estos términos y condiciones crean un contrato entre usted y la compañía Asesoramiento y Asistencias POLILEGAL ABOGADOS S.A. (el "Acuerdo"). Lea el Acuerdo detenidamente. Para confirmar que entiende y acepta este Acuerdo haga click en "ENVIAR".</p><br>    
                  <p><span class="dt-sc-highlightt skin-color"> <strong>PRIMERA: ANTECEDENTES.-</strong></span> POLILEGAL es una compañía legalmente constituida en el Ecuador, que ofrece servicios legales especializados y de calidad para el personal policial; así como también a personas naturales y jurídicas, y se encuentra legalmente autorizada por la Cooperativa Policía Nacional Ltda. para ofrecer sus servicios a los socios de la mencionada institución a través del Plan de Asistencia Jurídica. El Plan de Asistencia Jurídica de POLILEGAL tiene la finalidad de salvaguardar los intereses de los BENEFICIARIOS, brindando asesoría jurídica y patrocinio en defensa de sus intereses en el ámbito policial y en otras ramas del derecho, evitando que tengan que hacer frente a los altos costes por respaldo legal en otros sitios. Nuestros servicios tienen una cobertura nacional al contar con una extensa red de abogados colaboradores en todas las provincias del país.</p><br>     
                  <p><span class="dt-sc-highlightt skin-color"> <strong>SEGUNDA: OBJETO.-</strong></span> El objeto de este Contrato es la prestación por parte de POLILEGAL, de los siguientes servicios legales, los mismos que están clasificados para socios policías y socios civiles. De conformidad a este Contrato, la cobertura del Plan de Asistencia Jurídica para socios policías será exclusiva para policías, y la cobertura del Plan de Asistencia Jurídica para socios civiles será exclusiva para civiles. En caso de tratarse de socios policías en servicio pasivo, éstos tendrán los beneficios incluidos en el Plan de Asistencia Jurídica para socios civiles. En el siguiente cuadro se encuentra detallado cada servicio.</p><br>

                  <p>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>SERVICIOS INCLUIDOS EN EL PLAN DE ASISTENCIA JURÍDICA</th>
                          <th>PARA SOCIOS POLICÍAS</th>
                          <th>PARA SOCIOS CIVILES</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Defensa en Sumarios administrativos</td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Defensa en Cuotas/Cupos de Eliminación Anual</td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Defensa en faltas y sanciones disciplinarias</td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Reconsideraciones: Ascensos y Condecoraciones</td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Reincorporaciones</td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Recursos y reclamos administrativos internos</td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Asistencias de Abogado en versiones</td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Asesoría integral en Recursos Extraordinarios de Revisión</td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Asesoría integral en procedimientos contencioso-administrativos</td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                        </tr>
                        <tr>
                          <td>Asesoría integral en procedimientos en vía constitucional</td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                        </tr>
                        <tr>
                          <td>Asesoría integral sobre asuntos civiles y de familia</td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                        </tr>
                        <tr>
                          <td>Asesoría en cualquier tema legal</td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                        </tr>
                        <tr>
                          <td>Elaboración de peticiones ante los organismos policiales</td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Elaboración de procuraciones judiciales</td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                        </tr>
                        <tr>
                          <td>Elaboración de permisos de salida del país de menores</td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                        </tr>
                        <tr>
                          <td>Elaboración de poderes generales y especiales</td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                        </tr>
                        <tr>
                          <td>Asesoría en constitución de compañías y apertura de negocios</td>
                          <td></td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                        </tr>
                        <tr>
                          <td>Cancelaciones de hipoteca</td>
                          <td></td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                        </tr>
                        <tr>
                          <td>Declaraciones juramentadas</td>
                          <td></td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                        </tr>
                        <tr>
                          <td>Elaboración de todo tipo de contratos</td>
                          <td></td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                        </tr>
                        <tr>
                          <td>Elaboración de minutas de compraventa</td>
                          <td></td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                        </tr>
                        <tr>
                          <td>Peticiones ante entidades públicas</td>
                          <td></td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                        </tr>
                        <tr>
                          <td>Consultas físicas personales</td>
                          <td><i class="fa fa-check" aria-hidden="true"></td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                        </tr>
                        <tr>
                          <td>Consultas a través de la página web, correo electrónico y redes sociales</td>
                          <td><i class="fa fa-check" aria-hidden="true"></td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                        </tr>
                        <tr>
                          <td>Consultas a través de nuestro Contact Center</td>
                          <td><i class="fa fa-check" aria-hidden="true"></td>
                          <td><i class="fa fa-check" aria-hidden="true"></i>
                          </td>
                        </tr>
                      </tbody>
                    </table>

                  </p><br>
                  <p><span class="dt-sc-highlightt skin-color"> <strong>TERCERA: PRECIO, FORMA DE PAGO Y FACTURACIÓN.-</strong></span> El precio correspondiente al Plan de Asistencia Jurídica es de USD 6,99 (Seis dólares con noventa y nueve centavos americanos), precio en el cual está incluido el IVA. El BENEFICIARIO podrá acceder inmediatamente a partir del débito de la primera cuota por afiliación. POLILEGAL entregará al BENEFICIARIO la correspondiente factura mensual del Plan de Asistencia Jurídica a través de correo electrónico. El pago de los servicios se efectuará por parte del BENEFICIARIO a través de débito automático conforme los datos proporcionados por el BENEFICIARIO en este proceso de afiliación. </p><br>  
                  <p><span class="dt-sc-highlightt skin-color"> <strong>CUARTA. UTILIZACIÓN DEL SERVICIO:</strong></span> Para hacer uso del servicio del Plan de Asistencia Jurídica descrito en el presente Contrato, el BENEFICIARIO deberá comunicarse previamente a nuestras líneas de CONTACT CENTER (02)3 825530 y (02)3 333533, correo electrónico asistencia@polilegal.ec, página web <a href="www.polilegal.ec">www.polilegal.ec</a>, o redes sociales, con el fin de gestionar sus solicitudes y asignarle a un Abogado.  El CONTACT CENTER y la línea directa atenderán en el horario de 09H00 a 18h00 de lunes a viernes, mientras que el correo electrónico, página web y redes sociales funcionarán los 365 días del año. El BENEFICIARIO deberá encontrarse al día en el pago de sus cuotas para que tenga derecho a los servicios que se detallan en este contrato.<br><br>En caso de no existir profesionales Abogados adscritos a POLILEGAL en el lugar de residencia del BENEFICIARIO, éste podrá acudir al lugar más cercano donde consten profesionales Abogados adscritos a POLILEGAL para recibir la atención.<br><br>POLILEGAL no se responsabilizará por el pago de atenciones legales efectuadas por profesionales Abogados o Estudios Jurídicos no autorizados POLILEGAL.<br><br>La cobertura de los Planes de Asistencias Jurídica excluye el pago de tasas administrativas, así como cualquier valor que no corresponda a honorarios profesionales, como gastos por concepto de movilización o desplazamiento de algún profesional u otros rubros.<br><br>En caso de cualquier reclamo, éste deberá presentarse por escrito en la oficina matriz de POLILEGAL en la ciudad de Quito, adjuntando los respectivos documentos de respaldo. Se establece un plazo máximo de quince días calendario, contados desde ocurrido el hecho, luego del cual POLILEGAL no se responsabilizará por dicha reclamación.<br><br>Queda expresamente convenido que POLILEGAL se responsabilizará exclusivamente por el pago de honorarios a los Abogados autorizados de acuerdo a las condiciones establecidas en el presente contrato, sin que tenga responsabilidad alguna por hechos que queden fuera de esta limitación.</p><br> 
                  <p><span class="dt-sc-highlightt skin-color"> <strong>QUINTA. SERVICIOS NO CUBIERTOS POR EL PLAN Y PREEXISTENCIAS.-</strong></span> En caso de tratarse de servicios no cubiertos por el Plan o preexistencias, el BENEFICIARIO, por estar afiliado al Plan de Asistencia Jurídica de POLILEGAL, tendrá un descuento del 50% de acuerdo al servicio que requiera, los mismos que constan en la página web del Estudio Jurídico.</p><br>
                  <p><span class="dt-sc-highlightt skin-color"> <strong>SEXTA. MORA.-</strong></span> En caso de mora, el BENEFICIARIO acepta que se le realice el débito correspondiente por el total de las cuotas adeudadas. Mientras el BENEFICIARIO no cancele los valores totales por su afiliación, POLILEGAL no podrá prestar el servicio, hasta que el BENEFICIARIO cancele las cuotas pendientes. En caso de que la mora se extienda por más de 12 meses, POLILEGAL se reserva el derecho a dar por terminados los servicios y dejar de cobrar al BENEFICIARIO, sin perjuicio del derecho a cobrar el monto adeudado más los intereses legales que correspondan, de ser el caso.</p><br>  
                  <p><span class="dt-sc-highlightt skin-color"> <strong>SÉPTIMA. PLAZO.-</strong></span> El presente Contrato tendrá un plazo de un año a partir de la firma del mismo. El plazo se renovará automáticamente por el mismo período, salvo que una de las partes comunique por escrito su voluntad de darlo por terminado con por lo menos QUINCE (15) días de anticipación.</p><br> 
                  <p><span class="dt-sc-highlightt skin-color"> <strong>OCTAVA. RESPONSABILIDAD.-</strong></span> POLILEGAL prestará los servicios del Plan de Asistencia Jurídica constantes en este instrumento, con altos estándares de calidad, durante el plazo de duración y términos acordados en el mismo, salvo las situaciones de fuerza mayor o caso fortuito, según las disposiciones del Art. 30 del Código
                    Civil. </p><br> 
                    <p><span class="dt-sc-highlightt skin-color"> <strong>NOVENA: TERMINACIÓN DEL CONTRATO.-</strong></span> Las partes intervinientes acuerdan en forma recíproca que el Contrato que hoy suscriben y su Anexo, podrá darse por terminado en los siguientes casos: </p>
                    <ul>
                      <li>a) En caso de que el BENEFICIARIO haya decidido no renovar el Contrato. </li>
                      <li>b) Por insolvencia, concurso de acreedores, quiebra, disolución o liquidación, de POLILEGAL, de la COOPERATIVA POLICÍA NACIONAL, o de insolvencia del BENEFICIARIO.</li>
                      <li>c) Por decisión unilateral del BENEFICIARIO a partir del segundo año del Contrato, previa notificación formal por escrito, y siempre y cuando cancele los valores adeudados pendientes, de ser el caso.</li>
                      <li>d) Por mutuo acuerdo de las partes en casos excepcionales, siempre y cuando el BENEFICIARIO cancele todos los valores adeudados pendientes, de ser el caso. Para ello, el BENEFICIARIO deberá presentar una notificación formal por escrito, con los respectivos respaldos si fuera el caso y deberá existir un documento firmado por ambas partes.</li>
                      <li>e) Por el uso indebido del contrato y suplantación en la utilización de los servicios legales.</li>
                      <li>f) Por finalización del Convenio suscrito entre POLILEGAL y la Cooperativa Policía Nacional.</li>
                    </ul><br>
                    <p><span class="dt-sc-highlightt skin-color"> <strong>DÉCIMA. PROHIBICIÓN DE CESIÓN.-</strong></span> El Plan de Asistencia Jurídica de POLILEGAL es de uso exclusivo del BENEFICIARIO. El BENEFICIARIO no podrá ceder total ni parcialmente los derechos y obligaciones adquiridos mediante este Contrato, a terceras personas.</p><br>  
                    <p><span class="dt-sc-highlightt skin-color"> <strong>DÉCIMO PRIMERA. JURISDICCIÓN Y COMPETENCIA.-</strong></span> En caso de controversias, las partes acuerdan someterse a los jueces de lo civil del cantón Quito, provincia de Pichincha, y al procedimiento sumario. </p><br> 
                    <p><span class="dt-sc-highlightt skin-color"> <strong>DÉCIMO SEGUNDA. CLÁUSULA DE AUTORIZACIÓN DE DÉBITO:</strong></span> El BENEFICIARIO de este Contrato, socio de la Cooperativa Policía Nacional Ltda., autoriza de manera expresa, libre y voluntariamente para que se debite en forma mensual de su cuenta de ahorros que mantiene en la Cooperativa Policía Nacional, la cantidad de $ 6,99 (SEIS DÓLARES CON NOVENTA Y NUEVE CENTAVOS), por concepto del Plan de Asistencia Jurídica, de su cuota mensual por afiliación a la empresa POLILEGAL. Asimismo, conoce y solicita que dicho valor será depositado en la cuenta corriente perteneciente a la empresa POLILEGAL. Adicionalmente, conoce y acepta que la Cooperativa Policía Nacional no mantiene ningún vínculo legal ni de hecho con la empresa POLILEGAL, a más del Convenio de débitos, para lo cual expresamente renuncia a cualquier tipo de reclamo a la Cooperativa Policía Nacional, por los servicios o derechos que preste POLILEGAL que se generen por el descuento de su cuenta de ahorros.  </p><br>
                    <p><span class="dt-sc-highlightt skin-color"> <strong>DÉCIMO TERCERA. ACEPTACIÓN.-</strong></span> El BENEFICIARIO acepta conocer las condiciones establecidas en el presente Contrato, y se compromete a cumplir lo estipulado en el mismo.  </p>
                    <p>
                    <p><span class="dt-sc-highlightt skin-color"> <strong>DÉCIMO CUARTA.-</strong></span> El presente contrato podrá ser modificado sin previo aviso de acuerdo a la evaluación, de requerimientos que se presenten, expedición de nuevas normas y siniestralidad del contrato. Cualquier modificación estará siempre disponible para el usuario en este sitio web. </p>
                    <p>    
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>PLAN DE ASISTENCIA JURÍDICA PARA SOCIOS POLICÍAS</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th>DEFENSA EN:</th>
                          </tr>
                          <tr>
                            <td>Sumarios Administrativos: Desde el momento en el que se inician las investigaciones preliminares o desde cualquier etapa. Incluye todas las actuaciones y escritos legales necesarios en defensa de los intereses de los BENEFICIARIOS durante toda la etapa en la instancia administrativa policial.</td>
                          </tr>
                          <tr>
                            <td>Cupos de Eliminación Anual: En caso de integrar la lista de eliminación anual de la Policía Nacional, PoliLegal se encarga de la respectiva Reconsideración, Apelación y tramitación ante los órganos competentes.</td>
                          </tr>
                          <tr>
                            <td>Faltas y sanciones disciplinarias: Incluye reclamos en casos de sanciones disciplinarias de acuerdo a la normativa vigente.</td>
                          </tr>
                          <tr>
                            <td>Reincorporaciones: PoliLegal se encarga de la defensa completa en caso de reincorporaciones de acuerdo a la normativa vigente.</td>
                          </tr>
                          <tr>
                            <td>Reconsideraciones: Ascensos y Condecoraciones: El servidor policial que se considere afectado por la negativa de ascenso o condecoración, tiene derecho a impugnar la resolución. PoliLegal se encarga de los escritos legales de impugnación ante la instancia correspondiente.</td>
                          </tr>
                          <tr>
                            <td>Recursos y Reclamos administrativos: Elaboración de oficios de reclamos varios o comunicaciones ante las autoridades competentes de acuerdo al caso.</td>
                          </tr>
                          <tr>
                            <td>Asistencias en versiones: Los BENEFICIARIOS tendrán a su disposición Abogados para asesoría, acompañamiento y representación en versiones y declaraciones.</td>
                          </tr>
                          <tr>
                            <th>ASESORÍA EN:</th>
                          </tr>
                          <tr>
                            <td>Asesoría integral en procedimientos contencioso-administrativos: El servidor policial puede recurrir ante la jurisdicción contencioso-administrativa interponiendo un recurso contra los reglamentos, resoluciones o actos de la Administración Pública.  El Plan de Asistencia Jurídica de PoliLegal incluye asesoría jurídica en cualquier etapa que comprende esta jurisdicción.</td>
                          </tr>
                          <tr>
                            <td>Asesoría integral en procedimientos en vía constitucional: Brindamos una asesoría completa para recurrir a la vía constitucional en defensa de la protección de derechos reconocidos en la Constitución, instrumentos internacionales y derechos humanos.</td>
                          </tr>
                          <tr>
                            <td>Asesoría integral en asuntos civiles y de familia: Asesoría en cualquier asunto civil y de familia, como divorcios, alimentos, régimen económico, capitulaciones matrimoniales, disolución de la sociedad conyugal, testamentos, daños y perjuicios, proporcionando siempre la mejor alternativa.</td>
                          </tr>
                          <tr>
                            <td>Asesoría integral en general, en cualquier asunto legal.</td>
                          </tr>
                          <tr>
                            <th>ELABORACIÓN DE:</th>
                          </tr>
                          <tr>
                            <td>Peticiones ante todos los organismos policiales como ISSPOL, Servicio de Cesantía, entre otros.</td>
                          </tr>
                          <tr>
                            <td>Elaboración de Procuraciones judiciales: El Plan cubre las procuraciones judiciales necesarias para otorgar al Abogado la representación y comparecencia en todas las actuaciones.</td>
                          </tr>
                          <tr>
                            <td>Elaboración de permisos de salida del país de menores: La autorización de salida del país aplica para los hijos menores de edad del servidor policial BENEFICIARIO que quieren realizar un viaje en compañía de familiares u otras personas sin la compañía de sus padres.  PoliLegal se encarga de la elaboración de la minuta, así como la gestión ante las Notarías Públicas.</td>
                          </tr>
                          <tr>
                            <td>Elaboración de Poderes generales y especiales: Cuando nuestros BENEFICIARIOS deseen otorgar la administración general de bienes u otros negocios jurídicos a la persona de su confianza es necesario el otorgamiento de un poder general; y, cuando el acto o contrato es específico, como compraventa de determinados bienes y otros actos, será necesario un poder especial.  Desde PoliLegal asesoramos y elaboramos todo tipo de poder general o especial y gestionamos ante el Notario Público.</td>
                          </tr>
                          <tr>
                            <th>COBERTURA DE CONSULTAS</th>
                          </tr>
                          <tr>
                            <td>Consultas físicas personales: El Plan de Asistencia Jurídica de PoliLegal cubre consultas en sus oficinas en Quito, y en las oficinas de nuestra red de Abogados a nivel nacional, los 365 días del año.</td>
                          </tr>
                          <tr>
                            <td>Consultas a través de la página web, correo electrónico y redes sociales:  PoliLegal dispone de su propio sitio web: <a href="www.polilegal.ec">www.polilegal.ec</a>, donde podrá realizar cualquier consulta y obtener asesoría desde la comodidad de su hogar o trabajo, o al correo electrónico asistencia@polilegal.ec o redes sociales, con respuesta inmediata o con un tiempo máximo de 48 horas de acuerdo a la complejidad del caso, los 365 días del año. </td>
                          </tr>
                          <tr>
                            <td>Consultas a través de nuestro Contact Center:  Los BENEFICIARIOS podrán llamar a nuestra líneas de Contact Center (02)3 825530 o (02)3 333533 en el horario de 09h00 a 18h00 de lunes a viernes, a fin de obtener la asesoría en cualquier momento y de cualquier tema cubierto en el Plan de Asistencia Jurídica. </td>
                          </tr>
                        </tbody>
                      </table>
                    </p><br>
                    <p>
                        <table class="table table-striped">
                        <thead>
                          <tr>
                            <th colspan="2">PLAN DE ASISTENCIA JURÍDICA PARA SOCIOS CIVILES</th>
                          </tr>
                          <tr>
                            <th colspan="2">COBERTURA DE CONSULTAS:</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td colspan="2">Asuntos civiles y de familia: Asesoría en cualquier asunto civil y de familia, como divorcios, alimentos, régimen económico, capitulaciones matrimoniales, disolución de la sociedad conyugal, testamentos, daños y perjuicios, etc.</td>
                          </tr>
                          <tr>
                            <td colspan="2">Procedimientos constitucionales: Asesoría completa para recurrir a la vía constitucional en defensa de la protección de derechos reconocidos en la Constitución, instrumentos internacionales y derechos humanos.</td>
                          </tr>
                          <tr>
                            <td colspan="2">Procedimientos contencioso-administrativos: El Plan de Asistencia Jurídica de PoliLegal incluye asesoría jurídica en cualquier etapa que comprende esta jurisdicción.</td>
                          </tr>
                          <tr>
                            <td colspan="2">Apertura de negocios y constitución de compañías: PoliLegal le asesorará respecto de las mejores vías para aperturar su propio negocio, invertir o constituir una compañía, y todas las actividades relacionadas al giro del negocio o empresa.</td>
                          </tr>
                          <tr>
                            <th colspan="2">ELABORACIÓN DE:</th>
                          </tr>
                          <tr>
                            <td>Minutas de compraventa</td>
                            <td>Permisos de salida del país de menores</td>
                          </tr>
                          <tr>
                            <td>Declaraciones juramentadas</td>
                            <td>Poderes generales y especiales</td>
                          </tr>
                          <tr>
                            <td>Cancelación de hipotecas</td>
                            <td>Todo tipo de contratos</td>
                          </tr>
                          <tr>
                            <td>Procuraciones judiciales</td>
                            <td>Peticiones ante entidades públicas</td>
                          </tr>
                          <tr>
                            <th colspan="2">COBERTURA DE CONSULTAS:</th>
                          </tr>
                          <tr>
                            <td colspan="2">Consultas físicas personales: El Plan de Asistencia Jurídica de PoliLegal cubre consultas en sus oficinas en Quito, y en las oficinas de nuestra red de Abogados a nivel nacional, los 365 días del año.</td>
                          </tr>
                          <tr>
                            <td colspan="2">Consultas a través de la página web, correo electrónico y redes sociales:  PoliLegal dispone de su propio sitio web: <a href="www.polilegal.ec">www.polilegal.ec</a>, donde podrá realizar cualquier consulta y obtener asesoría desde la comodidad de su hogar o trabajo, o al correo electrónico asistencia@polilegal.ec o redes sociales, con respuesta inmediata o con un tiempo máximo de 48 horas de acuerdo a la complejidad del caso, los 365 días del año.<br><br>Consultas a través de nuestro Contact Center: Los BENEFICIARIOS podrán llamar a nuestra líneas de Contact Center (02)3 825530 o (02)3 333533 en el horario de 09h00 a 18h00 de lunes a viernes, a fin de obtener la asesoría en cualquier momento y de cualquier tema cubierto en el Plan de Asistencia Jurídica.</td>
                          </tr>
                        </tbody>
                      </table>
                    </p><br>
                  </section>
                </div>
              </div>
            </div>

            <!--dt-sc-toggle-frame-set ends-->
          </div>     

          <!--primary ends-->

        </div>
        <!--container ends-->

        <div class="dt-sc-hr-invisible-large"></div>     

      </div>
    <!--main ends-->
    
    <!--footer starts-->
		<?php include 'footer.php'; ?>
	 <!--footer ends-->
</div>
<!--inner-wrapper ends-->    
</div>
<!--wrapper ends-->
<!--wrapper ends-->
<?php include "asistencias.php"; ?>
<a href="" title="Go to Top" class="back-to-top"> <span class="fa fa-angle-up"></span> </a> 
<!--Java Scripts--> 
<!--Java Scripts-->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-migrate.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-easing-1.3.js"></script>
<script type="text/javascript" src="js/jquery.sticky.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/jquery.smartresize.js"></script> 
<script type="text/javascript" src="js/shortcodes.js"></script>   

<script type="text/javascript" src="js/custom.js"></script>

<!-- Layer Slider --> 
<script type="text/javascript" src="js/jquery-transit-modified.js"></script> 
<script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script> 
<script type='text/javascript' src="js/greensock.js"></script> 
<script type='text/javascript' src="js/layerslider.transitions.js"></script> 

<script type="text/javascript">var lsjQuery = jQuery;</script><script type="text/javascript"> lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice('layerslider_2','jquery'); } else { lsjQuery("#layerslider_2").layerSlider({responsiveUnder: 1240, layersContainer: 1170, skinsPath: 'js/layerslider/skins/'}) } }); </script>

                     <!-- Cerrar Session -->
                     <script>
                        jQuery(document).ready(function(){
                           jQuery("#close_session").click(function(){
                              alert('Su sesión ha sido cerrada');
                              window.location.href='closesession.php';
                           });
                        });
                     </script>

</body>
</html>

<?php 
   session_start();
   session_unset();
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
   mysqli_set_charset($conn, "utf8");
?>
<!DOCTYPE HTML>
<html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
  <meta http-equiv="content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
  <title>Polilegal | terminos y condiciones</title>
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
  <link id="default-css" href="style.css" rel="stylesheet" type="text/css">
  <link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" /> 
  <link rel="stylesheet" href="css/style.css">
  <style>
	   .tdwhite {
      background: white;
   }
  </style>
<!--[if IE 7]>
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lt IE 9]>
<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--Fonts-->
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--jquery-->
<script src="js/modernizr-2.6.2.min.js"></script>
</head>
<body>
  <!--wrapper starts-->
  <div class="wrapper">
    <!--inner-wrapper starts-->
    <div class="inner-wrapper">
      <!--header starts-->
		<?php include 'header.php'; ?>	
      <!--header ends-->
            <!--main starts-->
            <div id="main">

              <div class="breadcrumb-section">
                <div class="container">
                  <h1> TÉRMINOS Y CONDICIONES </h1>
                  <div class="breadcrumb">
                    <a href="index.php"> Inicio </a>
                    <a href="terminos.php">Terminos y Condiciones </a>
                  </div>
                </div>
              </div>

              <!--container starts-->
              <div class="container"> 

                <!--primary starts-->
                <section id="primary" class="content-full-width">
                  <p>Estos términos y condiciones crean un contrato entre usted y la compañía POLILEGAL S.A. (el "Acuerdo"). Lea el Acuerdo detenidamente. Para confirmar que entiende y acepta este Acuerdo haga click en "Enviar solicitud".</p><br>    
                  <p><span class="dt-sc-highlightt skin-color"> <strong>PRIMERA: ANTECEDENTES.-</strong></span> Polilegal S.A. es una compañía legalmente constituida en el Ecuador, que ofrece servicios legales especializados y de calidad a personas naturales y jurídicas.  Polilegal S.A. posee varios productos y servicios, entre uno de ellos, el Plan de Asistencia Jurídica para miembros de la institución policial y sus familiares, con el cual los afiliados acceden a varios servicios dentro de la cobertura que será detallada posteriormente.</p><br>     
                  <p><span class="dt-sc-highlightt skin-color"> <strong>SEGUNDA: OBJETO.-</strong></span>El objeto de este Contrato es la prestación por parte de Polilegal S.A., de los siguientes servicios legales que forman parte del Plan de Asistencia Jurídica para miembros de la institución policial y sus familiares, los mismos que están clasificados de acuerdo al tipo de Plan: 1. Para socios policías en servicio activo, 2. Para socios policías en servicio pasivo y familiares. De conformidad a este Contrato, la cobertura del Plan de Asistencia Jurídica para socios policías será exclusiva para socios en servicio activo, y la cobertura del Plan de Asistencia Jurídica para socios en servicio pasivo y familiares será exclusiva para ellos. En el siguiente cuadro se encuentra detallado cada servicio.</p><br> 

            <div class="comparison" style="padding-top: 0px;">
              <table>
                <thead>
                  <tr>
                    <th class="price-info">
                      <div class="price-try"><span class="hide-mobile">SERVICIOS INCLUIDOS</span></div>
                    </th>
                    <th class="price-info">
                      <div class="price-try"><span class="">SOCIOS POLICÍAS (SERVICIO ACTIVO)</span></div>
                    </th>
                    <th class="price-info">
                      <div class="price-try"><span class="">SOCIOS SP Y CIVILES</span></div>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                     $sql =   "SELECT *
                              FROM servicios
                              ORDER BY posicion ASC";
                     if ($result = mysqli_query($conn, $sql)){
                        while ($row = mysqli_fetch_assoc($result)) {
                           if ($row['posicion']%2==0){
                  ?>
                  <tr>
                    <td>&nbsp;</td>
                    <td colspan="2"><?php echo $row['servicio']; ?>.</td>
                  </tr>
                  <tr>
                    <td class="tdwhite"><?php echo $row['servicio']; ?>.</td>
                     <?php    if($row['plan'] == 21){ ?>
                    <td class="tdwhite"><span class="tickblue">✔</span></td>
                    <td class="tdwhite"><span class="tickgreen">✔</span></td>
                     <?php    }else if($row['plan'] == 2){ ?>
                    <td class="tdwhite"><span class="tickblue">✔</span></td>
                    <td class="tdwhite"></td>
                     <?php    }else{ ?>
                    <td class="tdwhite"></td>
                    <td class="tdwhite"><span class="tickgreen">✔</span></td>
                     <?php    } ?>
                  </tr>
                  <?php    }else{ ?>
                  <tr>
                    <td></td>
                    <td colspan="2"><?php echo $row['servicio']; ?>.</td>
                  </tr>
                  <tr class="compare-row">
                    <td><?php echo $row['servicio']; ?>.</td>
                     <?php    if($row['plan'] == 21){ ?>
                    <td><span class="tickblue">✔</span></td>
                    <td><span class="tickgreen">✔</span></td>
                     <?php    }else if($row['plan'] == 2){ ?>
                    <td><span class="tickblue">✔</span></td>
                    <td></td>
                     <?php    }else{ ?>
                    <td></td>
                    <td><span class="tickgreen">✔</span></td>
                     <?php    } ?>
                  </tr>
                  <?php    }
                        }
                     }else error_log("Error: " . $sql . "..." . mysqli_error($conn));
                  ?>
                </tbody>
              </table>
            </div>

                  <p><span class="dt-sc-highlightt skin-color"> <strong>TERCERA: UTILIZACIÓN DEL SERVICIO.-</strong></span> El Plan de Asistencia Jurídica para miembros de la institución policial y familiares es un servicio a nivel nacional. El afiliado podrá hacer uso de este Plan a partir del débito de la primera cuota por afiliación.  Todos los requerimientos serán receptados únicamente por medio de nuestro Contact Center, WhatsApp, correo electrónico o redes sociales, a través de dichos medios, se le asignará al abogado de acuerdo al caso. Todas las vías de comunicación estarán disponibles en un horario de 08h30 a 18h00. El afiliado deberá encontrarse al día en sus cuotas para que pueda acceder a los servicios, caso contrario, se suspenderá la prestación del servicio hasta que el afiliado se encuentre al día en sus cuotas.  Para ciertos requerimientos, Polilegal S.A. solicitará al afiliado la remisión de documentación para su análisis previo. En caso de no existir profesionales abogados adscritos a Polilegal S.A. en el lugar del requerimiento del afiliado, éste podrá acudir al lugar más cercano donde consten profesionales adscritos para recibir la atención. La cobertura del Plan de Asistencia Jurídica excluye el pago de tasas administrativas, así como cualquier valor que no corresponda a honorarios profesionales, como gastos por concepto de movilización o desplazamiento de algún profesional u otros rubros. En caso de cualquier reclamo, éste deberá presentarse por escrito en la oficina matriz de Polilegal S.A. en la ciudad de Quito, adjuntando los respectivos documentos de respaldo. Se establece un plazo máximo de quince días calendario, contados desde ocurrido el hecho, luego del cual Polilegal S.A. no se responsabilizará por dicha reclamación. Queda expresamente convenido que Polilegal S.A. se responsabilizará exclusivamente por el pago de honorarios a los abogados autorizados de acuerdo a las condiciones establecidas en el presente contrato, sin que tenga responsabilidad alguna por hechos que queden fuera de esta limitación, de tal manera que el afiliado no podrá acudir donde el profesional directamente, sin que antes se haya receptado el requerimiento por los medios de contacto establecidos. </p><br>  
                  <p><span class="dt-sc-highlightt skin-color"> <strong>CUARTA. PRECIO Y FORMA DE PAGO.-</strong></span> El precio correspondiente a este Plan de Asistencia Jurídica es de USD 6,99 (Seis dólares con noventa y nueve centavos americanos), precio en el cual está incluido el IVA. El afiliado podrá pagar sus cuotas correspondientes al Plan de Asistencia Jurídica a través de su cuenta de ahorros Cooperativa Policía Nacional, así como también de su cuenta en otra institución bancaria y tarjetas de crédito o débito, a través de una de las empresas más seguras y confiables en el Ecuador como es Paymentez, empresa que maneja sus propios términos y condiciones que siempre estarán visibles en su página web www.paymentez.com.ec. Polilegal S.A. cuenta con un sistema de facturación electrónica, de tal manera que, todas las facturas estarán siempre disponibles en esta página web. </p><br> 
                  <p><span class="dt-sc-highlightt skin-color"> <strong>QUINTA. SERVICIOS NO CUBIERTOS POR EL PLAN Y PREEXISTENCIAS.-</strong></span> En caso de tratarse de servicios no cubiertos por el Plan, el afiliado tendrá un 50%.  En caso de preexistencia, Polilegal S.A. podrá otorgar un descuento de acuerdo al servicio que se trate, previo análisis.</p><br>
                  <p><span class="dt-sc-highlightt skin-color"> <strong>SEXTA. MORA.-</strong></span> En caso de mora, el afiliado acepta que se le realice el débito correspondiente por el total de las cuotas adeudadas. Mientras el afiliado no cancele los valores totales por su afiliación, Polilegal S.A. no podrá prestar el servicio, hasta que el afiliado cancele las cuotas pendientes. En caso de que la mora se extienda por más de 12 meses, POLILEGAL se reserva el derecho a dar por terminados los servicios y dejar de cobrar al afiliado.</p><br>  
                  <p><span class="dt-sc-highlightt skin-color"> <strong>SÉPTIMA. PLAZO.-</strong></span> El presente Contrato tendrá un plazo de un año. El plazo se renovará automáticamente por el mismo período, salvo que una de las partes comuniquen por escrito su voluntad de darlo por terminado con por lo menos QUINCE (15) días de anticipación.</p><br> 
                  <p><span class="dt-sc-highlightt skin-color"> <strong>OCTAVA. RESPONSABILIDAD.-</strong></span> Polilegal S.A. prestará los servicios del Plan de Asistencia Jurídica constantes en este instrumento, con altos estándares de calidad y agilidad durante el plazo de duración y términos acordados en el mismo, salvo las situaciones de fuerza mayor o caso fortuito, según las disposiciones del Art. 30 del Código
                    Civil. </p><br> 
                    <p><span class="dt-sc-highlightt skin-color"> <strong>NOVENA: TERMINACIÓN DEL CONTRATO.-</strong></span> Las partes intervinientes acuerdan en forma recíproca que este contrato podrá darse por terminado en los siguientes casos: </p>
                    <ul>
                      <li>a) En caso de que el afiliado haya decidido no renovar el contrato. </li>
                      <li>b) Por insolvencia, concurso de acreedores, quiebra, disolución o liquidación, de POLILEGAL, de la COOPERATIVA POLICÍA NACIONAL, de las demás instituciones financieras o de insolvencia del afiliado.</li>
                      <li>c) Por decisión unilateral del afiliado a partir del segundo año del contrato, previa notificación formal por escrito, y siempre y cuando cancele los valores adeudados pendientes, de ser el caso.</li>
                      <li>d) Por mutuo acuerdo de las partes en casos excepcionales, siempre y cuando el afiliado cancele todos los valores adeudados pendientes, de ser el caso. Para ello, el afiliado deberá presentar una notificación formal por escrito, con los respectivos respaldos si fuera el caso y deberá existir un documento firmado por ambas partes.</li>
                      <li>e) Por el uso indebido del contrato y suplantación en la utilización de los servicios legales.</li>
                    </ul><br>
                    <p><span class="dt-sc-highlightt skin-color"> <strong>DÉCIMA. PROHIBICIÓN DE CESIÓN.-</strong></span> Este Plan de Asistencia Jurídica es de uso exclusivo del afiliado. El afiliado no podrá ceder total ni parcialmente los derechos y obligaciones adquiridos mediante este contrato, a terceras personas.</p><br>  
                    <p><span class="dt-sc-highlightt skin-color"> <strong>DÉCIMO PRIMERA. JURISDICCIÓN Y COMPETENCIA.-</strong></span> En caso de controversias, las partes acuerdan someterse a arreglos amigables, mediación, y de no existir acuerdo, a los jueces de lo civil del cantón Quito, provincia de Pichincha, y al procedimiento sumario. </p><br> 
                    <p><span class="dt-sc-highlightt skin-color"> <strong>DÉCIMO SEGUNDA. CLÁUSULA DE AUTORIZACIÓN DE DÉBITO COOPERATIVA POLICÍA NACIONAL:</strong></span> El afiliado, socio de la Cooperativa Policía Nacional Ltda., autoriza de manera expresa, libre y voluntariamente para que se debite en forma mensual de su cuenta de ahorros que mantiene en la Cooperativa Policía Nacional, la cantidad de $ 6,99 (SEIS DÓLARES CON NOVENTA Y NUEVE CENTAVOS), por concepto del Plan de Asistencia Jurídica, de su cuota mensual por afiliación a la empresa Polilegal S.A. Asimismo, conoce y solicita que dicho valor será depositado en la cuenta corriente perteneciente a la empresa Polilegal S.A. Adicionalmente, conoce y acepta que la Cooperativa Policía Nacional no mantiene ningún vínculo legal ni de hecho con la empresa Polilegal S.A., a más del convenio de débitos, para lo cual expresamente renuncia a cualquier tipo de reclamo a la Cooperativa Policía Nacional, por los servicios o derechos que preste Polilegal S.A. que se generen por el descuento de su cuenta de ahorros.  </p><br>
                    <p><span class="dt-sc-highlightt skin-color"> <strong>DÉCIMO TERCERA. ACEPTACIÓN.-</strong></span> El afiliado acepta conocer las condiciones establecidas en el presente Contrato, y se compromete a cumplir lo estipulado en el mismo.  </p>
                    <p>
                    <p><span class="dt-sc-highlightt skin-color"> <strong>DÉCIMO CUARTA.-</strong></span> El presente contrato podrá ser modificado sin previo aviso de acuerdo a la evaluación de requerimientos que se presenten, expedición de nuevas normas y siniestralidad del contrato. Cualquier modificación estará siempre disponible para el usuario en este sitio web. </p>
                  </section>
                </div>
              </div>
            </div>

            <!--dt-sc-toggle-frame-set ends-->
          </div>     

          <!--primary ends-->

        </div>
        <!--container ends-->

        <div class="dt-sc-hr-invisible-large"></div>     

      </div>
      <!--main ends-->
      
      <!--footer starts-->
		<?php include 'footer.php'; ?>
      <!--footer ends-->
      </div>
      <!--inner-wrapper ends-->    
    </div>
    <!--wrapper ends-->
    <!--wrapper ends-->
	<?php include "asistencias.php"; ?>
    <a href="" title="Go to Top" class="back-to-top"> <span class="fa fa-angle-up"></span> </a> 
    <!--Java Scripts--> 
    <!--Java Scripts-->
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/jquery-easing-1.3.js"></script>
    <script type="text/javascript" src="js/jquery.sticky.js"></script>
    <script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
    <script type="text/javascript" src="js/jquery.smartresize.js"></script> 
    <script type="text/javascript" src="js/shortcodes.js"></script>   

    <script type="text/javascript" src="js/custom.js"></script>

    <!-- Layer Slider --> 
    <script type="text/javascript" src="js/jquery-transit-modified.js"></script> 
    <script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script> 
    <script type='text/javascript' src="js/greensock.js"></script> 
    <script type='text/javascript' src="js/layerslider.transitions.js"></script> 

    <script type="text/javascript">var lsjQuery = jQuery;</script><script type="text/javascript"> lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice('layerslider_2','jquery'); } else { lsjQuery("#layerslider_2").layerSlider({responsiveUnder: 1240, layersContainer: 1170, skinsPath: 'js/layerslider/skins/'}) } }); </script>

                     <!-- Cerrar Session -->
                     <script>
                        jQuery(document).ready(function(){
                           jQuery("#close_session").click(function(){
                              alert('Su sesión ha sido cerrada');
                              window.location.href='closesession.php';
                           });
                        });
                     </script>

  </body>
  </html>

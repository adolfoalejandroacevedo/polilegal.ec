<?php 
	session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");
	$imgind = array();
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>
         Tu Abogado:
         Users :: Directorio de Abogados del Ecuador
      </title>
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="css/style.css"/>
		<link rel="stylesheet" type="text/css" href="css/prettyPhoto.css"/>
      <link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <link href="css/animate.min.css" rel="stylesheet">
      <link href="css/prettyPhoto.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="bower_components/datatables.net-dt/css/jquery.dataTables.css">
		<link rel="stylesheet" type="text/css" href="/css/bootstrap-markdown.min.css">
      <!-- JQuery Validator and form -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.js"></script>

      <style>
			.nota p{
				font-size: 14px;
				font-weight: 600;
				margin-bottom: 0px;
			}
         /* Color del mensaje de error
         del checkbox y posicion debajo del cuadro */
         .errorMsq {
            color: red;
            display: block;
          }
          /* Color rojo para el texto de error de los campos */
         .error{
            color: red;
         }
         /* Borde rojo y grosor de linea de los inputs */
         .email-form input[type=email].error,
         .infogeneral-form input[type=text].error,
         .infogeneral-form input[type=password].error{
            padding:15px 18px;
            border:1px solid #FF0000;
         }
			.infogeneral-form select.error{
				padding:15px 18px;
    			border:1px solid #FF0000;
			}			
         /* Grosor y color de linea del checkbox en modo error */
         .reg-form input[type=checkbox].error{
            outline: 1px solid #FF0000;
         }
			.panel-body .media .row .form-group{
			margin-left: 45px;
			margin-right: 45px;
			}
         .pricingTable{
         text-align: center;
         transition: all 0.5s ease 0s;
         }
         .pricingTable:hover{
         box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
         }
         .pricingTable .pricingTable-header{
         color: #feffff;
         }
         .pricingTable .heading{
         display: block;
         padding-top: 25px;
         }
         .pricingTable .heading > h3{
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .textotitle{
         color: #feffff;
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .pricingTable .subtitle{
         display: block;
         font-size: 13px;
         margin-top: 5px;
         text-transform: capitalize;
         }
         .pricingTable .price-value{
         display: block;
         font-size: 60px;
         font-weight: 700;
         padding-bottom: 25px;
         }
         .pricingTable .price-value span{
         display: block;
         font-size: 14px;
         line-height: 20px;
         text-transform: uppercase;
         }
         .pricingTable .pricingContent{
         /*text-transform: capitalize;*/
         background: #fbfbfb;
         color: #fefeff;
         }
         .pricingTable .pricingContent ul{
         list-style: none;
         padding: 15px 14px 10px;
         margin: 0;
         text-align: left;
         }
         .pricingTable .pricingContent ul li{
         font-size: 14px;
         padding: 12px 0;
         border-bottom: 1px dashed #e1e1e1;
         color: #9da1ad;
         }
         .pricingTable .pricingContent ul li i{
         font-size: 14px;
         float: right;
         }
         .pricingTable .pricingTable-sign-up{
         padding: 20px 0;
         background: #fbfbfb;
         color: #fff;
         text-transform: capitalize;
         }
         .pricingTable .btn-block{
         width: 60%;
         margin: 0 auto;
         font-size: 17px;
         color: #fff;
         text-transform: capitalize;
         border: none;
         border-radius: 5px;
         padding: 10px;
         transition: all 0.5s ease 0s;
         }
         .pricingTable .btn-block:before{
         content: "\f007";
         font-family: 'FontAwesome';
         margin-right: 10px;
         }
         .pricingTable.blue .pricingTable-header,
         .pricingTable.blue .btn-block{
         background: #fce400;
         }
         .pricingTable.pink .pricingTable-header,
         .pricingTable.pink .btn-block{
         background: #c9b600;
         }
         .pricingTable.orange .pricingTable-header,
         .pricingTable.orange .btn-block{
         background: #e0d83a;
         }
         .pricingTable.green .pricingTable-header,
         .pricingTable.green .btn-block{
         background: #e0cd00;
         }
         .pricingTable.blue .btn-block:hover,
         .pricingTable.pink .btn-block:hover,
         .pricingTable.orange .btn-block:hover,
         .pricingTable.green .btn-block:hover{
         background: #e6e6e6;
         color: #939393;
         }
         @media screen and (max-width: 990px){
         .pricingTable{ margin-bottom: 20px; }
         }
			.breadcrumb>.active{
			color: #c1ab09;
    		font-weight: 600;
			}
			.glyphicon-trash{
				cursor: pointer;
			}
			/* Para la imagen personalizada */
          .drop_file_zone {
              background-color: #EEE;
              border: #999 5px dashed;
              width: 100%;
              height: 150px;
              padding-left: 0px;
              padding-right: 0px;
              padding-top: 0px;
              padding-bottom: 0px;
              font-size: 18px;
          }
          .drag_upload_file {
              width:50%;
              margin:0 auto;
          }
          .drag_upload_file p {
              text-align: center;
				  font-family: 'Open Sans', Arial, sans-serif;
				  font-size: 16px;
          }
          .drag_upload_file #selectfile {
              display: none;
          }
			.separador{
				padding-bottom: 55px;
			}
      </style>
   </head>
   <body class="homepage">
<?php include "header.php"; ?>
      <!--/header-->
      <section id="blog" class="container">
         <ol class="breadcrumb">
            <li><a href="index">Inicio</a></li>
            <li><a href="miperfil.php">Mi perfil</a></li>
            <li class="active">Mis anuncios</li>
         </ol>
         <div class="container">
            <div class="center">
               <h2> Galeria de Imágenes </h2>
            </div>
				<div class="row space-top">
					<div id="imagenes">
					<?php
						$sql = "SELECT * FROM galeria WHERE iduser='59'";
						if ($result = mysqli_query($conn, $sql)){
							while ($row = mysqli_fetch_assoc($result)) {
					?>
								<!-- image 1 -->
								<div class="boxfourcolumns">
									<div class="boxcontainer">
										<span class="gallery">
										<a data-gal="prettyPhoto[gallery1]" href="<?php echo "uploads/$row[img]"; ?>">
										<img src="<?php echo "uploads/$row[img]"; ?>" alt="Add Title" class="imgOpa" height="185" width="275" /></a>
										</span>
										<div id="<?php echo "$row[id]"; ?>" class="fotos"><h1>ELIMINAR<h1></div>
									</div>
								</div>
						<?php
							}
						}else{
							error_log("Error: " . $sql . "..." . mysqli_error($conn));
						}
					?>

						 <!-- image 11 etc -->

					</div>
				</div>
			<div>
      </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <div class="container">
            <div class="row">
               <div class="col-sm-6">
                  &copy; 2018 <a target="_blank" href="http://www.tuabogado.ec/" title="">tuabogado.ec</a>. All Rights Reserved.
               </div>
               <!--
                  All links in the footer should remain intact.
                  Licenseing information is available at: http://bootstraptaste.com/license/
                  You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Gp
                  -->
               <div class="col-sm-6">
                  <ul class="pull-right">
                     <li><a href="index.php">Inicio</a></li>
                     <li><a href="anunciate.php">Regístrate</a></li>
                     <li><a href="blog.php">Blog</a></li>
                     <li><a href="contactanos.php">Contáctenos</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </footer>
      <!--/#footer-->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="js/bootstrap.min.js"></script>
      <script src="js/markdown.js"></script>
      <script src="js/to-markdown.js"></script>
      <script src="js/bootstrap-markdown.js"></script>
      <script src="js/bootstrap-markdown.es.js"></script>
      <script type="text/javascript" src="js/jquery.simpleslider.js"></script>
      <script type="text/javascript" charset="utf8" src="bower_components/datatables.net/js/jquery.dataTables.min.js"> </script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/jquery.isotope.min.js"></script>
      <script src="js/wow.min.js"></script>
      <script src="js/main.js"></script>
      <script src="js/custom.js"></script>

		<!-- JAVASCRIPTS IMPORTADOS
		================================================== -->
		<!-- cycle -->
		<script src="js/jquery.cycle.js"></script>
		<!-- carousel -->
		<script src="js/jquery.carouFredSel-6.0.3-packed.js"></script>
		<!-- gallery -->
		<script src="js/jquery.prettyPhoto.js"></script>
		<!-- Elimina fotos -->
		<script src="js/eliminafoto.js"></script>

		<!-- CALL opacity on hover images -->
		<script type="text/javascript">
		$(document).ready(function(){
			 $("img.imgOpa").hover(function() {
				$(this).stop().animate({opacity: "0.6"}, 'slow');
			 },
			 function() {
				$(this).stop().animate({opacity: "1.0"}, 'slow');
			 });
		  });
		</script>

		<!-- CALL filtering -->
		<script>
		$(document).ready(function(){
		var $container = $('#content');
		  $container.imagesLoaded( function(){
			 $container.isotope({
			filter: '*',
			animationOptions: {
			  duration: 750,
			  easing: 'linear',
			  queue: false,
			}
		});
		});
		$('#nav a').click(function(){
		  var selector = $(this).attr('data-filter');
			 $container.isotope({ 
			filter: selector,
			animationOptions: {
			  duration: 750,
			  easing: 'linear',
			  queue: false,
			}
		  });
		  return false;
		});

		$('#nav a').click(function (event) {
			 $('a.selected').removeClass('selected');
			 var $this = $(this);
			 $this.addClass('selected');
			 var selector = $this.attr('data-filter');

			 $container.isotope({
					filter: selector
			 });
			 return false; // event.preventDefault()
		});

		});
		 </script>

		<!-- CALL lightbox prettyphoto -->
		<script type="text/javascript">
		  $(document).ready(function(){
			 $("a[data-gal^='prettyPhoto']").prettyPhoto({social_tools:'', animation_speed: 'normal' , theme: 'dark_rounded'});
		  });
		</script>


   </body>
</html>




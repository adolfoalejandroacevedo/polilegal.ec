<?php
$cuerpo = '
<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Email</title>
    <style>
        body {
            width: 100%;
            font-family: "Open Sans", sans-serif;
        }

        svg {
            width: 100px;
        }
        
        .main-table{
            backgnumber_format:white;
            
        }
        
        table {
            width: 500px;
            display: flex;
            flex-flow: column;
            margin: 0px auto;
        }

        table thead tr th {
            border-bottom: solid 5px black;
            width: 500px;
        }

        thead tr th svg {
            text-align: center;
        }

        .tabla-gris {
            backgnumber_format: #f8f8f8;
            
            border-radius: 5px;
            margin-bottom: 5px;
            padding: 15px;
           font-family: "Open Sans", sans-serif;
            font-size: 15px;
        }

        .tabla-gris .td1 {
            border-bottom: solid 1px #ccc;
        }
        .titulo{
          font-family: "Open Sans", sans-serif;
            padding: 5px;
        }
        .titulo h1 {
            font-size: 25px;
            line-height: 0px;
        }
        .line{
            border-bottom: solid 5px black;
        }
    </style>
</head>

<body>
    <table class="main-table">
        <thead>
            <tr>

            </tr>
        </thead>
        <tbody>
            <tr>
                <th class="titulo"><h1 style="width: 500px;">Hola '.$nombres.'</h1> <br> Gracias por su compra.</th>
            </tr>
            <tr>
                <td>
                    <table class="tabla-gris">
                        <tr>
                            <td class="td1">Usted ha renovado el '.$order_description.'.</td>
                        </tr>
                        <tr>
                            <td>Su pago ha sido procesado de forma satisfactoria.</td>
                        </tr>
                    </table>
                    <table class="tabla-gris">
                        <tr>
                            <td class="td1">DATOS DE LA TRANSACCIÓN</td>
                        </tr>
                        <tr>
                            <td>ID de transaccion: '.$df.'</td>
                        </tr>
                        <tr>
                            <td>Numero de autorización: '.$txtCod.'</td>
                        </tr>
                        <tr>
                            <td>Sub Total Plan: USD $'.number_format($sub_total,2).'</td>
                        </tr>
                        <tr>
                            <td>Monto IVA: USD $'.number_format($order_vat,2).'</td>
                        </tr>
                        <tr>
                            <td>Total Plan: USD $'.number_format($order_amount,2).'</td>
                        </tr>
                        <tr>
                            <td>Porcentaje IVA: 12%</td>
                        </tr>
                    </table>
                     <table>
                        <tr>
                           <td align="middle"><img src="https://polilegal.ec/images1/jutnos3.png" style="width: 80%;" /></td>
                        </tr>
                     </table>
                    <table class="tabla-gris">
                        <tr>
                            <td></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>

    </table>

</body>

</html>
';
?>

<?php

   session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);

	//Dejando solo dos areas de practica
	$sql = "DELETE FROM usersareas WHERE iduser='$_SESSION[id]' 
			  AND id NOT IN ( SELECT id FROM ( SELECT id FROM usersareas WHERE iduser='$_SESSION[id]' ORDER BY id DESC LIMIT 2 ) x )";
	if (!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

	//Actualiza transaccion
	$sql = "INSERT INTO transacciones (iduser, concepto, estatus) VALUES ('$_SESSION[id]', '1', '1')";
	if (!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

	//Actualizando el plan
   $sql = "UPDATE users SET plan='1' WHERE id='$_SESSION[id]'";
	if (mysqli_query($conn, $sql)){
		$_SESSION['plan'] = 1;
		header('Location: miperfil.php');
	}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

?>


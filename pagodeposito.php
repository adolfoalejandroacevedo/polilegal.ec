<?php
   session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
	$cart = $_SESSION['cart']
?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>
         Tu Abogado:
         Users :: Directorio de Abogados del Ecuador
      </title>
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <link href="css/animate.min.css" rel="stylesheet">
      <link href="css/prettyPhoto.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="bower_components/datatables.net-dt/css/jquery.dataTables.css">
      <link rel="stylesheet" type="text/css" href="css/bootstrap-markdown.min.css">
      <!-- JQuery Validator and form -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.js"></script>
      <style>
         .error{
            color: red;
         }
         .login-form input[type=text].error,
         .login-form input[type=email].error,
         .login-form input[type=password].error{
            padding:15px 18px;
            border:1px solid #FF0000;
         }
         .pricingTable{
         text-align: center;
         transition: all 0.5s ease 0s;
         }
         .pricingTable:hover{
         box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
         }
         .pricingTable .pricingTable-header{
         color: #feffff;
         }
         .pricingTable .heading{
         display: block;
         padding-top: 25px;
         }
         .pricingTable .heading > h3{
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .textotitle{
         color: #feffff;
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .pricingTable .subtitle{
         display: block;
         font-size: 13px;
         margin-top: 5px;
         text-transform: capitalize;
         }
         .pricingTable .price-value{
         display: block;
         font-size: 60px;
         font-weight: 700;
         padding-bottom: 25px;
         }
         .pricingTable .price-value span{
         display: block;
         font-size: 14px;
         line-height: 20px;
         text-transform: uppercase;
         }
         .pricingTable .pricingContent{
         /*text-transform: capitalize;*/
         background: #fbfbfb;
         color: #fefeff;
         }
         .pricingTable .pricingContent ul{
         list-style: none;
         padding: 15px 14px 10px;
         margin: 0;
         text-align: left;
         }
         .pricingTable .pricingContent ul li{
         font-size: 14px;
         padding: 12px 0;
         border-bottom: 1px dashed #e1e1e1;
         color: #9da1ad;
         }
         .pricingTable .pricingContent ul li i{
         font-size: 14px;
         float: right;
         }
         .pricingTable .pricingTable-sign-up{
         padding: 20px 0;
         background: #fbfbfb;
         color: #fff;
         text-transform: capitalize;
         }
         .pricingTable .btn-block{
         width: 60%;
         margin: 0 auto;
         font-size: 17px;
         color: #fff;
         text-transform: capitalize;
         border: none;
         border-radius: 5px;
         padding: 10px;
         transition: all 0.5s ease 0s;
         }
         .pricingTable .btn-block:before{
         content: "\f007";
         font-family: 'FontAwesome';
         margin-right: 10px;
         }
         .pricingTable.blue .pricingTable-header,
         .pricingTable.blue .btn-block{
         background: #fce400;
         }
         .pricingTable.pink .pricingTable-header,
         .pricingTable.pink .btn-block{
         background: #c9b600;
         }
         .pricingTable.orange .pricingTable-header,
         .pricingTable.orange .btn-block{
         background: #e0d83a;
         }
         .pricingTable.green .pricingTable-header,
         .pricingTable.green .btn-block{
         background: #e0cd00;
         }
         .pricingTable.blue .btn-block:hover,
         .pricingTable.pink .btn-block:hover,
         .pricingTable.orange .btn-block:hover,
         .pricingTable.green .btn-block:hover{
         background: #e6e6e6;
         color: #939393;
         }
         @media screen and (max-width: 990px){
         .pricingTable{ margin-bottom: 20px; }
         }
      </style>
		<style type="text/css">
			/* CSS for Credit Card Payment form */
			.credit-card-box .panel-title {
				display: inline;
				font-weight: bold;
			}

			.credit-card-box .form-control.error {
				border-color: red;
				outline: 0;
				box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6);
			}

			.credit-card-box label.error {
				font-weight: bold;
				color: red;
				padding: 2px 8px;
				margin-top: 2px;
			}

			.credit-card-box .payment-errors {
				font-weight: bold;
				color: red;
				padding: 2px 8px;
				margin-top: 2px;
			}
			.credit-card-box label {
				display: block;
			}

			/* The old "center div vertically" hack */
			.credit-card-box .display-table {
				display: table;
			}

			.credit-card-box .display-tr {
				display: table-row;
			}

			.credit-card-box .display-td {
				display: table-cell;
				vertical-align: middle;
				width: 100%;
			}
			/* Just looks nicer */
			.credit-card-box .panel-heading img {
				min-width: 180px;
			}

			[type="radio"]:checked,
			[type="radio"]:not(:checked) {
				position: absolute;
				left: -9999px;
			}

			[type="radio"]:checked + label,
			[type="radio"]:not(:checked) + label {
				position: relative;
				padding-left: 28px;
				cursor: pointer;
				line-height: 20px;
				display: inline-block;
				color: #666;
			}

			[type="radio"]:checked + label:before,
			[type="radio"]:not(:checked) + label:before {
				content: '';
				position: absolute;
				left: 0;
				top: 0;
				width: 18px;
				height: 18px;
				border: 1px solid #ddd;
				border-radius: 100%;
				background: #fff;
			}
			[type="radio"]:checked + label:after,
			[type="radio"]:not(:checked) + label:after {
				content: '';
				width: 12px;
				height: 12px;
				background: #00a1e4;
				position: absolute;
				top: 4px;
				left: 4px;
				border-radius: 100%;
				-webkit-transition: all 0.2s ease;
				transition: all 0.2s ease;
			}

			[type="radio"]:not(:checked) + label:after {
				opacity: 0;
				-webkit-transform: scale(0);
				transform: scale(0);
			}

			[type="radio"]:checked + label:after {
				opacity: 1;
				-webkit-transform: scale(1);
				transform: scale(1);
			}
			.elemento {
				-webkit-box-shadow: 2px 2px 5px #999;
				-moz-box-shadow: 2px 2px 5px #999;
			}
		.center {
				display: block;
				margin-left: auto;
				margin-right: auto;
				width: 25%;
		}
		</style>
   </head>
   <body class="homepage">
<?php include "header.php"; ?>
      <!--/header-->
      <section id="blog" class="container" style="width: 500px">
				<div class="grid">
					<div class="row">
						<div class="c4">
						</div>
						<div class="c4">
							<!-- CREDIT CARD FORM STARTS HERE -->
							<div class="elemento panel panel-default credit-card-box">
								<div class="panel-heading display-table" style="
									padding-top: 10px;
									padding-bottom: 10px;
									padding-left: 15px;
									padding-right: 15px;
									background: #ddd;
									">
									<div class="row display-tr">
										<h6 class="panel-title display-td">Opciones de pago </h6>
										<div class="display-td">
											<img class="img-responsive pull-right"  src="images/tarjetas.png">
										</div>
									</div>
								</div>
								<div class="panel-body" style="
									padding-top: 15px;
									padding-bottom: 15px;
									padding-left: 30px;
									padding-right: 30px;
									background-color: white;
									">
									<div class="row" style="margin-left: 0px; ">
										<div class=" col-md-12">
											<label style="font-size: 16px; color: #0E8FAB;" class="text-center"><b> Depósito o Transferencia</b></label>
											<br>
										</div>
									</div>
									<div class="row" style="margin-left: 0px; ">
										<div class="  col-md-12">
											<p>Debe realizar un depósito o transferencia en <b>Banco Pichincha</b>. A nombre de: <b>POLILEGAL S.A., Ruc: 1792768519001,
												Cuenta Corriente No.: 2100151328,</b> por un <b>monto</b> de <b>$<?php echo $cart['total_depo']; ?></b>, por concepto de 
												suscripción por 1 año al <b><?php echo $cart['order_description']; ?></b> (10% de descuento aplicado).</p>
											<p>Una vez realizado el depósito o transferencia, debe de enviar el comprobante al siguiente correo electrónico: 
												info@tuabogado.ec para que el servicio pueda ser activado.</p>
										</div>
									</div>
									<div class="row" align="center" style="margin-left: 0px; ">
										<div class="pricingTable green pricingTable-sign-up" style="width: 300px; margin-top: 20px">
											<a href="index.php" class="btn btn-block" target="_self" rel="nofollow" id="btnpago">Finalizar</a>
										</div>
										<div id="barra" style="display: block; width:50%; padding-top: 20px; display: none">
											<center><img id="enviando" src="images/barra.gif" \><br>
											<b>Por favor espere</b></center>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="c4">
						</div>
					</div>
				</div><!-- end grid -->
      </section>
      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="js/bootstrap.min.js"></script>
      <script src="js/markdown.js"></script>
      <script src="js/to-markdown.js"></script>
      <script src="js/bootstrap-markdown.js"></script>
      <script src="js/bootstrap-markdown.es.js"></script>
      <script type="text/javascript" src="js/jquery.simpleslider.js"></script>
      <script type="text/javascript" charset="utf8" src="bower_components/datatables.net/js/jquery.dataTables.min.js"> </script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/jquery.isotope.min.js"></script>
      <script src="js/wow.min.js"></script>
      <script src="js/main.js"></script>
      <script src="js/custom.js"></script>
      <script src="https://cdn.paymentez.com/checkout/1.0.1/paymentez-checkout.min.js"></script>

   </body>
</html>


<?php 
   session_start();
   session_unset();
?>
<!DOCTYPE HTML>
<html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
  <meta http-equiv="content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
  <title>Polilegal | terminos y condiciones</title>
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
  <link id="default-css" href="style.css" rel="stylesheet" type="text/css">
  <link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" /> 
<!--[if IE 7]>
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lt IE 9]>
<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--Fonts-->
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--jquery-->
<script src="js/modernizr-2.6.2.min.js"></script>
</head>
<body>
  <!--wrapper starts-->
  <div class="wrapper">
    <!--inner-wrapper starts-->
    <div class="inner-wrapper">
      <!--header starts-->
		<?php include 'header.php'; ?>
      <!--header ends-->
            <!--main starts-->
            <div id="main">

              <div class="breadcrumb-section">
                <div class="container">
                  <h1> CONOCE A ALGUNOS DE NUESTROS ABOGADOS QUE FORMAN PARTE DEL EQUIPO POLILEGAL EN QUITO </h1>
                  <div class="breadcrumb">
                    <a href="index.php"> Inicio </a>
                    <a href="nosotros.php">Nosotros</a>
                  </div>
                </div>
              </div>

              <!--container starts-->
              <div class="container"> 

                <!--primary starts-->
                <section id="primary" class="content-full-width">
                  <p><span class="dt-sc-highlightt skin-color"> <strong>Dr. Alfredo López M.</strong></span><br><br>Presidente de PoliLegal Abogados S.A.  Coronel de Policía de E.M. Doctor en Jurisprudencia y Abogado de los Tribunales y Juzgados de la República del Ecuador por la Universidad Central.  Dentro de la Policía Nacional, ocupó importantes cargos como Juez, Fiscal General y Director Nacional de Asesoría Jurídica, en los cuales se destacó por su inigualable profesionalismo.  Ha participado en cursos nacionales e internacionales en la institución policial y ha sido merecedor de varias condecoraciones y felicitaciones por su excelente y reconocida labor. Experto en asuntos policiales, administrativos y constitucionales.</p><br>     
                  <p><span class="dt-sc-highlightt skin-color"> <strong>Dr. Eduardo Moncayo G.</strong></span><br><br>General de Distrito de la Policía Nacional.  Doctor en Jurisprudencia y Abogado de los Tribunales de la República del Ecuador por la Universidad Central.  Dentro de la Policía Nacional, ejerció los cargos de Juez, Fiscal, Comisario Nacional, Ministro de la Corte Distrital de Justicia de la Policía Nacional, además de haber sido catedrático en la Escuela Superior de Policía.  Experto en asuntos policiales y administrativos.</p><br> 
		  <p><span class="dt-sc-highlightt skin-color"> <strong>Ab. Andrea López J. (MSC)</strong></span><br><br>Abogada por la Universidad de las Américas.  Master en Derecho de la Empresa y de los Negocios por la Universidad de Barcelona.  Master en Estudios Internacionales por la Universidad de Barcelona. Ha participado en cursos nacionales e internacionales en varias ramas del derecho.  Además, ha participado en la elaboración de Convenios Internacionales en la Cancillería ecuatoriana.  Su experiencia se concentra en: administrativo, civil, constitucional, familia, contratos, inversiones, societario, policial e internacional.</p><br>
		  <p><span class="dt-sc-highlightt skin-color"> <strong>Ab. Víctor Fernández B. (MSC)</strong></span><br><br>Abogado por la Universidad de Barcelona, España.  Master en Derecho Empresarial por la Universidad de Barcelona.  Ha participado en varios seminarios y congresos a nivel internacional.   Ha sido asesor de empresas a nivel nacional e internacional.  Su experiencia se concentra en temas mercantiles, policiales, administrativos, internacionales, de arbitraje y regulatorios (asuntos de importación y registros sanitarios).</p><br>
		  <p><span class="dt-sc-highlightt skin-color"> <strong>Dr. Fernando Fabara B.</strong></span><br><br>Doctor en Jurisprudencia y Abogado de los Tribunales y Juzgados de la República del Ecuador, Licenciado en CC. Públicas y Sociales por la Universidad Central del Ecuador.  Maestría en Recursos, Procesos Jurídico Administrativos de la Pontificia Universidad Javeriana de Colombia.  Magíster en Derecho Procesal Penal y en Derecho Procesal Civil de la Universidad Tecnológica Indoamérica.  Fue asesor jurídico de varias instituciones públicas y privadas.  Fue Juez y Fiscal del área Penal y de Tránsito.  Áreas de práctica: penal, tránsito, infracciones administrativas, contencioso-administrativo, laboral y desarrollo organizacional y de empresa. </p><br>
		  <p><span class="dt-sc-highlightt skin-color"> <strong>Ab. María Sol Sevilla V. (MSC)</strong></span><br><br>Abogada por la Universidad de las Américas.  Máster en Derecho de los Sectores Regulados con mención en Derecho Energético por la Universidad Carlos III de Madrid.  Especialización en Derecho Bursátil por la Universidad Andina Simón Bolívar.  Ha dictado cursos en temas ambientales para empresas renombradas del Ecuador y ha sido consultora para el desarrollo de reglamentos municipales.  Su experiencia se concentra en temas corporativos, asesoría a compañías del sector petrolero, comercial, de salud, constructoras, entre otras.</p><br>
		  <p><span class="dt-sc-highlightt skin-color"> <strong>Ab. Eduardo Veintimilla T. (MSC)</strong></span><br><br>Abogado por la Universidad de las Américas.  Máster en Derecho Empresarial por la Universitat de Barcelona.  Docente en la Universidad Técnica Particular de Loja.  Ha sido partícipe de varios cursos a nivel nacional e internacional.  Posee amplia experiencia en coactiva y recuperación de cartera, así como en asuntos laborales, derecho comercial, corporativo y derecho de competencia.</p><br>
		  <p><span class="dt-sc-highlightt skin-color"> <strong>Dr. Milton Paredes E.</strong></span><br><br>Doctor en Jurisprudencia y Abogado de los Tribunales y Juzgados de la República de la Universidad Central del Ecuador.  Ha desempeñado importantes cargos dentro del Servicio de Rentas Internas, Ministerio de Finanzas y Banco Nacional de Fomento.  Tiene varios años de experiencia en el libre ejercicio, sobre todo, es experto en temas civiles, laborales, menores, administrativo, contratos, entre otros.</p><br>
		  <p><span class="dt-sc-highlightt skin-color"> <strong>Ec. Alex López J. (MSC)</strong></span><br><br>Economista de la Pontifica Universidad Católica del Ecuador.  Especialista en sistemas de gestión de calidad de la Pontificia Universidad Católica del Ecuador. Magister en Administración y Marketing de la Universidad Indoamérica. Diplomado internacional en Alta Gerencia y Gestión Estratégica del TEC de Monterrey.  Actualmente, es Gerente de Operaciones de una importante empresa ecuatoriana y es Consultor Externo de PoliLegal.  Es experto en levantamiento de información, seguimiento y evaluación de sistemas de gestión de calidad y por procesos, planificación estratégica y de gestión empresarial, así como asesor en asuntos regulatorios relacionados con registros sanitarios, entre otros.</p><br>
		  <p><span class="dt-sc-highlightt skin-color"> <strong>Ing. María Gabriela López</strong></span><br><br>Ingeniera en comercio exterior por la Universidad Internacional del Ecuador.  Ha desempeñado sus labores en importantes empresas como Diners Club del Ecuador, Kuomodo, entre otras, así como en instituciones públicas como Secretaría Nacional del Agua y Empresa Pública del Agua.  Experta en temas de planificación y ejecución de planes y proyectos, y asesora en temas de importación y exportación.  Áreas de práctica: procesos, planes estratégicos, finanzas y comercio exterior</p><br>
                  <div class="dt-sc-toggle-frame" style="width: 555px;">
                    <h5 class="dt-sc-toggle"><a href="#">CONOCE NUESTRA RED DE ABOGADOS A NIVEL NACIONAL.</a></h5>
                    <div class="dt-sc-toggle-content">
                      <div class="dt-sc-block" style="text-align: left;"> 
<p align="center" style="margin-bottom: 0.28cm; line-height: 108%"><table dir="ltr" align="left" width="519" hspace="9" cellpadding="7" cellspacing="0">
	<col width="127">
	<col width="241">
	<col width="108">
	<tr valign="top">
		<th width="127" style="border: 1px solid #00000a; padding: 0cm 0.19cm">
			<p align="center" style="margin-bottom: 0cm"><br/>

			</p>
			<p align="center"><b>PROVINCIA</b></p>
		</th>
		<th width="241" style="border: 1px solid #00000a; padding: 0cm 0.19cm">
			<p align="center" style="margin-bottom: 0cm"><br/>

			</p>
			<p align="center"><b>CIUDADES</b></p>
		</th>
		<th width="108" style="border: 1px solid #00000a; padding: 0cm 0.19cm; text-align: left">
			<b>ABOGADOS O ESTUDIOS JURÍDICOS</b>
		</th>
	</tr>
	<tr valign="top">
		<td width="127" height="7" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #cc99ff; color: #000000">
			<p align="justify">Esmeraldas</p>
		</td>
		<td width="241" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #cc99ff; color: #000000">
			<p align="justify">Ciudad Esmeraldas</p>
		</td>
		<td width="108" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #cc99ff; color: #000000">
			<p align="center">6</p>
		</td>
	</tr>
	<tr valign="top">
		<td width="127" height="13" bgcolor="#cc99ff" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #cc99ff; color: #000000">
			<p align="justify">Imbabura</p>
		</td>
		<td width="241" bgcolor="#cc99ff" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #cc99ff; color: #000000">
			<p align="justify">Ibarra</p>
		</td>
		<td width="108" bgcolor="#cc99ff" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #cc99ff; color: #000000">
			<p align="center">3</p>
		</td>
	</tr>
	<tr valign="top">
		<td width="127" height="14" bgcolor="#cc99ff" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #cc99ff; color: #000000">
			<p align="justify">Carchi 
			</p>
		</td>
		<td width="241" bgcolor="#cc99ff" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #cc99ff; color: #000000">
			<p align="justify">Tulcán</p>
		</td>
		<td width="108" bgcolor="#cc99ff" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #cc99ff; color: #000000">
			<p align="center">2</p>
		</td>
	</tr>
	<tr valign="top">
		<td width="127" height="14" bgcolor="#cc99ff" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #cc99ff; color: #000000">
			<p align="justify">Sucumbíos</p>
		</td>
		<td width="241" bgcolor="#cc99ff" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #cc99ff; color: #000000">
			<p align="justify">Nueva Loja</p>
		</td>
		<td width="108" bgcolor="#cc99ff" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #cc99ff; color: #000000">
			<p align="center">2</p>
		</td>
	</tr>
	<tr valign="top" bgcolor="#cc99ff">
		<td width="127" height="5" bgcolor="#9cc2e5" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #9cc2e5; color: #000000">
			<p align="justify">Pichincha</p>
		</td>
		<td width="241" bgcolor="#9cc2e5" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #9cc2e5; color: #000000">
			<p align="justify" style="margin-bottom: 0cm">Quito 
			</p>
			<p align="justify">Cayambe</p>
		</td>
		<td width="108" bgcolor="#9cc2e5" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #9cc2e5; color: #000000">
			<p align="center">15</p>
		</td>
	</tr>
	<tr valign="top">
		<td width="127" height="13" bgcolor="#9cc2e5" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #9cc2e5; color: #000000">
			<p align="justify">Napo</p>
		</td>
		<td width="241" bgcolor="#9cc2e5" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #9cc2e5; color: #000000">
			<p align="justify" style="margin-bottom: 0cm">Tena</p>
			<p align="justify">El Chaco/Loreto/Coca</p>
		</td>
		<td width="108" bgcolor="#9cc2e5" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #9cc2e5; color: #000000">
			<p align="center">3</p>
		</td>
	</tr>
	<tr valign="top">
		<td width="127" height="5" bgcolor="#9cc2e5" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #9cc2e5; color: #000000">
			<p align="justify">Orellana</p>
		</td>
		<td width="241" bgcolor="#9cc2e5" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #9cc2e5; color: #000000">
			<p align="justify">Francisco de Orellana</p>
		</td>
		<td width="108" bgcolor="#9cc2e5" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #9cc2e5; color: #000000">
			<p align="center">2</p>
		</td>
	</tr>
	<tr valign="top">
		<td width="127" height="11" bgcolor="#0033cc" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #0033cc; color: #000000">
			<p align="justify">Cotopaxi</p>
		</td>
		<td width="241" bgcolor="#0033cc" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #0033cc; color: #000000">
			<p align="justify">Latacunga</p>
		</td>
		<td width="108" bgcolor="#0033cc" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #0033cc; color: #000000">
			<p align="center">2</p>
		</td>
	</tr>
	<tr valign="top">
		<td width="127" height="13" bgcolor="#0033cc" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #0033cc; color: #000000">
			<p align="justify">Tungurahua</p>
		</td>
		<td width="241" bgcolor="#0033cc" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #0033cc; color: #000000">
			<p align="justify">Ambato</p>
		</td>
		<td width="108" bgcolor="#0033cc" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #0033cc; color: #000000">
			<p align="center">3</p>
		</td>
	</tr>
	<tr valign="top">
		<td width="127" height="7" bgcolor="#0033cc" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #0033cc; color: #000000">
			<p align="justify">Chimborazo</p>
		</td>
		<td width="241" bgcolor="#0033cc" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #0033cc; color: #000000">
			<p align="justify">Riobamba</p>
		</td>
		<td width="108" bgcolor="#0033cc" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #0033cc; color: #000000">
			<p align="center">3</p>
		</td>
	</tr>
	<tr valign="top">
		<td width="127" bgcolor="#0033cc" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #0033cc; color: #000000">
			<p align="justify">Pastaza</p>
		</td>
		<td width="241" bgcolor="#0033cc" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #0033cc; color: #000000">
			<p align="justify">Puyo</p>
		</td>
		<td width="108" bgcolor="#0033cc" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #0033cc; color: #000000">
			<p align="center">2</p>
		</td>
	</tr>
	<tr valign="top">
		<td width="127" bgcolor="#ff6600" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="justify">Manabí</p>
		</td>
		<td width="241" bgcolor="#ff6600" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="justify" style="margin-bottom: 0cm">Portoviejo</p>
			<p align="justify">Manta</p>
		</td>
		<td width="108" bgcolor="#ff6600" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="center">5</p>
		</td>
	</tr>
	<tr valign="top">
		<td width="127" height="14" bgcolor="#ff6600" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="justify">S.D. Tsachilas</p>
		</td>
		<td width="241" bgcolor="#ff6600" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="justify">Santo Domingo</p>
		</td>
		<td width="108" bgcolor="#ff6600" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="center">2</p>
		</td>
	</tr>
	<tr valign="top">
		<td width="127" height="13" bgcolor="#66ff33" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="justify">Guayas</p>
		</td>
		<td width="241" bgcolor="#66ff33" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="justify" style="margin-bottom: 0cm">Guayaquil</p>
			<p align="justify">Milagro</p>
		</td>
		<td width="108" bgcolor="#66ff33" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="center">8</p>
		</td>
	</tr>
	<tr valign="top">
		<td width="127" height="14" bgcolor="#66ff33" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="justify">Santa Elena</p>
		</td>
		<td width="241" bgcolor="#66ff33" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="justify">Santa Elena</p>
		</td>
		<td width="108" bgcolor="#66ff33" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="center">1</p>
		</td>
	</tr>
	<tr valign="top">
		<td width="127" height="7" bgcolor="#66ff33" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="justify">Los Ríos</p>
		</td>
		<td width="241" bgcolor="#66ff33" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="justify" style="margin-bottom: 0cm">Quevedo</p>
			<p align="justify">Babahoyo</p>
		</td>
		<td width="108" bgcolor="#66ff33" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="center">4</p>
		</td>
	</tr>
	<tr valign="top">
		<td width="127" height="9" bgcolor="#66ff33" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="justify">Bolívar</p>
		</td>
		<td width="241" bgcolor="#66ff33" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="justify">Guaranda</p>
		</td>
		<td width="108" bgcolor="#66ff33" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="center">2</p>
		</td>
	</tr>
	<tr valign="top">
		<td width="127" height="9" bgcolor="#66ff33" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="justify">Galápagos</p>
		</td>
		<td width="241" bgcolor="#66ff33" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="justify" style="margin-bottom: 0cm">Puerto Baquerizo
			Moreno 
			</p>
			<p align="justify" style="margin-bottom: 0cm">San Cristóbal</p>
			<p align="justify">Santa Cruz</p>
		</td>
		<td width="108" bgcolor="#66ff33" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ff6600; color: #000000">
			<p align="center">3</p>
		</td>
	</tr>
	<tr valign="top">
		<td width="127" height="8" bgcolor="#ffff00" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ffff00; color: #000000">
			<p align="justify">Azuay</p>
		</td>
		<td width="241" bgcolor="#ffff00" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ffff00; color: #000000">
			<p align="justify">Cuenca</p>
		</td>
		<td width="108" bgcolor="#ffff00" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ffff00; color: #000000">
			<p align="center">2</p>
		</td>
	</tr>
	<tr valign="top">
		<td rowspan="2" width="127" height="4" bgcolor="#ffff00" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ffff00; color: #000000">
			<p align="justify">Cañar 
			</p>
		</td>
		<td rowspan="2" width="241" bgcolor="#ffff00" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ffff00; color: #000000">
			<p align="justify">Azogues	</p>
		</td>
		<td rowspan="2" width="108" bgcolor="#ffff00" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ffff00; color: #000000">
			<p align="center">1</p>
		</td>
	</tr>
	<tr valign="top">
	</tr>
	<tr valign="top">
		<td width="127" height="10" bgcolor="#ffff00" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ffff00; color: #000000">
			<p align="justify">Morona Santiago</p>
		</td>
		<td width="241" bgcolor="#ffff00" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ffff00; color: #000000">
			<p align="justify">Macas	</p>
		</td>
		<td width="108" bgcolor="#ffff00" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #ffff00; color: #000000">
			<p align="center">1</p>
		</td>
	</tr>
	<tr valign="top">
		<td width="127" height="10" bgcolor="#c00000" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #c00000; color: #000000">
			<p align="justify">El Oro</p>
		</td>
		<td width="241" bgcolor="#c00000" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #c00000; color: #000000">
			<p align="justify">Machala</p>
		</td>
		<td width="108" bgcolor="#c00000" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #c00000; color: #000000">
			<p align="center">2</p>
		</td>
	</tr>
	<tr valign="top">
		<td width="127" height="10" bgcolor="#c00000" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #c00000; color: #000000">
			<p align="justify">Loja</p>
		</td>
		<td width="241" bgcolor="#c00000" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #c00000; color: #000000">
			<p align="justify">Loja</p>
		</td>
		<td width="108" bgcolor="#c00000" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #c00000; color: #000000">
			<p align="center">3</p>
		</td>
	</tr>
	<tr valign="top">
		<td width="127" bgcolor="#c00000" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #c00000; color: #000000">
			<p align="justify">Zamora Chinchipe</p>
		</td>
		<td width="241" bgcolor="#c00000" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #c00000; color: #000000">
			<p align="justify">Zamora</p>
		</td>
		<td width="108" bgcolor="#c00000" style="border: 1px solid #00000a; padding: 0cm 0.19cm; background: #c00000; color: #000000">
			<p align="center">1</p>
		</td>
	</tr>
</table><br/>
<br/>

</p>
		      </div>
	            </div>
		  </div>
                  </section>
                </div>
              </div>
            </div>

            <!--dt-sc-toggle-frame-set ends-->
          </div>     

          <!--primary ends-->

        </div>
        <!--container ends-->

        <div class="dt-sc-hr-invisible-large"></div>     

      </div>
      <!--main ends-->
      
      <!--footer starts-->
		<?php include 'footer.php'; ?>
      <!--footer ends-->
      </div>
      <!--inner-wrapper ends-->    
    </div>
    <!--wrapper ends-->
    <!--wrapper ends-->
	<?php include "asistencias.php"; ?>
   <a href="" title="Go to Top" class="back-to-top"> <span class="fa fa-angle-up"></span> </a> 
    <!--Java Scripts--> 
    <!--Java Scripts-->
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/jquery-easing-1.3.js"></script>
    <script type="text/javascript" src="js/jquery.sticky.js"></script>
    <script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
    <script type="text/javascript" src="js/jquery.smartresize.js"></script> 
    <script type="text/javascript" src="js/shortcodes.js"></script>   

    <script type="text/javascript" src="js/custom.js"></script>

    <!-- Layer Slider --> 
    <script type="text/javascript" src="js/jquery-transit-modified.js"></script> 
    <script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script> 
    <script type='text/javascript' src="js/greensock.js"></script> 
    <script type='text/javascript' src="js/layerslider.transitions.js"></script> 

    <script type="text/javascript">var lsjQuery = jQuery;</script><script type="text/javascript"> lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice('layerslider_2','jquery'); } else { lsjQuery("#layerslider_2").layerSlider({responsiveUnder: 1240, layersContainer: 1170, skinsPath: 'js/layerslider/skins/'}) } }); </script>

                     <!-- Cerrar Session -->
                     <script>
                        jQuery(document).ready(function(){
                           jQuery("#close_session").click(function(){
                              alert('Su sesión ha sido cerrada');
                              window.location.href='closesession.php';
                           });
                        });
                     </script>

  </body>
  </html>

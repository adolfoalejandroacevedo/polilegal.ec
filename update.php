<?php
	header('Content-Type: text/html; charset=utf-8');
	session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");

	//Conexion a la BD
	$config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
	mysqli_set_charset($conn, "utf8");

	//Validamos que se recibe un formulario
	if(!isset($_POST['formname'])){
		error_log('Se requiere de un formulario');
		header("location: index.php");
		exit;
	}

	switch($_POST['formname']){

		case "frminfogeneral":
			$nombres = mysqli_real_escape_string($conn,utf8_decode($_POST['nombres']));
			$matricula = mysqli_real_escape_string($conn,$_POST['matricula']);
			$sql = "UPDATE users SET 
					  nombres='$nombres',
					  matricula='$matricula',
					  ciudad='$_POST[ciudad]'
					  WHERE email='$_SESSION[email]'";
			if(mysqli_query($conn, $sql)){
				echo "updated";
			}else{ 
				error_log("Error: " . $sql . "..." . mysqli_error($conn));
				echo "Error Inesperado";
			}
			break;

		case "frmemail":
			//Email:
			//Si eiste un registro actualizamos
			$sql = "SELECT id FROM emails WHERE iduser='$_POST[iduser]'";
			if($result = mysqli_query($conn, $sql)){
				if(mysqli_num_rows($result) > 0) {
					$sql = "UPDATE emails SET iduser='$_POST[iduser]', email='$_POST[email]' WHERE iduser='$_POST[iduser]'";
					if(mysqli_query($conn, $sql)){
						$msjupdateemail = "updated";
					}else{
						error_log("Error: " . $sql . "..." . mysqli_error($conn));
						$msjupdateemail = "Error Inesperado";
					}
				}else{
					$sql = "INSERT INTO emails (iduser, email) VALUES ('$_POST[iduser]', '$_POST[email]')";
               if(mysqli_query($conn, $sql)){
                  $msjupdateemail = "updated";
               }else{
                  error_log("Error: " . $sql . "..." . mysqli_error($conn));
                  $msjupdateemail = "Error Inesperado";
               }
				}
			}else{
            error_log("Error: " . $sql . "..." . mysqli_error($conn));
            echo "Error Inesperado";
				exit;
			}
         //Phone:
         //Si eiste un registro actualizamos
         $sql = "SELECT id FROM phones WHERE iduser='$_POST[iduser]'";
         if($result = mysqli_query($conn, $sql)){
            if(mysqli_num_rows($result) > 0) {
               $sql = "UPDATE phones SET phone='$_POST[phone]' WHERE iduser='$_POST[iduser]'";
               if(mysqli_query($conn, $sql)){
                  $msjupdatephone = "updated";
               }else{
                  error_log("Error: " . $sql . "..." . mysqli_error($conn));
                  $msjupdatephone = "Error Inesperado";
               }
            }else{
               $sql = "INSERT INTO phones (iduser, phone) VALUES ('$_POST[iduser]', '$_POST[phone]')";
               if(mysqli_query($conn, $sql)){
                  $msjupdatephone = "updated";
               }else{
                  error_log("Error: " . $sql . "..." . mysqli_error($conn));
                  $msjupdatephone = "Error Inesperado";
               }
            }
         }else{
            error_log("Error: " . $sql . "..." . mysqli_error($conn));
            echo "Error Inesperado";
				exit;
         }
			if ($msjupdateemail == "updated" || $msjupdatephone == "updated"){
				echo "updated";
			}else{
				echo "Error Inesperado";
			}
			break;
		
		case "frmemails":
			//Validamos que el email no este en la lista
			$sql = "SELECT id FROM emails WHERE iduser='$_POST[iduser]' AND email='$_POST[email]'";
			if($result = mysqli_query($conn, $sql)){
				if(mysqli_num_rows($result) > 0) {
					echo "enlista";
					exit;
				}
			}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

			$sql = "INSERT INTO emails (iduser, email) VALUES ('$_POST[iduser]', '$_POST[email]')";
			if(mysqli_query($conn, $sql)){
				echo mysqli_insert_id($conn);
				exit;
			}else{
				error_log("Error: " . $sql . "..." . mysqli_error($conn));
				echo "Error Inesperado";
			}
			break;

		case "frmphones":
			$sql = "INSERT INTO phones (iduser, phone) VALUES ('$_POST[iduser]', '$_POST[phone]')";
			if(mysqli_query($conn, $sql)){
				echo mysqli_insert_id($conn);
			}else{
				error_log("Error: " . $sql . "..." . mysqli_error($conn));
				echo "Error Inesperado";
			}
			break;

		case "frmareas":
			//Validamos que no exista el area en el listado 
			$sql = "SELECT id FROM usersareas WHERE iduser='$_POST[iduser]' AND idarea='$_POST[area_id]'";
			if($result = mysqli_query($conn, $sql)){
				if(mysqli_num_rows($result) > 0) {
					echo "existe";
					exit;
				}
			}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
			//Validamos que si el plan es igual a 2, no existan 5 areas de practica
			if ($_SESSION['plan'] == 2){
				$sql = "SELECT id FROM usersareas WHERE iduser='$_POST[iduser]'";
				if($result = mysqli_query($conn, $sql)){
					if(mysqli_num_rows($result) < 5) {
						$sql = "INSERT INTO usersareas (iduser, idarea) VALUES ('$_POST[iduser]', '$_POST[area_id]')";
						if(mysqli_query($conn, $sql)){
							echo mysqli_insert_id($conn);
						}else{
							error_log("Error: " . $sql . "..." . mysqli_error($conn));
							echo "Error Inesperado";
						}
					}else{
						echo "Mas de Cinco";
					}
				}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
			}else if ($_SESSION['plan'] == 1){
            $sql = "SELECT id FROM usersareas WHERE iduser='$_POST[iduser]'";
            if($result = mysqli_query($conn, $sql)){
               if(mysqli_num_rows($result) < 1) {
                  $sql = "INSERT INTO usersareas (iduser, idarea) VALUES ('$_POST[iduser]', '$_POST[area_id]')";
                  if(mysqli_query($conn, $sql)){
                     echo mysqli_insert_id($conn);
                  }else{
                     error_log("Error: " . $sql . "..." . mysqli_error($conn));
                     echo "Error Inesperado";
                  }
               }else{
                  echo "Mas de uno";
               }
            }else error_log("Error: " . $sql . "..." . mysqli_error($conn));
			}else {
				$sql = "INSERT INTO usersareas (iduser, idarea) VALUES ('$_POST[iduser]', '$_POST[area_id]')";
				if(mysqli_query($conn, $sql)){
					echo mysqli_insert_id($conn);
				}else{
					error_log("Error: " . $sql . "..." . mysqli_error($conn));
					echo "Error Inesperado";
				}
			}
			break;

		case "frmdescription":
			$desc = mysqli_real_escape_string($conn, utf8_decode($_POST['description']));
			$sql = "REPLACE INTO descriptions VALUES ('$_SESSION[id]', '$desc')";
			if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
			echo "updated";
			break;

		case "frmjobaddress":
			$address = mysqli_real_escape_string($conn, utf8_decode($_POST['address']));
			$sql = "REPLACE INTO jobaddress VALUES ('$_SESSION[id]', '$address')";
			if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
			echo "updated";
			break;

		case "frmgeo":
         if (empty($_POST["geo"])){
            $geo = "http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=disney+paris&amp;
                     aq=&amp;sll=37.0625,-95.677068&amp;sspn=39.371738,86.572266&amp;ie=UTF8&amp;hq=disney&amp;
                     hnear=Paris,+%C3%8Ele-de-France,+France&amp;t=m&amp;fll=48.881877,2.535095&amp;
                     fspn=0.512051,1.352692&amp;st=103241701817924407489&amp;rq=1&amp;ev=zo&amp;split=1&amp;
                     ll=49.027964,2.772675&amp;spn=0.315159,0.585022&amp;z=10&amp;iwloc=D&amp;output=embed";
         }else{
            $urlmap = explode('"', utf8_decode($_POST["geo"]));
            $geo = substr($urlmap[1],0);
         }			
			//$geo = mysqli_real_escape_string($conn, $geo);
			$sql = "REPLACE INTO geolocation VALUES ('$_SESSION[id]', '$geo')";
			if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
			echo $geo;
			break;

      case "frmweb":
			$web = utf8_decode($_POST['web']);
         $sql = "REPLACE INTO sitioweb (iduser,url) VALUES ('$_SESSION[id]', '$web')";
         if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
			echo "updated";
         break;

      case "frmsocial":
         $sql = "INSERT INTO social (iduser,idsocial,url) VALUES ('$_SESSION[id]', '$_POST[social_id]', '$_POST[url]')";
         if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
			echo mysqli_insert_id($conn);
         break;

		case "frmarticulo":
			//Validamos si hay un archivo cargado
			if($_POST['filename'] == NULL){
				echo "nofilename";
				exit;
			} 

			//Procedemos a guardar en BD	
			$titulo = mysqli_real_escape_string($conn, utf8_decode($_POST['titulo']));
			$contenido = mysqli_real_escape_string($conn, utf8_decode($_POST['contenido']));
			$img = $_SESSION['file'];
			if($_POST['idart'] != NULL) {
				$sql = "	UPDATE articulos 
							SET 	titulo='$titulo', 
									contenido='$contenido',
									img='$img'
							WHERE id='$_POST[idart]'";
				if(mysqli_query($conn, $sql)) {
					echo $_POST['idart'];
				}else {
					error_log("Error: " . $sql . "..." . mysqli_error($conn));
					echo "Error en sql de BD";
				}
			}else{
				$sql = "	INSERT INTO articulos (iduser,titulo,contenido,img) 
							VALUES ('$_SESSION[id]','$titulo','$contenido','$img')";
				if(mysqli_query($conn, $sql)) {
					echo mysqli_insert_id($conn); 
				}else {
					error_log("Error: " . $sql . "..." . mysqli_error($conn));
					echo "Error en sql de BD";
				}
			}
			break;

		case "frmcupon":
			//Validamos si hay un archivo cargado
			if($_POST['filename'] == NULL){
				echo "nofilename";
				exit;
			} 
			
			//Procedemos a guardar en BD
			//$titulo = mysqli_real_escape_string($conn, $_POST['titulo']);
			$titulo = mysqli_real_escape_string($conn,utf8_decode($_POST['titulo']));
			$subtitulo = mysqli_real_escape_string($conn,utf8_decode($_POST['subtitulo']));
			$contenido = mysqli_real_escape_string($conn, utf8_decode($_POST['contenido']));
			$img = $_SESSION['file'];
			if($_POST['idcupon'] != NULL) {
				$sql = 	"UPDATE cupones 
							SET titulo='$titulo', 
							subtitulo='$subtitulo', 
							contenido='$contenido',
							img='$img' 
							WHERE id='$_POST[idcupon]'";
				if(mysqli_query($conn, $sql)) {
					echo $_POST['idcupon'];
				}else {
					error_log("Error: " . $sql . "..." . mysqli_error($conn));
					echo "Error en sql de BD";
				}
			}else{
				$sql = "INSERT INTO cupones (iduser,titulo,subtitulo,contenido,img) 
							VALUES ('$_SESSION[id]','$titulo','$subtitulo','$contenido','$img')";
				if(mysqli_query($conn, $sql)) {
					echo mysqli_insert_id($conn); 
				}else {
					error_log("Error: " . $sql . "..." . mysqli_error($conn));
					echo "Error en sql de BD";
				}
			}
			break;

		case "frmrecomendacion":
			$sql = "UPDATE item_rating SET visible='$_POST[visible]' WHERE ratingId='$_POST[itemId]'";
			if(mysqli_query($conn, $sql)) echo "Actualizado"; else error_log("Error: " . $sql . "..." . mysqli_error($conn));
			break;

      case "frmfactura":
			$nombres = mysqli_real_escape_string($conn, utf8_decode($_POST['nombres']));
			$dir = mysqli_real_escape_string($conn, utf8_decode($_POST['address']));
         $sql = "REPLACE INTO datafactura (
						iduser,
						nombres,
						identificacion,
						telefono,
						direccion,
						correo )
					VALUE (
						'$_SESSION[id]',
						'$nombres',
						'$_POST[doc_nr]',
						'$_POST[phone]',
						'$dir',
						'$_POST[email]')";	
         if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
         echo "updated";
         break;

		case "frmpayoptions":
			$cart = $_SESSION['cart'];
			switch($_POST['option_select']){
				case "0":
					$cart['installment'] = 0;
					break;
				case "2":
					$cart['installment'] = 2;
					break;
				case "3":
					$cart['installment'] = 3;
					break;
				case "4":
					$cart['installment'] = 9999;
					$cart['total_depo'] = round($cart['order_amount']*12*0.9,2);
					break;
			}
			$_SESSION['cart'] = $cart;
			echo $cart['installment'];	
			break;

		case "frmtiporecu":
			$cart = $_SESSION['cart'];
			if($_POST['option_select'] == 1) $cart['recu'] = 1;
			if($_POST['option_select'] == 2) $cart['recu'] = 2;
			$_SESSION['cart'] = $cart;
			echo $cart['recu'];
			break;

		case "frmperiodorecu":
			$cart = $_SESSION['cart'];
			$cart['periodo'] = $_POST['option_select'];
			$_SESSION['cart'] = $cart;
			echo $cart['periodo'];
			break;

		case "frmdepositobanco":

			//Verificamos que no exista el numero de deposito
			$sql="SELECT df
					FROM transacciones
					WHERE df='$_POST[deposito]'";
         if($result = mysqli_query($conn, $sql)){
            if(mysqli_num_rows($result) > 0) {
					echo "existe";
					exit;
				}
			}else error_log("Error: " . $sql . "..." . mysqli_error($conn));			

			//Buscamos el costo del plan
			$sql = "SELECT idplan,costo
						FROM planes
						WHERE idplan='$_POST[plan]'";
			if($result = mysqli_query($conn, $sql)){
				$row = mysqli_fetch_assoc($result);
				$amount = $row['costo'];
			}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

			//Calculamo el total a pagar
			if($_POST['periodo']	== '12'){
				$total = round($amount*12*.9,2);		
			}else{
				$total = round($amount*$_POST['periodo'],2);
			}

			//Insertamos en transacciones
			$sql = "INSERT INTO transacciones 
						SET 	iduser='$_POST[iduser]',
								df='$_POST[deposito]',
								concepto='$_POST[plan]',
								monto='$total',
								estatus='1',
								fechavence=DATE_ADD(NOW(), INTERVAL $_POST[periodo] MONTH),
								recu='0'";
			if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

			//Actualizamos el plan del cliente
			$sql = "UPDATE users SET plan='$_POST[plan]' WHERE id='$_POST[iduser]'";
			if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));	
		
			echo "success"; 
			break;

      case "frmnuevoplan":
         $plan = mysqli_real_escape_string($conn, utf8_decode($_POST['plan']));
         $sql = "REPLACE INTO planes (
                  idplan,
                  plan,
                  costo )
               VALUE (
                  '$_POST[idplan]',
                  '$plan',
                  '$_POST[costo]')";
         if(!mysqli_query($conn, $sql)) {
				error_log("Error: " . $sql . "..." . mysqli_error($conn));
				$msj = "error";
			}else $msj = "success";
			echo $msj;
         break;

	}
?>

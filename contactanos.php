<?php session_start(); ?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Plataforma Digital de Abogados del Ecuador | Contactanos</title>
		<meta name="keywords" content="abogado, ecuador, abogados, quito, tena, puyo, santo domingo, nueva loja">	
		<meta name="description" content="Ponte en contacto con nuestro equipo de soporte" />
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <link href="gp/css/animate.min.css" rel="stylesheet">
      <link href="gp/css/prettyPhoto.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="gp/css/responsive.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="bower_components/datatables.net-dt/css/jquery.dataTables.css">
      <link rel="stylesheet" type="text/css" href="css/bootstrap-markdown.min.css">
      <!-- JQuery Validator and form -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.js"></script>


      <style>
         .pricingTable{
         text-align: center;
         transition: all 0.5s ease 0s;
         }
         .pricingTable:hover{
         box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
         }
         .pricingTable .pricingTable-header{
         color: #feffff;
         }
         .pricingTable .heading{
         display: block;
         padding-top: 25px;
         }
         .pricingTable .heading > h3{
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .textotitle{
         color: #feffff;
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .pricingTable .subtitle{
         display: block;
         font-size: 13px;
         margin-top: 5px;
         text-transform: capitalize;
         }
         .pricingTable .price-value{
         display: block;
         font-size: 60px;
         font-weight: 700;
         padding-bottom: 25px;
         }
         .pricingTable .price-value span{
         display: block;
         font-size: 14px;
         line-height: 20px;
         text-transform: uppercase;
         }
         .pricingTable .pricingContent{
         /*text-transform: capitalize;*/
         background: #fbfbfb;
         color: #fefeff;
         }
         .pricingTable .pricingContent ul{
         list-style: none;
         padding: 15px 14px 10px;
         margin: 0;
         text-align: left;
         }
         .pricingTable .pricingContent ul li{
         font-size: 14px;
         padding: 12px 0;
         border-bottom: 1px dashed #e1e1e1;
         color: #9da1ad;
         }
         .pricingTable .pricingContent ul li i{
         font-size: 14px;
         float: right;
         }
         .pricingTable .pricingTable-sign-up{
         padding: 20px 0;
         background: #fbfbfb;
         color: #fff;
         text-transform: capitalize;
         }
         .pricingTable .btn-block{
         width: 60%;
         margin: 0 auto;
         font-size: 17px;
         color: #fff;
         text-transform: capitalize;
         border: none;
         border-radius: 5px;
         padding: 10px;
         transition: all 0.5s ease 0s;
         }
         .pricingTable .btn-block:before{
         content: "\f007";
         font-family: 'FontAwesome';
         margin-right: 10px;
         }
         .pricingTable.blue .pricingTable-header,
         .pricingTable.blue .btn-block{
         background: #fce400;
         }
         .pricingTable.pink .pricingTable-header,
         .pricingTable.pink .btn-block{
         background: #c9b600;
         }
         .pricingTable.orange .pricingTable-header,
         .pricingTable.orange .btn-block{
         background: #e0d83a;
         }
         .pricingTable.green .pricingTable-header,
         .pricingTable.green .btn-block{
         background: #e0cd00;
         }
         .pricingTable.blue .btn-block:hover,
         .pricingTable.pink .btn-block:hover,
         .pricingTable.orange .btn-block:hover,
         .pricingTable.green .btn-block:hover{
         background: #e6e6e6;
         color: #939393;
         }
         @media screen and (max-width: 990px){
         .pricingTable{ margin-bottom: 20px; }
         }
         .error{
            color: red;
         }
         /* Borde rojo y grosor de linea de los inputs */
         .frmcontacto input[type=email].error,
         .frmcontacto input[type=number].error,
         .frmcontacto input[type=text].error{
            padding:15px 18px;
            border:1px solid #FF0000;
         }
         textarea.error {
            border:1px solid #FF0000;
         }
         .titulos {
             padding-bottom: 55px;
             margin-bottom: 0px;
             margin-top: 0px;
         }
      </style>

		<!-- WhatsHelp.io widget -->
		<script type="text/javascript">
			 (function () {
				  var options = {
						whatsapp: "+593(99)738-3040", // WhatsApp number
						call_to_action: "Soporte Chat", // Call to action
						position: "right", // Position may be 'right' or 'left'
				  };
				  var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
				  var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
				  s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
				  var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
			 })();
		</script>

   </head>
   <body class="homepage">
<?php include "header.php"; ?>
      <!--/header-->
      <div class="map">
         <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2525.056897430557!2d-78.48114177167457!3d-0.180519140260444!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMMKwMTAnNDguOCJTIDc4wrAyOCc0NS4xIlc!5e0!3m2!1ses!2sec!4v1489091634720" allowfullscreen="" height="350" style="border:0" width="550" frameborder="0"></iframe>
      </div>
      <section id="contact-page">
         <div class="container">
            <div class="center">
               <h2>Contáctanos</h2>
               <p class="lead">No dudes en contactarte con nosotros.  </p>
            </div>
            <div class="row contact-wrap jumbotron">
               <div class="status alert alert-success" style="display: none"></div>
               <form id="frmcontacto" class="frmcontacto" name="frmcontacto" method="post" action="contacto.php">
                  <input type="hidden" name='formname' value="frmcotiza">
                  <div class="col-sm-3">
                     <div class="form-group">
                        <label>Nombres y apellidos *</label>
                        <input type="text" name="name" class="form-control" required="required">
                     </div>
                     <div class="form-group">
                        <label>Correo electrónico *</label>
                        <input type="email" name="email" class="form-control" required="required">
                     </div>
                     <div class="form-group">
                        <label>Teléfono *</label>
                        <input type="number" name="number" class="form-control" required="required">
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="form-group">
                        <label>Asunto *</label>
                        <input type="text" name="subject" class="form-control" required="required">
                     </div>
                     <div class="form-group">
                        <label>Mensaje *</label>
                        <textarea name="message" id="message" required="required" class="form-control" rows="8"></textarea>
                     </div>
                     <div class="form-group">
                        <button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Enviar mensaje</button>
								<img id="enviando" src="images/barra.gif" width="100px" height="25px"
									style="position: relative; vertical-align:middle; display: none;">
                     </div>
                  </div>
                  <div class="col-sm-5">
                     <div class="contact-wrap jumbotron">
                        <ul>
                           <li style="text-decoration:none; color: #eee"><img src="img/logo-tuabogadoec-new.png" width="165px"></li>
                           <li>Teléfono: 0997383040</li>
                           <li>Dirección oficina: Pasaje El Jardín No. 168 y Av. 6 de Diciembre, 
											Edificio Century Plaza I. Quito – Ecuador.</li>
                           <li>e-mail: info@tuabogado.ec</li>
                           <li>web: www.tuabogado.ec</li>
                        </ul>
                     </div>
                  </div>
               </form>
            </div>
            <!--/.row-->
				<!-- Zona de mensajes -->
				<div id="mensajes"></div>
				<!-- FIN Zona de mensajes -->
         </div>
         <!--/.container-->
      </section>
      <!--/#contact-page-->
      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="gp/js/bootstrap.min.js"></script>
      <script src="js/markdown.js"></script>
      <script src="js/to-markdown.js"></script>
      <script src="js/bootstrap-markdown.js"></script>
      <script src="js/bootstrap-markdown.es.js"></script>
      <script type="text/javascript" src="js/jquery.simpleslider.js"></script>
      <script type="text/javascript" charset="utf8" src="bower_components/datatables.net/js/jquery.dataTables.min.js"> </script>
      <script src="gp/js/jquery.prettyPhoto.js"></script>
      <script src="gp/js/jquery.isotope.min.js"></script>
      <script src="gp/js/wow.min.js"></script>
      <script src="gp/js/main.js"></script>
      <script src="js/custom.js"></script>

   </body>
</html>



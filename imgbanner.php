<?php
	session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");
   $info = array();
	$info['id'] = $_SESSION['id'];
	$idbanner = $_POST['idbanner'];
	//Conexion a BD
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );

	$arr_file_types = ['image/png', 'image/gif', 'image/jpg', 'image/jpeg'];

	if (!(in_array($_FILES['file']['type'], $arr_file_types))) {
		 echo "false";
		 return;
	}

	if (!file_exists('uploads')) {
		 mkdir('uploads', 0777);
	}

	$file = time() . $_FILES['file']['name'];
	move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/' . $file);

	//Actualizamos el Banner
	$info['file'] = $file;
	$file = htmlentities($file);
	if($idbanner == '0'){
		$sql = "INSERT INTO banners (iduser, banner) VALUES ('$_SESSION[id]', '$file')";
	}else{
		$sql = "UPDATE banners SET banner='$file' WHERE idbanner='$idbanner'";
	}
	if(mysqli_query($conn, $sql)) {
		$info['file'] = $file;
	}else{
		error_log("Error: " . $sql . "..." . mysqli_error($conn));
		$info['error'] =  "Error en sql de BD";
	}

	//Respuesta
	if (isset($_GET['callback'])) {
		 echo $_GET['callback'] . '( ' . json_encode($info) . ' )';
	}else {
		 echo 'callbackEjercicio( ' . json_encode($info) . ' )';
	}

?>


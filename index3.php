<?php 
   session_start();
   session_unset();
?>
<!DOCTYPE HTML>

<html lang="en-gb" class="no-js">
<head>
  <meta http-equiv="content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <title>Polilegal| Inicio</title>
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
  <link id="default-css" href="style.css" rel="stylesheet" type="text/css">
  <link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/flip.css" rel="stylesheet" type="text/css">
  <link rel='stylesheet' id='layerslider-css' href="css/layerslider.css" type='text/css' media='all' />
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" />
<!--[if IE 7]>
  <!--Fonts-->
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <!--jquery-->
  <script src="js/modernizr-2.6.2.min.js"></script>
</head>
<body>
  <!--wrapper starts-->
  <div class="wrapper">
    <!--inner-wrapper starts-->
    <div class="inner-wrapper">
      <!--header starts-->
      <header>
        <!--top-bar starts-->
        <div class="top-bar">
          <div class="container">
            <ul class="dt-sc-social-icons">
              <li><a href="https://www.facebook.com/polilegal" title="Facebook"><span class="fa fa-facebook"></span></a></li>
              <li><a href="https://www.linkedin.com/company/27118661/" title="Linkedin"><span class="fa fa-linkedin"></span></a></li>
            </ul>
            <div class="dt-sc-contact-number"> <span class="fa fa-phone"> </span> Contact Center: (02)3 825530<!--br><span class="telefonoLogo" style="margin-left: 65%;">(765453)</span--> </div>
          </div>
        </div>
        <!--top-bar ends--> 

        <!--menu-container starts-->
        <div id="menu-container">
          <div id="logo" style="width: 200px; height: 70px; position: absolute; left: 73%;">
            <img src="images/logoblancotransp.png" width="auto" height="60px" style="margin-top: 5px;"/>
          </div>
          <div class="container"> 
            <!--nav starts-->
            <nav id="main-menu">
              <div class="dt-menu-toggle" id="dt-menu-toggle">Menu<span class="dt-menu-toggle-icon"></span></div>
              <ul id="menu-main-menu" class="menu">
                <li class="current_page_item"> <a href="index.php"> Inicio </a> </li>
                <li> <a href="nosotros.php">Nosotros </a> </li>
                <li class="menu-item-simple-parent menu-item-depth-0">
                  <a href="servicios01.php"> servicios </a>
                  <ul class="sub-menu">
                    <li> <a href="servicios01.php"> Nuestros servicios </a> </li>
                    <li> <a href="plan.php"> Plan de Asistencia Jurídica </a> </li>
                    <a class="dt-menu-expand">+</a>
                  </ul>
                </li>
                <li><a href="actualidad.php" title="">Actualidad</a></li>
                <li class="menu-item-simple-parent menu-item-depth-0">
                  <a href="plan.php"> Asistencias </a>
                  <ul class="sub-menu">
		    <li class="menu-item-simple-parent menu-item-depth-0">
                      <a href="plan.php"> Asistencia jurídica socios CPN </a> 
		      <ul class="sub-menu"> 
                          <li> <a href="plan1.php"> Plan de Asistencia Jurídica para socios policías de la CPN </a> </li>
                          <li> <a href="plan2.php"> Plan de Asistencia Jurídica para socios SP y civiles de la CPN </a> </li>
		          <a class="dt-menu-expand">+</a>
		      </ul>
		    </li>
                    <a class="dt-menu-expand">+</a>
                  </ul>
                </li>
                <li> <a href="buzon.php"> Buzón </a> </li>
                <li> <a href="contacto.php">Contacto</a></li>
              </nav>
            </header>
            <!--header ends--> 
            <!--main starts-->
            <div id="main"> 
              
              <!--slider starts-->
              <div id="slider">
                <div id="layerslider_2" class="ls-wp-container" style="width:100%;height:660px;margin:0 auto;margin-bottom: 0px;">
                  <div class="ls-slide" data-ls=" transition2d: all;"> <img src="images/encabezado04.jpg" class="ls-bg" alt="Slide background" />
                    <h2 class="ls-l caption-1" style="top:385px;left:7px; font-size:20px; padding:15px 30px 15px 25px;white-space: nowrap;" data-ls="offsetxin:-80;delayin:1000;skewxin:-80;"> CONOCIMIENTO, EXPERIENCIA, SERIEDAD, CONFIANZA. </h2>
                    <p class="ls-l caption-1" style="top:452px;left:7px; font-weight:300; font-size:14px; padding:15px 50px 15px 25px; line-height:27px;white-space: nowrap;" data-ls="delayin:2000;skewxin:80;"> CREEMOS EN LAS COSAS BIEN HECHAS. </p>
                    </div>
                    
                    <div class="ls-slide" data-ls="slidedelay:5500; transition2d: all;"> <img src="images/encabezado02.jpg" class="ls-bg" alt="Slide background" /> <img class="ls-l" style="top:22px;left:4px;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:100;durationin:1800;delayin:500;scalexin:0.8;scaleyin:0.8;" src="images/logotransp.png" alt="" width="550px" height="auto">
                      <h3 class="ls-l" style="top:162px;left:560px;font-family:'Domine', serif; font-weight:bold; font-size:26px; color:#ffffff;white-space: nowrap;" data-ls="delayin:1500;skewxin:80;"> Asesoramiento y Asistencia PoliLegal Abogados S.A. </h3>
                      <h2 class="ls-l" style="top:198px;left:560px;font-family:'Domine', serif; font-weight:bold; font-size:39px; color:#ffffff; text-transform:uppercase;white-space: nowrap;" data-ls="offsetxin:-80;delayin:1500;skewxin:-80;"> PoliLegal </h2>
                      <p class="ls-l" style="top:270px;left:560px;font-family:'Domine', serif; font-size:16px; color:#ffffff; line-height:27px;white-space: nowrap;" data-ls="offsetxin:0;delayin:2000;scalexin:0.8;scaleyin:0.8;"></p>
                      <div class="ls-l caption-list" style="top:350px;left:560px;font-family:'Domine', serif; font-size:16px; font-weight:normal; padding:10px 0px 10px 20px;white-space: nowrap;" data-ls="delayin:3000;rotateyin:180;transformoriginin:Left 50% 0;"><i class="fa fa-picture-o" style="margin-right:15px;"> </i> Oficina central en Quito </div>
                      <div class="ls-l caption-list" style="top:410px;left:560px;font-family:'Domine', serif; font-size:16px; font-weight:normal; padding:10px 0px 10px 20px;white-space: nowrap;" data-ls="delayin:3500;rotateyin:180;transformoriginin:Left 50% 0;"><i class="fa fa-user" style="margin-right:15px;"> </i> Profesionales altamente capacitados </div>
                      <div class="ls-l caption-list" style="top:470px;left:560px;font-family:'Domine', serif; font-size:16px; font-weight:normal; padding:10px 0px 10px 20px;white-space: nowrap;" data-ls="delayin:4000;rotateyin:180;transformoriginin:Left 50% 0;"><i class="fa fa-briefcase" style="margin-right:15px;"> </i> Cobertura a nivel nacional </div>
                      <div class="ls-l caption-list" style="top:530px;left:560px;font-family:'Domine', serif; font-size:16px; font-weight:normal; padding:10px 0px 10px 20px;white-space: nowrap;" data-ls="delayin:4500;rotateyin:180;transformoriginin:Left 50% 0;"><i class="fa fa-align-justify" style="margin-right:15px;"> </i> Brindamos las mejores soluciones jurídicas </div>
                    </div>
                    
                    <div class="ls-slide" data-ls="slidedelay:5000; transition2d: all;">
                     <img src="images/encabezado511.png" class="ls-bg" alt="Slide background" />
                     <h3 class="ls-l caption-2" style="top:240px;left:270px;font-family:'Domine', serif; font-size:30px; white-space: nowrap; text-transform: none;
                     font-weight: 300;" data-ls="delayin:1500;rotateyin:90;skewxin:-60;"> <P ALIGN=center style="margin-top: -130px"><span style="color:#1d3e88"> <b>Conoce el Plan de Asistencia Jurídica CPN-POLILEGAL <br> El servicio que estabas esperando </b></span> </p> </h3>
                     <a href="plan.php" class="ls-l dt-sc-bordered-button" style='top:190px;left:560px;font-family:"Source Sans Pro",sans-serif; font-size:15px; padding:10px 28px; border:1px solid #ffffff; color:#ffffff; display:inline-block; font-weight:600; padding:12px 26px; text-transform:uppercase; white-space: nowrap;' data-ls="delayin:2000;rotateyin:90;skewxin:60;"> Afiliarme </a>
                   </div>
                   
                 </div>
               </div>
               <!--slider ends--> 
               
               <!--primary starts-->
               <section id="primary" class="content-full-width">
                <!--container starts-->
                <div class="container">
                  <h2 class="dt-sc-hr-title"> Servicios </h2>

                  <div class="dt-sc-one-fifth column first">
                    <div class="flip">
                      <div class="front">
                        <div class="dt-sc-ico-content type1">
                          <div class="icon"> <img src="images/practice_icon5.png" alt="" title="" width="50px" height="50px"> </div>
                          <h5><a href="servicios01.php"> ADMINISTRATIVO </a></h5>
                        </div>
                      </div>
                      <div class="back" style="background-color: #1d3e88">
                        <p style="text-align: center; padding-top: 30px">Conforme a la regulación dispuesta en el Código Orgánico General de Procesos...</p>
                        <a href="servicios01.php" class="ls-l dt-sc-bordered-button" style='top:320px;left:560px;font-family:"Source Sans Pro",sans-serif; font-size:12px; padding:10px 28px; border:1px solid #ffffff; color:#ffffff; display:inline-block; font-weight:600; padding:12px 26px; text-transform:uppercase; white-space: nowrap;' data-ls="delayin:4000;rotateyin:90;skewxin:60;"> Leer M&aacute;s </a>
                      </div>
                    </div>
                    <br>
                    <div class="flip">
                      <div class="front">
                        <div class="dt-sc-ico-content type1">
                          <div class="icon"> <img src="images/practice_icon6.png" alt="" title="" width="50px" height="50px"> </div>
                          <h5><a href="servicios01.php"> CIVIL Y FAMILIA </a></h5>
                        </div>
                      </div>
                      <div class="back" style="background-color: #1d3e88">
                        <p style="text-align: center; padding-top: 30px">Asesoramos y patrocinamos en cuestiones civiles relacionadas a bienes muebles e inmuebles...</p>
                        <a href="servicios01.php" class="ls-l dt-sc-bordered-button" style='top:320px;left:560px;font-family:"Source Sans Pro",sans-serif; font-size:12px; padding:10px 28px; border:1px solid #ffffff; color:#ffffff; display:inline-block; font-weight:600; padding:12px 26px; text-transform:uppercase; white-space: nowrap;' data-ls="delayin:4000;rotateyin:90;skewxin:60;"> Leer M&aacute;s </a>
                      </div>
                    </div>

                  </div>
                  
                  <div class="dt-sc-one-fifth column">
                    <div class="flip">
                      <div class="front">
                        <div class="dt-sc-ico-content type1">
                          <div class="icon"> <img src="images/practice_icon1.png" alt="" title="" width="50px" height="50px"> </div>
                          <h5><a href="servicios01.php"> ADMINISTRATIVO POLICIAL </a></h5>
                        </div>
                      </div>
                      <div class="back" style="background-color: #1d3e88">
                        <p style="text-align: center; padding-top: 30px">Nos encargamos del asesoramiento y defensa...</p>
                        <a href="servicios01.php" class="ls-l dt-sc-bordered-button" style='top:320px;left:560px;font-family:"Source Sans Pro",sans-serif; font-size:12px; padding:10px 28px; border:1px solid #ffffff; color:#ffffff; display:inline-block; font-weight:600; padding:12px 26px; text-transform:uppercase; white-space: nowrap;' data-ls="delayin:4000;rotateyin:90;skewxin:60;"> Leer M&aacute;s </a>
                      </div>
                    </div>
                    <br>
                    <div class="flip">
                      <div class="front">
                        <div class="dt-sc-ico-content type1">
                          <div class="icon"> <img src="images/practice_icon2.png" alt="" title="" width="50px" height="50px"> </div>
                          <h5><a href="servicios01.php"> PENAL Y TRÁNSITO </a></h5>
                        </div>
                      </div>
                      <div class="back" style="background-color: #1d3e88">
                        <p style="text-align: center; padding-top: 30px">PoliLegal ofrece sus servicios de asesoría y...</p>
                        <a href="servicios01.php" class="ls-l dt-sc-bordered-button" style='top:320px;left:560px;font-family:"Source Sans Pro",sans-serif; font-size:12px; padding:10px 28px; border:1px solid #ffffff; color:#ffffff; display:inline-block; font-weight:600; padding:12px 26px; text-transform:uppercase; white-space: nowrap;' data-ls="delayin:4000;rotateyin:90;skewxin:60;"> Leer M&aacute;s </a>
                      </div>
                    </div>
                  </div>
                  
                  <div class="dt-sc-one-fifth column">
                    <div class="flip">
                      <div class="front">
                        <div class="dt-sc-ico-content type1">
                          <div class="icon"> <img src="images/practice_icon3.png" alt="" title="" width="50px" height="50px"> </div>
                          <h5><a href="servicios01.php"> CONSTITUCIONAL </a></h5>
                        </div>
                      </div>
                      <div class="back" style="background-color: #1d3e88">
                        <p style="text-align: center; padding-top: 30px">En virtud de las acciones reguladas en la Ley Orgánica de Garantías Jurisdiccionales...</p>
                        <a href="servicios01.php" class="ls-l dt-sc-bordered-button" style='top:320px;left:560px;font-family:"Source Sans Pro",sans-serif; font-size:12px; padding:10px 28px; border:1px solid #ffffff; color:#ffffff; display:inline-block; font-weight:600; padding:12px 26px; text-transform:uppercase; white-space: nowrap;' data-ls="delayin:4000;rotateyin:90;skewxin:60;"> Leer M&aacute;s </a>
                      </div>
                    </div>
                    <br>
                    <div class="flip">
                      <div class="front">
                        <div class="dt-sc-ico-content type1">
                          <div class="icon"> <img src="images/practice_icon7.png" alt="" title="" width="50px" height="50px"> </div>
                          <h5><a href="servicios01.php"> SOCIETARIO Y CORPORATIVO </a></h5>
                        </div>
                      </div>
                      <div class="back" style="background-color: #1d3e88">
                        <p style="text-align: center; padding-top: 30px">Nos encargamos de brindar asesoría a emprendedores...</p>
                        <a href="servicios01.php" class="ls-l dt-sc-bordered-button" style='top:320px;left:560px;font-family:"Source Sans Pro",sans-serif; font-size:12px; padding:10px 28px; border:1px solid #ffffff; color:#ffffff; display:inline-block; font-weight:600; padding:12px 26px; text-transform:uppercase; white-space: nowrap;' data-ls="delayin:4000;rotateyin:90;skewxin:60;"> Leer M&aacute;s </a>
                      </div>
                    </div>
                  </div>
                  
                  <div class="dt-sc-one-fifth column">
                    <div class="flip">
                      <div class="front">
                        <div class="dt-sc-ico-content type1">
                          <div class="icon"> <img src="images/jury.png" alt="" title="" width="50px" height="50px"> </div>
                          <h5><a href="servicios01.php"> ARBITRAJE Y MEDIACIÓN </a></h5>
                        </div>
                      </div>
                      <div class="back" style="background-color: #1d3e88">
                        <p style="text-align: center; padding-top: 30px">En virtud de las acciones reguladas en la Ley Orgánica de Garantías Jurisdiccionales...</p>
                        <a href="servicios01.php" class="ls-l dt-sc-bordered-button" style='top:320px;left:560px;font-family:"Source Sans Pro",sans-serif; font-size:12px; padding:10px 28px; border:1px solid #ffffff; color:#ffffff; display:inline-block; font-weight:600; padding:12px 26px; text-transform:uppercase; white-space: nowrap;' data-ls="delayin:4000;rotateyin:90;skewxin:60;"> Leer M&aacute;s </a>
                      </div>
                    </div>
                    <br>
                    <div class="flip">
                      <div class="front">
                        <div class="dt-sc-ico-content type1">
                          <div class="icon"> <img src="images/agreement.png" alt="" title="" width="50px" height="50px"> </div>
                          <h5><a href="servicios01.php"> ASUNTOS REGULATORIOS </a></h5>
                        </div>
                      </div>
                      <div class="back" style="background-color: #1d3e88">
                        <p style="text-align: center; padding-top: 30px">Nos encargamos de brindar asesoría a emprendedores...</p>
                        <a href="servicios01.php" class="ls-l dt-sc-bordered-button" style='top:320px;left:560px;font-family:"Source Sans Pro",sans-serif; font-size:12px; padding:10px 28px; border:1px solid #ffffff; color:#ffffff; display:inline-block; font-weight:600; padding:12px 26px; text-transform:uppercase; white-space: nowrap;' data-ls="delayin:4000;rotateyin:90;skewxin:60;"> Leer M&aacute;s </a>
                      </div>
                    </div>
                  </div>

                  <div class="dt-sc-one-fifth column">
                    <div class="dt-sc-ico-content type1" style="background-color: #f27705; height:475px">
                      <div class="icon"> <img src="images/practice_icon4.png" alt="" title=""> </div>
                      <h5><a href="plan1.php"> PLAN DE ASISTENCIA JURÍDICA COOPERATIVA POLICÍA NACIONAL-POLILEGAL S.A. </a></h5>
                      <p>A través de la alianza estratégica entre la Cooperativa Policía Nacional y Polilegal Abogados, te damos la bienvenida a nuestro Plan de Asistencia Jurídica para socios policías y ...</p>
                    </div>
                  </div>
                  
                  
                  <div class="dt-sc-hr-invisible-large"></div>
                  <h4 class="aligncenter no-transform"> Somos plenamente conscientes de que cada cliente tiene necesidades propias, por lo que ofrecemos <br>
                    soluciones prácticas y viables con un trato personalizado. </h4>
                    <div class="dt-sc-hr-invisible-very-small"></div>
                    <p class="aligncenter" style="text-align: justify;"> La creación de PoliLegal se vio impulsada debido a la importancia de ofrecer servicios jurídicos de calidad para cualquier tipo de persona natural y/o jurídica; con el plus que nos caracteriza, de ofrecer asistencia legal a los miembros de la Policía Nacional del Ecuador. </p>
                    
                    <div class="dt-sc-hr-invisible-medium"></div>
                    <div class="dt-sc-hr-image"></div>
                    <div class="dt-sc-hr-invisible"></div>
                    <div class="column dt-sc-one-fifth first">
                      <div class="dt-sc-ico-content type2">
                        <a> <img src="images1/mapa.png"/></a>
                        <h6><a href="#" target="_blank"> Cobertura nacional </a></h6>
                      </div>
                    </div>
                    <div class="column dt-sc-one-fifth">
                      <div class="dt-sc-ico-content type2">
                        <a> <img src="images1/persona.png"/></a>
                        <h6><a href="#" target="_blank"> Atención personalizada </a></h6>
                      </div>
                    </div>
                    <div class="column dt-sc-one-fifth">
                      <div class="dt-sc-ico-content type2">
                        <a> <img src="images1/estrella.png"/></a>
                        <h6><a href="#" target="_blank"> Ámbito policial </a></h6>
                      </div>
                    </div>
                    <div class="column dt-sc-one-fifth">
                      <div class="dt-sc-ico-content type2">
                        <a> <img src="images1/foco.png"/></a>
                        <h6><a href="#" target="_blank"> Soluciones óptimas </a></h6>
                      </div>
                    </div>
                    <div class="column dt-sc-one-fifth">
                      <div class="dt-sc-ico-content type2">
                        <a> <img src="images1/libros.png"/></a>
                        <h6><a href="#" target="_blank"> Experiencia y especialización </a></h6>
                      </div>
                    </div>
                    <div class="dt-sc-hr-invisible"></div>
                    <!--dt-sc-one-half starts-->
                    <div class="column dt-sc-one-half first">
                      <h2 class="dt-sc-hr-icon-title"> <span class="fa fa-question-circle"></span> Preguntas frecuentes </h2>
                      <!--dt-sc-toggle-frame-set starts-->
                      <div class="dt-sc-toggle-frame-set faq">
                        <h5 class="dt-sc-toggle-accordion"><a href="#">¿A quiénes van dirigidos los servicios de PoliLegal? </a></h5>
                        <div class="dt-sc-toggle-content">
                          <div class="dt-sc-block" style="text-align: justify"> PoliLegal S.A. brinda sus servicios legales a personas naturales y jurídicas; y, gracias a la especialización y profesionalismo de nuestros Abogados, brindamos servicios jurídicos de calidad a los miembros de la institución policial.  </div>
                        </div>
                        <h5 class="dt-sc-toggle-accordion"><a href="#"> ¿Por qué contratar los servicios de PoliLegal?</a></h5>
                        <div class="dt-sc-toggle-content">
                          <div class="dt-sc-block" style="text-align: justify"> Porque contamos con Abogados especializados en cada rama, cobertura a nivel nacional, y porque somos el primer Estudio Jurídico que ha incluido el área administrativa policial dentro de su oferta de servicios legales.  </div>
                        </div>
                        <h5 class="dt-sc-toggle-accordion"><a href="#"> ¿Cómo puedo mantenerme informado de mi caso? </a></h5>
                        <div class="dt-sc-toggle-content">
                          <div class="dt-sc-block" style="text-align: justify"> La transparencia y comunicación son valores que nos caracterizan, y son fundamentales para el éxito de la relación entre Cliente y Abogado.  En PoliLegal, usted podrá acceder a una comunicación directa con nuestros Abogados, quienes le guiarán y le mantendrán informado de cada uno de los avances de su caso, creando un vínculo más cercano de información.   </div>
                        </div>
                        <h5 class="dt-sc-toggle-accordion"><a href="#">¿Cuál será el valor de los honorarios profesionales? </a></h5>
                        <div class="dt-sc-toggle-content">
                          <div class="dt-sc-block" style="text-align: justify"> Con PoliLegal Abogados usted recibirá una atención personalizada y una asesoría de calidad a precios justos y razonables.  Los honorarios profesionales son pactados de forma contractual con cada cliente y de acuerdo a cada caso.  Si usted adquiere el Plan de Asistencia Jurídica, únicamente debe abonar el valor de 6.99 USD (mensual) conforme la cobertura descrita en el mismo.   </div>
                        </div>
                        
                      </div>
                      <!--dt-sc-toggle-frame-set ends--> 
                    </div>
                    <!--dt-sc-one-half ends--> 
                    
                    <!--dt-sc-one-half starts-->
                    <div class="column dt-sc-one-half">
                      <h2 class="dt-sc-hr-icon-title"> <span class="fa fa-file-text"></span> Actualidad </h2>
                      <div class="aligncenter"> <img src="images/blog02.png" alt="" title="">
                        <h4 class="dt-sc-simple-hr-title">Las faltas disciplinarias leves en el Código Orgánico de Entidades de Seguridad Ciudadana y Orden Público</h4>
                        <p style="text-align: justify"> El Código Orgánico de Entidades de Seguridad Ciudadana y Orden Público (COESCOP), el cual cumplió medio año de vigencia el pasado 19 de junio de 2018, regula varios aspectos fundamentales de las entidades de seguridad ecuatorianas, entre ellas... </p>
                          <div class="dt-sc-hr-invisible-small"></div>
                          <a href="actualidad.php" class="dt-sc-simple-border-button"> Leer más </a> </div>
                        </div>
                        <div class="dt-sc-hr-invisible-large"></div>
                        
                        <!--container ends-->
                      </section>
                      <!--primary ends--> 
                      
                    </div>
                    <!--main ends--> 
                    
                    <!--footer starts-->
                    <footer> 
                      <!--footer-widgets-wrapper starts-->
                      <div class="footer-widgets-wrapper"> 
                        <!--container starts-->
                        <div class="container">
                          <div class="footer_top"> </div>
                          <div class="column dt-sc-one-sixth first">
                            <aside class="widget widget_text">
                              <div class="widget_text_logo"> <img src="#" alt="" title="">
                                
                                <div class="dt-sc-hr-invisible-very-small"></div>
                                <p> <img src="images/logoblancotransp.png" /><span class="fa fa-angle-double-right"></span> </strong> </a> </div>
                                </aside>
                              </div>
                              <div class="column dt-sc-two-sixth">
                                <aside class="widget widget_text">
                                  <h3 class="widgettitle">Detalles de contacto</h3>
                                  <div class="textwidget">
                                    <p class="dt-sc-contact-info"><span><i class="fa fa-print"></i> Contact Center: </span> (02)3 825530</p>
                                    <p class="dt-sc-contact-info"><span><i class="fa fa-phone"></i> Tel&eacute;fono: </span>  (02) 3333533 </p>
                                    <p class="dt-sc-contact-info"><span><i class="fa fa-envelope"></i> E-mail: </span><a href="mailto:yourname@somemail.com"> info@polilegal.ec </a></p>
                                    <p class="dt-sc-contact-info"><i class="fa fa-location-arrow"></i>Pasaje El Jardín No. 168 y Av. 6 de Diciembre <br>
                                      (frente al Megamaxi) <br>
                                      Edificio Century Plaza I, piso 5, Of. 14. <br>
                                      Quito, Ecuador</p>
                                    </div>
                                  </aside>
											<aside class="widget widget_text">
												<h3 class="widgettitle" style="margin-bottom: 8px;">Facturación Electrónica</h3>
												<ul>
													<li> <a href="login.php"> INGRESO AFILIADOS </a> </li>
													<?php
														if (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] == 1) {	
															echo '<li> <a href="closesession.php" id="close_session"> CERRAR SESION </a> </li>';
														}
													?>
												</ul>
											</aside>
                                </div>
                                <div class="column dt-sc-one-sixth">
                                  <aside class="widget widget_text">
                                    <h3 class="widgettitle"> Servicios </h3>
                                    <ul>
                                      <li> <a href="servicios01.php"> ADMINISTRATIVOS </a> </li>
                                      <li> <a href="servicios01.php"> ADMINISTRATIVOS POLICIALES </a> </li>
                                      <li> <a href="servicios01.php"> CONSTITUCIONAL </a> </li>
                                      <li> <a href="servicios01.php"> CIVIL Y DE FAMILIA </a> </li>
                                      <li> <a href="servicios01.php"> PENAL Y TRÁNSITO </a> </li>
                                      <li> <a href="servicios01.php"> SOCIETARIO Y CORPORATIVO </a> </li>
                                      <li> <a href="servicios01.php"> ARBITRAJE Y MEDIACIÓN </a> </li>
                                      <li> <a href="servicios01.php"> ASUNTOS REGULATORIOS </a> </li>
                                    </ul>
                                  </aside>
                                </div>
                                <div class="column dt-sc-two-sixth">
                                  <aside class="widget widget_text">
                                    <h3 class="widgettitle"> VIDEOS </h3>
				    <iframe width="300" height="200" src="https://www.youtube.com/embed/6IEDnTE9DQw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> 
				 <!--   <video class="motionpicture" width="300" height="200" autobuffer controls preload="auto">
				        <source src="VideoPolilegal.mp4" />
				        Your browser does not appear to support HTML5 media. Try updating
					your browser or (if you are not already) using an open source browser like Firefox.
				    </video>
				-->
                                  </aside>
                                </div>
                              </div>
                              <!--container ends--> 
                            </div>
                            <!--footer-widgets-wrapper ends-->
                            <div class="copyright">
                              <div class="container">
                                <div class="copyright-info">Polilegal © 2017 <a href="http://polilegal.ec./" target="_blank"> www.polilegal.ec </a> Todos los derechos reservados. </div>
                              </ul>
                              <p> Design by <a href="http://themeforest.net/user/buddhathemes" target="_blank" title="">BigDesign</a> </p>
                            </div>
                          </div>
                        </footer>
                        <!--footer ends--> 
                      </div>
                      <!--inner-wrapper ends--> 
                    </div>
                    <!--wrapper ends-->
                    <div class="fixed-help-form">
                      <div class="fixed-help-form-icon"> <img src="images/fixed-help-form-icon.png" alt="" title=""> </div>
                      <h4> Asistencias </h4>
                      <p> Comienza el proceso de afiliaci&oacute;n llenando los siguientes datos. </p>
                      <p> <span> Gracias por confiar en nosotros </span> </p>
                      <form name="helpform" method="post" class="help-form" action="php/helpform.php">
                        <p>
                          <input type="text" placeholder="Nombres" class="text_input" name="hf_first_name" required />
                        </p>
                        <p>
                          <input type="text" placeholder="Apellidos" class="text_input" name="hf_last_name" required />
                        </p>
                        <p>
                          <input type="text" placeholder="Teléfono" class="text_input" name="phone" required />
                        </p>
                        
                        
                        <div id="ajax_helpform_msg"> </div>
                        <p>
                          <p><a href="plan1.php" class="ls-l dt-sc-bordered-button" title="">siguiente</a></p>
                        </p>
                      </form>
                    </div>
                    <a href="" title="Go to Top" class="back-to-top"> <span class="fa fa-angle-up"></span> </a> 
                    <!--Java Scripts--> 

                    <script type="text/javascript" src="js/jquery.js"></script> 
                    <script type="text/javascript" src="js/jquery-migrate.min.js"></script> 
                    <script type="text/javascript" src="js/jquery.validate.min.js"></script> 
                    <script type="text/javascript" src="js/jquery-easing-1.3.js"></script> 
                    <script type="text/javascript" src="js/jquery.sticky.js"></script> 
                    <script type="text/javascript" src="js/jquery.nicescroll.min.js"></script> 
                    <script type="text/javascript" src="js/jquery.smartresize.js"></script> 
                    <script type="text/javascript" src="js/shortcodes.js"></script> 
                    <script type="text/javascript" src="js/custom.js"></script> 

                    <!-- Layer Slider --> 
                    <script type="text/javascript" src="js/jquery-transit-modified.js"></script> 
                    <script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script> 
                    <script type='text/javascript' src="js/greensock.js"></script> 
                    <script type='text/javascript' src="js/layerslider.transitions.js"></script> 
                    <script type="text/javascript">
                      var lsjQuery = jQuery;</script><script type="text/javascript"> lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice('layerslider_2','jquery'); } else { lsjQuery("#layerslider_2").layerSlider({responsiveUnder: 1240, layersContainer: 1170, skinsPath: 'js/layerslider/skins/'}) } }); </script>

                      <!--script flip-->
                      <script type='text/javascript' src="js/flip/jquery.flip.min.js"></script> 
                      <script type='text/javascript' src="js/flip/script.js"></script> 

                     <!-- Cerrar Session -->
                     <script>
                        jQuery(document).ready(function(){
                           jQuery("#close_session").click(function(){
                              alert('Su sesión ha sido cerrada');
                              window.location.href='closesession.php';
                           });
                        });
                     </script>


                    </body>
                    </html>

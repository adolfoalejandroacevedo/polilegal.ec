<?php 
   session_start();
   session_unset();
?>
<!DOCTYPE HTML>
<html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
  <meta http-equiv="content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
  <title>Polilegal |Contacto</title>
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
  <link id="default-css" href="style.css" rel="stylesheet" type="text/css">
  <link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" /> 
<!--[if IE 7]>
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lt IE 9]>
<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--Fonts-->
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--jquery-->
<script src="js/modernizr-2.6.2.min.js"></script>
</head>
<body>
	<!--wrapper starts-->
  <div class="wrapper">
    <!--inner-wrapper starts-->
    <div class="inner-wrapper">
      <!--header starts-->
		<?php include 'header.php'; ?>
           <!--header ends-->
            <!--main starts-->
            <div id="main">

              <div class="breadcrumb-section">
                <div class="container">
                  <h1> Plan de Asistencia Jurídica </h1>
                  <div class="breadcrumb">
                    <a href="index.php"> Inicio </a>
                    <span class="current"> Plan </span>
                  </div>
                </div>
              </div>
              
              <section id="primary" class="content-full-width">
                <!--container starts-->
                <div class="container">
                  <p class="alignleft"><span class="dt-sc-highlight skin-color"> <strong> El Plan de Asistencia Jurídica </strong> </span> consiste en un paquete de servicios jurídicos integrales para los miembros de la Policía Nacional cuya finalidad es salvaguardar los intereses de nuestros beneficiarios brindando asesoría jurídica y patrocinio en el ámbito policial y en otras ramas del derecho, evitando que tengan que hacer frente a los altos costes por respaldo legal con otros Estudios Jurídicos o Abogados. </p>
                </div>
                <!--container ends-->    
                <div class="dt-sc-hr-invisible-medium"></div>
                
                <!--fullwidth-background starts-->
                <div class="fullwidth-background dt-sc-parallax-section trust_section">
                  <div class="container">
                    <h5> Comienza ahora con nosotros </h5>
                  </div>
                </div>
                <!--fullwidth-background ends-->
                
                <!--container starts-->
                <div class="container">
                  <div class="dt-sc-one-fourth column first">
                    <div class="dt-sc-colored-box">
                      <h5> Paso 1 <br> Rellena la informacion<br> solicitada </h5>
                    </div>
                  </div>
                  <div class="dt-sc-one-fourth column">
                    <div class="dt-sc-colored-box">
                      <h5> Paso 2 <br> Selecciona metodo de<br> pago </h5>
                    </div>
                  </div>
                  
                  <div class="dt-sc-one-fourth column">
                    <div class="dt-sc-colored-box">
                      <h5> Paso 3 <br> Acepta los Terminos y Condiciones </h5>
                    </div>
                  </div>
                  <div class="dt-sc-one-fourth column">
                    <div class="dt-sc-colored-box">
                      <h5> Paso 4 <br> Espera nuestro <br> correo de confirmacion </h5>
                    </div>
                  </div>
                  
                  <div class="container">	
                    <form name="helpform" class="help-form" method="post" action="PHPMailer/examples/gmail.php">
                      <input type="hidden" name="tipo" value="2">
                      <!--primary starts-->
                      <section id="primary" class="content-full-width">

                        <h2 class="dt-sc-hr-title">Paso 1 <br> </h2>
                        <p class="column dt-sc-one-third first">
                          <input id="name" name="txtname" type="text" placeholder="Nombres y Apellidos" required="">
                        </p>
                        <p class="column dt-sc-one-third">
                          <input id="email" name="txtemail" type="email" placeholder="E-mail" required="">
                        </p>
                        <p class="column dt-sc-one-third">
                          <input id="subject" name="txtsubject" type="text" placeholder="Grado de institucion" required="">
                        </p>
                        <p class="column dt-sc-one-third">
                            <input id="subject" name="txttelefono" type="text" placeholder="Télefono" required="">
                        </p>
                        <p class="column dt-sc-one-third">
                            <input id="subject" name="txtciudad" type="text" placeholder="Ciudad" required="">
                        </p>
                        <p class="column dt-sc-one-third">
                            <input id="subject" name="txtprovincia" type="text" placeholder="Provincia" required="">
                        </p>

                        <div class="dt-sc-hr-invisible-large"></div>

                        <h2 class="dt-sc-hr-title">Paso 2 y 3 </h2>
                        <!--dt-sc-three-fourth starts-->
                        <div class="dt-sc-three-fourth column first" style="margin-left: 12%">
                          <!--dt-sc-tabs-container starts-->            
                          <div class="dt-sc-tabs-container">
                            <ul class="dt-sc-tabs-frame">
                              <li id="pestana1"><a href="#"> Débito mensual de la cuenta en la CPN </a></li> 
                              <li id="pestana2"><a href="#"> Débito mensual de otra institución </a></li>
                            </ul>
                            <div class="dt-sc-tabs-frame-content">
                              <div class="dt-sc-tabs-frame-content">
                               <p> <span> La informacion proporcionada debe ser correcta </span> </p>
                               <p>
                                 <input type="text" placeholder="Numero de cuenta" class="text_input" name="hf_first_cuenta" id="hf_first_cuenta" required />	
                               </p>
                               <p>
                                 <input type="text" placeholder="Tipo de cuenta" class="text_input" name="hf_first_tipo_cuenta" id="hf_first_tipo_cuenta" required/>  
                               </p>

                               <p> <span> Acepto y autorizo expresamente el débito de mi cuenta conforme los datos anteriormente señalados. <br> Acepto los <a href="terminos.php"> Terminos y condiciones </style> </a> <input type="checkbox" name="hf_first_condiciones_1" /> </span></p>

                               <div id="ajax_helpform_msg1"> </div>
                               <p>
                                 <input type="submit" value="&#xf002; &nbsp; Enviar">
                                 <img id="enviando1" src="images/loading.gif" width="100px" height="100px" style="position: relative; vertical-align:middle; display: none;">
                               </p>    
                             </div>
                           </div>

                           <div class="dt-sc-tabs-frame-content">
                             <p> <span> La informacion proporcionada debe ser correcta  </span> </p>
                             <p>
                               <input type="text" placeholder="Nombre de la Institucion financiera" class="text_input" name="hf_secund_institucion" id="hf_secund_institucion" required/>	
                             </p>
                             <p>
                               <input type="text" placeholder="Numero de cuenta" class="text_input" name="hf_secund_cuenta" id="hf_secund_cuenta" required/>	
                             </p>
                             <p>
                               <input type="text" placeholder="Tipo de cuenta" class="text_input" name="hf_secund_tipo_cuenta" id="hf_secund_tipo_cuenta" required/>  
                             </p>
                             <p> <span> Acepto y autorizo expresamente el débito de mi cuenta conforme los datos anteriormente señalados. <br> Acepto los <a href="terminos.php"> Terminos y condiciones </a> <input type="checkbox" name="hf_secund_condiciones_2" /> </span></p>

                             <div id="ajax_helpform_msg2"> </div>
                             <p>
                               <input type="submit" value="&#xf002; &nbsp; Enviar">
                               <img id="enviando2" src="images/loading.gif" width="100px" height="100px" style="position: relative; vertical-align:middle; display: none">
                             </p>    
                           </div>
                         </div>


                         <!--dt-sc-tabs-container ends-->
                       </div>


                     </section>
                     <!--primary ends-->
                   </form>
                 </div>
                 <!--container ends-->


               </div>
               <!--main ends-->

             </section>
             <!--primary ends-->

           </div>

           <!--dt-sc-one-fourth ends-->

           <div class="dt-sc-clear"></div>

         </section>
         <!--container starts-->
         <div class="dt-sc-hr-invisible-very-small"></div>
         <!--dt-sc-toggle-frame-set starts-->
         <div align="center" class="container">
          <div class="column dt-sc-one-fifth first">
            <div class=" type2">
              <div class="icon"> <span class=""> </span> </div>
              <h6><a href="#" target="_blank"> </a></h6>
            </div>
          </div>
          <div class="column dt-sc-one-fifth first">
            <div class="dt-sc-ico-content type2">
              <a> <img src="images1/mapa.png"/></a>
              <h6><a href="#" target="_blank"> Cobertura nacional </a></h6>
            </div>
          </div>
          <div class="column dt-sc-one-fifth">
            <div class="dt-sc-ico-content type2">
              <a> <img src="images1/estrella.png"/></a>
              <h6><a href="#" target="_blank"> Ámbito policial </a></h6>
            </div>
          </div>
          <div class="column dt-sc-one-fifth">
            <div class="dt-sc-ico-content type2">
              <a> <img src="images1/foco.png"/></a>
              <h6><a href="#" target="_blank"> Soluciones óptimas </a></h6>
            </div>
          </div>
        </div>

        <div class="dt-sc-hr-invisible-large"></div>
        <!--container ends-->
        <div class="fullwidth-background dt-sc-parallax-section benefits_section">
          <div align="center" class="container">

            <!--dt-sc-one-half starts-->
            <div class="dt-sc-one-half textwidget first">
              <h5> EL SERVICIO JURÍDICO QUE ESTABAS ESPERANDO, CON LA MEJOR TARIFA Y COBERTURA, CONTRÁTALO YA! <br> EL ABOGADO MÁS CERCA DE TI </h5>
            </div>
            <div class="dt-sc-hr-invisible-large"></div>

          </div>
        </div>     
        <div class="dt-sc-hr-invisible-large"></div>
        <!--main ends-->
        <div class="container">


        </div> 
        <!--footer starts-->
			<?php include 'footer.php'; ?>
       <!--footer ends-->
      </div>
      <!--inner-wrapper ends-->    
    </div>
    <!--wrapper ends-->
    <!--wrapper ends-->
		<?php include "asistencias.php"; ?>
    <a href="" title="Go to Top" class="back-to-top"> <span class="fa fa-angle-up"></span> </a> 
    <!--Java Scripts-->
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/jquery-easing-1.3.js"></script>
    <script type="text/javascript" src="js/jquery.sticky.js"></script>
    <script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>


    <script type="text/javascript" src="js/jquery.tabs.min.js"></script>
    <script type="text/javascript" src="js/jquery.smartresize.js"></script> 
    <script type="text/javascript" src="js/shortcodes.js"></script>   

    <script type="text/javascript" src="js/custom.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDz1RGAW3gGyDtvmBfnH2_fE2DVVNWq4Eo&callback=initMap" type="text/javascript"></script>
    <script src="js/gmap3.min.js"></script>
    <!-- Layer Slider --> 
    <script type="text/javascript" src="js/jquery-transit-modified.js"></script> 
    <script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script> 
    <script type='text/javascript' src="js/greensock.js"></script> 
    <script type='text/javascript' src="js/layerslider.transitions.js"></script> 

    <script type="text/javascript">var lsjQuery = jQuery;</script><script type="text/javascript"> lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice('layerslider_2','jquery'); } else { lsjQuery("#layerslider_2").layerSlider({responsiveUnder: 1240, layersContainer: 1170, skinsPath: 'js/layerslider/skins/'}) } }); </script>

    <script type="text/javascript">
      jQuery( "#pestana1" ).click(function() {
        jQuery('#hf_secund_name').removeAttr('required');
        jQuery('#hf_secund_cedula').removeAttr('required');
        jQuery('#hf_secund_movil').removeAttr('required');
        jQuery('#hf_secund_institucion').removeAttr('required');
        jQuery('#hf_secund_tipo_cuenta').removeAttr('required');
        jQuery('#hf_secund_cuenta').removeAttr('required');

        jQuery('#hf_first_name').attr('required','true');
        jQuery('#hf_first_cedula').attr('required','true');
        jQuery('#hf_first_telefono').attr('required','true');
        jQuery('#hf_first_cuenta').attr('required','true');
      });

      jQuery( "#pestana2" ).click(function() {
        jQuery('#hf_first_name').removeAttr('required');
        jQuery('#hf_first_cedula').removeAttr('required');
        jQuery('#hf_first_telefono').removeAttr('required');
        jQuery('#hf_first_cuenta').removeAttr('required');

        jQuery('#hf_secund_name').attr('required','true');
        jQuery('#hf_secund_cedula').attr('required','true');
        jQuery('#hf_secund_movil').attr('required','true');
        jQuery('#hf_secund_institucion').attr('required','true');
        jQuery('#hf_secund_tipo_cuenta').attr('required','true');
        jQuery('#hf_secund_cuenta').attr('required','true');
      });

    </script>

                     <!-- Cerrar Session -->
                     <script>
                        jQuery(document).ready(function(){
                           jQuery("#close_session").click(function(){
                              alert('Su sesión ha sido cerrada');
                              window.location.href='closesession.php';
                           });
                        });
                     </script>

  </body>
  </html>

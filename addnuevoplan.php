<?php 
	session_start(); 
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");
?>
<?php
	header('Content-Type: text/html; charset=utf-8');
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
	mysqli_set_charset($conn, "utf8");

	if(isset($_GET['idplan'])){
		$sql = "SELECT idplan,
							plan,
							costo
					FROM  planes
					WHERE idplan='$_GET[idplan]'";
		if($result = mysqli_query($conn, $sql)){
			$row = mysqli_fetch_assoc($result);
			$idplan = $row['idplan'];
			$plan = $row['plan'];
			$costo = $row['costo'];
		}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
	}else{
		$idplan = "";
		$plan = "";
		$costo = "";
	}
?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>
         Tu Abogado:
         Áreaes :: Directorio de Abogados del Ecuador
      </title>
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <link href="css/animate.min.css" rel="stylesheet">
      <link href="css/prettyPhoto.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="bower_components/datatables.net-dt/css/jquery.dataTables.css">
      <link rel="stylesheet" type="text/css" href="css/bootstrap-markdown.min.css">
      <!-- JQuery Validator and form -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.js"></script>

      <style>
         .pricingTable{
         text-align: center;
         transition: all 0.5s ease 0s;
         }
         .pricingTable:hover{
         box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
         }
         .pricingTable .pricingTable-header{
         color: #feffff;
         }
         .pricingTable .heading{
         display: block;
         padding-top: 25px;
         }
         .pricingTable .heading > h3{
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .textotitle{
         color: #feffff;
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .pricingTable .subtitle{
         display: block;
         font-size: 13px;
         margin-top: 5px;
         text-transform: capitalize;
         }
         .pricingTable .price-value{
         display: block;
         font-size: 60px;
         font-weight: 700;
         padding-bottom: 25px;
         }
         .pricingTable .price-value span{
         display: block;
         font-size: 14px;
         line-height: 20px;
         text-transform: uppercase;
         }
         .pricingTable .pricingContent{
         /*text-transform: capitalize;*/
         background: #fbfbfb;
         color: #fefeff;
         }
         .pricingTable .pricingContent ul{
         list-style: none;
         padding: 15px 14px 10px;
         margin: 0;
         text-align: left;
         }
         .pricingTable .pricingContent ul li{
         font-size: 14px;
         padding: 12px 0;
         border-bottom: 1px dashed #e1e1e1;
         color: #9da1ad;
         }
         .pricingTable .pricingContent ul li i{
         font-size: 14px;
         float: right;
         }
         .pricingTable .pricingTable-sign-up{
         padding: 20px 0;
         background: #fbfbfb;
         color: #fff;
         text-transform: capitalize;
         }
         .pricingTable .btn-block{
         width: 60%;
         margin: 0 auto;
         font-size: 17px;
         color: #fff;
         text-transform: capitalize;
         border: none;
         border-radius: 5px;
         padding: 10px;
         transition: all 0.5s ease 0s;
         }
         .pricingTable .btn-block:before{
         content: "\f007";
         font-family: 'FontAwesome';
         margin-right: 10px;
         }
         .pricingTable.blue .pricingTable-header,
         .pricingTable.blue .btn-block{
         background: #fce400;
         }
         .pricingTable.pink .pricingTable-header,
         .pricingTable.pink .btn-block{
         background: #c9b600;
         }
         .pricingTable.orange .pricingTable-header,
         .pricingTable.orange .btn-block{
         background: #e0d83a;
         }
         .pricingTable.green .pricingTable-header,
         .pricingTable.green .btn-block{
         background: #e0cd00;
         }
         .pricingTable.blue .btn-block:hover,
         .pricingTable.pink .btn-block:hover,
         .pricingTable.orange .btn-block:hover,
         .pricingTable.green .btn-block:hover{
         background: #e6e6e6;
         color: #939393;
         }
         @media screen and (max-width: 990px){
         .pricingTable{ margin-bottom: 20px; }
         }
			/*Errores*/
          /* Color rojo para el texto de error de los campos */
         .error{
            color: red;
         }
         /* Borde rojo y grosor de linea de los inputs */
         .plan-form input[type=text].error,
         .plan-form input[type=number].error{
            padding:15px 18px;
            border:1px solid #FF0000;
         }
      </style>
   </head>
   <body class="homepage">
<?php include "adm_header.php"; ?>
      <!--/header-->
      <section id="blog" class="container">
         <ol class="breadcrumb">
				<li><a href="miperfil.php">Mi perfil</a></li>
				<li><a href="adm_planes.php">Planes</a></li>
				<li class="active">Nuevo/Editar Plan</li>
         </ol>
         <div class="container">
         <div class="center" style="padding-bottom: 10px;">
            <h2 style="color: #8c8c8c">Nuevo/Editar Plan</h2>
         </div>
         <div class="col-md-12">
            <div class="jumbotron">
               <div class="row">
                  <div class="col-md-4 col-md-offset-4" align="center">
							<form id="frmnuevoplan" class="plan-form" name="frmnuevoplan" method="post" action="update.php">
								<input type="hidden" name='formname' value="frmnuevoplan">
								<div class="form-group">
									<label for="idplan">ID Plan *</label>
									<input type="text" name="idplan" class="form-control" required="required" id="idplan"
									value="<?php echo $idplan; ?>">
								</div>
								<div class="form-group">
									<label for="plan">Plan *</label>
									<input type="text" name="plan" class="form-control" required="required" id="plan"
									value="<?php echo htmlspecialchars($plan); ?>">
								</div>
								<div class="form-group">
									<label for="costo">Costo *</label>
									<input type="number" name="costo" class="form-control" step=".01" required="required" id="costo"
									value="<?php echo $costo; ?>">
								</div>
								<div class="form-group" align="center">
									<button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Guardar</button>
									<img id="enviando" src="images/barra.gif" width="100px" height="25px" 
										  style="position: relative; vertical-align:middle; display: none;">
								</div>
								<!-- Zona de mensajes -->
								<div id="mensajes"></div>
								<!-- FIN Zona de mensajes -->
							</form>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="js/bootstrap.min.js"></script>
      <script src="js/markdown.js"></script>
      <script src="js/to-markdown.js"></script>
      <script src="js/bootstrap-markdown.js"></script>
      <script src="js/bootstrap-markdown.es.js"></script>
      <script type="text/javascript" src="js/jquery.simpleslider.js"></script>
      <script type="text/javascript" charset="utf8" src="bower_components/datatables.net/js/jquery.dataTables.min.js"> </script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/jquery.isotope.min.js"></script>
      <script src="js/wow.min.js"></script>
      <script src="js/main.js"></script>
      <script src="js/custom.js"></script>
   </body>
</html>



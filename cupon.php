<?php session_start(); ?>
<?php
	header('Content-Type: text/html; charset=utf-8');
	$idcupon = $_GET['idcupon'];
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
	mysqli_set_charset($conn, "utf8");
	$sql = "SELECT titulo, 
						subtitulo, 
						contenido, 
						img,
                  nombres,
                  t2.id iduser  
				FROM cupones t1
            INNER JOIN users t2
				ON t1.iduser=t2.id
				WHERE t1.id='$idcupon'";
   if ($result = mysqli_query($conn, $sql)){
      $row = mysqli_fetch_assoc($result);
   }else error_log("Error: " . $sql . "..." . mysqli_error($conn));

?>

<!DOCTYPE html>
<html lang="en">
  <head>
	 <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Plataforma Digital de Abogados del Ecuador | Promociones </title>
	 <meta name="keywords" content="abogado, abogados, ecuador, promociones, descuentos, asesoria, quito, guayaquil, cuenca, manta, ambato, ibarra" />
	 <meta name="description" content="Obten los mejores descuentos en servicios profesionales de Abogados en Ecuador" />
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
   <link rel="stylesheet" href="gp/css/font-awesome.min.css">
   <link href="gp/css/animate.min.css" rel="stylesheet">
    <link href="gp/css/prettyPhoto.css" rel="stylesheet">      
   <link href="css/main.css" rel="stylesheet">
    <link href="gp/css/responsive.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
	<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5cd0ed504c6e360019381352&product='inline-share-buttons' async='async'></script>

	 <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       

      <style>
			hr {
				 display:block;
				 border:0px;
				 height:19px;
				 background-image:url('salique/images/separators.png');
				 margin-top: 0px;
			}
			#portfolio .row {
				margin-left: 0px;
				margin-right: 0px;
				margin-bottom: 10px;
			}
			.lista {
				font-size: 16px;
			}
			ul#areas li {
				display: inline;
			}
			ul#areas {
				padding-left: 0px;
			}
			.ciudad h4{
				font-family: 'Open Sans', Arial, sans-serif;
				font-size: 18px;
				font-weight: 500;
				color: #000;
			}
			#portfolio {
				padding-top: 0px;
				padding-bottom: 0px;
				margin-top: 0px;
			}
			#blog {
				padding-bottom: 20px;
				padding-top: 50px;
				padding-left: 0px;
				padding-right: 0px;
			}
			.col-md-12 {
				padding-left: 0px;
				padding-right: 0px;
			}
			.col-md-9 {
				padding-right: 0px;
				padding-left: 30px;
			}
			.col-md-6 {
				padding-right: 0px;
				padding-left: 30px;
			}
			.col-md-3 {
				padding-right: 0px;
			}
			.container {
				padding-left: 15px;
				padding-right: 15px;
			}
			.team {
				background-color: #f9f9f9;
				padding-top: 0px;
				padding-bottom: 0px;
				margin-bottom: 0px;
			}
			.btn {
				padding-left: 0px;
			}
			.rateButton {
				padding-left: 10px;
			}

      </style>    
 
  </head>
  <body class="homepage" style="background: #f9f9f9">   
		<?php include "header.php"; ?>
      <!--/header-->

		<section id="blog" class="container">
			<div class="container wow fadeInDown">
				<ol class="breadcrumb" align="center" style="margin-bottom: 0px">
					<li class="active" id="bottom">
						<h3 style="font-size: 30px"><?php echo $row['titulo']; ?></h3>
					</li>
				</ol>
				<div class="separador">
					<hr>
				</div>
				<div class="row">
					<div class="col-md-6">
						<h2 style="margin-bottom: 30px;margin-top: 0px;"><?php echo $row['subtitulo']; ?></h2>
						<p><?php echo str_replace("\n","</p><p>",$row['contenido']); ?></p>
                  <p>Por: <a href="perfil.php?iduser=<?php echo $row['iduser']; ?>"><?php echo $row['nombres']; ?></a>
						<div class="sharethis-inline-share-buttons"></div>
					</div>
					<div class="col-md-6">
						<a class="preview" href="uploads/<?php echo $row['img']; ?>" rel="prettyPhoto">
							<img src="uploads/<?php echo $row['img']; ?>" alt="" width="100%">
						</a>
					</div>
				</div>
			</div>
		</section>

      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
	
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="gp/js/jquery.js"></script>
    <script src="gp/js/bootstrap.min.js"></script>
    <script src="gp/js/jquery.prettyPhoto.js"></script>
    <script src="gp/js/jquery.isotope.min.js"></script>
    <script src="gp/js/wow.min.js"></script>
    <script src="gp/js/main.js"></script>
	 <script type="text/javascript" src="js/jquery.simpleslider.js"></script>

  </body>
</html>

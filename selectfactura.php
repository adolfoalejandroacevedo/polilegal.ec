<?php

$config = require 'config.php';

//Obtencion de las variables GET
$fecha = $_POST["fecha"];
$cedula = $_POST["cedula"];
$info = array();

//Obteniendo el mes y el año
$date = strtotime($fecha);
$mes = date('m',$date);
$year = date('Y',$date);
$partname = $cedula;

//Buscando la factura dentro del directorio
$directorio = "./facturas/$year/$mes/";
if(is_dir($directorio)){
	$ficheros1  = scandir($directorio);
	$find = false;
	foreach ($ficheros1 as $file){
		if (strpos($file, $partname) !== false) {
			$find = true;
			$info['file'] = $directorio.$file;
		}
	}
	if($find) {
		$info['OK'] = "found";
	}else{
		$info['OK'] = "nofound";
		$info['file'] = "sin archivo";
	}
}else{
	$info['OK'] = "nofound";
	$info['file'] = "sin archivo";
}

//Respuesta
if (isset($_GET['callback'])) {
    echo $_GET['callback'] . '( ' . json_encode($info) . ' )';
}else {
    echo 'callbackEjercicio( ' . json_encode($info) . ' )';
}

?>

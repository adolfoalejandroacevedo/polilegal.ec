<?php
$cuerpo2 = '
<!DOCTYPE html>
<html lang="en">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Afiliaci&oacute;n POLILEGAL S.A.</title>
   <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <style>
        body {
            width: 100%;
            font-family: "Open Sans", sans-serif;
        }

        svg {
            width: 100px;
        }
        
        .main-table{
            background:white;
            
        }
        
        table {
            width: 500px;
            display: flex;
            flex-flow: column;
            margin: 0px auto;
        }

        table thead tr th {
            border-bottom: solid 5px black;
            width: 500px;
        }

        thead tr th svg {
            text-align: center;
        }

        .tabla-gris {
            background: #f8f8f8;
            
            border-radius: 5px;
            margin-bottom: 5px;
            padding: 15px;
           font-family: "Open Sans", sans-serif;
            font-size: 15px;
        }

        .tabla-gris .td1 {
            border-bottom: solid 1px #ccc;
        }
        .titulo{
          font-family: "Open Sans", sans-serif;
            padding: 5px;
        }
        .titulo h1 {
            font-size: 25px;
            line-height: 0px;
        }
        .line{
            border-bottom: solid 5px black;
        }
    </style>
</head>

<body>
    <table class="main-table">
        <thead>
            <tr>

            </tr>
        </thead>
        <tbody>
            <tr>
                <th class="titulo"><h1 style="width: 500px;">NUEVO AFILIADO</h1> <br></th>
            </tr>
            <tr>
                <td>
                    <table class="tabla-gris">
                        <tr>
                            <td>Nombres: 	'.utf8_decode($nombres).'</td>
                        </tr>
                        <tr>
                            <td>C&eacute;dula: 	'.$cedula.'</td>
                        </tr>
                        <tr>
                            <td>Celular: 	'.$celular.'</td>
                        </tr>
                        <tr>
                            <td>E-mail: 	'.$email.'</td>
                        </tr>
                        <tr>
                            <td>Ciudad: 	'.utf8_decode($ciudad).'</td>
                        </tr>
                        <tr>
                            <td>Sector: 	'.utf8_decode($sector).'</td>
                        </tr>
                        <tr>
                            <td>Tipo de Plan: '.$plan.'</td>
                        </tr>
                        <tr>
                            <td>Medio de pago: '.$medio_pago.'</td>
                        </tr>
                        <tr>
                            <td>Términos y condicioes: Aceptadas</td>
                        </tr>
                        <tr>
                            <td>Política de privacida: Aceptadas</td>
                        </tr>
                     </table>
							<table>
								<tr>
									<td align="middle"><img src="https://polilegal.ec/images1/jutnos3.png" style="width: 80%;" /></td>
								</tr>
							</table>
                </td>
            </tr>
        </tbody>

    </table>

</body>

</html>
';
?>

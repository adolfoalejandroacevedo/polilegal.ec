<section id="primary" class="content-full-width">
	 <!--container starts-->
	 
	<p class="plan2-text" style="text-align: justify;"><span class="dt-sc-highlightt skin-color"> <strong style="color: rgb(43, 155, 15);"> 
	¡PROTEGE TUS INTERESES!</strong></span><img src="" style="position:absolute; margin-left: 42%; margin-top: -4%; width: 325px;"><br><br> 
	Si eres socio en servicio pasivo o civil de la Cooperativa Policía Nacional, tenemos en PLAN perfecto para ti. A través de la alianza entre la 
	Cooperativa Policía Nacional y PoliLegal Abogados, te damos la bienvenida a nuestro PLAN DE ASISTENCIA JURÍDICA PARA SOCIOS SP Y CIVILES DE LA CPN.</p>
		  
		  <!--p class="alignleft plan2-text" style="text-align: justify;"><span class="dt-sc-highlightt skin-color"> <strong> PROTEGE TUS INTERES.<br>Si eres socio civil de la Cooperativa Policía Nacional, tenemos en PLAN perfecto para ti. </strong> </span> A través de la alianza entre la Cooperativa Policía Nacional y PoliLegal Abogados, te damos la bienvenida a nuestro PLAN DE ASISTENCIA JURÍDICA PARA SOCIOS CIVILES.  </p-->

	<!--primary starts-->
	<section id="secundary" class="content-full-width margintop2">
		<div class="dt-sc-hr-invisible-small"></div>

   <!--dt-sc-toggle-frame-set starts -->
   <div class="dt-sc-toggle-frame-set">
      <div class="dt-sc-toggle-frame">
         <h5 class="dt-sc-toggle-accordion"><a href="#"> Elaboración De </a></h5>
         <div id="collapse1" class="dt-sc-toggle-content">
            <div class="block" style="text-align: left;">
               <p class="plan2-text">• Minutas de compraventa. </p>
               <p class="plan2-text">• Declaraciones juramentadas. </p>
               <p class="plan2-text">• Cancelación de hipotecas. </p>
               <p class="plan2-text">• Procuraciones judiciales. </p>
               <p class="plan2-text">• Permisos de salida del país de menores. </p>
               <p class="plan2-text">• Poderes generales y especiales. </p>
               <p class="plan2-text">• Todo tipo de contratos. </p>
               <p class="plan2-text">• Peticiones ante entidades públicas. </p>
               <p class="plan2-text">• Requerimientos judiciales. </p>
            </div>
			</div>
		</div>
      <div class="dt-sc-toggle-frame">
         <h5 class="dt-sc-toggle-accordion"><a href="#"> Asesoría Integral En </a></h5>
         <div id="collapse1" class="dt-sc-toggle-content">
            <div class="block" style="text-align: left;">
               <p class="plan2-text">• Cualquier asunto legal. </p>
            </div>
			</div>
		</div>
      <div class="dt-sc-toggle-frame">
         <h5 class="dt-sc-toggle-accordion"><a href="#"> Descuentos </a></h5>
         <div id="collapse1" class="dt-sc-toggle-content">
            <div class="block" style="text-align: left;">
               <p class="plan2-text">• 50% de descuento en todos los servicios no incluidos en la cobertura. </p>
            </div>
			</div>
		</div>
      <div class="dt-sc-toggle-frame">
         <h5 class="dt-sc-toggle-accordion"><a href="#"> Consultas </a></h5>
         <div id="collapse1" class="dt-sc-toggle-content">
            <div class="block" style="text-align: left;">
               <p class="plan2-text">• De forma personal en nuestras oficinas. </p>
               <p class="plan2-text">• A través de la página web, correo electrónico y redes sociales. </p>
               <p class="plan2-text">• A través de llamadas telefónicas a nuestro Contact Center.</p>
            </div>
			</div>
		</div>
      <div class="dt-sc-toggle-frame">
         <h5 class="dt-sc-toggle-accordion"><a href="#"> Beneficios </a></h5>
         <div id="collapse1" class="dt-sc-toggle-content">
            <div class="block" style="text-align: left;">
               <p class="plan2-text">• 100% de cobertura. </p>
               <p class="plan2-text">• Atención ilimitada. </p>
               <p class="plan2-text">• Abogados especializados. </p>
               <p class="plan2-text">• Cobertura a nivel nacional. </p>
               <p class="plan2-text">Para el uso de los servicios, el beneficiario deberá comunicarse a nuestro<br>
               Contact Center (02)3 825530 / (02)3 333533, correo electrónico, página web<br> o redes sociales, con el fin de gestionar sus solicitudes
               y asignarle a un Abogado.</p>
            </div>
			</div>
		</div>
      <div class="dt-sc-toggle-frame">
         <h5 class="dt-sc-toggle-accordion"><a href="#"> Cobertura </a></h5>
         <div id="collapse1" class="dt-sc-toggle-content">
            <div class="block" style="text-align: left;">
               <p><span class="dt-sc-highlightt skin-color"> <strong>Elaboración completa de:</strong></span></p>
               <p class="plan2-text">• Minutas de compraventa. </p>
               <p class="plan2-text">• Declaraciones juramentadas. </p>
               <p class="plan2-text">• Cancelación de hipotecas. </p>
               <p class="plan2-text">• Procuraciones judiciales. </p>
               <p class="plan2-text">• Permisos de salida del país de menores. </p>
               <p class="plan2-text">• Poderes generales y especiales. </p>
               <p class="plan2-text">• Todo tipo de contratos. </p>
               <p class="plan2-text">• Peticiones ante entidades públicas. </p>
               <p class="plan2-text">• Requerimientos judiciales. </p>
               <p><span class="dt-sc-highlightt skin-color"> <strong>Asesoría integral en:</strong></span></p>
               <p class="plan2-text">• Asuntos civiles y de familia. </p>
               <p class="plan2-text">• Procedimientos constitucionales. </p>
               <p class="plan2-text">• Procedimientos contencioso-administrativos. </p>
               <p class="plan2-text">• Apertura de negocios y constitución de compañías. </p>
               <p class="plan2-text">• Asuntos penales y de tránsito. </p>
               <p class="plan2-text">• Asesoría en cualquier otro asunto legal. </p>
               <p><span class="dt-sc-highlightt skin-color">
               <strong>Cobertura de consultas:</strong></span></p>
               <p class="plan2-text">• Consultas personales ilimitadas en nuestras oficinas. </p>
               <p class="plan2-text">• Consultas a través de la página web y correo electrónico. </p>
               <p class="plan2-text">• Consultas a través de llamadas telefónicas a nuestro CONTACT CENTER. </p>
            </div>
			</div>
		</div>
      <div class="dt-sc-toggle-frame">
         <h5 class="dt-sc-toggle-accordion"><a href="#"> Costo y Forma de Pago </a></h5>
         <div id="collapse1" class="dt-sc-toggle-content">
            <div class="block" style="text-align: left;">
               <p class="plan2-text">• $ 6,99 USD Mensual (tarifa incluye impuestos). </p>
               <p class="plan2-text">* El socio puede ingresar a cualquiera de nuestros Planes mediante:</p>
               <p class="plan2-text">• Débito mensual de su cuenta de ahorros de la CPN. </p>
               <p class="plan2-text">• Débito mensual de su cuenta en otra institución financiera. </p>
            </div>
			</div>
		</div>
	</div>



		<!--dt-sc-three-fourth starts-->
		<!--
		<div class="dt-sc-three-uno column first">
		-->
		<!--dt-sc-tabs-container starts-->            
<!--
			<div class="dt-sc-tabs-container">
  				<ul class="dt-sc-tabs-frame">
	 				<li><a href="#"> Elaboración De </a></li>  
	 				<li><a href="#"> Asesoría Integral En </a></li>  
	 				<li><a href="#"> Descuentos </a></li>  
	 				<li><a href="#"> Consultas </a></li>  
	 				<li><a href="#"> Beneficios </a></li>  
	 				<li><a href="#"> Cobertura </a></li> 
 					<li><a href="#"> Costo y Forma de Pago </a></li>
  				</ul>
				<div class="dt-sc-tabs-frame-content">
					<p class="plan2-text">• Minutas de compraventa. </p>
					<p class="plan2-text">• Declaraciones juramentadas. </p>
					<p class="plan2-text">• Cancelación de hipotecas. </p>
					<p class="plan2-text">• Procuraciones judiciales. </p>
					<p class="plan2-text">• Permisos de salida del país de menores. </p>
					<p class="plan2-text">• Poderes generales y especiales. </p>
					<p class="plan2-text">• Todo tipo de contratos. </p>
					<p class="plan2-text">• Peticiones ante entidades públicas. </p>
					<p class="plan2-text">• Requerimientos judiciales. </p>
				</div>
				<div class="dt-sc-tabs-frame-content">
					<p class="plan2-text">• Cualquier asunto legal. </p>
				</div>
				<div class="dt-sc-tabs-frame-content">
					<p class="plan2-text">• 50% de descuento en todos los servicios no incluidos en la cobertura. </p>
				</div>
				<div class="dt-sc-tabs-frame-content">
					<p class="plan2-text">• De forma personal en nuestras oficinas. </p>
					<p class="plan2-text">• A través de la página web, correo electrónico y redes sociales. </p>
					<p class="plan2-text">• A través de llamadas telefónicas a nuestro Contact Center.</p>
				</div>
				<div class="dt-sc-tabs-frame-content">
					<p class="plan2-text">• 100% de cobertura. </p>
					<p class="plan2-text">• Atención ilimitada. </p>
					<p class="plan2-text">• Abogados especializados. </p>
					<p class="plan2-text">• Cobertura a nivel nacional. </p>
					<p class="plan2-text">Para el uso de los servicios, el beneficiario deberá comunicarse a nuestro<br> 
					Contact Center (02)3 825530 / (02)3 333533, correo electrónico, página web<br> o redes sociales, con el fin de gestionar sus solicitudes 
					y asignarle a un Abogado.</p>
				</div>
				<div class="dt-sc-tabs-frame-content">
					<p><span class="dt-sc-highlightt skin-color"> <strong>Elaboración completa de:</strong></span></p>
					<p class="plan2-text">• Minutas de compraventa. </p>
					<p class="plan2-text">• Declaraciones juramentadas. </p>
					<p class="plan2-text">• Cancelación de hipotecas. </p>
					<p class="plan2-text">• Procuraciones judiciales. </p>
					<p class="plan2-text">• Permisos de salida del país de menores. </p>
					<p class="plan2-text">• Poderes generales y especiales. </p>
					<p class="plan2-text">• Todo tipo de contratos. </p>
					<p class="plan2-text">• Peticiones ante entidades públicas. </p>
					<p class="plan2-text">• Requerimientos judiciales. </p>
					<p><span class="dt-sc-highlightt skin-color"> <strong>Asesoría integral en:</strong></span></p>
					<p class="plan2-text">• Asuntos civiles y de familia. </p>
					<p class="plan2-text">• Procedimientos constitucionales. </p>
					<p class="plan2-text">• Procedimientos contencioso-administrativos. </p>
					<p class="plan2-text">• Apertura de negocios y constitución de compañías. </p>
					<p class="plan2-text">• Asuntos penales y de tránsito. </p>
					<p class="plan2-text">• Asesoría en cualquier otro asunto legal. </p>
					<p><span class="dt-sc-highlightt skin-color">
					<strong>Cobertura de consultas:</strong></span></p>
					<p class="plan2-text">• Consultas personales ilimitadas en nuestras oficinas. </p>
					<p class="plan2-text">• Consultas a través de la página web y correo electrónico. </p>
					<p class="plan2-text">• Consultas a través de llamadas telefónicas a nuestro CONTACT CENTER. </p>
				</div>
				<div class="dt-sc-tabs-frame-content">
 					<p class="plan2-text">• $ 6,99 USD Mensual (tarifa incluye impuestos). </p>
					<p class="plan2-text">* El socio puede ingresar a cualquiera de nuestros Planes mediante:</p>
					<p class="plan2-text">• Débito mensual de su cuenta de ahorros de la CPN. </p>
					<p class="plan2-text">• Débito mensual de su cuenta en otra institución financiera. </p>
				</div>
			</div>
		</div>
-->
		<!--dt-sc-tabs-container ends-->
		<!--dt-sc-three-fourth ends-->
		<!--dt-sc-one-fourth starts-->
		<!--dt-sc-one-fourth ends-->
		<div class="dt-sc-clear"></div>
		<div class="dt-sc-hr-invisible"></div>

</section>

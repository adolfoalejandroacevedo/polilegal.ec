<?php
   session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");
	header('Content-Type: text/html; charset=utf-8');
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
	mysqli_set_charset($conn, "utf8")
?>

<!DOCTYPE html>
<html>
   <head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>
         Tu Abogado:
         Anuncios :: Directorio de Abogados del Ecuador
      </title>
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <link href="css/animate.min.css" rel="stylesheet">
      <link href="css/prettyPhoto.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="bower_components/datatables.net-dt/css/jquery.dataTables.css">
      <link rel="stylesheet" type="text/css" href="css/bootstrap-markdown.min.css">
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/markdown.js"></script>
      <script src="js/to-markdown.js"></script>
      <script src="js/bootstrap-markdown.js"></script>
      <script src="js/bootstrap-markdown.es.js"></script>
      <script type="text/javascript" src="js/jquery.simpleslider.js"></script>
      <script type="text/javascript" charset="utf8" src="bower_components/datatables.net/js/jquery.dataTables.min.js"> </script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/jquery.isotope.min.js"></script>
      <script src="js/wow.min.js"></script>
      <script src="js/main.js"></script>
      <script src="js/custom.js"></script>
      <!-- JQuery Validator and form -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.js"></script>
      <style>
			.nota p{
				margin-bottom: 15px;
				font-size: 14px;
				font-weight: 600;
				margin-left: 30px;
				margin-right: 30px;
				text-align: center;
			}
         /* Color rojo para el texto de error de los campos */
         .error{
            color: red;
         }
         /* Borde rojo y grosor de linea de los inputs */
         .frmarticulo input[type=text].error {
            padding:15px 18px;
            border:1px solid #FF0000;
         }
			textarea.error {
				border:1px solid #FF0000;	
			}
			.panel-body .media .row .form-group{
			margin-left: 45px;
			margin-right: 45px;
			}
         .pricingTable{
         text-align: center;
         transition: all 0.5s ease 0s;
         }
         .pricingTable:hover{
         box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
         }
         .pricingTable .pricingTable-header{
         color: #feffff;
         }
         .pricingTable .heading{
         display: block;
         padding-top: 25px;
         }
         .pricingTable .heading > h3{
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .textotitle{
         color: #feffff;
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .pricingTable .subtitle{
         display: block;
         font-size: 13px;
         margin-top: 5px;
         text-transform: capitalize;
         }
         .pricingTable .price-value{
         display: block;
         font-size: 60px;
         font-weight: 700;
         padding-bottom: 25px;
         }
         .pricingTable .price-value span{
         display: block;
         font-size: 14px;
         line-height: 20px;
         text-transform: uppercase;
         }
         .pricingTable .pricingContent{
         /*text-transform: capitalize;*/
         background: #fbfbfb;
         color: #fefeff;
         }
         .pricingTable .pricingContent ul{
         list-style: none;
         padding: 15px 14px 10px;
         margin: 0;
         text-align: left;
         }
         .pricingTable .pricingContent ul li{
         font-size: 14px;
         padding: 12px 0;
         border-bottom: 1px dashed #e1e1e1;
         color: #9da1ad;
         }
         .pricingTable .pricingContent ul li i{
         font-size: 14px;
         float: right;
         }
         .pricingTable .pricingTable-sign-up{
         padding: 20px 0;
         background: #fbfbfb;
         color: #fff;
         text-transform: capitalize;
         }
         .pricingTable .btn-block{
         width: 60%;
         margin: 0 auto;
         font-size: 17px;
         color: #fff;
         text-transform: capitalize;
         border: none;
         border-radius: 5px;
         padding: 10px;
         transition: all 0.5s ease 0s;
         }
         .pricingTable .btn-block:before{
         content: "\f007";
         font-family: 'FontAwesome';
         margin-right: 10px;
         }
         .pricingTable.blue .pricingTable-header,
         .pricingTable.blue .btn-block{
         background: #fce400;
         }
         .pricingTable.pink .pricingTable-header,
         .pricingTable.pink .btn-block{
         background: #c9b600;
         }
         .pricingTable.orange .pricingTable-header,
         .pricingTable.orange .btn-block{
         background: #e0d83a;
         }
         .pricingTable.green .pricingTable-header,
         .pricingTable.green .btn-block{
         background: #e0cd00;
         }
         .pricingTable.blue .btn-block:hover,
         .pricingTable.pink .btn-block:hover,
         .pricingTable.orange .btn-block:hover,
         .pricingTable.green .btn-block:hover{
         background: #e6e6e6;
         color: #939393;
         }
         @media screen and (max-width: 990px){
         .pricingTable{ margin-bottom: 20px; }
         }
			.breadcrumb>.active{
			color: #c1ab09;
    		font-weight: 600;
			}
			.glyphicon-trash{
				cursor: pointer;
			}
			/* Para la imagen personalizada */
          #drop_file_zone {
              background-color: #EEE;
              border: #999 5px dashed;
              width: 100%;
              height: 450px;
              padding-left: 0px;
              padding-right: 0px;
              padding-top: 0px;
              padding-bottom: 0px;
              font-size: 18px;
          }
          #drag_upload_file {
              width:50%;
              margin:0 auto;
          }
          #drag_upload_file p {
              text-align: center;
				  font-family: 'Open Sans', Arial, sans-serif;
				  font-size: 16px;
				  margin-bottom: 5px;
          }
          #drag_upload_file #selectfile {
              display: none;
          }
          .elimina {
              cursor: pointer;
          }
      </style>
   </head>
   <body class="homepage">
<?php include "adm_header.php"; ?>
      <!--/header-->
      <section id="blog" class="container" style="padding-top: 50px;">
         <ol class="breadcrumb">
            <li><a href="miperfil.php">Mi perfil</a></li>
            <li><a href="adm_transac_abogados.php">Transacciones Abogados</a></li>
            <li class="active">Depósito Bancario</li>
         </ol>
         <div class="jumbotron">
            <div class="container">
               <div class="center" style="padding-bottom: 25px;">
                  <h2> DEPÓSITO BANCARIO</h2>
               </div>
               <div class="row">
                  <form id="frmdepositobanco" name="frmdepositobanco" class="frmdepositobanco" method="POST" role="form" action="update.php">
							<input type="hidden" name='formname' value="frmdepositobanco">
                     <fieldset>
                        <div class="form-group text required">
									<label for="deposito" class="control-label">Numero de depósito *</label>
									<input type="text" name="deposito" class="form-control">
								</div>
                        <div class="form-group select">
                           <label for="iduser" class="control-label">ID Abogado *</label>
                           <select name="iduser" class="form-control">
										<?php
											$selected = "SELECTED";
											$sql = "SELECT id
														FROM users
														ORDER BY id ASC";
											if($result = mysqli_query($conn, $sql)){
												while($row = mysqli_fetch_assoc($result)){
										?>
                              <option value="<?php echo $row['id']; ?>" <?php echo $selected; ?>><?php echo $row['id']; ?></option>
										<?php 
													$selected = "";
												}
											}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
										?>
                           </select>
                        </div>
                        <div class="form-group select">
                           <label for="plan" class="control-label">PLAN *</label>
                           <select name="plan" class="form-control">
										<?php
											$selected = "SELECTED";
											$sql = "SELECT idplan,
																plan
														FROM planes
														ORDER BY idplan ASC";
											if($result = mysqli_query($conn, $sql)){
												while($row = mysqli_fetch_assoc($result)){
										?>
                              <option value="<?php echo $row['idplan']; ?>" <?php echo $selected; ?>><?php echo $row['plan']; ?></option>
										<?php 
													$selected = "";
												}
											}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
										?>
                           </select>
                        </div>
                        <div class="form-group select">
                           <label for="periodo" class="control-label">PERIODO *</label>
                           <select name="periodo" class="form-control">
										<?php
											for ($i = 1; $i <= 12; $i++) {
										?>
                              <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
										<?php 
												$selected = "";
											}
										?>
                           </select>
                        </div>
                     </fieldset>
							<div class="row">
								<button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Guardar</button>
								<img id="enviando" src="images/barra.gif" width="100px" height="25px"
									style="position: relative; vertical-align:middle; display: none;">
							</div>
						</form>
               </div>
            </div>
         </div>
         <!-- Zona de mensajes -->
         <div id="mensajes"></div>
         <!-- FIN Zona de mensajes -->
      </section>
      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
   </body>
</html>

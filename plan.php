<?php 
   session_start();
   session_unset();
?>
<!DOCTYPE HTML>
<html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
  <meta http-equiv="content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
  <title>Polilegal |Contacto</title>
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
  <link id="default-css" href="style.css" rel="stylesheet" type="text/css">
  <link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" /> 
<!--[if IE 7]>
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lt IE 9]>
<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--Fonts-->
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--jquery-->
<script src="js/modernizr-2.6.2.min.js"></script>
<style>
.column2{
    float: left;
    margin: 0px 0px 0px 17%;
}    
</style>
</head>
<body>
	<!--wrapper starts-->
  <div class="wrapper">
    <!--inner-wrapper starts-->
    <div class="inner-wrapper">
      <!--header starts-->
		<?php include 'header.php'; ?>
      <!--header ends-->
            <!--main starts-->
            <div id="main">
              
              <div class="breadcrumb-section">
                <div class="container">
                  <h1> Plan de Asistencia Jurídica Cooperativa Policía Nacional – PoliLegal Abogados S.A. </h1>
                  <div class="breadcrumb">
                    <a href="index.php"> Inicio </a>
                    <span class="current"> Plan </span>
                  </div>
                </div>
              </div>
              
              <section id="primary" class="content-full-width">
                <!--container starts-->
                <div class="container">
                    <span class="dt-sc-highlight skin-color"> <strong> El Plan de Asistencia Jurídica </strong> </span><br><br>
                  <p style="text-align: justify"> A través de la alianza estratégica entre la Cooperativa Policía Nacional Ltda. y PoliLegal Abogados S.A., te damos la bienvenida a nuestro Plan de Asistencia Jurídica para miembros policiales en servicio activo, pasivo (SP) y socios civiles con los siguientes beneficios: <br>
                  <ul>
                      <li><img src="images1/100cobertura.png" width="50px" height="50px">100% de cobertura.</li>
                      <li><img src="images1/atencion.png" width="50px" height="50px">Atención ilimitada.</li>
                      <li><img src="images1/abogado.png" width="50px" height="50px">Abogados especializados.</li>
                      <li><img src="images1/cnacional.png" width="50px" height="50px">Cobertura a nivel nacional.</li>
                  </ul>
                  <img src="images1/jutnos3.png" style="position:absolute; margin-left: 50%; margin-top: -15%;width: 400px;">
                  <br>
                  <span class="dt-sc-highlight skin-color"> <strong> ¿Qué es el Plan de Asistencia Jurídica CPN-POLILEGAL? </strong> </span><br><br>Es un paquete exclusivo de servicios jurídicos integrales para los socios de la Cooperativa Policía Nacional Ltda., cuya finalidad es salvaguardar los intereses de los beneficiarios brindando patrocinio y asesoría jurídica en el ámbito policial y en otras ramas del Derecho, con una excelente tarifa y cobertura completa.<br>
                  
                  </p>
                </div>
                <!--container ends-->    
                <div class="dt-sc-hr-invisible-medium"></div>
                
                <!--fullwidth-background starts-->
                <div class="fullwidth-background dt-sc-parallax-section trust_section">
                  <div class="container">
                  </div>
                </div>
                <!--fullwidth-background ends-->
                
                <!--container starts-->
                <div class="container">
                  
                  <div class="dt-sc-one-fourth column2">
                    <a href="plan1.php"><div class="dt-sc-colored-box">
                      <h5> Plan de Asistencia Jurídica para socios policías </h5>
                    </div></a>
                  </div>
                  
                  <div class="dt-sc-one-fourth column2">
                    <a href="plan2.php"><div class="dt-sc-colored-box">
                      <h5> Plan de Asistencia Jurídica para socios SP y civiles </h5>
                    </div></a>
                  </div>
                  
                  <div class="container">	
                    
                    <!--primary starts-->
                    <section id="primary" class="content-full-width">
                      <div class="dt-sc-hr-invisible-small"></div>
                   </div>
                   
                   
                   <!--dt-sc-tabs-container ends-->
                 </div>
                 <!--dt-sc-three-fourth ends-->
                 <!--dt-sc-one-fourth starts-->
                 <!--dt-sc-one-fourth ends-->
                 
                 <div class="dt-sc-clear"></div>
                 <div class="dt-sc-hr-invisible"></div>

               </section>
               <!--primary ends-->
               
             </div>

             <!--dt-sc-one-fourth ends-->
             
             <div class="dt-sc-clear"></div>

           </section>
           <!--container starts-->
           <div class="dt-sc-hr-invisible-very-small"></div>
           <!--dt-sc-toggle-frame-set starts-->
           <div align="center" class="container">
            <div class="column dt-sc-one-fifth first">
              <div class=" type2">
                <div class="icon"> <span class=""> </span> </div>
                <h6><a href="#" target="_blank"> </a></h6>
              </div>
            </div>
            <div class="column dt-sc-one-fifth first">
              <div class="dt-sc-ico-content type2">
                <a> <img src="images1/mapa.png"/></a>
                <h6><a href="#" target="_blank"> Cobertura nacional </a></h6>
              </div>
            </div>
            <div class="column dt-sc-one-fifth">
              <div class="dt-sc-ico-content type2">
                <a> <img src="images1/estrella.png"/></a>
                <h6><a href="#" target="_blank"> Ámbito policial </a></h6>
              </div>
            </div>
            <div class="column dt-sc-one-fifth">
              <div class="dt-sc-ico-content type2">
                <a> <img src="images1/foco.png"/></a>
                <h6><a href="#" target="_blank"> Soluciones óptimas </a></h6>
              </div>
            </div>
          </div>
          
          <div class="dt-sc-hr-invisible-very-large"></div>   
          <div class=container>
            <div class="dt-sc-toggle-frame-set">
              <div class="dt-sc-toggle-frame">
                <h5 class="dt-sc-toggle-accordion"><a href="#"> Costo </a></h5>
                <div class="dt-sc-toggle-content">
                  <div class="block">
                    El Plan de Asistencia Jurídica tiene un costo de 6.99 USD (tarifa incluye impuestos) 
                  </div>
                </div>
              </div>
             <div class="dt-sc-toggle-frame">
              <h5 class="dt-sc-toggle-accordion"><a href="#">El socio puede ingresar a cualquiera de nuestros Planes mediante: </a></h5>
              <div class="dt-sc-toggle-content">
                <div class="block">
                  • Débito mensual de su cuenta de ahorros CPN.<br>
		  • Débito mensual de su cuenta en otra institución financiera.
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
     
     
     <div class="dt-sc-hr-invisible-large"></div>
     <!--container ends-->
     <div class="fullwidth-background dt-sc-parallax-section benefits_section">
      <div align="center" class="container">
        
        <!--dt-sc-one-half starts-->
        <div class="dt-sc-one-half textwidget first">
          <h5> EL SERVICIO JURÍDICO QUE ESTABAS ESPERANDO, CON LA MEJOR TARIFA Y COBERTURA, ¡CONTRÁTALO YA! <br><br> EL ABOGADO QUE NECESITAS, MÁS CERCA DE TI </h5>
        </div>
        <div class="dt-sc-hr-invisible-large"></div>
        
      </div>
    </div>     
    <div class="dt-sc-hr-invisible-large"></div>
    <!--main ends-->
    <div class="container">
      
      
    </div> 
    <!--footer starts-->
		<?php include 'footer.php'; ?>
    <!--footer ends-->
  </div>
  <!--inner-wrapper ends-->    
</div>
<!--wrapper ends-->
<!--wrapper ends-->
<!--wrapper ends-->
<?php include "asistencias.php"; ?>
<a href="" title="Go to Top" class="back-to-top"> <span class="fa fa-angle-up"></span> </a> 
<!--Java Scripts-->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-migrate.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-easing-1.3.js"></script>
<script type="text/javascript" src="js/jquery.sticky.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>


<script type="text/javascript" src="js/jquery.tabs.min.js"></script>
<script type="text/javascript" src="js/jquery.smartresize.js"></script> 
<script type="text/javascript" src="js/shortcodes.js"></script>   

<script type="text/javascript" src="js/custom.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDz1RGAW3gGyDtvmBfnH2_fE2DVVNWq4Eo&callback=initMap" type="text/javascript"></script>
<script src="js/gmap3.min.js"></script>
<!-- Layer Slider --> 
<script type="text/javascript" src="js/jquery-transit-modified.js"></script> 
<script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script> 
<script type='text/javascript' src="js/greensock.js"></script> 
<script type='text/javascript' src="js/layerslider.transitions.js"></script> 

<script type="text/javascript">var lsjQuery = jQuery;</script><script type="text/javascript"> lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice('layerslider_2','jquery'); } else { lsjQuery("#layerslider_2").layerSlider({responsiveUnder: 1240, layersContainer: 1170, skinsPath: 'js/layerslider/skins/'}) } }); </script>



</body>
                     <!-- Cerrar Session -->
                     <script>
                        jQuery(document).ready(function(){
                           jQuery("#close_session").click(function(){
                              alert('Su sesión ha sido cerrada');
                              window.location.href='closesession.php';
                           });
                        });
                     </script></html>

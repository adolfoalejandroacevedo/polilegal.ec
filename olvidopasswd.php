<?php 
   session_start();
   session_unset();
?>
<!DOCTYPE HTML>
<html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
    <meta http-equiv="content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
    <title>Polilegal |Contacto</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
    <link id="default-css" href="style.css" rel="stylesheet" type="text/css">
    <link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" /> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!--[if IE 7]>
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lt IE 9]>
<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--Fonts-->
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--jquery-->
<script src="js/modernizr-2.6.2.min.js"></script>
</head>
<body>
	<!--wrapper starts-->
    <div class="wrapper">
        <!--inner-wrapper starts-->
        <div class="inner-wrapper">
            <!--header starts-->
				<?php include 'header.php'; ?>
            <!--header ends-->
                    <!--main starts-->
                    <div id="main">
                        
                        <div class="breadcrumb-section">
                            <div class="container">
                                <h1>RECUPERAR CONTRASEÑA</h1>
                                <div class="breadcrumb">
                                    <a href="index.php"> Inicio </a>
                                    <span class="current"> Recuperar Contraseña </span>
                                </div>
                            </div>
                        </div>
                        
                        <!--container starts-->
                        <div class="container">	
                            
                            <!--primary starts-->
                            <section id="primary" class="content-full-width" style="margin-top: 10px;">
                                
                                <!--dt-sc-three-fourth starts-->
                                 <!--dt-sc-tabs-container ends-->
                             <!--dt-sc-three-fourth ends-->
                             <!--dt-sc-one-fourth starts-->
                            <!--dt-sc-one-fourth ends-->
                            
                  	  <div class="dt-sc-three-fourth column first">
                    	    <br>
                    	    <form name="frmolvidopasswd" method="post" class="contact-form" action="recoverpasswd.php" enctype="multipart/form-data">
                              <input type="hidden" name="tipo" value="5">
                              <p><input type="email" placeholder="Correo" class="text_input" name="email" required /></p>
							  <a style="color: #1d3e88;" href="registro.php">Crear una Cuenta</a> | 
							  <a style="color: #1d3e88;" href="olvidopasswd.php">¿Olvidó su contraseña?</a>
                              <!-- Zona de mensajes -->
                              <div id="mensajes"></div>
                              <!-- Fin zona de mensajes -->
                              <div id="ajax_contact_msg"> </div>
                              <p class="aligncenter">
                                <input name="submit" type="submit" id="submit" class="dt-sc-bordered-button" value="Enviar">
                                <img id="enviando" src="images/loading.gif" width="100px" height="100px" 
								style="position: relative; vertical-align:middle; display: none;">
                              </p>
                            </form>
                          </div>

                            <div class="dt-sc-hr-invisible-large"></div>
                            
                            
                            
                        </section>
                        <!--primary ends-->
                        
                    </div>
                    <!--container ends-->
                    
                    
                </div>
                <!--main ends-->
                
                <!--footer starts-->
					 <?php include 'footer.php'; ?>
                <!--footer ends-->
    </div>
    <!--inner-wrapper ends-->    
</div>
<!--wrapper ends-->
<?php include "asistencias.php"; ?>
<a href="" title="Go to Top" class="back-to-top"> <span class="fa fa-angle-up"></span> </a>    <!--Java Scripts-->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-migrate.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-easing-1.3.js"></script>
<script type="text/javascript" src="js/jquery.sticky.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>


<script type="text/javascript" src="js/jquery.tabs.min.js"></script>
<script type="text/javascript" src="js/jquery.smartresize.js"></script> 
<script type="text/javascript" src="js/shortcodes.js"></script>   

<script type="text/javascript" src="js/custom.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDz1RGAW3gGyDtvmBfnH2_fE2DVVNWq4Eo&callback=initMap" type="text/javascript"></script>
<script src="js/gmap3.min.js"></script>
<!-- Layer Slider --> 
<script type="text/javascript" src="js/jquery-transit-modified.js"></script> 
<script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script> 
<script type='text/javascript' src="js/greensock.js"></script> 
<script type='text/javascript' src="js/layerslider.transitions.js"></script> 

<script type="text/javascript">var lsjQuery = jQuery;</script><script type="text/javascript"> lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice('layerslider_2','jquery'); } else { lsjQuery("#layerslider_2").layerSlider({responsiveUnder: 1240, layersContainer: 1170, skinsPath: 'js/layerslider/skins/'}) } }); </script>

<script>
    jQuery(document).ready(function() {
        jQuery("#mensajes").click(function(){
            jQuery("#mensajes").hide();
        });
    });
</script>

                     <!-- Cerrar Session -->
                     <script>
                        jQuery(document).ready(function(){
                           jQuery("#close_session").click(function(){
                              alert('Su sesión ha sido cerrada');
                              window.location.href='closesession.php';
                           });
                        });
                     </script>

</body>
</html>

<?php session_start(); ?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Plataforma Digital de Abogados del Ecuador | Beneficios al Registrarse </title>
		<meta name="keywords" content="abogado, abogados, ecuador, quito, guayaquil, cuenca, loja, ibarra, ambato, manta">
		<meta name="description" content="Beneficios al formar parte de tuabogado.ec." />
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <link href="css/animate.min.css" rel="stylesheet">
      <link href="css/prettyPhoto.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="bower_components/datatables.net-dt/css/jquery.dataTables.css">
      <link rel="stylesheet" type="text/css" href="css/bootstrap-markdown.min.css">
      <style>
         .pricingTable{
         text-align: center;
         transition: all 0.5s ease 0s;
         }
         .pricingTable:hover{
         box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
         }
         .pricingTable .pricingTable-header{
         color: #feffff;
         }
         .pricingTable .heading{
         display: block;
         padding-top: 25px;
         }
         .pricingTable .heading > h3{
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .textotitle{
         color: #feffff;
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .pricingTable .subtitle{
         display: block;
         font-size: 13px;
         margin-top: 5px;
         text-transform: capitalize;
         }
         .pricingTable .price-value{
         display: block;
         font-size: 60px;
         font-weight: 700;
         padding-bottom: 25px;
         }
         .pricingTable .price-value span{
         display: block;
         font-size: 14px;
         line-height: 20px;
         text-transform: uppercase;
         }
         .pricingTable .pricingContent{
         /*text-transform: capitalize;*/
         background: #fbfbfb;
         color: #fefeff;
         }
         .pricingTable .pricingContent ul{
         list-style: none;
         padding: 15px 14px 10px;
         margin: 0;
         text-align: left;
         }
         .pricingTable .pricingContent ul li{
         font-size: 14px;
         padding: 12px 0;
         border-bottom: 1px dashed #e1e1e1;
         color: #9da1ad;
         }
         .pricingTable .pricingContent ul li i{
         font-size: 14px;
         float: right;
         }
         .pricingTable .pricingTable-sign-up{
         padding: 20px 0;
         background: #fbfbfb;
         color: #fff;
         text-transform: capitalize;
         }
         .pricingTable .btn-block{
         width: 60%;
         margin: 0 auto;
         font-size: 17px;
         color: #fff;
         text-transform: capitalize;
         border: none;
         border-radius: 5px;
         padding: 10px;
         transition: all 0.5s ease 0s;
         }
         .pricingTable .btn-block:before{
         content: "\f007";
         font-family: 'FontAwesome';
         margin-right: 10px;
         }
         .pricingTable.blue .pricingTable-header,
         .pricingTable.blue .btn-block{
         background: #fce400;
         }
         .pricingTable.pink .pricingTable-header,
         .pricingTable.pink .btn-block{
         background: #c9b600;
         }
         .pricingTable.orange .pricingTable-header,
         .pricingTable.orange .btn-block{
         background: #e0d83a;
         }
         .pricingTable.green .pricingTable-header,
         .pricingTable.green .btn-block{
         background: #e0cd00;
         }
         .pricingTable.blue .btn-block:hover,
         .pricingTable.pink .btn-block:hover,
         .pricingTable.orange .btn-block:hover,
         .pricingTable.green .btn-block:hover{
         background: #e6e6e6;
         color: #939393;
         }
         @media screen and (max-width: 990px){
         .pricingTable{ margin-bottom: 20px; }
         }
      </style>
   </head>
   <body class="homepage">
<?php include "header.php"; ?>
      <!--/header-->
      <section id="services" class="service-item">
         <div class="container">
            <div class="center wow fadeInDown">
               <h2>¿Qué beneficios obtienes al formar parte de <strong style="color: #f6dc00;">tuabogado.ec?</strong></h2>
            </div>
            <div class="row">
               <div class="col-sm-6 col-md-4">
                  <div class="media services-wrap wow fadeInDown">
                     <div class="pull-left">
                        <img class="img-responsive" src="images/services/services1.png">
                     </div>
                     <div class="media-body">
                        <h3 class="media-heading">Tienes presencia digital </h3>
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-4">
                  <div class="media services-wrap wow fadeInDown">
                     <div class="pull-left">
                        <img class="img-responsive" src="images/services/services8.png">
                     </div>
                     <div class="media-body">
                        <h3 class="media-heading">Llegas a nuevos clientes</h3>
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-4">
                  <div class="media services-wrap wow fadeInDown">
                     <div class="pull-left">
                        <img class="img-responsive" src="images/services/services7.png" style="width: 81px; height: 70px;">
                     </div>
                     <div class="media-body">
                        <h3 class="media-heading">Das a conocer tus servicios</h3>
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-4">
                  <div class="media services-wrap wow fadeInDown">
                     <div class="pull-left">
                        <img class="img-responsive" src="images/services/services4.png">
                     </div>
                     <div class="media-body">
                        <h3 class="media-heading">Te destacas de la competencia</h3>
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-4">
                  <div class="media services-wrap wow fadeInDown">
                     <div class="pull-left">
                        <img class="img-responsive" src="images/services/services5.png">
                     </div>
                     <div class="media-body">
                        <h3 class="media-heading">Potencias tu contactabilidad</h3>
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-4">
                  <div class="media services-wrap wow fadeInDown">
                     <div class="pull-left">
                        <img class="img-responsive" src="images/services/services6.png" style="width: 81px; height: 81px;">
                     </div>
                     <div class="media-body">
                        <h3 class="media-heading">Crecimiento profesional</h3>
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-4">
                  <div class="media services-wrap wow fadeInDown">
                     <div class="pull-left">
                        <img class="img-responsive" src="images/services/services9.png">
                     </div>
                     <div class="media-body">
                        <h3 class="media-heading">Formas parte de la innovación</h3>
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-4">
                  <div class="media services-wrap wow fadeInDown">
                     <div class="pull-left">
                        <img class="img-responsive" src="images/services/services10.png">
                     </div>
                     <div class="media-body">
                        <h3 class="media-heading">Generas mayores oportunidades</h3>
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-4">
                  <div class="media services-wrap wow fadeInDown">
                     <div class="pull-left">
                        <img class="img-responsive" src="images/services/services3.png">
                     </div>
                     <div class="media-body">
                        <h3 class="media-heading">Transmites seriedad</h3>
                     </div>
                  </div>
               </div>
            </div>
            <!--/.row-->
         </div>
         <!--/.container-->
      </section>
      <!--/#services-->
      <section id="feature" class="transparent-bg">
         <div class="container">
            <div class="get-started center wow fadeInDown">
               <h2 style="font-style: italic">TU PRESENCIA DIGITAL HOY, DETERMINARÁ TU ÉXITO PROFESIONAL MAÑANA.</h2>
               <h1><a href="planes.php">REGÍSTRATE AHORA</a></h1>
            </div>
            <!--/.get-started-->
         </div>
         <!--/.container-->
      </section>
      <!--/#feature-->
      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
    <!--/#footer-->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/markdown.js"></script>
      <script src="js/to-markdown.js"></script>
      <script src="js/bootstrap-markdown.js"></script>
      <script src="js/bootstrap-markdown.es.js"></script>
      <script type="text/javascript" src="js/jquery.simpleslider.js"></script>
      <script type="text/javascript" charset="utf8" src="bower_components/datatables.net/js/jquery.dataTables.min.js"> </script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/jquery.isotope.min.js"></script>
      <script src="js/wow.min.js"></script>
      <script src="js/main.js"></script>
   </body>
</html>


<?php 
   session_start();
   session_unset();
?>
<!DOCTYPE HTML>

<html lang="en-gb" class="no-js">
<head>
  <meta http-equiv="content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <title>Polilegal| Inicio</title>
  <meta name="description" content="">
  <meta name="author" content="adolfo" >
  <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
  <link id="default-css" href="style.css" rel="stylesheet" type="text/css">
  <link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/flip.css" rel="stylesheet" type="text/css">
  <link rel='stylesheet' id='layerslider-css' href="css/layerslider.css" type='text/css' media='all' />
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" />
  <!-- Boostrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<!--[if IE 7] -->
  <!--Fonts-->
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <!--jquery-->
  <script src="js/modernizr-2.6.2.min.js"></script>
</head>
<body>
  <!--wrapper starts-->
  <div class="wrapper">
    <!--inner-wrapper starts-->
    <div class="inner-wrapper">
      <!--header starts-->
		<?php include 'header.php'; ?>
      <!--header ends--> 
            <!--main starts-->
            <div id="main"> 
               <div class="alert alert-success alert-dismissible fade in" role="alert" align="center" 
               style="margin-top: 10%;
               		margin-bottom: 10%;
               		margin-left: 15%;
               		margin-right: 15%;">
               	<h1 class="alert-heading">!Bienvenido!</h1>
               	<h3>A tu correo electrónico te llegará el comprobante de transacción y la guía de usuario del servicio POLILEGAL.</h3>
            	</div>
            </div>                    
            <!--footer starts-->                    
            <?php include 'footer.php'; ?>
            <!--footer ends--> 
    </div>
  </div>
  <?php include "asistencias.php"; ?>
  <a href="" title="Go to Top" class="back-to-top"> <span class="fa fa-angle-up"></span> </a> 
  <!--Java Scripts--> 

  <script type="text/javascript" src="js/jquery.js"></script> 
  <script type="text/javascript" src="js/jquery-migrate.min.js"></script> 
  <script type="text/javascript" src="js/jquery.validate.min.js"></script> 
  <script type="text/javascript" src="js/jquery-easing-1.3.js"></script> 
  <script type="text/javascript" src="js/jquery.sticky.js"></script> 
  <script type="text/javascript" src="js/jquery.nicescroll.min.js"></script> 
  <script type="text/javascript" src="js/jquery.smartresize.js"></script> 
  <script type="text/javascript" src="js/shortcodes.js"></script> 
  <script type="text/javascript" src="js/custom.js"></script> 

  <!-- Layer Slider --> 
  <script type="text/javascript" src="js/jquery-transit-modified.js"></script> 
  <script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script> 
  <script type='text/javascript' src="js/greensock.js"></script> 
  <script type='text/javascript' src="js/layerslider.transitions.js"></script> 
  <script type="text/javascript">
    var lsjQuery = jQuery;</script><script type="text/javascript"> lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice('layerslider_2','jquery'); } else { lsjQuery("#layerslider_2").layerSlider({responsiveUnder: 1240, layersContainer: 1170, skinsPath: 'js/layerslider/skins/'}) } }); </script>

    <!--script flip-->
    <script type='text/javascript' src="js/flip/jquery.flip.min.js"></script> 
    <script type='text/javascript' src="js/flip/script.js"></script> 

   <!-- Cerrar Session -->
   <script>
      jQuery(document).ready(function(){
         jQuery("#close_session").click(function(){
            alert('Su sesión ha sido cerrada');
            window.location.href='closesession.php';
         });
      });
   </script>
</body>       

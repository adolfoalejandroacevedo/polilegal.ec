<?php 
	session_start(); 
	include "detect_mobile.php";
?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Plataforma Digital de Abogados del Ecuador | Quienes Somos</title>
		<meta name="keywords" content="abogado, ecuador, abogados, quito, guayaquil, manta, portoviejo, ibarra, ambato, riobamba, esmeraldas, accidente, transito, penal, civil, deuda, derechos, apelacion, policial">
		<meta name="description" content="TUABOGADO.EC es la plataforma digital de abogados más completa del Ecuador, que a través de su territorialidad ecuatoriana .ec ofrece una herramienta única, innovadora y muy útil para el ciudadano y para los profesionales juristas de todo el país." />
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <link href="css/animate.min.css" rel="stylesheet">
      <link href="css/prettyPhoto.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="bower_components/datatables.net-dt/css/jquery.dataTables.css">
      <link rel="stylesheet" type="text/css" href="css/bootstrap-markdown.min.css">
      <style>
			.ml2 {
			  font-weight: 900;
			  font-size: 1.5em;
			  padding-top: 100px;
			  color: #0b57a5;
			}

			.ml2 .letter {
			  display: inline-block;
			  line-height: 1em;
			  color: #0b57a5;
			}
         .pricingTable{
         text-align: center;
         transition: all 0.5s ease 0s;
         }
         .pricingTable:hover{
         box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
         }
         .pricingTable .pricingTable-header{
         color: #feffff;
         }
         .pricingTable .heading{
         display: block;
         padding-top: 25px;
         }
         .pricingTable .heading > h3{
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .textotitle{
         color: #feffff;
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .pricingTable .subtitle{
         display: block;
         font-size: 13px;
         margin-top: 5px;
         text-transform: capitalize;
         }
         .pricingTable .price-value{
         display: block;
         font-size: 60px;
         font-weight: 700;
         padding-bottom: 25px;
         }
         .pricingTable .price-value span{
         display: block;
         font-size: 14px;
         line-height: 20px;
         text-transform: uppercase;
         }
         .pricingTable .pricingContent{
         /*text-transform: capitalize;*/
         background: #fbfbfb;
         color: #fefeff;
         }
         .pricingTable .pricingContent ul{
         list-style: none;
         padding: 15px 14px 10px;
         margin: 0;
         text-align: left;
         }
         .pricingTable .pricingContent ul li{
         font-size: 14px;
         padding: 12px 0;
         border-bottom: 1px dashed #e1e1e1;
         color: #9da1ad;
         }
         .pricingTable .pricingContent ul li i{
         font-size: 14px;
         float: right;
         }
         .pricingTable .pricingTable-sign-up{
         padding: 20px 0;
         background: #fbfbfb;
         color: #fff;
         text-transform: capitalize;
         }
         .pricingTable .btn-block{
         width: 60%;
         margin: 0 auto;
         font-size: 17px;
         color: #fff;
         text-transform: capitalize;
         border: none;
         border-radius: 5px;
         padding: 10px;
         transition: all 0.5s ease 0s;
         }
         .pricingTable .btn-block:before{
         content: "\f007";
         font-family: 'FontAwesome';
         margin-right: 10px;
         }
         .pricingTable.blue .pricingTable-header,
         .pricingTable.blue .btn-block{
         background: #fce400;
         }
         .pricingTable.pink .pricingTable-header,
         .pricingTable.pink .btn-block{
         background: #c9b600;
         }
         .pricingTable.orange .pricingTable-header,
         .pricingTable.orange .btn-block{
         background: #e0d83a;
         }
         .pricingTable.green .pricingTable-header,
         .pricingTable.green .btn-block{
         background: #e0cd00;
         }
         .pricingTable.blue .btn-block:hover,
         .pricingTable.pink .btn-block:hover,
         .pricingTable.orange .btn-block:hover,
         .pricingTable.green .btn-block:hover{
         background: #e6e6e6;
         color: #939393;
         }
         @media screen and (max-width: 990px){
         .pricingTable{ margin-bottom: 20px; }
         }
      </style>
   </head>
   <body class="homepage">
<?php include "header.php"; ?>
      <!--/header-->
      <section id="about-us">
         <div class="container">
            <div class="skill-wrap clearfix">
               <div class="center wow fadeInDown">
                  <h2>¿Quiénes <span>somos?</span></h2>
                  <p class="lead">TUABOGADO.EC es la plataforma digital de abogados más completa del Ecuador, que a través de su territorialidad ecuatoriana .ec ofrece una herramienta única, innovadora y muy útil para el ciudadano y para los profesionales juristas de todo el país.  TUABOGADO.EC, será, en corto tiempo, la guía más completa de Abogados ecuatorianos en todas las especialidades, a través de la cual, los visitantes podrán contactar y contratar directamente los servicios de los mejores juristas del país.</p>
               </div>
               <div class="row team-bar">
                  <div class="first-one-arrow hidden-xs">
                     <hr>
                  </div>
                  <div class="first-arrow hidden-xs">
                     <hr>
                     <i class="fa fa-angle-up"></i>
                  </div>
                  <div class="second-arrow hidden-xs">
                     <hr>
                     <i class="fa fa-angle-down"></i>
                  </div>
                  <div class="third-arrow hidden-xs">
                     <hr>
                     <i class="fa fa-angle-up"></i>
                  </div>
                  <div class="fourth-arrow hidden-xs">
                     <hr>
                     <i class="fa fa-angle-down"></i>
                  </div>
               </div>
               <!--skill_border-->       
               <div class="center wow fadeInDown">
                  <h2>Nuestros <span>Objetivos</span></h2>
                  <p>TUABOGADO.EC tiene dos objetivos fundamentales: </p>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div>
								<p class="lead"><i class="fas fa-angle-double-right"></i> Facilitar y ayudar a las personas a solucionar sus problemas y asuntos legales de la forma más sencilla y con profesionales del derecho de alto nivel.  Nos enfocamos en que los ciudadanos puedan encontrar, contactar y contratar los servicios de los mejores Abogados del país.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-12">
                     <div class=" wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div>
                           <p class="lead"><i class="fas fa-angle-double-right"></i> Ser una de las principales fuentes de oportunidades para el crecimiento y posicionamiento de los Abogados y Estudios Jurídicos en el Ecuador.  Ofrecemos a los Abogados ecuatorianos la posibilidad de alcanzar más clientes potenciales, brindándoles una plataforma vanguardista para que den a conocer sus servicios.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row team-bar">
               <div class="first-one-arrow hidden-xs">
                  <hr>
               </div>
               <div class="first-arrow hidden-xs">
                  <hr>
                  <i class="fa fa-angle-up"></i>
               </div>
               <div class="second-arrow hidden-xs">
                  <hr>
                  <i class="fa fa-angle-down"></i>
               </div>
               <div class="third-arrow hidden-xs">
                  <hr>
                  <i class="fa fa-angle-up"></i>
               </div>
               <div class="fourth-arrow hidden-xs">
                  <hr>
                  <i class="fa fa-angle-down"></i>
               </div>
            </div>
            <!--skill_border-->       
            <!-- our-team -->
            <div class="team">
               <div class="center wow fadeInDown">
                  <h2>¿Por qué <span>TUABOGADO.EC?</span></h2>
                  <p class="lead">El mundo cada vez es más digital y el número de personas que utilizan el internet para adquirir bienes y servicios crece a pasos agigantados.  Los Abogados cada día están más conectados a la red, empleando recursos efectivos para demostrar su profesionalismo y acceder de manera directa a sus clientes.  Tuabogado.ec es la opción más innovadora, y un canal directo y seguro para afianzar las relaciones entre cliente – Abogado, creando beneficios para ambas partes. </p>
						<?php if($_SESSION['mobile']) { ?>						
							<h1 class="ml2 letter" style="margin-bottom: 0px;">EN ESTA ERA TECNOLÓGICA,</h1>
							<h1 class="ml2 letter" style="margin-bottom: 0px; margin-top: 0px; padding-top: 0px;">TENER PRESENCIA DIGITAL</h1>
							<h1 class="ml2 letter" style="margin-bottom: 0px; margin-top: 0px; padding-top: 0px;">ES VITAL PARA CUALQUIER</h1>
							<h1 class="ml2 letter" style="margin-bottom: 0px; margin-top: 0px; padding-top: 0px;">ABOGADO.</h1>
						<?php }else{ ?>
							<h1 class="ml2 letter" >EN ESTA ERA TECNOLÓGICA, TENER PRESENCIA DIGITAL ES VITAL PARA CUALQUIER ABOGADO.</h1>
						<?php } ?>	
               </div>
            </div>
            <!--section-->
         </div>
         <!--/.container-->
      </section>
      <!--/about-us-->
      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/markdown.js"></script>
      <script src="js/to-markdown.js"></script>
      <script src="js/bootstrap-markdown.js"></script>
      <script src="js/bootstrap-markdown.es.js"></script>
      <script type="text/javascript" src="js/jquery.simpleslider.js"></script>
      <script type="text/javascript" charset="utf8" src="bower_components/datatables.net/js/jquery.dataTables.min.js"> </script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/jquery.isotope.min.js"></script>
      <script src="js/wow.min.js"></script>
      <script src="js/main.js"></script>

		<script>
			// Wrap every letter in a span
			$('.ml2').each(function(){
			  $(this).html($(this).text().replace(/([^\x00-\x80]|\w|\.|\,)/g, "<span class='letter'>$&</span>"));
			});

			anime.timeline({loop: true})
			  .add({
				 targets: '.ml2 .letter',
				 scale: [4,1],
				 opacity: [0,1],
				 translateZ: 0,
				 easing: "easeOutExpo",
				 duration: 950,
				 delay: function(el, i) {
					return 70*i;
				 }
			  }).add({
				 targets: '.ml2',
				 opacity: 0,
				 duration: 1000,
				 easing: "easeOutExpo",
				 delay: 1000
			  });
		</script>
   </body>
</html>



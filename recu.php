<?php
header('Content-Type: text/html; charset=utf-8');
session_start();
$config = require 'config.php';
$conn=mysqli_connect(
      $config['database']['server'],
      $config['database']['username'],
      $config['database']['password'],
      $config['database']['db']
);
if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
mysqli_set_charset($conn, "utf8");

//Obtencion del IVA
$sql = "SELECT costo FROM productos WHERE id='2'";
if($result = mysqli_query($conn, $sql)){
	$row = mysqli_fetch_assoc($result);
	$iva = $row['costo'];;
}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

//Iniciamos el bucle leyendo las recurrencias del dia.
/*
$sql = "SELECT df,
					userid,
					token,
					amount,
					plan,
					description,
					installments_type,
					tax_percentage,
					periodo
			FROM recurrencias t1
			INNER JOIN planes t2 ON description = t2.id
			WHERE DATE_FORMAT(fechaexec, '%Y-%m-%d') = curdate() AND active='1'";
*/
$sql = "SELECT df,
					userid,
					token,
					amount,
					concepto,
					description,
					installments_type,
					tax_percentage,
					periodo
			FROM recurrencias t1
			INNER JOIN productos t2 ON description = t2.id
			WHERE active='1'";
if($result = mysqli_query($conn, $sql)) {
	while($row = mysqli_fetch_assoc($result)){
		
		//Buscamos nombre y apellido del usuario
		$sql = "SELECT nombres FROM afiliacion WHERE email='$row[userid]'";
		if($result2 = mysqli_query($conn, $sql)){
			$row2 = mysqli_fetch_assoc($result2);
			$nombres = $row2['nombres'];
			$_SESSION["id"] = $row['userid'];
		}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

		//Calculamos el resto de las variables
		$dev_reference = time();
		settype($row['amount'], "float");
		settype($row['tax_percentage'], "integer");
		settype($row['installments_type'], "integer");
		$taxable_amount = round($row['amount']/(1+$iva/100),2);
		$vat = round($taxable_amount*$row['tax_percentage']/100,2);
		$periodo = $row['periodo'];
		$description = $row['description'];
		$plan = $row['concepto'];

		//Cabecera del API
		$userId = $row['userid'];
		$token = $row['token'];
		$auth_timestamp = time();
		$application_code = $config['paymentez']['server_app_code'];
		$appkey = $config['paymentez']['server_app_key'];
		$uniq_token_string = $appkey.$auth_timestamp;
		$uniq_token_hash = hash('sha256', $uniq_token_string);
		$auth_token = base64_encode($application_code.';'.$auth_timestamp.';'.$uniq_token_hash);

		//Parametros del API
		$param = array(
						"user" => array( 
										"id" => "$userId",
										"email" => "osalas@paymentez.com"),
						"order" => array(
										"amount" => $row['amount'],
										"description" => "$plan",
										"dev_reference" => "$dev_reference",
										"vat" => $vat,
										"taxable_amount" => $taxable_amount,
										"installments_type" => $row['installments_type'],
										"tax_percentage" => $row['tax_percentage']
										),
						"card" => array(
										"token" => "$row[token]")
						);
		$payload = json_encode($param);
		echo serialize($payload)."\n";

		//Execucion del API
		$ch = curl_init($config['paymentez']['environment']."/v2/transaction/debit/");
		curl_setopt ($ch, CURLOPT_POST, 1);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $payload);
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","Auth-Token: $auth_token"));
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		$respuesta = curl_exec ($ch);
		$error = curl_error($ch);
		curl_close ($ch);
		$data = json_decode($respuesta, TRUE);

		//Si fue exitoso el pago seguimos
		if (isset($data['transaction']['status']) && $data['transaction']['status'] == 'success'){

			//Nuevas variables de trabajo
			$df = $data['transaction']['id'];
			$token = $data['card']['token'];

			//Insertamos transaccion en recurrencias
			$sql = "INSERT INTO recurrencias (
							df,
							userid,
							token,
							amount,
							description,
							installments_type,
							tax_percentage,
							periodo,
							fechaexec)
						VALUES (
							'$df',
							'$userId',
							'$token',
							'$row[amount]',
							'$description',
							'$row[installments_type]',
							'$row[tax_percentage]',
							'$periodo',
							DATE_ADD(NOW(), INTERVAL $periodo MONTH))";
			if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

			//Inactivamos la recurrencia anterior
			$sql = "UPDATE recurrencias SET active='0' WHERE df='$row[df]'";
			if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

			//Enviamos correo
			sendmail($df,$row['amount'],$vat,$nombres,$data['transaction']['authorization_code'],$plan,$taxable_amount);

			echo "success";

		}else{

			echo "nopay"."\n";

		}
    print_r($data)."\n";
    error_log(print_r($data,true));
    echo serialize($payload)."\n";

	}
}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

function sendmail($df,$order_amount,$order_vat,$nombres,$txtCod,$order_description,$sub_total){
   $config = require 'config.php';

   //Enviando comprobnte de pago al cliente
   require 'PHPMailer/PHPMailerAutoload.php';
   include("emailpayrecu.php");
   $mail = new PHPMailer;
   $mail->isSMTP();
   $mail->CharSet = 'UTF-8';
   $mail->SMTPDebug = 0;
   //$mail->Debugoutput = 'html';
   //$mail->Host = 'smtp.polilegal.ec';
   $mail->Host = $config['general']['mailserver'];
   //$mail->Host = 'localhost';
   $mail->Port = 465;
   $mail->SMTPSecure = 'ssl';
   $mail->SMTPAuth = true;
   $mail->SMTPOptions = array(
     'ssl' => array(
       'verify_peer' => false,
       'verify_peer_name' => false,
       'allow_self_signed' => true
     )
   );
   $mail->Username = $config['general']['username'];
   $mail->Password = $config['general']['password'];
   $mail->setFrom('admin@polilegal.ec', 'Polilegal S.A.');
   $mail->addAddress($_SESSION['id'], '');
   $mail->addReplyTo('noreplyto@polilegal.ec', 'POLILEGAL S.A.');
   $mail->Subject = 'POLILEGAL.EC - Afiliación';
   $mail->msgHTML($cuerpo);
   //$mail->AddAttachment('Guia_del_usuario_POLILEGAL_2019.pdf');
   if (!$mail->send()) {
      $error = "Mailer Error: " . $mail->ErrorInfo;
      error_log($error);
       echo "Error al enviar correo";
   }

}

?>

<?php
	session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");
	$config = require 'config.php';

	//Se valida si existe una session admin previa
	if(!isset($_SESSION['idadmin'])) $_SESSION['idadmin'] = $_SESSION['id'];

	//Conexion a BD
	$conn=mysqli_connect($config['database']['server'],
			$config['database']['username'],
			$config['database']['password'],
			$config['database']['db']);
	if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);

	//Se busca al abogado
	$sql = "SELECT id,
						plan,
						plandestacado,
						role,
						email
				FROM users
				WHERE id='$_GET[id]'";

	//Se toma el control
	if($result = mysqli_query($conn, $sql)){
		$row = mysqli_fetch_assoc($result);
      $_SESSION['loggedIn'] = true;
      $_SESSION['email'] = $row['email'];
      $_SESSION['plan'] = $row['plan'];
      $_SESSION['plandestacado'] = $row['plandestacado'];
      $_SESSION['id'] = $row['id'];
      $_SESSION['role'] = $row['role'];
      $login = true;
		if($row['role'] == 'admin') header("location: admin.php");
		else header("location: miperfil.php");
	}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

?>

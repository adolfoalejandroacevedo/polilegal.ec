<?php


/**
 * This example shows settings to use when sending via Google's Gmail servers.
 * The IMAP section shows how to save this message to the 'Sent Mail' folder using IMAP commands.
 */

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Etc/UTC');

require '../PHPMailerAutoload.php';

//Create a new PHPMailer instance
$mail = new PHPMailer;

$post['tipo'] = $_POST['tipo'];
//paso 1
    $post['txtname'] = (isset($_POST['txtname'])) ? utf8_decode($_POST['txtname']):null;
    $post['txtemail'] = (isset($_POST['txtemail'])) ? utf8_decode($_POST['txtemail']):null;
    $post['txtsubject'] = (isset($_POST['txtsubject'])) ? utf8_decode($_POST['txtsubject']):null;
    $post['txtcedula'] = (isset($_POST['txtcedula'])) ? utf8_decode($_POST['txtcedula']):null;
    $post['txtmessage'] = (isset($_POST['txtmessage'])) ? utf8_decode($_POST['txtmessage']):null;
    $post['txttelefono'] = (isset($_POST['txttelefono'])) ? utf8_decode($_POST['txttelefono']):null;
    $post['txtciudad'] = (isset($_POST['txtciudad'])) ? utf8_decode($_POST['txtciudad']):null;
    $post['txtsector'] = (isset($_POST['txtsector'])) ? utf8_decode($_POST['txtsector']):null;
    $post['txtprovincia'] = (isset($_POST['txtprovincia'])) ? utf8_decode($_POST['txtprovincia']):null;
    $post['tipo-pago'] = (isset($_POST['tipo-pago'])) ? utf8_decode($_POST['tipo-pago']):null;

//paso 2 
//primera pestaña
    $post['hf_first_name'] = (isset($_POST['hf_first_name'])) ? utf8_decode($_POST['hf_first_name']):null;
    $post['hf_first_cedula'] = (isset($_POST['hf_first_cedula'])) ? utf8_decode($_POST['hf_first_cedula']):null;
    $post['hf_first_telefono'] = (isset($_POST['hf_first_telefono'])) ? utf8_decode($_POST['hf_first_telefono']):null;
    $post['hf_first_provincia'] = (isset($_POST['hf_first_provincia'])) ? utf8_decode($_POST['hf_first_provincia']):null;
    $post['hf_first_ciudad'] = (isset($_POST['hf_first_ciudad'])) ? utf8_decode($_POST['hf_first_ciudad']):null;
    $post['hf_first_cuenta'] = (isset($_POST['hf_first_cuenta'])) ? utf8_decode($_POST['hf_first_cuenta']):null;
    $post['hf_first_tipo_cuenta'] = (isset($_POST['hf_first_tipo_cuenta'])) ? utf8_decode($_POST['hf_first_tipo_cuenta']):null;
    $post['hf_first_condiciones_1'] = (isset($_POST['hf_first_condiciones_1'])) ? utf8_decode($_POST['hf_first_condiciones_1']):null;
//segunda pestaña
    $post['hf_secund_name'] = (isset($_POST['hf_secund_name'])) ? utf8_decode($_POST['hf_secund_name']):null;
    $post['hf_secund_cedula'] = (isset($_POST['hf_secund_cedula'])) ? utf8_decode($_POST['hf_secund_cedula']):null;
    $post['hf_secund_movil'] = (isset($_POST['hf_secund_movil'])) ? utf8_decode($_POST['hf_secund_movil']):null;
    $post['hf_secund_provincia'] = (isset($_POST['hf_secund_provincia'])) ? utf8_decode($_POST['hf_secund_provincia']):null;
    $post['hf_secund_ciudad'] = (isset($_POST['hf_secund_ciudad'])) ? utf8_decode($_POST['hf_secund_ciudad']):null;
    $post['hf_secund_institucion'] = (isset($_POST['hf_secund_institucion'])) ? utf8_decode($_POST['hf_secund_institucion']):null;
    $post['hf_secund_tipo_cuenta'] = (isset($_POST['hf_secund_tipo_cuenta'])) ? utf8_decode($_POST['hf_secund_tipo_cuenta']):null;
    $post['hf_secund_cuenta'] = (isset($_POST['hf_secund_cuenta'])) ? utf8_decode($_POST['hf_secund_cuenta']):null;
    $post['hf_secund_condiciones_2'] = (isset($_POST['hf_secund_condiciones_2'])) ? utf8_decode($_POST['hf_secund_condiciones_2']):null;

//te contactaremos
    $post['hf_tree_name'] = (isset($_POST['hf_tree_name'])) ? utf8_decode($_POST['hf_tree_name']):null;
    $post['hf_tree_email'] = (isset($_POST['hf_tree_email'])) ? utf8_decode($_POST['hf_tree_email']):null;
    $post['hf_tree_telefono'] = (isset($_POST['hf_tree_telefono'])) ? utf8_decode($_POST['hf_tree_telefono']):null;
    $post['hf_tree_mensaje'] = (isset($_POST['hf_tree_mensaje'])) ? utf8_decode($_POST['hf_tree_mensaje']):null;

//Buzon de sugerencias
    $post['hf_five_name'] = (isset($_POST['hf_five_name'])) ? utf8_decode($_POST['hf_five_name']):null;
    $post['hf_five_cedula'] = (isset($_POST['hf_five_cedula'])) ? utf8_decode($_POST['hf_five_cedula']):null;
    $post['hf_five_ciudad'] = (isset($_POST['hf_five_ciudad'])) ? utf8_decode($_POST['hf_five_ciudad']):null;
    $post['hf_five_dir'] = (isset($_POST['hf_five_dir'])) ? utf8_decode($_POST['hf_five_dir']):null;
    $post['hf_five_celular'] = (isset($_POST['hf_five_celular'])) ? utf8_decode($_POST['hf_five_celular']):null;
    $post['hf_five_email'] = (isset($_POST['hf_five_email'])) ? utf8_decode($_POST['hf_five_email']):null;
    $post['hf_five_servicio'] = (isset($_POST['hf_five_servicio'])) ? utf8_decode($_POST['hf_five_servicio']):null;
    $post['hf_five_otros'] = (isset($_POST['hf_five_otros'])) ? utf8_decode($_POST['hf_five_otros']):null;
    $post['hf_five_descripcion'] = (isset($_POST['hf_five_descripcion'])) ? utf8_decode($_POST['hf_five_descripcion']):null;
    $post['hf_five_sugerencia'] = (isset($_POST['hf_five_sugerencia'])) ? utf8_decode($_POST['hf_five_sugerencia']):null;

//Registo de usuarios
	$post['nombres'] = (isset($_POST['nombres'])) ? utf8_decode($_POST['nombres']):null;
	$post['cedula'] = (isset($_POST['cedula'])) ? utf8_decode($_POST['cedula']):null;
	$post['email'] = (isset($_POST['email'])) ? utf8_decode($_POST['email']):null;
	$post['email2'] = (isset($_POST['email2'])) ? utf8_decode($_POST['email2']):null;
	$post['passwd'] = (isset($_POST['passwd'])) ? utf8_decode($_POST['passwd']):null;
	$post['passwd2'] = (isset($_POST['passwd2'])) ? utf8_decode($_POST['passwd2']):null;

//Validacion previa para $post['tipo'] == '6'
if ($post['tipo'] == '6'){
	$error = false;
	if($post['email'] != $post['email2']){
		echo "Correos Diferentes";
		exit;
	}
	if($post['passwd'] != $post['passwd2']){
		echo "Passwords Diferentes";
		exit;
	}
	$error_clave = "";
	if (!validar_clave($post['passwd'], $error_clave)) {
		echo "Error en Password";
		exit;
	}
}


$config = require '../../config.php';

//Tell PHPMailer to use SMTP
$mail->isSMTP();
$mail->CharSet = 'UTF-8';
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;

//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';

//Set the hostname of the mail server
//$mail->Host = 'smtp.polilegal.ec';
$mail->Host = $config['general']['mailserver'];
// use
// $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6

//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 465;

//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'ssl';

//Whether to use SMTP authentication
$mail->SMTPAuth = true;
$mail->SMTPOptions = array(
  'ssl' => array(
    'verify_peer' => false,
    'verify_peer_name' => false,
    'allow_self_signed' => true
  )
);

//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = $config['general']['username'];

//Password to use for SMTP authentication
$mail->Password = $config['general']['password'];

//Set who the message is to be sent from
$mail->setFrom('admin@polilegal.ec', 'Polilegal S.A.');

//Set an alternative reply-to address
//$mail->addReplyTo('ulcomerce07@gmail.com', 'Ulcommerce 07');

//Set who the message is to be sent to
$mail->addAddress('info@polilegal.ec', '');
//$mail->addAddress('footnike07@gmail.com', '');

//Set the subject line

$mail->isHTML(false);
        //Build a simple message body

    //Set the subject line
if($post['tipo'] == '1'){
    $mail->Subject = 'Solicitud de Afiliación';
    $mail->Body = '<b>PLAN DE ASISTENCIA JURIDICA PARA POLICIAS SOCIOS DE LA CPN</b><br><br>';
    $mail->Body .='Nombre: '.$post['txtname'].'<br>';
    $mail->Body .='Correo: '.$post['txtemail'].'<br>';
    $mail->Body .='Cédula: '.$post['txtcedula'].'<br>';
    $mail->Body .='Teléfono: '.$post['txttelefono'].'<br>';
    //$mail->Body .='Provincia: '.$post['txtprovincia'].'<br>';
    $mail->Body .='Ciudad: '.$post['txtciudad'].'<br>';
    $mail->Body .='Sector de Residencia: '.$post['txtsector'].'<br><br>';

    if($post['tipo-pago']=='1'){
        //$mail->Body .='Tipo Afiliación: <b>Débito mensual de la cuenta CPN</b><br>';
        //$mail->Body .='Tipo de Cuenta: '.$post['hf_first_tipo_cuenta'].'<br>';
        //$mail->Body .='Número de Cuenta: '.$post['hf_first_cuenta'].'<br>';
	    $mail->Body .='<b>Terminos y Condiciones</b>: Aceptadas<br>';
    }else if ($post['tipo-pago']=='2') {
        //$mail->Body .='Tipo Afiliación: <b>Débito mensual de otra institución</b><br>';
        //$mail->Body .='Institución Financiera: '.$post['hf_secund_institucion'].'<br>';
        //$mail->Body .='Tipo de Cuenta: '.$post['hf_secund_tipo_cuenta'].'<br>';
        //$mail->Body .='Número de Cuenta: '.$post['hf_secund_cuenta'].'<br>';
        $mail->Body .='<b>Terminos y Condiciones</b>: Aceptadas<br>';
    }
}else if($post['tipo'] == '2'){
    $mail->Subject = 'Solicitud de Afiliación';
    $mail->Body = '<b>PLAN DE ASISTENCIA JURIDICA PARA SOCIOS CIVILES DE LA CPN</b><br><br>';
    $mail->Body .='Nombre: '.$post['txtname'].'<br>';
    $mail->Body .='Correo: '.$post['txtemail'].'<br>';
    $mail->Body .='Cédula: '.$post['txtcedula'].'<br>';
    $mail->Body .='Teléfono: '.$post['txttelefono'].'<br>';
    //$mail->Body .='Provincia: '.$post['txtprovincia'].'<br>';
    $mail->Body .='Ciudad: '.$post['txtciudad'].'<br>';
    $mail->Body .='Sector de Residencia: '.$post['txtsector'].'<br><br>';

    if($post['tipo-pago']=='1'){
        //$mail->Body .='Tipo Afiliación: <b>Débito mensual de la cuenta CPN</b><br>';
        //$mail->Body .='Número de Cuenta: '.$post['hf_first_cuenta'].'<br>';
        //$mail->Body .='Tipo de Cuenta: '.$post['hf_first_tipo_cuenta'].'<br>';
	$mail->Body .='<b>Terminos y Condiciones</b>: Aceptadas<br>';
    }else if ($post['tipo-pago']=='2') {
        //$mail->Body .='Tipo Afiliación: <b>Débito mensual de otra institución</b><br>';
        //$mail->Body .='Institución Financiera: '.$post['hf_secund_institucion'].'<br>';
        //$mail->Body .='Número de Cuenta: '.$post['hf_secund_cuenta'].'<br>';
        //$mail->Body .='Tipo de Cuenta: '.$post['hf_secund_tipo_cuenta'].'<br>';
	$mail->Body .='<b>Terminos y Condiciones</b>: Aceptadas<br>';
    }
}else if($post['tipo'] == '3'){

        $mail->Subject = 'Contacto';
	      $mail->Body = '<b>Datos del Cliente:</b><br><br>';
        $mail->Body .= 'Correo: '.$post['txtemail'].'<br>';
        $mail->Body .='Nombre: '.$post['txtname'].'<br>';
        $mail->Body .='Asunto: '.$post['txtsubject'].'<br>';
        $mail->Body .='Mensaje: '.$post['txtmessage'].'<br>';

}else if($post['tipo'] == '4'){

  $mail->Subject = 'Te Contactaremos';
  $mail->Body = '<b>Información Adjunta:</b><br><br>';
  $mail->Body .= 'Nombre: '.$post['hf_tree_name'].'<br>';
  $mail->Body .='Correo: '.$post['hf_tree_email'].'<br>';
  $mail->Body .='Teléfono: '.$post['hf_tree_telefono'].'<br>';
  $mail->Body .='Mensaje: '.$post['hf_tree_mensaje'].'<br>';
	if (isset($_FILES['hf_file_cv']) && $_FILES['hf_file_cv']['error'] == UPLOAD_ERR_OK) {
	  if ($_FILES['hf_file_cv']['type'] != "application/pdf") {
      error_log("Error al subir archivo: No es un archivo PDF");
      echo "noespdf";
      exit;
    }else if ($_FILES['hf_file_cv']['size'] > 1000000){
      error_log("El archivo excede el límite máximo permitido de 1000000 Mb");
      echo "muygrande";
      exit;
    }else{
      $mail->AddAttachment($_FILES['hf_file_cv']['tmp_name'], $_FILES['hf_file_cv']['name']);
    }
	}else{
      error_log("Error al subir el archvio");
      echo "error";
      exit;
  }
}else if($post['tipo'] == '5'){
	$mail->Subject = 'Buzon de sugerencias';
	$mail->Body = '<b>Información Adjunta:</b><br><br>';
	$mail->Body .= 'Nombre y Apellido: '.$post['hf_five_name'].'<br>';
	$mail->Body .= 'Cedula: '.$post['hf_five_cedula'].'<br>';
	$mail->Body .= 'Ciudad: '.$post['hf_five_ciudad'].'<br>';
	$mail->Body .= 'Dirección: '.$post['hf_five_dir'].'<br>';
	$mail->Body .= 'Celular: '.$post['hf_five_celular'].'<br>';
	$mail->Body .= 'Email: '.$post['hf_five_email'].'<br>';
	$mail->Body .= 'Servicio: '.$post['hf_five_servicio'].'<br>';
	$mail->Body .= 'Detalle de otro servicio: '.$post['hf_five_otros'].'<br>';
	$mail->Body .= 'Descripción de los hechos: '.$post['hf_five_descripcion'].'<br>';
	$mail->Body .= 'Petición o sugerencia: '.$post['hf_five_sugerencia'].'<br>';
	if (isset($_FILES['hf_file_pdf']) && $_FILES['hf_file_pdf']['error'] == UPLOAD_ERR_OK) {
	  if ($_FILES['hf_file_pdf']['type'] != "application/pdf") {
      error_log("Error al subir archivo: No es un archivo PDF");
      echo "noespdf";
      exit;
    }else if ($_FILES['hf_file_pdf']['size'] > 1000000){
      error_log("El archivo excede el límite máximo permitido de 1000000 Mb");
      echo "muygrande";
      exit;
    }else{
      $mail->AddAttachment($_FILES['hf_file_pdf']['tmp_name'], $_FILES['hf_file_pdf']['name']);
    }
	}else{
      error_log("Error al subir el archvio");
      echo "error";
      exit;
  }
}else if($post['tipo'] == '6'){

	//Generando hash
	$hash = md5( rand(0,1000) );

	//Conectando con BD
	$conn=mysqli_connect($config['database']['server'],$config['database']['username'],
		$config['database']['password'],$config['database']['db']);

    //Buscando si hay correo registrado
    $sql = "select id from users where email='$post[email]' and active='1'";
    $result = mysqli_query($conn, $sql);
    $numero_filas = mysqli_num_rows($result);
    if ($numero_filas > '0') {
		 echo "Correo Existe";
		 exit;
	 }
	 
	 //Buscando si hay cedula registrada
    $sql = "select id from users where cedula='$post[cedula]' and active='1'";
    $result = mysqli_query($conn, $sql);
    $numero_filas = mysqli_num_rows($result);
    if ($numero_filas > '0') {
       echo "Cedula Existe";
       exit;
    }

   //Insertando usuario
  	$sql = "insert into users (nombres, cedula, email, password, hash, active) 
           values ('$post[nombres]', '$post[cedula]', '$post[email]', MD5('$post[passwd]'), '$hash', '0')";
   if (!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

	/*
	//Se crea factura modelo de prueba para el cliente
	$file = rand(100000,10000000)."-".$post['cedula']."-".rand(100000,10000000).".pdf";
	$srcfile = "../../facturas/factura.pdf";
	$dstfile = "../../facturas/2000/01/$file";
	if(!copy($srcfile, $dstfile)) error_log("Error al copiar la factura");
	*/

    //Creando el URL de validacion
    $url = $config["general"]["url"];
    $url = "$url/verify.php?email=$post[email]&hash=$hash";


	$mail->Subject = 'POLILEGAL.EC - Registro de usuario';
	$mail->Body = '<b>Estimado Socio:</b><br><br>';
	$mail->Body .= 'Para continuar con el proceso de registro, copie y pegue en su navegador, la siguiente direccion:<br>';
	$mail->Body .= $url.'<br>';
	$mail->Body .='Bienvenido'.'<br>';
	$mail->addAddress($post['email'], '');
}

//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
//$mail->msgHTML(file_get_contents('contents.php'), dirname(__FILE__));

//Replace the plain text body with one created manually
$mail->AltBody = 'This is a plain-text message body';

//Attach an image file
//$mail->addAttachment('images/phpmailer_mini.png');

//send the message, check for errors
if (!$mail->send()) {
	$error = "Mailer Error: " . $mail->ErrorInfo;
	error_log($error);
    echo "Error al enviar correo";
} else {
    echo "Correo enviado";
    //Section 2: IMAP
    //Uncomment these to save your message in the 'Sent Mail' folder.
    #if (save_mail($mail)) {
    #    echo "Message saved!";
    #}
}

return;

//Section 2: IMAP
//IMAP commands requires the PHP IMAP Extension, found at: https://php.net/manual/en/imap.setup.php
//Function to call which uses the PHP imap_*() functions to save messages: https://php.net/manual/en/book.imap.php
//You can use imap_getmailboxes($imapStream, '/imap/ssl') to get a list of available folders or labels, this can
//be useful if you are trying to get this working on a non-Gmail IMAP server.
function save_mail($mail) {
    //You can change 'Sent Mail' to any other folder or tag
    $path = "{imap.gmail.com:993/imap/ssl}[Gmail]/Sent Mail";

    //Tell your server to open an IMAP connection using the same username and password as you used for SMTP
    $imapStream = imap_open($path, $mail->Username, $mail->Password);

    $result = imap_append($imapStream, $path, $mail->getSentMIMEMessage());
    imap_close($imapStream);

    return $result;
}

function validar_clave($clave,&$error_clave){
   if(strlen($clave) < 6){
      $error_clave = "La clave debe tener al menos 6 caracteres";
      return false;
   }
   if(strlen($clave) > 16){
      $error_clave = "La clave no puede tener más de 16 caracteres";
      return false;
   }
   if (!preg_match('`[a-z]`',$clave)){
      $error_clave = "La clave debe tener al menos una letra minúscula";
      return false;
   }
   if (!preg_match('`[A-Z]`',$clave)){
      $error_clave = "La clave debe tener al menos una letra mayúscula";
      return false;
   }
   if (!preg_match('`[0-9]`',$clave)){
      $error_clave = "La clave debe tener al menos un caracter numérico";
      return false;
   }
   $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/';
   if (!preg_match($pattern, $clave)) {
      $error_clave = "La clave debe tener al menos un caracter especial";
      return false;
   }
   $error_clave = "";
   return true;
}


<?php 
   session_start();
   session_unset();
?>
<!DOCTYPE HTML>

<html lang="en-gb" class="no-js">
<head>
  <meta http-equiv="content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <title>Polilegal| Inicio</title>
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
  <link id="default-css" href="style.css" rel="stylesheet" type="text/css">
  <link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/flip.css" rel="stylesheet" type="text/css">
  <link rel='stylesheet' id='layerslider-css' href="css/layerslider.css" type='text/css' media='all' />
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" />
<!--[if IE 7]>
  <!--Fonts-->
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <!--jquery-->
  <script src="js/modernizr-2.6.2.min.js"></script>
</head>
<body>
  <!--wrapper starts-->
  <div class="wrapper">
    <!--inner-wrapper starts-->
    <div class="inner-wrapper">
      <!--header starts-->
      <header>
        <!--top-bar starts-->
        <div class="top-bar">
          <div class="container">
            <ul class="dt-sc-social-icons">
              <li><a href="https://www.facebook.com/polilegal" title="Facebook"><span class="fa fa-facebook"></span></a></li>
              <li><a href="https://www.linkedin.com/company/27118661/" title="Linkedin"><span class="fa fa-linkedin"></span></a></li>
            </ul>
            <div class="dt-sc-contact-number"> <span class="fa fa-phone"> </span> Contact Center: (02)3 825530<!--br><span class="telefonoLogo" style="margin-left: 65%;">(765453)</span--> </div>
          </div>
        </div>
        <!--top-bar ends--> 

        <!--menu-container starts-->
        <div id="menu-container">
          <div id="logo" style="width: 200px; height: 70px; position: absolute; left: 73%;">
            <img src="images/logopolilegal.png" width="200px" height="60px" style="margin-top: 5px;"/>
          </div>
          <div class="container"> 
            <!--nav starts-->
            <nav id="main-menu">
              <div class="dt-menu-toggle" id="dt-menu-toggle">Menu<span class="dt-menu-toggle-icon"></span></div>
              <ul id="menu-main-menu" class="menu">
                <li class="current_page_item"> <a href="index.php"> Inicio </a> </li>
                <li> <a href="nosotros.php">Nosotros </a> </li>
                <li class="menu-item-simple-parent menu-item-depth-0">
                  <a href="servicios01.php"> servicios </a>
                  <ul class="sub-menu">
                    <li> <a href="servicios01.php"> Nuestros servicios </a> </li>
                    <li> <a href="plan.php"> Plan de Asistencia Jurídica </a> </li>
                    <a class="dt-menu-expand">+</a>
                  </ul>
                </li>
                <li><a href="actualidad.php" title="">Actualidad</a></li>
                <li class="menu-item-simple-parent menu-item-depth-0">
                  <a href="plan.php"> Asistencias </a>
                  <ul class="sub-menu">
		      <li class="menu-item-simple-parent menu-item-depth-0">
                          <a href="plan.php"> Asistencia jurídica socios CPN </a>
			  <ul class="sub-menu">
                              <li> <a href="plan1.php"> Plan de Asistencia Jurídica para socios policías de la CPN </a> </li>
                      	      <li> <a href="plan2.php"> Plan de Asistencia Jurídica para socios SP y civiles de la CPN </a> </li>
		      	      <a class="dt-menu-expand">+</a>
			  </ul>
		      </li>
                      <a class="dt-menu-expand">+</a>
                  </ul>
                </li>
                <li> <a href="buzon.php"> Buzón </a> </li>
                <li> <a href="contacto.php">Contacto</a></li>
              </nav>
            </header>
            <!--header ends--> 
            <!--main starts-->
            <div id="main"> 
              
               <!--primary starts-->
                      <!--primary ends--> 
                      
                    </div>
                    <!--main ends--> 
                    
                    <!--footer starts-->
                    <footer> 
                      <!--footer-widgets-wrapper starts-->
                      <div class="footer-widgets-wrapper"> 
                        <!--container starts-->
                        <div class="container">
                          <div class="footer_top"> </div>
                          <div class="column dt-sc-one-sixth first">
                            <aside class="widget widget_text">
                              <div class="widget_text_logo"> <img src="#" alt="" title="">
                                
                                <div class="dt-sc-hr-invisible-very-small"></div>
                                <p> <img src="images/logopl.png" <span class="fa fa-angle-double-right"></span> </strong> </a> </div>
                                </aside>
                              </div>
                              <div class="column dt-sc-two-sixth">
                                <aside class="widget widget_text">
                                  <h3 class="widgettitle">Detalles de contacto</h3>
                                  <div class="textwidget">
                                    <p class="dt-sc-contact-info"><span><i class="fa fa-print"></i> Contact Center: </span> (02)3 825530</p>
                                    <p class="dt-sc-contact-info"><span><i class="fa fa-phone"></i> Tel&eacute;fono: </span>  (02) 3333533 </p>
                                    <p class="dt-sc-contact-info"><span><i class="fa fa-envelope"></i> E-mail: </span><a href="mailto:yourname@somemail.com"> info@polilegal.ec </a></p>
                                    <p class="dt-sc-contact-info"><i class="fa fa-location-arrow"></i>Pasaje El Jardín No. 168 y Av. 6 de Diciembre <br>
                                      (frente al Megamaxi) <br>
                                      Edificio Century Plaza I, piso 5, Of. 14. <br>
                                      Quito, Ecuador</p>
                                    </div>
                                  </aside>
                                 <aside class="widget widget_text">
                                    <h3 class="widgettitle" style="margin-bottom: 8px;">Facturación Electrónica</h3>
                                    <ul>
                                       <li> <a href="login.php"> INGRESO AFILIADOS </a> </li>
                                       <?php
                                          if (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] == true) {
                                             echo '<li> <a href="#" id="close_session"> CERRAR SESION </a> </li>';
                                          }
                                       ?>
                                    </ul>
                                 </aside>
                                </div>
                                <div class="column dt-sc-one-sixth">
                                  <aside class="widget widget_text">
                                    <h3 class="widgettitle"> Servicios </h3>
                                    <ul>
                                      <li> <a href="servicios01.php"> ADMINISTRATIVOS </a> </li>
                                      <li> <a href="servicios01.php"> ADMINISTRATIVOS POLICIALES </a> </li>
                                      <li> <a href="servicios01.php"> CONSTITUCIONAL </a> </li>
                                      <li> <a href="servicios01.php"> CIVIL Y DE FAMILIA </a> </li>
                                      <li> <a href="servicios01.php"> PENAL Y TRÁNSITO </a> </li>
                                      <li> <a href="servicios01.php"> SOCIETARIO Y CORPORATIVO </a> </li>
                                      <li> <a href="servicios01.php"> ARBITRAJE Y MEDIACIÓN </a> </li>
                                      <li> <a href="servicios01.php"> ASUNTOS REGULATORIOS </a> </li>
                                    </ul>
                                  </aside>
                                </div>
                                <div class="column dt-sc-two-sixth">
                                  <aside class="widget widget_text">
                                    <h3 class="widgettitle"> VIDEOS </h3>
				    <iframe width="300" height="200" src="https://www.youtube.com/embed/6IEDnTE9DQw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> 
				 <!--   <video class="motionpicture" width="300" height="200" autobuffer controls preload="auto">
				        <source src="VideoPolilegal.mp4" />
				        Your browser does not appear to support HTML5 media. Try updating
					your browser or (if you are not already) using an open source browser like Firefox.
				    </video>
				-->
                                  </aside>
                                </div>
                              </div>
                              <!--container ends--> 
                            </div>
                            <!--footer-widgets-wrapper ends-->
                            <div class="copyright">
                              <div class="container">
                                <div class="copyright-info">Polilegal © 2017 <a href="http://polilegal.ec./" target="_blank"> www.polilegal.ec </a> Todos los derechos reservados. </div>
                              </ul>
                              <p> Design by <a href="http://themeforest.net/user/buddhathemes" target="_blank" title="">BigDesign</a> </p>
                            </div>
                          </div>
                        </footer>
                        <!--footer ends--> 
                      </div>
                      <!--inner-wrapper ends--> 
                    </div>
                    <!--wrapper ends-->
                    <div class="fixed-help-form">
                      <div class="fixed-help-form-icon"> <img src="images/fixed-help-form-icon.png" alt="" title=""> </div>
                      <h4> Asistencias </h4>
                      <p> Comienza el proceso de afiliaci&oacute;n llenando los siguientes datos. </p>
                      <p> <span> Gracias por confiar en nosotros </span> </p>
                      <form name="helpform" method="post" class="help-form" action="php/helpform.php">
                        <p>
                          <input type="text" placeholder="Nombres" class="text_input" name="hf_first_name" required />
                        </p>
                        <p>
                          <input type="text" placeholder="Apellidos" class="text_input" name="hf_last_name" required />
                        </p>
                        <p>
                          <input type="text" placeholder="Teléfono" class="text_input" name="phone" required />
                        </p>
                        
                        
                        <div id="ajax_helpform_msg"> </div>
                        <p>
                          <p><a href="plan1.php" class="ls-l dt-sc-bordered-button" title="">siguiente</a></p>
                        </p>
                      </form>
                    </div>
                    <a href="" title="Go to Top" class="back-to-top"> <span class="fa fa-angle-up"></span> </a> 
                    <!--Java Scripts--> 

                    <script type="text/javascript" src="js/jquery.js"></script> 
                    <script type="text/javascript" src="js/jquery-migrate.min.js"></script> 
                    <script type="text/javascript" src="js/jquery.validate.min.js"></script> 
                    <script type="text/javascript" src="js/jquery-easing-1.3.js"></script> 
                    <script type="text/javascript" src="js/jquery.sticky.js"></script> 
                    <script type="text/javascript" src="js/jquery.nicescroll.min.js"></script> 
                    <script type="text/javascript" src="js/jquery.smartresize.js"></script> 
                    <script type="text/javascript" src="js/shortcodes.js"></script> 
                    <script type="text/javascript" src="js/custom.js"></script> 

                    <!-- Layer Slider --> 
                    <script type="text/javascript" src="js/jquery-transit-modified.js"></script> 
                    <script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script> 
                    <script type='text/javascript' src="js/greensock.js"></script> 
                    <script type='text/javascript' src="js/layerslider.transitions.js"></script> 
                    <script type="text/javascript">
                      var lsjQuery = jQuery;</script><script type="text/javascript"> lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice('layerslider_2','jquery'); } else { lsjQuery("#layerslider_2").layerSlider({responsiveUnder: 1240, layersContainer: 1170, skinsPath: 'js/layerslider/skins/'}) } }); </script>

                      <!--script flip-->
                      <script type='text/javascript' src="js/flip/jquery.flip.min.js"></script> 
                      <script type='text/javascript' src="js/flip/script.js"></script> 

                     <!-- Cerrar Session -->
                     <script>
                        jQuery(document).ready(function(){
                           jQuery("#close_session").click(function(){
                              alert('Su sesión ha sido cerrada');
                              window.location.href='closesession.php';
                           });
                        });
                     </script>

                    </body>
                    </html>

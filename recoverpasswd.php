<?php

$post['email'] = utf8_decode($_POST['email']);

//Generando hash
$hash = md5( rand(0,1000) );

//Conectando con BD
$config = require 'config.php';
$conn=mysqli_connect($config['database']['server'],$config['database']['username'],
	$config['database']['password'],$config['database']['db']);

//Peprando link de recuperacion
$sql = "select id from users where email='$post[email]' AND active='1'";
if ($result = mysqli_query($conn, $sql)){
	$numero_filas = mysqli_num_rows($result);
	if ($numero_filas > '0') {

		//Generando hash
		$hash = md5( rand(0,1000) );

		while ($row = mysqli_fetch_assoc($result)) {
		
			//Creando el URL de validacion
			$url = $config["general"]["url"];
			$url = "$url/verifyemail.php?email=$post[email]&hash=$hash";
		}

		//Enviando por correo el link de recuperacion
		enviar_correo($url, $post['email']);


		//Actualizando el hash
		$sql = "UPDATE users SET hash='$hash' WHERE email='$post[email]'";
		if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

		echo "Correo Enviado";
		
	}else{

		echo "No existe correo";

	}
}else{
 	error_log("Error: " . $sql . "..." . mysqli_error($conn));
	echo "Error en BD";
}

function enviar_correo($url, $email){

	require 'PHPMailer/PHPMailerAutoload.php';
	require 'config.php';

	//Create a new PHPMailer instance
	$mail = new PHPMailer;

	//Tell PHPMailer to use SMTP
	$mail->isSMTP();
	$mail->CharSet = 'UTF-8';

	//Enable SMTP debugging
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages
	$mail->SMTPDebug = 0;

	//Ask for HTML-friendly debug output
	$mail->Debugoutput = 'html';

	//Set the hostname of the mail server
	$mail->Host = $config['general']['mailserver'];

	//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
	$mail->Port = 465;

	//Set the encryption system to use - ssl (deprecated) or tls
	$mail->SMTPSecure = 'ssl';

	//Whether to use SMTP authentication
	$mail->SMTPAuth = true;
	$mail->SMTPOptions = array(
	  'ssl' => array(
		'verify_peer' => false,
		'verify_peer_name' => false,
		'allow_self_signed' => true
	  )
	);

	//Username to use for SMTP authentication - use full email address for gmail
	$mail->Username = $config['general']['username'];

	//Password to use for SMTP authentication
	$mail->Password = $config['general']['password'];
	
	//Set who the message is to be sent from
	$mail->setFrom('admin@polilegal.ec', 'Polilegal S.A.');
	
	//Set who the message is to be sent to
	$mail->addAddress($email, '');

	$mail->isHTML(false);

	//Set the subject line
    $mail->Subject = 'POLILEGAL.EC - Recuperación de contraseña';

	//Set the body
    $mail->Body = '<b>Estimado Socio:</b><br><br>';
    $mail->Body .= 'Para continuar con el proceso de recuperación de contraseña, haga click en el siguiente enlace:<br><br>';
    $mail->Body .= $url.'<br><br>';
    $mail->Body .= 'Si este link no funciona, por favor copie y pegue en su navegador.<br><br>';
    $mail->Body .= 'Bienvenido.'.'<br>';

	$mail->AltBody = 'This is a plain-text message body';

	//send the message, check for errors
	if (!$mail->send())	error_log("Error al enviar correo");

}

?>

<?php 
   session_start();
   session_unset();
	header('Content-Type: text/html; charset=utf-8');
	$config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
   mysqli_set_charset($conn, "utf8");

	//Obteniendo el valor de la afiliacion
	$sql = "SELECT costo FROM productos WHERE id='1'";
	if($result = mysqli_query($conn, $sql)){
		$row = mysqli_fetch_assoc($result);
		$amount = $row['costo'];
	}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
?>
<!DOCTYPE HTML>
<html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
  <meta http-equiv="content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
  <title>Polilegal | Afiliaci&oacute;n</title>
  <meta name="description" content="">
  <meta name="author" content="adolfo" >
  <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
  <link id="default-css" href="style.css" rel="stylesheet" type="text/css">
  <link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" /> 
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<!--[if IE 7]>
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lt IE 9]>
<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--Fonts-->
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--jquery-->
<script src="js/modernizr-2.6.2.min.js"></script>
<style>
		body {font-size: 16px;}
      .contact-form input[type=checkbox].error{
         outline: 1px solid #FF0000;
      }
    .plan1-text{
        color: rgb(29, 62, 136);
        font-size: 18px;
        font-weight: normal;
    }
	.plan2-text{
        color: rgb(29, 62, 136);
        font-size: 18px;
        font-weight: normal;
    }
	 .columnas {float: left; margin: 0px 0px 0px 4%; width: 44%}
	.margintop1 {margin-top: 110px;}
	.margintop2{margin-top: 60px;}
	p {text-align: left;}
	.dt-sc-toggle-frame h5 a {font-size: 21px;}
	.subtitulo {
		text-align: center; 
		margin-bottom: 20px; 
		margin-top: 0px;
	}
	.tdwhite {
		background: white;
	}
	@media (max-width: 600px) {
	  .columnas {
		 width: 92%;
	  }
		.logodiv {
			position: relative;
			width: auto;
			height: auto;
			left: 0%;
		}
		.logoheader{margin-left: 0px; height: 140px;}
		.breadcrumb-section{
			padding-top: 50px;
			padding-right: 0px;
			padding-bottom: 25px;
			padding-left: 0px;
		}
		#primary{
			margin-top: 50px;
    		margin-right: 0px;
    		margin-bottom: 0px;
    		margin-left: 0px
		}
		.margintop1 {margin-top: 0px;}
		.margintop2{margin-top: 0px;}
		h5.dt-sc-toggle-accordion a, 
		.dt-sc-toggle-frame h5.dt-sc-toggle-accordion a, 
		.dt-sc-toggle-frame h5.dt-sc-toggle a, 
		h5.dt-sc-toggle-accordion, 
		h5.dt-sc-toggle, 
		h5.dt-sc-toggle-accordion a, 
		h5.dt-sc-toggle a {
			font-size: 19px;
		}
		.subtitulo {
			text-align: center; 
			margin-bottom: 20px; 
			margin-top: 40px;
		}
	}
</style>
</head>
<body>
	<!--wrapper starts-->
  <div class="wrapper">
    <!--inner-wrapper starts-->
    <div class="inner-wrapper">
      <!--header starts-->
		<?php include 'header.php'; ?>
      <!--header ends-->
		
		<div id="main">
			<section id="primary" class="content-full-width">
				<div class="container" style="text-align: center; height: 60px;">
					<span class="dt-sc-highlight skin-color" style="margin-bottom: 50px;">
						<strong> PLAN DE ASISTENCIA JURÍDICA POLILEGAL S.A. </strong> 
					</span>
				</div>
				<div class="container">
					<p class="subtitulo"> Te damos la bienvenida a nuestro Plan de Asistencia Jurídica para miembros policiales
						en servicio activo y pasivo, y familiares con los siguientes beneficios:
					</p>
				</div>
				<div class="container">
					<div class="dt-sc-one-fourth column first" align="center">
						<img src="images1/100cobertura.png" width="auto" height="100px"/><br>
						100% de cobertura.
					</div>
					<div class="dt-sc-one-fourth column" align="center">
						<img src="images1/atencion.png" width="auto" height="100px"/><br>
						Atención ilimitada.
					</div>
					<div class="dt-sc-one-fourth column" align="center">
						<img src="images1/abogado.png" width="auto" height="100px"/><br>
						Abogados especializados.
					</div>
					<div class="dt-sc-one-fourth column" align="center">
						<img src="images1/cnacional.png" width="auto" height="100px"/><br>
						Cobertura a nivel nacional.
					</div>
				</div>

				<div class="comparison" style="padding-top: 50px;">
				  <table>
					 <thead>
						<tr>
						  <th class="price-info">
							 <div class="price-try"><span class="hide-mobile">SERVICIOS INCLUIDOS</span></div>
						  </th>
						  <th class="price-info">
							 <div class="price-try"><span class="">SOCIOS POLICÍAS (SERVICIO ACTIVO)</span></div>
						  </th>
						  <th class="price-info">
							 <div class="price-try"><span class="">SOCIOS SP Y CIVILES</span></div>
						  </th>
						</tr>
					 </thead>
					 <tbody>
						<?php
							$sql =   "SELECT *
										FROM servicios
										ORDER BY posicion ASC";
							if ($result = mysqli_query($conn, $sql)){
								while ($row = mysqli_fetch_assoc($result)) {
									if ($row['posicion']%2==0){
						?>
                  <tr>
                    <td>&nbsp;</td>
                    <td colspan="2"><?php echo $row['servicio']; ?>.</td>
                  </tr>
                  <tr>
                    <td class="tdwhite"><?php echo $row['servicio']; ?>.</td>
							<?php 	if($row['plan'] == 21){ ?>
                    <td class="tdwhite"><span class="tickblue">✔</span></td>
                    <td class="tdwhite"><span class="tickgreen">✔</span></td>
							<?php 	}else if($row['plan'] == 2){ ?>
                    <td class="tdwhite"><span class="tickblue">✔</span></td>
                    <td class="tdwhite"></td>
							<?php 	}else{ ?>
                    <td class="tdwhite"></td>
                    <td class="tdwhite"><span class="tickgreen">✔</span></td>
							<?php 	} ?>
                  </tr>
						<?php 	}else{ ?>
                  <tr>
                    <td></td>
                    <td colspan="2"><?php echo $row['servicio']; ?>.</td>
                  </tr>
                  <tr class="compare-row">
                    <td><?php echo $row['servicio']; ?>.</td>
							<?php    if($row['plan'] == 21){ ?>
                    <td><span class="tickblue">✔</span></td>
                    <td><span class="tickgreen">✔</span></td>
                     <?php    }else if($row['plan'] == 2){ ?>
                    <td><span class="tickblue">✔</span></td>
                    <td></td>
                     <?php    }else{ ?>
                    <td></td>
                    <td><span class="tickgreen">✔</span></td>
                     <?php    } ?>
                  </tr>
						<?php 	}
								}
							}else error_log("Error: " . $sql . "..." . mysqli_error($conn));
						?>
					 </tbody>
				  </table>
				</div>
            <div class="container" style="text-align: center; height: 25px; padding-bottom: 75px;">
               <span class="dt-sc-highlight skin-color" style="margin-bottom: 50px;">
                  <strong> Afiliación: $<?php echo $amount; ?> (incluido impuestos). </strong>
               </span>
            </div>
            <div class="container" style="text-align: center; height: 25px;">
               <span class="dt-sc-highlight skin-color" style="margin-bottom: 50px;">
                  <strong> FORMULARIO DE AFILIACIÓN </strong>
               </span>
            </div>
				<div class="comparison">
					 <br>
					 <form name="frmcontact" method="post" class="contact-form" action="afiliacion.php" novalidate="novalidate">
                 	<input type="hidden" name="tipo" value="5">
						<p><select name="plan" class="selectoptions">
						  		<option value="SELECCIONE" disabled selected>Tipo de Plan</option>
                           <?php
                              $sql = "SELECT * FROM planes";
                              if ($result = mysqli_query($conn, $sql)){
                                 while ($row2 = mysqli_fetch_assoc($result)) {
                                 	echo "<option value='$row2[id]'>".$row2['plan']."</option>";
                                 }
                              }else{
                                 error_log("Error: " . $sql . "..." . mysqli_error($conn));
                              }
                           ?>
							</select>
						</p>
						<p><input type="text" placeholder="Nombres y Apellidos" class="text_input" name="nombres" required=""></p>
						<p><input type="text" placeholder="Número de cédula" class="text_input" name="cedula" required=""></p>
						<p><input type="text" placeholder="Ciudad" class="text_input" list="ciudades" name="ciudad" style="color: #75757d" required="">
							<datalist id="ciudades">
                           <?php
                              $sql = "SELECT * FROM ciudades ORDER BY ciudad";
                              if ($result = mysqli_query($conn, $sql)){
                                 while ($row2 = mysqli_fetch_assoc($result)) {
                                    echo "<option value='$row2[ciudad]'>";
                                 }
                              }else{
                                 error_log("Error: " . $sql . "..." . mysqli_error($conn));
                              }
                           ?>
							</datalist>
						</p>
						<p><input type="text" placeholder="Sector de residencia" class="text_input" name="sector" required=""></p>
						<p><input type="text" placeholder="Celular" class="text_input" name="celular" required="" 
								oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"></p>
						<p><input type="text" placeholder="E-mail" class="text_input" name="email" required=""></p>
                  <p><select name="medio_pago" id="medio_pago" class="selectoptions" required="">
                        <option value="SELECCIONE" disabled selected>Medios de pago</option>
                           <?php
                              $sql = "SELECT * FROM medios_pagos";
                              if ($result = mysqli_query($conn, $sql)){
                                 while ($row2 = mysqli_fetch_assoc($result)) {
                                    echo "<option value='$row2[id]'>".$row2['medio']."</option>";
                                 }
                              }else{
                                 error_log("Error: " . $sql . "..." . mysqli_error($conn));
                              }
                           ?>
                     </select>
                  </p>
						<div class="form-group checkbox">
							<label class="control-label" for="tos">
								<input type="checkbox" name="tos" value="1" id="tos">Acepto los
									<a href="terminos.php" target="_blank"> términos y condiciones </a>y la
									<a href="ppd.php" target="_blank"> política de privacidad.</a>
							</label>
						</div>
						<div id="ajax_contact_msg"> </div>
						<p class="aligncenter">
						  <input name="submit" type="submit" id="submit" class="dt-sc-bordered-button" value="Enviar Solicitud" 
							style="background-color: #1d3e88; margin-top: 20px;">
						  <img id="enviando" src="images/loading.gif" width="100px" height="100px" style="position: relative; vertical-align:middle; display: none;">
						</p>
						<!-- Zona de mensajes -->
						<div id="mensajes"></div>
						<!-- FIN Zona de mensajes -->
					 </form>
				</div>
			</section>
		</div>	

      <!--footer starts-->
		<?php include 'footer.php'; ?>
      <!--footer ends-->
    </div>
    <!--inner-wrapper ends-->    
  </div>
  <!--wrapper ends-->
  <!--wrapper ends-->
	<?php include "asistencias.php"; ?>
  <a href="" title="Go to Top" class="back-to-top"> <span class="fa fa-angle-up"></span> </a> 
  <!--Java Scripts-->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate.min.js"></script>
  <script type="text/javascript" src="js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="js/jquery-easing-1.3.js"></script>
  <script type="text/javascript" src="js/jquery.sticky.js"></script>
  <script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>


  <script type="text/javascript" src="js/jquery.tabs.min.js"></script>
  <script type="text/javascript" src="js/jquery.smartresize.js"></script> 
  <script type="text/javascript" src="js/shortcodes.js"></script>   

  <script type="text/javascript" src="js/custom.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDz1RGAW3gGyDtvmBfnH2_fE2DVVNWq4Eo&callback=initMap" type="text/javascript"></script>
  <script src="js/gmap3.min.js"></script>
  <!-- Layer Slider --> 
  <script type="text/javascript" src="js/jquery-transit-modified.js"></script> 
  <script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script> 
  <script type='text/javascript' src="js/greensock.js"></script> 
  <script type='text/javascript' src="js/layerslider.transitions.js"></script> 

  <script type="text/javascript">var lsjQuery = jQuery;</script><script type="text/javascript"> lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice('layerslider_2','jquery'); } else { lsjQuery("#layerslider_2").layerSlider({responsiveUnder: 1240, layersContainer: 1170, skinsPath: 'js/layerslider/skins/'}) } }); </script>

  <script type="text/javascript">
	  
      jQuery( "#pestana1" ).click(function() {
          
        jQuery('#hf_secund_institucion option:first-child').attr('selected','selected');
        jQuery('#hf_secund_tipo_cuenta option:first-child').attr('selected','selected');
        jQuery('#hf_secund_cuenta').val('');
        jQuery('#hf_first_tipo_cuenta option:first-child').attr('selected','selected');
        jQuery('#hf_first_cuenta').val(''); 
          
        jQuery('#hf_secund_institucion').removeAttr('required');
        jQuery('#hf_secund_tipo_cuenta').removeAttr('required');
        jQuery('#hf_secund_cuenta').removeAttr('required');
        jQuery('#hf_first_tipo_cuenta').attr('required','true');
        jQuery('#hf_first_cuenta').attr('required','true');
        jQuery('#tipo-pago').val('1');
      });

      jQuery( "#pestana2" ).click(function() {
          
        jQuery('#hf_first_tipo_cuenta option:first-child').attr('selected','selected');
        jQuery('#hf_first_cuenta').val('');
        jQuery('#hf_secund_institucion option:first-child').attr('selected','selected');
        jQuery('#hf_secund_tipo_cuenta option:first-child').attr('selected','selected');
        jQuery('#hf_secund_cuenta').val('');
        
        jQuery('#hf_first_tipo_cuenta').removeAttr('required');
        jQuery('#hf_first_cuenta').removeAttr('required');
        jQuery('#hf_secund_institucion').attr('required','true');
        jQuery('#hf_secund_tipo_cuenta').attr('required','true');
        jQuery('#hf_secund_cuenta').attr('required','true');
        jQuery('#tipo-pago').val('2');
      });
      
    </script>

	<!-- Cerrar Session -->
	<script>
		jQuery(document).ready(function(){
			jQuery("#close_session").click(function(){
				alert('Su sesión ha sido cerrada');
				window.location.href='closesession.php';
			});
		});
	</script>

	<script>
		jQuery("#defensatitle").click(function() {
			 jQuery('html,body').animate({
				  scrollTop: $("#defensacontent").offset().top},
				  'slow');
		});
	</script>

</body>
</html>

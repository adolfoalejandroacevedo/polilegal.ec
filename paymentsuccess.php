<?php
   session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");
	$plan = (isset($_GET['plan'])) ? $_GET['plan']:0;
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);

	//Variables
	$df = $_GET['df'];
	$order_amount = $_GET['order_amount'];
	$order_vat = $_GET['order_vat'];
	$sub_total = round($order_amount - $order_vat, 2);
	$order_description = urldecode($_GET['order_description']);	
	$periodo = $_GET['periodo'];

	//Obteniendo el nombre y apellido
	$sql = "SELECT nombres FROM users WHERE id='$_SESSION[id]'";
	if($result = mysqli_query($conn, $sql)){
		$row = mysqli_fetch_assoc($result);
		$nombres = $row['nombres'];
	}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

	//Obteniendo el codigo de autorizacion
   $sql = "SELECT authorization_code FROM transactions WHERE transaction_id='$df'";
   while(true){
    if ($result=mysqli_query($conn, $sql)){
        if (mysqli_num_rows($result) > 0) {
         while ($row = mysqli_fetch_assoc($result)) {
            $txtCod = $row['authorization_code'];
         }
	 break;
      }else{
	 sleep(3);
         //$txtCod = 999999;
      }
    }else{
        error_log("Error: " . $sql . "..." . mysqli_error($conn));
        $txtCod = 999999;
    }
   }
	if ($plan == 2){

		//Actualizando el plan
		$sql = "UPDATE users SET plan='2' WHERE id='$_SESSION[id]'";
		if (mysqli_query($conn, $sql)){
			$_SESSION['plan'] = 2;
		}else error_log("Error: " . $sql . "..." . mysqli_error($conn));

		//Actualiza transaccion
		$sql = "INSERT INTO transacciones (
						iduser,
						df,
						concepto,
						monto,
						estatus,
						fechavence)
					VALUES (
						'$_SESSION[id]',
						'$df',
						'2',
						'$order_amount',
						'1',
						DATE_ADD(NOW(), INTERVAL $periodo MONTH))";
		if (!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

		//Dejando solo dos areas de practica
      $sql = "DELETE FROM usersareas WHERE iduser='$_SESSION[id]' 
              AND id NOT IN ( SELECT id FROM ( SELECT id FROM usersareas WHERE iduser='$_SESSION[id]' ORDER BY id DESC LIMIT 2 ) x )";
      if (!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

		//Se envia correo
		sendmail($df,$order_amount,$order_vat,$nombres,$txtCod,$order_description,$sub_total);
	}

	if ($plan == 3){

      //Actualizando el plan
      $sql = "UPDATE users SET plan='3' WHERE id='$_SESSION[id]'";
      if (mysqli_query($conn, $sql)){
         $_SESSION['plan'] = 3;
      }else error_log("Error: " . $sql . "..." . mysqli_error($conn));

      //Actualiza transaccion
		$sql = "INSERT INTO transacciones (
						iduser,
						df,
						concepto,
						monto,
						estatus,
						fechavence)
					VALUES (
						'$_SESSION[id]',
						'$df',
						'3',
						'$order_amount',
						'1',
						DATE_ADD(NOW(), INTERVAL $periodo MONTH))";
      if (!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
      
      //Periodo en string
      if($periodo == 1) $periodo = "1 mes"; else $periodo = "12 meses";

      //Se envia correo
		sendmail($df,$order_amount,$order_vat,$nombres,$txtCod,$order_description,$sub_total,$periodo);

	}

   if ($plan == "DESTACADO"){

      //Actualizando el plan
      $sql = "UPDATE users SET plandestacado='1' WHERE id='$_SESSION[id]'";
      if (mysqli_query($conn, $sql)){
         $_SESSION['plandestacado'] = 1;
      }else error_log("Error: " . $sql . "..." . mysqli_error($conn));

      //Actualiza transaccion
		$sql = "INSERT INTO transacciones (
						iduser,
						df,
						concepto,
						monto,
						estatus,
						fechavence)
					VALUES (
						'$_SESSION[id]',
						'$df',
						'4',
						'$order_amount',
						'1',
						DATE_ADD(NOW(), INTERVAL $periodo MONTH))";
      if (!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

      //Periodo en string
      if($periodo == 1) $periodo = "1 mes"; else $periodo = "12 meses";

      //Se envia correo
      sendmail($df,$order_amount,$order_vat,$nombres,$txtCod,$order_description,$sub_total,$periodo);

   }

?>

<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>
         Tu Abogado:
         Users :: Directorio de Abogados del Ecuador
      </title>
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <link href="css/animate.min.css" rel="stylesheet">
      <link href="css/prettyPhoto.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="bower_components/datatables.net-dt/css/jquery.dataTables.css">
      <link rel="stylesheet" type="text/css" href="css/bootstrap-markdown.min.css">
      <style>
         .pricingTable{
         text-align: center;
         transition: all 0.5s ease 0s;
         }
         .pricingTable:hover{
         box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
         }
         .pricingTable .pricingTable-header{
         color: #feffff;
         }
         .pricingTable .heading{
         display: block;
         padding-top: 25px;
         }
         .pricingTable .heading > h3{
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .textotitle{
         color: #feffff;
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .pricingTable .subtitle{
         display: block;
         font-size: 13px;
         margin-top: 5px;
         text-transform: capitalize;
         }
         .pricingTable .price-value{
         display: block;
         font-size: 60px;
         font-weight: 700;
         padding-bottom: 25px;
         }
         .pricingTable .price-value span{
         display: block;
         font-size: 14px;
         line-height: 20px;
         text-transform: uppercase;
         }
         .pricingTable .pricingContent{
         /*text-transform: capitalize;*/
         background: #fbfbfb;
         color: #fefeff;
         }
         .pricingTable .pricingContent ul{
         list-style: none;
         padding: 15px 14px 10px;
         margin: 0;
         text-align: left;
         }
         .pricingTable .pricingContent ul li{
         font-size: 14px;
         padding: 12px 0;
         border-bottom: 1px dashed #e1e1e1;
         color: #9da1ad;
         }
         .pricingTable .pricingContent ul li i{
         font-size: 14px;
         float: right;
         }
         .pricingTable .pricingTable-sign-up{
         padding: 20px 0;
         background: #fbfbfb;
         color: #fff;
         text-transform: capitalize;
         }
         .pricingTable .btn-block{
         width: 60%;
         margin: 0 auto;
         font-size: 17px;
         color: #fff;
         text-transform: capitalize;
         border: none;
         border-radius: 5px;
         padding: 10px;
         transition: all 0.5s ease 0s;
         }
         .pricingTable .btn-block:before{
         content: "\f007";
         font-family: 'FontAwesome';
         margin-right: 10px;
         }
         .pricingTable.blue .pricingTable-header,
         .pricingTable.blue .btn-block{
         background: #fce400;
         }
         .pricingTable.pink .pricingTable-header,
         .pricingTable.pink .btn-block{
         background: #c9b600;
         }
         .pricingTable.orange .pricingTable-header,
         .pricingTable.orange .btn-block{
         background: #e0d83a;
         }
         .pricingTable.green .pricingTable-header,
         .pricingTable.green .btn-block{
         background: #e0cd00;
         }
         .pricingTable.blue .btn-block:hover,
         .pricingTable.pink .btn-block:hover,
         .pricingTable.orange .btn-block:hover,
         .pricingTable.green .btn-block:hover{
         background: #e6e6e6;
         color: #939393;
         }
         @media screen and (max-width: 990px){
         .pricingTable{ margin-bottom: 20px; }
         }
      </style>
   </head>
   <body class="homepage">
<?php include "header.php"; ?>
      <!--/header-->
      <section id="blog" class="container">
         <div class="container">
            <div class="alert alert-success alert-dismissible fade in" role="alert" align="center" style="margin-bottom: 2px">
               <h1 class="alert-heading" style="color: #249400cf">Su pago ha sido realizado con éxito.</h1>
               <p>Ahora puede ir a su panel de administración en "Mi Perfil" para ver su nuevo plan.</p>
               <p>Recibirá un correo electrónico con la notificación de su pago.</p>
            </div>
         </div>
      </section>
      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/markdown.js"></script>
      <script src="js/to-markdown.js"></script>
      <script src="js/bootstrap-markdown.js"></script>
      <script src="js/bootstrap-markdown.es.js"></script>
      <script type="text/javascript" src="js/jquery.simpleslider.js"></script>
      <script type="text/javascript" charset="utf8" src="bower_components/datatables.net/js/jquery.dataTables.min.js"> </script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/jquery.isotope.min.js"></script>
      <script src="js/wow.min.js"></script>
      <script src="js/main.js"></script>
   </body>
</html>

<?php

function sendmail($df,$order_amount,$order_vat,$nombres,$txtCod,$order_description,$sub_total,$periodo){

	//Preparando el envio del correo    
	require 'phpmailer/PHPMailerAutoload.php';
	include("emailpayplan.php");
	$message = $cuerpo;
	$subject = "TUABOGADO.EC - COMPRA DE PLAN";
	$mail = new PHPMailer;
	$mail->CharSet = 'UTF-8';
	$mail->isSMTP();
	//$mail->Host = 'smtp.gmail.com';
	$mail->Host = 'ssl://md-100.webhostbox.net';
	//$mail->Port = 587;
	$mail->Port = 465;
	$mail->SMTPSecure = 'ssl';
	$mail->SMTPAuth = true;
	//$mail->Username = $email;
	$mail->Username = $config["general"]["username"];
	//$mail->Password = $password;
	$mail->Password = $config["general"]["password"];
	$mail->setFrom('info@tuabogado.ec', 'TUABOGADO.EC');
	$mail->addReplyTo('noreplyto@tuabogado.ec', 'TUABOGADO.EC');
	$mail->addAddress($_SESSION["email"]);
	$mail->addCC('info@tuabogado.ec');
	$mail->Subject = $subject;
	$mail->msgHTML($message);
	if (!$mail->send()) {
		$error = "Mailer Error: " . $mail->ErrorInfo;
		error_log($error, 0);
	}

}

?>

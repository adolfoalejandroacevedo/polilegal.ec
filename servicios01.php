<?php 
   session_start();
   session_unset();
	$current = "servicios";
?>
<!DOCTYPE HTML>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> 
<html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
    <meta http-equiv="content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
    <title>Polilegal | Servicios</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
    <link id="default-css" href="style.css" rel="stylesheet" type="text/css">
    <link id="shortcodes-css" href="css/shortcodes.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link id="skin-css" href="skins/gray/style.css" rel="stylesheet" media="all" />
<!--[if IE 7]>
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lt IE 9]>
<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--Fonts-->
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,900italic,900,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--jquery-->
<script src="js/modernizr-2.6.2.min.js"></script>
</head>
<body>
	<!--wrapper starts-->
    <div class="wrapper">
        <!--inner-wrapper starts-->
        <div class="inner-wrapper">
            <!--header starts-->
				<?php include 'header.php'; ?>
            <!--header ends-->
                        <!--main starts-->
                        <div id="main">
                            
                            <div class="breadcrumb-section">
                                <div class="container">
                                    <h1> Servicios que ofrecemos </h1>
                                    <div class="breadcrumb">
                                        <a href="index.php"> Inicio </a>
                                        <span class="current"> Nuestros Servicios </span>
                                    </div>
                                </div>
                            </div>
                            
                            <!--primary starts-->
                            <section id="primary" class="content-full-width">
                               <!--container starts-->
                               <div class="container">
                                
                                   <div class="dt-sc-one-half column first">
                                       <a href="#"><img src="images/ser02.jpg" alt="" title=""></a>
                                   </div>
                                   
                                   <div class="dt-sc-one-half column">
                                       <h4 class="dt-sc-simple-hr-title"> ADMINISTRATIVO </h4>
                                       <p class="alignleft" style="text-align: justify;"></p><ul>
                                          <li>- Reclamos administrativos.</li>
                                          <li>- Procedimientos disciplinarios.</li>
                                          <li>- Juicios contencioso administrativos.</li>
                                          <li>- Impugnación de actos administrativos (Recurso de Apelación/Recurso Extraordinario de Revisión).</li>
                                      </ul>
                                       <div class="dt-sc-hr-invisible-small"></div>
                                       <div class="dt-sc-hr-button">
                                           <a href="#" class="dt-sc-toggle-practice"> Leer m&aacute;s <span class="fa fa-angle-double-down"></span> </a>
                                       </div>
                                       <div class="dt-sc-practice-content">
                                           <div class="dt-sc-hr-invisible-small"></div>
                                           
                                           <p class="alignleft" style="text-align: justify;"></p> 
                                       </div>
                                   </div>
                                   
                                   <div class="dt-sc-hr-invisible"></div>
                                   
                                   <div class="dt-sc-one-half column first">
                                       <h4 class="dt-sc-simple-hr-title"> ADMINISTRATIVO POLICIAL</h4>
                                       <p class="alignleft" style="text-align: justify;"></p><ul>
                                          <li>- Seguros sociales (cesantía, retiro, accidentes, enfermedad).</li>
                                          <li>- Acciones previas.</li>
                                          <li>- Sumarios administrativos.</li>
                                          <li>- Cupos de Eliminación Anual.</li>
                                          <li>- Ascensos y condecoraciones.</li>
                                          <li>- Procedimiento por faltas leves.</li>
                                          <li>- Rehabilitación de faltas disciplinarias.</li>
                                      </ul>
                                      </ul>
                                       <div class="dt-sc-hr-invisible-small"></div>
                                       <div class="dt-sc-hr-button">
                                           <a href="#" class="dt-sc-toggle-practice"> Leer m&aacute;s <span class="fa fa-angle-double-down"></span> </a>
                                       </div>
                                       <div class="dt-sc-practice-content">
                                           <div class="dt-sc-hr-invisible-small"></div>
                                           <p class="alignleft" style="text-align: justify;"></p>
                                       </div>
                                   </div>
                                   
                                   <div class="dt-sc-one-half column">
                                       <a href="#"><img src="images/poligirl.jpg" alt="" title=""></a>
                                   </div>
                                   
                                   <div class="dt-sc-hr-invisible"></div>
                                   
                                   <div class="dt-sc-one-half column first">
                                       <a href="#"><img src="images/ser03.jpg" alt="" title=""></a>
                                   </div>
                                   
                                   <div class="dt-sc-one-half column">
                                       <h4 class="dt-sc-simple-hr-title"> CONSTITUCIONAL </h4>
                                      <ul>
                                          <li>- Acción de Protección.</li>
                                          <li>- Acción Extraordinaria de Protección.</li>
                                          <li>- Acción por Incumplimiento.</li>
                                          <li>- Acción por Hábeas Corpus.</li>
                                          <li>- Acción de Acceso a la Información Pública.</li>
                                      </ul>
                                       <div class="dt-sc-hr-invisible-small"></div>
                                       
                                   </div>
                                   
                                   <div class="dt-sc-hr-invisible"></div>
                                   
                                   <div class="dt-sc-one-half column first">
                                       <h4 class="dt-sc-simple-hr-title"> CIVIL Y FAMILIA </h4>
                                       <p class="alignleft" style="text-align: justify;"></p><ul>
                                          <li>- Contratos civiles  - Daños y perjuicios.</li>
                                          <li>- Cobro de dinero (procedimiento monitorio/procedimiento ejecutivo).</li>
                                          <li>- Recuperación de cartera.</li>
                                          <li>- Procedimientos de ejecución. 
                                          <li>- Cesión de derechos y acciones.</li>
                                          <li>- Compraventa de bienes inmuebles.</li>
                                          <li>- Sucesiones  -  Inventario y partición.</li>
                                          <li>- Alimentos, tenencia, regimen de visitas  - Impugnación de paternidad.</li><ul>
                                          <li>- Divorcios (voluntarios y contenciosos).</li>
                                          <li>- Capitulaciones matrimoniales.  - Disolución de la sociedad conyugal.</li>
                            
                                      </ul>
                                       <div class="dt-sc-hr-invisible-small"></div>
                                       
                                   </div>
                                   
                                   <div class="dt-sc-one-half column">
                                       <a href="#"><img src="images/ser04.jpg" alt="" title=""></a>
                                   </div>
                                   
                                   <div class="dt-sc-hr-invisible-small"></div>
                               
                                   <div class="dt-sc-one-half column first">
                                       <a href="#"><img src="images/ser05.jpg" alt="" title=""></a>
                                   </div>
                               
                                   <div class="dt-sc-one-half column">
                                       <h4 class="dt-sc-simple-hr-title"> PENAL Y TRÁNSITO </h4>
                                       <p class="alignleft" style="text-align: justify;"></p> <ul>
                                          <li>- Violencia intrafamiliar.</li>
                                          <li>- Delitos y contravenciones.</li>
                                          <li>- Reparación integral de daños.</li>
                                          <li>- Impugnación de contravenciones de tránsito.</li>
                                      </ul>
				   </div>

                                   <div class="dt-sc-hr-invisible-small"></div>
                                   
                                   <div class="dt-sc-hr-invisible"></div>
                               
                                   <div class="dt-sc-one-half column first">
                                       <h4 class="dt-sc-simple-hr-title"> SOCIETARIO Y CORPORATIVO </h4>
                                       <p class="alignleft" style="text-align: justify;"></p><ul>
                                          <li>- Asesoría empresarial.</li>
                                          <li>- Reforma de estatutos.</li>
                                          <li>- Contratos comerciales.</li>
                                          <li>- Constitución de compañías.</li>
                                          <li>- Disolución y liquidación.</li>  <li>- Propiedad intelectual e industrial (registro de marcas, patentes, y protección de propiedad intelectual e industrial.</li>
                                      </ul> 
                                       <div class="dt-sc-hr-invisible-small"></div>
                                   </div>

                                   <div class="dt-sc-one-half column">
                                       <a href="#"><img src="images/ser06.jpg" alt="" title=""></a>
                                   </div>
                               
                                   <div class="dt-sc-hr-invisible"></div>

                                   <div class="dt-sc-one-half column first">
                                       <a href="#"><img src="images/ser07.jpg" alt="" title=""></a>
                                   </div>

                                   <div class="dt-sc-one-half column">
                                       <h4 class="dt-sc-simple-hr-title"> ARBITRAJE Y MEDIACIÓN </h4>
                                       <p class="alignleft" style="text-align: justify;"></p> 
                                       <ul>
                                          <li>- Mediación para llegar a acuerdos exitosos.</li>
                                          <li>- Convenios arbitrales.</li>
                                          <li>- Procedimiento arbitral.</li>
                                         
                                      </ul>
                                       <div class="dt-sc-hr-invisible-small"></div>
                                   </div>

                                   <div class="dt-sc-hr-invisible"></d>

                                   <div class="dt-sc-one-half column first">
                                       <h4 class="dt-sc-simple-hr-title"> ASUNTOS REGULATORIOS </h4>
                                       <p class="alignleft" style="text-align: justify;">Brindamos asesoría a nuestros clientes en la obtención de Registros Sanitarios de productos nacionales y extranjeros, así como en el trámite general de importación o exportación de productos.</p> 
                                       <div class="dt-sc-hr-invisible-small"></div>
                                   </div>

                                   <div class="dt-sc-one-half column">
                                       <a href="#"><img src="images/ser08.jpg" alt="" title=""></a>
                                   </div>

                                   <div class="dt-sc-hr-invisible"></d>
                               
                                   <h2 class="dt-sc-hr-title"> POLILEGAL S.A.</h2>
                               
                                   
          
      </section>
      
      
      
      
      
  </div>
  
  
  <!--container ends-->
  
  <div class="dt-sc-hr-invisible-large"></div>
  
  
  
</section>
<!--primary ends-->

</div>
<!--main ends-->

<!--footer starts-->
<?php include 'footer.php'; ?>
<!--footer ends-->
</div>
<!--inner-wrapper ends-->    
</div>
<!--wrapper ends-->
<?php include "asistencias.php"; ?>
<a href="" title="Go to Top" class="back-to-top"> <span class="fa fa-angle-up"></span> </a> 
<!--Java Scripts--> 
<!--Java Scripts-->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-migrate.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-easing-1.3.js"></script>
<script type="text/javascript" src="js/jquery.sticky.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/jquery.tabs.min.js"></script>
<script type="text/javascript" src="js/jquery.smartresize.js"></script> 
<script type="text/javascript" src="js/shortcodes.js"></script>   

<script type="text/javascript" src="js/custom.js"></script>

<!-- Layer Slider --> 
<script type="text/javascript" src="js/jquery-transit-modified.js"></script> 
<script type="text/javascript" src="js/layerslider.kreaturamedia.jquery.js"></script> 
<script type='text/javascript' src="js/greensock.js"></script> 
<script type='text/javascript' src="js/layerslider.transitions.js"></script> 

<script type="text/javascript">var lsjQuery = jQuery;</script><script type="text/javascript"> lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice('layerslider_2','jquery'); } else { lsjQuery("#layerslider_2").layerSlider({responsiveUnder: 1240, layersContainer: 1170, skinsPath: 'js/layerslider/skins/'}) } }); </script>

                     <!-- Cerrar Session -->
                     <script>
                        jQuery(document).ready(function(){
                           jQuery("#close_session").click(function(){
                              alert('Su sesión ha sido cerrada');
                              window.location.href='closesession.php';
                           });
                        });
                     </script>


</body>
</html>

<?php
$cuerpo = '
<!DOCTYPE html>
<html lang="en">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Email Nic</title>
    <style>
        body {
            width: 100%;
            font-family: "Open Sans", sans-serif;
        }

        svg {
            width: 100px;
        }
        
        .main-table{
            background:white;
            
        }
        
        table {
            width: 500px;
            display: flex;
            flex-flow: column;
            margin: 0px auto;
        }

        table thead tr th {
            border-bottom: solid 5px black;
            width: 500px;
        }

        thead tr th svg {
            text-align: center;
        }

        .tabla-gris {
            background: #f8f8f8;
            
            border-radius: 5px;
            margin-bottom: 5px;
            padding: 15px;
           font-family: "Open Sans", sans-serif;
            font-size: 15px;
        }

        .tabla-gris .td1 {
            border-bottom: solid 1px #ccc;
        }
        .titulo{
          font-family: "Open Sans", sans-serif;
            padding: 5px;
        }
        .titulo h1 {
            font-size: 25px;
            line-height: 0px;
        }
        .line{
            border-bottom: solid 5px black;
        }
    </style>
</head>

<body>
    <table class="main-table">
        <thead>
            <tr>

            </tr>
        </thead>
        <tbody>
            <tr>
                <th class="titulo"><h1 style="width: 500px;">Hola '.stripslashes($nombres).'</h1> <br> Solicitud de Cotización en TUABOGADO.EC</th>
            </tr>
            <tr>
                <td>
                    <table class="tabla-gris">
                        <tr>
                            <td class="td1">DATOS DE LA SOLICITUD:</td>
                        </tr>
                        <tr>
                            <td>Nombre: '.stripslashes(utf8_decode($name)).'</td>
                        </tr>
                        <tr>
                            <td>Correo: '.stripslashes(utf8_decode($email)).'</td>
                        </tr>
                        <tr>
                            <td>Teléfono: '.stripslashes(utf8_decode($phone)).'</td>
                        </tr>
                        <tr>
                            <td>Consulta: '.str_replace("rnrn","</p><p>",(stripslashes(utf8_decode($consulta)))).'</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>

    </table>

</body>

</html>
';
?>

      <header id="header">
         <nav class="navbar navbar-fixed-top" role="banner">
            <div class="container">
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand hidden-sm hidden-xs" href="index.php">
                  <img src="img/logo-tuabogadoec.png" width="300px" height="85px"></a>
                  <a class="navbar-brand visible-sm visible-xs" href="index.php">
                  </a>
               </div>
               <div class="collapse navbar-collapse navbar-right">
                  <ul class="nav navbar-nav">
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
									aria-expanded="false">Abogados<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                           <li><a href="misarticulos.php"> Articulos</a></li>
                           <li><a href="adm_banners.php"> Banners</a></li>
                           <li><a href="miscupones.php"> Cupones</a></li>
                           <li><a href="admin.php"> Listado</a></li>
                           <li><a href="misrecomendaciones.php"> Recomendaciones</a></li>
                           <li><a href="#"> </a></li>
                        </ul>
                     </li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
									aria-expanded="false">Tablas<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                           <li><a href="adm_areas.php"> Areas</a></li>
                           <li><a href="adm_ciudades.php"> Ciudades</a></li>
                           <li><a href="adm_planes.php"> Planes</a></li>
                        </ul>
                     </li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Transacciones<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                           <li><a href="adm_transacciones.php"> Botón de Pagos</a></li>
                           <li><a href="adm_transac_abogados.php"> Del Abogado</a></li>
                        </ul>
                     </li>
                     <?php
                        if (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] == true) {
							?>
									<li class="dropdown active">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
                              aria-expanded="false"><?php echo $_SESSION['email']; ?><span class="caret"></span></a>
										<ul class="dropdown-menu">
                           		<li><a href="miperfil.php">Mi perfil</a></li>
                           		<li><a href="closesession.php">Cerrar sesión</a></li>
										</ul>
									</li>
							<?php
                        }else{
							?>
                           <li class="dropdown">
                           	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
										aria-expanded="false">Abogados<span class="caret"></span></a>
                           	<ul class="dropdown-menu">
                           		<li><a href="login.php">Acceso Abogados</a></li>
                           		<li><a href="register.php">Crear cuenta</a></li>
                           	</ul>
                           </li>
							<?php
                        }
                     ?>
                  </ul>
               </div>
            </div>
            <!--/.container-->
         </nav>
         <!--/nav-->
      </header>


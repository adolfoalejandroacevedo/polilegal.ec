<?php

	session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");

	//Conexion a BD
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );

	$arr_file_types = ['image/png', 'image/gif', 'image/jpg', 'image/jpeg'];

	if (!(in_array($_FILES['file']['type'], $arr_file_types))) {
		 echo "false";
		 return;
	}

	if (!file_exists('uploads')) {
		 mkdir('uploads', 0777);
	}

	$file = time() . $_FILES['file']['name'];
	move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/' . $file);

	//Actualizamos la imagen del articulo 
	$file = mysqli_real_escape_string($conn, $file);
	$sql = "UPDATE users SET foto='$file' WHERE id='$_SESSION[id]'";
	if(!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));

	echo "$file";
?>


<?php 
	session_start();
   header('Content-Type: text/html; charset=utf-8');
	$config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);
   mysqli_set_charset($conn, "utf8");
?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Plataforma Digital de Abogados del Ecuador | Crear Cuenta</title>
		<meta name="keywords" content="abogado, ecuador, abogados, quito, guayaquil, cuenca, ibarra, ambato, machala, daule, economico, juicio, barato, empresa, legal, violacion, derechos, trabajador, indemnizacion, trabajo">
		<meta name="description" content="Crea tu cuenta de abogado para que tengas presencia digital" />
      <link href="favicon.png" type="image/x-icon" rel="icon"/>
      <link href="favicon.png" type="image/x-icon" rel="shortcut icon"/>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="font/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.css">
      <link href="css/animate.min.css" rel="stylesheet">
      <link href="css/prettyPhoto.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="bower_components/datatables.net-dt/css/jquery.dataTables.css">
      <link rel="stylesheet" type="text/css" href="css/bootstrap-markdown.min.css">
      <!-- JQuery Validator and form -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.js"></script>
      <style>
			/* Color del mensaje de error
			del checkbox y posicion debajo del cuadro */
			.errorMsq {
		  		color: red;
		    	display: block;
			 }
			 /* Color rojo para el texto de error de los campos */
         .error{
            color: red;
         }
			/* Borde rojo y grosor de linea de los inputs */
         .reg-form input[type=text].error,
         .reg-form input[type=email].error,
         .reg-form input[type=password].error{
            padding:15px 18px;
            border:1px solid #FF0000;
         }
			/* Grosor y color de linea del checkbox en modo error */
			.reg-form input[type=checkbox].error{
				outline: 1px solid #FF0000;
			}
         .pricingTable{
         text-align: center;
         transition: all 0.5s ease 0s;
         }
         .pricingTable:hover{
         box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
         }
         .pricingTable .pricingTable-header{
         color: #feffff;
         }
         .pricingTable .heading{
         display: block;
         padding-top: 25px;
         }
         .pricingTable .heading > h3{
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .textotitle{
         color: #feffff;
         font-size: 20px;
         margin: 0;
         text-transform: capitalize;
         }
         .pricingTable .subtitle{
         display: block;
         font-size: 13px;
         margin-top: 5px;
         text-transform: capitalize;
         }
         .pricingTable .price-value{
         display: block;
         font-size: 60px;
         font-weight: 700;
         padding-bottom: 25px;
         }
         .pricingTable .price-value span{
         display: block;
         font-size: 14px;
         line-height: 20px;
         text-transform: uppercase;
         }
         .pricingTable .pricingContent{
         /*text-transform: capitalize;*/
         background: #fbfbfb;
         color: #fefeff;
         }
         .pricingTable .pricingContent ul{
         list-style: none;
         padding: 15px 14px 10px;
         margin: 0;
         text-align: left;
         }
         .pricingTable .pricingContent ul li{
         font-size: 14px;
         padding: 12px 0;
         border-bottom: 1px dashed #e1e1e1;
         color: #9da1ad;
         }
         .pricingTable .pricingContent ul li i{
         font-size: 14px;
         float: right;
         }
         .pricingTable .pricingTable-sign-up{
         padding: 20px 0;
         background: #fbfbfb;
         color: #fff;
         text-transform: capitalize;
         }
         .pricingTable .btn-block{
         width: 60%;
         margin: 0 auto;
         font-size: 17px;
         color: #fff;
         text-transform: capitalize;
         border: none;
         border-radius: 5px;
         padding: 10px;
         transition: all 0.5s ease 0s;
         }
         .pricingTable .btn-block:before{
         content: "\f007";
         font-family: 'FontAwesome';
         margin-right: 10px;
         }
         .pricingTable.blue .pricingTable-header,
         .pricingTable.blue .btn-block{
         background: #fce400;
         }
         .pricingTable.pink .pricingTable-header,
         .pricingTable.pink .btn-block{
         background: #c9b600;
         }
         .pricingTable.orange .pricingTable-header,
         .pricingTable.orange .btn-block{
         background: #e0d83a;
         }
         .pricingTable.green .pricingTable-header,
         .pricingTable.green .btn-block{
         background: #e0cd00;
         }
         .pricingTable.blue .btn-block:hover,
         .pricingTable.pink .btn-block:hover,
         .pricingTable.orange .btn-block:hover,
         .pricingTable.green .btn-block:hover{
         background: #e6e6e6;
         color: #939393;
         }
         @media screen and (max-width: 990px){
         .pricingTable{ margin-bottom: 20px; }
         }
      </style>
   </head>
   <body class="homepage">
<?php include "header.php"; ?>
      <!--/header-->
      <section id="blog" class="container">
         <div class="container">
			   <!-- Zona de mensajes -->
			   <div id="mensajes"></div>
				<!-- FIN Zona de mensajes -->
            <div class="center">
               <h2>Crear una cuenta</h2>
            </div>
            <div class="row contact-wrap">
               <div class="col-md-12">
                  <div >
                     <form name="frmregistro" id="frmregistro" class="reg-form" method="post" accept-charset="utf-8" role="form" action="registro.php">
                        <div class="jumbotron wow fadeInDown">
                           <div class="form-group text required">
                              <label class="control-label" for="email">Correo electrónico</label>
                              <div class="input-group">
											<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
											<input type="email" name="email" class="form-control" maxlength="255" id="email" />
										</div>
                           </div>
                           <div class="form-group text required">
                              <label class="control-label" for="email">Confirmar Correo</label>
                              <div class="input-group">
											<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
											<input type="email" name="email_confirm" class="form-control" maxlength="255" id="email_confirm" autocomplete="off" />
										</div>
                           </div>
                           <div class="form-group password required">
                              <label class="control-label" for="password">Contraseña</label>
                              <div class="input-group"><span class="input-group-addon"><i class="fa fa-key"></i></span>
											<input type="password" name="password" class="form-control" id="password" />
										</div>
                           </div>
                           <div class="form-group password required">
                              <label class="control-label" for="password-confirm">Confirmar contraseña</label>
                              <div class="input-group"><span class="input-group-addon"><i class="fa fa-key"></i></span>
											<input type="password" name="password_confirm" class="form-control" id="password_confirm" />
										</div>
                           </div>
                           <div class="form-group text required">
                              <label class="control-label" for="first-name">Nombres y Apellidos / Estudio Jurídico</label>
                              <div class="input-group"><span class="input-group-addon"><i class="fa fa-user"></i></span>
											<input type="text" name="first_name" class="form-control" maxlength="50" id="first_name" />
										</div>
                           </div>
									<div class="form-group text required">
										<label class="control-label" for="ciudad">Ciudad</label>
										<div class="input-group"><span class="input-group-addon"><i class="fa fa-home"></i></span>
											<input type="text" class="form-control" list="ciudades" name="ciudad" style="color: #75757d" required="">
											<datalist id="ciudades">
													<?php
														$sql = "SELECT * FROM ciudades ORDER BY ciudad";
														if ($result = mysqli_query($conn, $sql)){
															while ($row2 = mysqli_fetch_assoc($result)) {
																echo "<option value='$row2[ciudad]'>";
															}
														}else{
															error_log("Error: " . $sql . "..." . mysqli_error($conn));
														}
													?>
											</datalist>
										</div>
									</div>
                           <div class="form-group text required">
                              <label class="control-label" for="matricula">Matrícula Profesional</label>
                              <div class="input-group"><span class="input-group-addon"><i class="fa fa-certificate"></i></span>
											<input type="text" name="matricula" class="form-control" maxlength="50" id="matricula" 
											placeholder="Si eres Estudio Jurídico coloca el número de Matrícula Profesional de cualquiera de sus integrantes"/>
										</div>
                           </div>
                           <div class="form-group checkbox required">
										<label class="control-label" for="tos">
											<input type="checkbox" name="tos" value="1" id="tos">Acepto los 
											<a href="terminos.php" target="_blank"> términos y condiciones </a>y la 
											<a href="politicasprivacidad.php" target="_blank">política de privacidad.</a>
										</label>
									</div>
                        </div>
                        <div class="jumbotron fadeInDown center">
                           <button class="btn btn-lg btn-success" type="submit">Crear cuenta</button>    
									<img id="enviando" src="images/barra.gif" width="100px" height="25px" 
									style="position: relative; vertical-align:middle; display: none;">
                     </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section id="bottom">
         <?php include "bottom.php"; ?>
       </section>
      <!--/#bottom-->
      <footer id="footer" class="midnight-blue">
         <?php include "footer.php"; ?>
      </footer>
      <!--/#footer-->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="js/bootstrap.min.js"></script>
      <script src="js/markdown.js"></script>
      <script src="js/to-markdown.js"></script>
      <script src="js/bootstrap-markdown.js"></script>
      <script src="js/bootstrap-markdown.es.js"></script>
      <script type="text/javascript" src="js/jquery.simpleslider.js"></script>
      <script type="text/javascript" charset="utf8" src="bower_components/datatables.net/js/jquery.dataTables.min.js"> </script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/jquery.isotope.min.js"></script>
      <script src="js/wow.min.js"></script>
      <script src="js/main.js"></script>
      <script src="js/custom.js"></script>
   </body>
</html>


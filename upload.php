<?php   

	session_start();
	if(!isset($_SESSION['loggedIn']) && !$_SESSION['loggedIn']) header("Location: index.php");
   $config = require 'config.php';
   $conn=mysqli_connect(
         $config['database']['server'],
         $config['database']['username'],
         $config['database']['password'],
         $config['database']['db']
   );
   if (mysqli_connect_errno()) error_log("Failed to connect to MySQL: " . mysqli_connect_error(),0);

	$output = '';  

	//Numero de registros actuales
	$sql = "SELECT * FROM galeria WHERE iduser='$_SESSION[id]'";
	if ($result = mysqli_query($conn, $sql)){
		$nreg = mysqli_num_rows($result);
	} else error_log("Error: " . $sql . "..." . mysqli_error($conn));

	//Numero de imagenes permitidas a insertar
	if($_SESSION['plan'] == 2){
		$dif = 5 - $nreg;
	}
	if($_SESSION['plan'] == 3){
		$dif = 30 - $nreg;
	}
	

	//Se se alcanzo el maximo no e hace nada
	if($dif <= 0){
		echo "limite";
	   exit;
	}

	$cont = 1;
	if(isset($_FILES['file']['name'][0]))  
	{  
		//Inserta las imagenes  
		foreach($_FILES['file']['name'] as $keys => $values)  
		{  
			if(move_uploaded_file($_FILES['file']['tmp_name'][$keys], 'uploads/' . $values))  
			{  
				$filename = htmlentities($values);
				$sql = "INSERT INTO galeria (iduser, img) VALUES ('$_SESSION[id]', '$filename')";
				if (!mysqli_query($conn, $sql)) error_log("Error: " . $sql . "..." . mysqli_error($conn));
				$cont += 1;
				if($cont > $dif) break;
			}  
		}

	//Seleccionamos las imagenes que quedaron para ser mostradas
	$sql = "SELECT * FROM galeria WHERE iduser='$_SESSION[id]'";
	if ($result = mysqli_query($conn, $sql)){
		while ($row = mysqli_fetch_assoc($result)) {
			$output .= "
					  <div class='portfolio-item apps col-xs-12 col-sm-4 col-md-3' id='$row[id]' >
							<div class='recent-work-wrap' >
								 <img class='img-responsive' src='uploads/$row[img]' alt=''>
								 <div class='overlay'>
									  <div class='recent-work-inner'>
											<div class='col-md-6' align='left'>
												<a class='preview' href='uploads/$row[img]' rel='prettyPhoto'><i class='fa fa-eye'></i> Ampliar</a>
											</div>
											<div class='col-md-6' align='right'>
												<a class='fotos' href='uploads/$row[img]'                  
												data-idfoto='$row[id]' ><i class='fa fa-trash-o'></i> Eliminar</a>
											</div>
									  </div>
								 </div>
							</div>
					  </div>
					  ";
		}
	}else{
		error_log("Error: " . $sql . "..." . mysqli_error($conn));
	}
		
	}  
	echo $output; 
 
?>
